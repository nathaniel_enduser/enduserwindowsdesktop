#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <Windows.h>
#include <vector>
#include <flexxlib.h>
#include <assert.h>

static flexxon::flexxDriveList driveList;

static const int namespace_size = 1*1024*1024*1024; // 1GB

char factory_passwd[] = "factory password";
char wrong_passwd[] = "24905u88tuGADFSrnfgasdfcFRDGML:";
char new_passwd[] = "Flexxon1234";

static void update_password(flexxon::FlexxDecorator &flexxdisk)
{
        int err;
        err = flexxdisk.change_xphy_passwd(factory_passwd, sizeof(factory_passwd),
                        new_passwd, sizeof(new_passwd));
        if (err) {
                assert(0);
        }

        return;
}

static void restore_password(flexxon::FlexxDecorator &flexxdisk)
{
        int err;
        err = flexxdisk.change_xphy_passwd(new_passwd, sizeof(new_passwd),
                                           factory_passwd, sizeof(factory_passwd));
        if (err) {
                assert(0);
        }
        return;
}


static void do_test_lock_vendor_specific(flexxon::FlexxDecorator &flexxdisk,
                char *password, int psize, int enable)
{
        int err;

        /* test lock file. assuming c:\testfile.bin exists */
        char testfilepath[] = "c:\\testfile.bin";
        err = flexxdisk.lock(password, psize,
                             testfilepath, sizeof(testfilepath), enable);
        if (err) {
                assert(0);
        }

        if (enable) {
                /* test if file is locked here. how? */
        } else {
                /* test if file in unlocked here. how? */
        }

        return;
}

static void test_lock_vendor_specific(flexxon::FlexxDecorator &flexxdisk,
                char *password, int psize)
{
        do_test_lock_vendor_specific(flexxdisk, password, psize, 1);
        return ;
}

static void test_unlock_vendor_specific(flexxon::FlexxDecorator &flexxdisk,
                char *password, int psize)
{
        do_test_lock_vendor_specific(flexxdisk, password, psize, 0);
        return ;
}

static void do_test_encrypt_vendor_specific(flexxon::FlexxDecorator &flexxdisk,
                char *password, int psize, int enable)
{
        int err;
        err = flexxdisk.encryption(password, psize, enable);
        if (err) {
                assert(0);
        }
        return;
}

static void test_encrypt_enable_vendor_specific(
                flexxon::FlexxDecorator &flexxdisk, char *password, int psize)
{
        do_test_encrypt_vendor_specific(flexxdisk, password, psize, 1);
        return;
}

static void test_encrypt_disable_vendor_specific(
                flexxon::FlexxDecorator &flexxdisk, char *password, int psize)
{
        do_test_encrypt_vendor_specific(flexxdisk, password, psize, 0);
        return;
}

static void do_test_prot_vendor_specific(
                flexxon::FlexxDecorator &flexxdisk, char *password, int psize,
                int enable)
{
        int err;
        err = flexxdisk.protect(password, psize, enable);
        if (err) {
                assert(0);
        }

        if (enable) {
                /* test protection here. how? */
        } else {
                /* test unprotection here. how? */
        }
        return;
}

static void test_prot_enable_vendor_specific(
                flexxon::FlexxDecorator &flexxdisk, char *password, int psize)
{
        do_test_prot_vendor_specific(flexxdisk, password, psize, 1);
        return;
}


static void test_prot_disable_vendor_specific(
                flexxon::FlexxDecorator &flexxdisk, char *password, int psize)
{
        do_test_prot_vendor_specific(flexxdisk, password, psize, 0);
        return;
}

static void test_secure_erase_vendor_specific(
                flexxon::FlexxDecorator &flexxdisk, char *password, int psize)
{
        int err;
        err = flexxdisk.secure_erase(password, psize);
        if (err) {
                assert(0);
        }
        return;
}

int main()
{
        static flexxon::flexxDriveList::iterator disk;
        struct identify_controller_struct id_control;
        struct identify_namespace_struct id_namespace;
        struct create_ns_template_struct create_ns;
        struct lba_formatx_struct *flbaPtr;
        uint32_t new_nsid;
        std::vector<struct log_err_info_struct> err_info;
        struct log_smart_info_struct smart_info_global;
        struct log_smart_info_struct smart_info_ns;
        struct flexxon::discovery_struct discovery_data;
        int err;

        flexxon::scan_devices(driveList);
        for (disk = driveList.begin(); disk < driveList.end(); disk++) {
                memset(&id_control, 0, sizeof(id_control));
                memset(&id_namespace, 0, sizeof(id_namespace));

                disk->identify(id_control);
                disk->identify(id_namespace);
                disk->logpage(1, smart_info_global);
                disk->logpage(0, smart_info_ns);
        }
        disk = driveList.begin();
       
        /* try sending vendor specific */
        flexxon::FlexxDecorator flexxdisk(*disk);

        /* try sending discovery */
        err = flexxdisk.discovery(discovery_data);
        if (err) {
                assert(0);
        }


        /* these vendor specific are protected by a variable 
         * so we don't accidentally run them. change the variables 
         * during runtime (while debugging) to test them 
         */
        int test_change_xphy_password = 0;
        int test_lock = 0;
        int test_policy = 0;
        int test_encryption = 0;
        int test_protection = 0;
        int test_secure_erase = 0;
        int test_signature_update = 0;

        if (test_change_xphy_password) {
                /* test changing xphy password with "wrong" password first */
                err = flexxdisk.change_xphy_passwd(wrong_passwd,
                                sizeof(wrong_passwd), new_passwd,
                                sizeof(new_passwd));
                if (!err) {
                        assert(0);
                }

                /* test changing xphy password with "correct" password now */
                update_password(flexxdisk);

                /* restore old password */
                restore_password(flexxdisk);
        }

        if (test_lock) {
                /* test with wrong password */
                test_lock_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                test_unlock_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));

                /* test with changed password */
                update_password(flexxdisk);
                test_lock_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                test_unlock_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                restore_password(flexxdisk);
        }

        if (test_policy) {
                /* not done because need more info about this flexxon supplied
                 * policies. sending wrong policy will risk bricking the drive.
                 */
        }

        if (test_encryption) {
                /* test with wrong password */
                test_encrypt_enable_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                test_encrypt_disable_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));

                /* test with changed password */
                update_password(flexxdisk);
                test_encrypt_enable_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                test_encrypt_disable_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                restore_password(flexxdisk);
        }

        if (test_protection) {
                /* test with wrong password */
                test_prot_enable_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                test_prot_disable_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));

                /* test with changed password */
                update_password(flexxdisk);
                test_prot_enable_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                test_prot_disable_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                restore_password(flexxdisk);
        }

        if (test_secure_erase) {
                /* test with wrong password */
                test_secure_erase_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));

                /* test with changed password */
                update_password(flexxdisk);
                test_secure_erase_vendor_specific(flexxdisk, new_passwd,
                                sizeof(new_passwd));
                restore_password(flexxdisk);
        }

        if (test_signature_update) {
                /* not done because need more info about this flexxon supplied
                 * signature file. sending any file will risk bricking the drive. 
                 */
        }

        int test_namespace_management = 0;

        if (test_namespace_management) {
                disk->identify(0xffffffff, cns_idns, &id_namespace,
                        sizeof(id_namespace));
                flbaPtr = &id_namespace.lbaf[0];
                create_ns.nsze = namespace_size / (1 << flbaPtr->lbads);
                create_ns.ncap = create_ns.nsze;
                create_ns.flbas = 0; 
                disk->create_namespace(create_ns, new_nsid);
        }

        return 0;
}
