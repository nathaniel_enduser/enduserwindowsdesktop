#include "pch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "flexx-nvme.h"
#include "flexx-winioctl.h"

using namespace flexxon;

static int valid_lid(const int lid)
{
        int valid;

        /* check if valid logpage */
        switch(lid & 0xff) {
        case lpid_error_info:
        case lpid_smart_health_info:
        case lpid_firmware_slot_info:
        case lpid_changed_ns_list:
        case lpid_cmds_supported_and_effects:
        case lpid_device_self_test:
        case lpid_telemetry_host:
        case lpid_telemetry_controller:
        case lpid_discover:
        case lpid_resv_notification :
        case lpid_sanitize_status:
                valid = 1;
                break;
        default:
                valid = 0;
        }
        return valid;
}

int NvmeDisk::log_page13(const uint32_t nsid, const uint32_t cdw10, const uint32_t cdw11,
                         const uint32_t cdw12, const uint32_t cdw13,
                         void *buffer, const uint32_t size)
{
        struct identify_controller_struct idcontrol;
        unsigned char *myIoctlBuf;
        PNVME_PASS_THROUGH_IOCTL     pMyIoctl;
        DWORD                        dw;
        struct nvme_command_packet *cmdPtr;
        void *dataPtr = NULL;
        int ioctlSize;
        int err;

        if (!valid_lid(cdw10 & 0xff))
                return -1;

        identify(idcontrol);
        if (!getlog_lpo_supported(idcontrol)) {
                int mask = (1 << 12) - 1;
                int numdw = ((cdw11 & 0xFFFF) << 16) | ((cdw10 & 0xFFFF0000) >> 16) ;
                long long lpo = cdw13;
                lpo = (lpo << 32) | (cdw12 & 0xFFFFFFFF);

                /* check if user is requesting for more than 12 bits of data */
                if ((numdw & ~mask) != 0) {
                        return -1;
                }

                /* since log page offset is not supported, lp must be zero */
                if (lpo) {
                        return -1;
                }
        }

        if (!buffer || !(size > 0)) {
                return -1;
        }

        ioctlSize = sizeof(NVME_PASS_THROUGH_IOCTL) + size;
        myIoctlBuf = (unsigned char *)malloc(ioctlSize);
        if (!myIoctlBuf)
                return -1;

        pMyIoctl = (PNVME_PASS_THROUGH_IOCTL)myIoctlBuf;
        memset(pMyIoctl, 0, ioctlSize);

        // Set up the SRB IO Control header
        pMyIoctl->SrbIoCtrl.ControlCode = (ULONG)NVME_PASS_THROUGH_SRB_IO_CODE;
        memcpy(pMyIoctl->SrbIoCtrl.Signature, NVME_SIG_STR, sizeof(NVME_SIG_STR));
        pMyIoctl->SrbIoCtrl.HeaderLength = (ULONG)sizeof(SRB_IO_CONTROL);
        pMyIoctl->SrbIoCtrl.Timeout = 30;
        pMyIoctl->SrbIoCtrl.Length = ioctlSize - sizeof(SRB_IO_CONTROL);

        // Set up the NVMe pass through IOCTL buffer
        cmdPtr = (struct nvme_command_packet*)pMyIoctl->NVMeCmd;
        cmdPtr->opcode = admin_get_log_page;
        cmdPtr->nsid = nsid;
        cmdPtr->cdw10 = cdw10;
        cmdPtr->cdw11 = cdw11;
        cmdPtr->cdw12 = cdw12;
        cmdPtr->cdw13 = cdw13;

        pMyIoctl->QueueId = 0; // Admin queue
        pMyIoctl->DataBufferLen = 0;
        pMyIoctl->Direction = NVME_FROM_DEV_TO_HOST;
        pMyIoctl->ReturnBufferLen = size + sizeof(NVME_PASS_THROUGH_IOCTL);
        pMyIoctl->VendorSpecific[0] = (DWORD)0;
        pMyIoctl->VendorSpecific[1] = (DWORD)0;
        memset(pMyIoctl->DataBuffer, 0x00, size);

        dataPtr = pMyIoctl->DataBuffer;
        dev.open();
        err = dev.ioctl(IOCTL_SCSI_MINIPORT, 
                        pMyIoctl,
                        ioctlSize,
                        pMyIoctl,
                        ioctlSize,
                        &dw);
        dev.close();
        if (!err) {
                memcpy(buffer, dataPtr, size);
        }
        free(myIoctlBuf);
        return err;
}

int NvmeDisk::logpage(const uint32_t nsid, const uint32_t lid, const uint32_t lsp, uint32_t rae,
                      const uint64_t lpo, void *buffer, const uint32_t size)
{
        int cdw10 = ((((size>>2) & 0xffff)-1) << 16) | ((rae & 1) << 15) |
                    ((lsp & 0xf) << 8) | (lid & 0xff);;
        int cdw11 = (((size>>2) & 0xffff0000) >> 16);
        int cdw12 = (int)(lpo & 0xffffffff);
        int cdw13 = (int)((lpo >> 32) & 0xffffffff);
        return log_page13(nsid, cdw10, cdw11, cdw12, cdw13, buffer, size);
}

int NvmeDisk::logpage(std::vector<struct log_err_info_struct> &err_info,
                      const uint32_t count)
{
        struct log_err_info_struct *data;
        const int size = sizeof(struct log_err_info_struct) * count;
        uint32_t i;
        int err;

        /* minimum of at least 1 error log page required */
        if (!(count > 0)) {
                return -1;
        }

        data = (struct log_err_info_struct *)malloc(size);
        if (!data)
                return -1;
        err = logpage(get_nsid(), lpid_error_info, 0, 0, 0,
                        data, size);
        if (!err) {
                i = 0;
                while(i++ < count) {
                        err_info.push_back(*data);
                }
                data++;
        }
        free(data);
        return err;
}

int NvmeDisk::logpage(const int global, struct log_smart_info_struct &smart)
{
        struct identify_controller_struct idcontrol;
        int nsid = 0xffffffff;
        
        if (!global) {
                identify(idcontrol);
                if (getlog_smart_per_ns_supported(idcontrol)) {
                        nsid = get_nsid();
                } else {
                        return -1;
                }
        }
        return logpage(nsid, lpid_smart_health_info, 0, 0, 0, 
                       &smart, sizeof(struct log_smart_info_struct));
}

int NvmeDisk::logpage(struct log_fw_slot_info_struct &fw_slot)
{
        return logpage(0, lpid_firmware_slot_info, 0, 0, 0,
                       &fw_slot, sizeof(struct log_fw_slot_info_struct));
}

int NvmeDisk::logpage(struct log_changed_ns_list_struct &cns_list)
{
        return logpage(0, lpid_changed_ns_list, 0, 0, 0,
                       &cns_list, sizeof(struct log_changed_ns_list_struct));
}

int NvmeDisk::logpage(struct log_cmds_supported_and_effects_struct &cse)
{
        return logpage(0, lpid_cmds_supported_and_effects, 0, 0, 0,
                       &cse, sizeof(struct log_cmds_se_struct));
}

int NvmeDisk::logpage(struct log_device_self_test_struct &selftest)
{
        return logpage(0, lpid_device_self_test, 0, 0, 0,
                       &selftest, sizeof(struct log_dst_struct));
}

static int prep_telemetry(const uint32_t size, uint64_t data_block_index,
                          uint32_t block_count, uint64_t &aligned_lp_offset,
                          uint32_t &aligned_size)
{
        uint64_t _aligned_size;
        /* size is sizeof(header) + (sizeof(datablock)*log_count) */
        _aligned_size = sizeof(struct tlmtry_head_struct) +
                       (block_count * sizeof(tlmtry_data_block_struct));
        /* not enough space in buffer for telemetry data */
        if (_aligned_size > size)
                return -1;
        aligned_lp_offset = sizeof(tlmtry_data_block_struct) *
                           data_block_index;
        aligned_size = (uint32_t)(_aligned_size & 0xFFFFFFFF);
        return 0;
}

int NvmeDisk::logpage(struct log_host_tlmtry_struct &tlmtry, uint32_t size,
                      uint64_t data_block_index, uint32_t block_count,
                      const uint32_t create_data)
{
        uint32_t lsp  = !!create_data;
        uint64_t aligned_lp_offset;
        uint32_t aligned_size;

        if (prep_telemetry(size, data_block_index, block_count, aligned_lp_offset,
                           aligned_size)) {
                return -1;
        }
        return logpage(0, lpid_telemetry_host, lsp, 0, aligned_lp_offset,
                       &tlmtry, aligned_size);
}

int NvmeDisk::logpage(struct log_controller_tlmtry_struct &tlmtry,
                      uint32_t size, uint64_t data_block_index,
                      uint32_t block_count)
{
        uint64_t aligned_lp_offset;
        uint32_t aligned_size;
        if (prep_telemetry(size, data_block_index, block_count, aligned_lp_offset,
                           aligned_size)) {
                return -1;
        }
        return logpage(0, lpid_telemetry_controller, 0, 0, aligned_lp_offset,
                       &tlmtry, aligned_size);
}

int NvmeDisk::logpage(struct log_resv_notification_struct &resv_ntfn)
{
        return logpage(0, lpid_resv_notification, 0, 0, 0,
                       &resv_ntfn, sizeof(struct log_resv_notification_struct));
}

int NvmeDisk::logpage(struct log_santize_status_struct &ss)
{
        return logpage(0, lpid_sanitize_status, 0, 0, 0,
                       &ss, sizeof(struct log_santize_status_struct));
}

#define CHECK_ERR_INFO_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct log_err_info_struct, field) == offset)

#define CHECK_SMART_INFO_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct log_smart_info_struct, field) == offset)

#define CHECK_FW_SLOT_INFO_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct log_fw_slot_info_struct, field) == offset)

#define CHECK_CMD_SE_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct log_cmds_se_struct, field) == offset)

#define CHECK_ST_RESULT_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct st_result_struct, field) == offset)

#define CHECK_DST_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct log_dst_struct, field) == offset)

#define CHECK_TLMTRY_HEAD_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct tlmtry_head_struct, field) == offset)

#ifdef _DEBUG
namespace flexxon {

void log_page_ct_assert(void)
{
        COMPILE_TIME_ASSERT(sizeof(struct log_err_info_struct) == 64);
        CHECK_ERR_INFO_OFFSET(error_count, 0);
        CHECK_ERR_INFO_OFFSET(sqid, 8);
        CHECK_ERR_INFO_OFFSET(cid, 10);
        CHECK_ERR_INFO_OFFSET(status_field, 12);
        CHECK_ERR_INFO_OFFSET(param_err_loc, 14);
        CHECK_ERR_INFO_OFFSET(lba, 16);
        CHECK_ERR_INFO_OFFSET(namespc, 24);
        CHECK_ERR_INFO_OFFSET(vendor_info_available, 28);
        CHECK_ERR_INFO_OFFSET(resv0, 29);
        CHECK_ERR_INFO_OFFSET(cmd_specific_info, 32);
        CHECK_ERR_INFO_OFFSET(resv1, 40);

        COMPILE_TIME_ASSERT(sizeof(struct log_smart_info_struct) == 512);
        CHECK_SMART_INFO_OFFSET(info1, 0);
        CHECK_SMART_INFO_OFFSET(avail_spare_threshold, 4);
        CHECK_SMART_INFO_OFFSET(percentage_used, 5);
        CHECK_SMART_INFO_OFFSET(resv0, 6);
        CHECK_SMART_INFO_OFFSET(data_units_read, 32);
        CHECK_SMART_INFO_OFFSET(data_units_written, 48);
        CHECK_SMART_INFO_OFFSET(host_read_commands, 64);
        CHECK_SMART_INFO_OFFSET(host_write_commands, 80);
        CHECK_SMART_INFO_OFFSET(controller_busy_time, 96);
        CHECK_SMART_INFO_OFFSET(power_cycles, 112);
        CHECK_SMART_INFO_OFFSET(power_on_hours, 128);
        CHECK_SMART_INFO_OFFSET(unsafe_shutdowns, 144);
        CHECK_SMART_INFO_OFFSET(data_integrity_errors, 160);
        CHECK_SMART_INFO_OFFSET(number_of_error_info_log_entries, 176);
        CHECK_SMART_INFO_OFFSET(warning_composite_temp_time, 192);
        CHECK_SMART_INFO_OFFSET(critical_composite_temp_time, 196);
        CHECK_SMART_INFO_OFFSET(temp_sensor, 200);
        CHECK_SMART_INFO_OFFSET(thermal_mgmt_temp_transition_count, 216);
        CHECK_SMART_INFO_OFFSET(total_time_for_thermal_mgmt_temp, 224);
        CHECK_SMART_INFO_OFFSET(resv1, 232);

        COMPILE_TIME_ASSERT(sizeof(struct log_fw_slot_info_struct) == 512);
        CHECK_FW_SLOT_INFO_OFFSET(afi, 0);
        CHECK_FW_SLOT_INFO_OFFSET(resv0, 1);
        CHECK_FW_SLOT_INFO_OFFSET(frsx, 8);
        CHECK_FW_SLOT_INFO_OFFSET(resv1, 64);

        COMPILE_TIME_ASSERT(sizeof(struct log_changed_ns_list_struct) == 4096);

        COMPILE_TIME_ASSERT(sizeof(struct cmd_se_struct) == 4);
        COMPILE_TIME_ASSERT(sizeof(struct log_cmds_se_struct) == 4096);
        CHECK_CMD_SE_OFFSET(acs, 0);
        CHECK_CMD_SE_OFFSET(iocs, 1024);
        CHECK_CMD_SE_OFFSET(resv0, 2048);

        COMPILE_TIME_ASSERT(sizeof(struct st_result_struct) == 28);
        CHECK_ST_RESULT_OFFSET(dst_status, 0);
        CHECK_ST_RESULT_OFFSET(segment, 1);
        CHECK_ST_RESULT_OFFSET(valid_diag_info, 2);
        CHECK_ST_RESULT_OFFSET(resv0, 3);
        CHECK_ST_RESULT_OFFSET(poh, 4);
        CHECK_ST_RESULT_OFFSET(nsid, 12);
        CHECK_ST_RESULT_OFFSET(failing_lba, 16);
        CHECK_ST_RESULT_OFFSET(sc_type, 24);
        CHECK_ST_RESULT_OFFSET(sc, 25);
        CHECK_ST_RESULT_OFFSET(vs, 26);

        COMPILE_TIME_ASSERT(sizeof(struct log_dst_struct) == 564);
        CHECK_DST_OFFSET(cdsto, 0);
        CHECK_DST_OFFSET(cdstc, 1);
        CHECK_DST_OFFSET(resv0, 2);
        CHECK_DST_OFFSET(nstr, 4);

        COMPILE_TIME_ASSERT(sizeof(struct tlmtry_head_struct) == 512);
        COMPILE_TIME_ASSERT(sizeof(struct tlmtry_data_block_struct) == 512);
        CHECK_TLMTRY_HEAD_OFFSET(log_id, 0);
        CHECK_TLMTRY_HEAD_OFFSET(resv0, 1);
        CHECK_TLMTRY_HEAD_OFFSET(ieee, 5);
        CHECK_TLMTRY_HEAD_OFFSET(last_block, 8);
        CHECK_TLMTRY_HEAD_OFFSET(resv1, 14);
        CHECK_TLMTRY_HEAD_OFFSET(data_avail, 382);
        CHECK_TLMTRY_HEAD_OFFSET(reason_id, 384);

        return;
}


}; // flexxon namespace
#endif
