#include "pch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "flexx-nvme.h"
#include "flexx-winioctl.h"

using namespace flexxon;

int NvmeDisk::identify(const uint32_t nsid, const uint32_t cns, void *buffer,
                       const uint32_t sz)
{
        UCHAR myIoctlBuf[sizeof(NVME_PASS_THROUGH_IOCTL) + 4096];
        PNVME_PASS_THROUGH_IOCTL     pMyIoctl = (PNVME_PASS_THROUGH_IOCTL)myIoctlBuf;
        DWORD                        dw;
        struct nvme_command_packet *cmdPtr;
        struct idcntrl_struct *dataPtr = NULL;
        int err;

        if (!buffer || sz == 0) {
                return -1;
        }

        memset(pMyIoctl, 0, sizeof(myIoctlBuf));

        // Set up the SRB IO Control header
        pMyIoctl->SrbIoCtrl.ControlCode = (ULONG)NVME_PASS_THROUGH_SRB_IO_CODE;
        memcpy(pMyIoctl->SrbIoCtrl.Signature, NVME_SIG_STR, sizeof(NVME_SIG_STR));
        pMyIoctl->SrbIoCtrl.HeaderLength = (ULONG)sizeof(SRB_IO_CONTROL);
        pMyIoctl->SrbIoCtrl.Timeout = 30;
        pMyIoctl->SrbIoCtrl.Length = sizeof(myIoctlBuf) - sizeof(SRB_IO_CONTROL);

        // Set up the NVMe pass through IOCTL buffer
        cmdPtr = (struct nvme_command_packet*)pMyIoctl->NVMeCmd;
        cmdPtr->opcode = admin_identify;
        cmdPtr->nsid = nsid;
        cmdPtr->cdw10 = cns;

        pMyIoctl->QueueId = 0; // Admin queue
        pMyIoctl->DataBufferLen = 0;
        pMyIoctl->Direction = NVME_FROM_DEV_TO_HOST;
        pMyIoctl->ReturnBufferLen = 4096 + sizeof(NVME_PASS_THROUGH_IOCTL);
        pMyIoctl->VendorSpecific[0] = (DWORD)0;
        pMyIoctl->VendorSpecific[1] = (DWORD)0;
        memset(pMyIoctl->DataBuffer, 0x55, 4096);

        dataPtr = (struct idcntrl_struct*)pMyIoctl->DataBuffer;
        dev.open();
        err = dev.ioctl(IOCTL_SCSI_MINIPORT, 
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        &dw);
        dev.close();
        if (!err) {
                uint32_t size = 4096;
                if (sz < size)
                        size = sz;
                memcpy(buffer, dataPtr, size);
        }
        return err;
}

int NvmeDisk::identify(struct identify_controller_struct &idctrl)
{
        return identify(get_nsid(), cns_idcntrl, &idctrl, sizeof(idctrl));
}

int NvmeDisk::identify(struct identify_namespace_struct &idns)
{
        return identify(get_nsid(), cns_idns, &idns, sizeof(idns));
}

#define CHECK_IDENTIFY_CONTROLLER_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct identify_controller_struct, field) == offset)

#define CHECK_IDENTIFY_NAMESPACE_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct identify_namespace_struct, field) == offset)

#define CHECK_LBA_FORMAT_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct lba_formatx_struct, field) == offset)

#ifdef _DEBUG
namespace flexxon {


void identify_controller_ct_assert(void)
{
        /* provide check so that future modifications will be flagged if they
                * mess up the structure's adherance to the standard */
        COMPILE_TIME_ASSERT(sizeof(struct nvme_command_packet) == 64);

        COMPILE_TIME_ASSERT(sizeof(struct identify_controller_struct) == 4096);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(vid, 0);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(ssvid, 2);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(sn, 4);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(mn, 24);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(fr, 64);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(rab, 72);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(ieee, 73);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(cmic, 76);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(mdts, 77);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(cntlid, 78);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(ver, 80);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(rtd3r, 84);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(rtd3e, 88);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(oaes, 92);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(ctratt, 96);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(resv0, 100);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(fguid, 112);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(resv1, 128);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(nvme_management_intf_spec_resv, 240);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(oacs, 256);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(acl, 258);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(aerl, 259);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(frmw, 260);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(lpa, 261);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(elpe, 262);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(npss, 263);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(avscc, 264);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(apsta, 265);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(wctemp, 266);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(cctemp, 268);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(mtfa, 270);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(hmpre, 272);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(hmmin, 276);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(tnvmcap, 280);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(unvmcap, 296);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(rpmbs, 312);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(edstt, 316);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(dsto, 318);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(fwug, 319);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(kas, 320);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(hctma,322);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(mntmt, 324);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(mxtmt, 326);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(sanicap, 328);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(resv2, 332);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(sqes, 512);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(cqes, 513);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(maxcmd, 514);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(nn, 516);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(oncs, 520);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(fuses, 522);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(fna, 524);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(vwc, 525);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(awun, 526);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(awupf, 528);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(nvscc, 530);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(resv3, 531);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(acwu, 532);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(resv4, 534);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(sgls, 536);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(resv5, 540);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(subnqn, 768);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(resv6, 1024);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(nvme_over_fabrics_resv, 1792);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(psdx[0], 2048);
        CHECK_IDENTIFY_CONTROLLER_OFFSET(vs, 3072);

        COMPILE_TIME_ASSERT(sizeof(struct power_state_desciptor) == 32);
        return;
}

void identify_namespace_ct_assert(void)
{
        COMPILE_TIME_ASSERT(sizeof(struct identify_namespace_struct) == 4096);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nsze, 0);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(ncap, 8);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nuse, 16);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nsfeat, 24);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nmic, 30);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nlbaf, 25);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(flbas, 26);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(mc, 27);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(dpc, 28);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(dps, 29);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nmic, 30);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(rescap, 31);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(fpi, 32);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(dlfeat, 33);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nawun, 34);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nawupf, 36);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nacwu, 38);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nabsn, 40);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nabo, 42);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nabspf, 44);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(noiob, 46);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nvmcap, 48);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(resv0, 64);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(nguid, 104);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(eui64, 120);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(lbaf, 128);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(resv1, 192);
        CHECK_IDENTIFY_NAMESPACE_OFFSET(vs, 384);

        COMPILE_TIME_ASSERT(sizeof(struct lba_formatx_struct) == 4);
        CHECK_LBA_FORMAT_OFFSET(ms, 0);
        CHECK_LBA_FORMAT_OFFSET(lbads, 2);
        CHECK_LBA_FORMAT_OFFSET(rp, 3);
        return;
}


}; // flexxon namespace
#endif
