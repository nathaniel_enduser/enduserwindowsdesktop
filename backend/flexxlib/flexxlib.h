#ifndef __FLEXXLIB_H__
#define __FLEXXLIB_H__

/** \file flexxlib.h
 * Header file for the flexxlib library. Applications should only need to
 * include only this file when using APIs provided by flexxlib.
 */

#include <windows.h>
#include <vector>
#include "flexx-nvme.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _DEBUG
#define FRIENDLY(fn)        friend void fn(void)
#else
#define FRIENDLY(fn)        /* none */
#endif

#ifdef _DEBUG
#define COMPILE_TIME_ASSERT(cond) {\
        char assert_this[(cond) ? 1 : -1]; \
        assert_this[0] = 'a';\
}
#else
#define COMPILE_TIME_ASSERT()
#endif

#define FLEXXON_VID     0x1d89
#define FLEXXON_SSVID   0x1d89

#define FLEXX_PASSED    1
#define FLEXX_FAILED    (!FLEXX_PASSED)

#define MAX_XPHY        128
#define MAX_NS_PER_XPHY 8

#define MAX_FILENAME_LEN        1024

/** \def __flexxlib_api
 * primarily for Windows DLL support. The Visual C++ compiler requires some 
 * compiler specific annotations for exported functions in libraries to be
 * linked properly to applications that use them.
 */

#ifdef FLEXXLIB_EXPORTS
#define __flexxlib_api __declspec(dllexport)
#else
#define __flexxlib_api __declspec(dllimport)
#endif 

/** \namespace flexxon
 * the flexxlib library uses the flexxon namespace to provide a consistent
 * container for all flexxon related APIs to access NVMe devices. */
namespace flexxon {

/** \enum flexxon_vendor_opcodes
 * flexxon specific opcodes for drive information and management.
 */
enum vendor_opcodes {
        /** for classification of commands that are informatory in nature. */
        flexxon_discovery = 0xc3,

        /** for classification of commands that are for management of vendor
         * specific features. */
        flexxon_drive_management = 0xc2,
};

/** \enum flexxon_discovery_sub_opcodes
 * valid subopcodes to be used under the flexxon_discovery_opcode.
 */
enum discovery_sub_opcodes {
        /** this is used to get vendor specific information from flexxon NVMe
         * drives. */
        discovery_subopc = 0x01,
};

/** \enum flexxon_drive_management_sub_opcodes
 * valid subopcodes to be used under the flexxon_drive_management opcode.
 */
enum drive_management_sub_opcodes {
        /** change password for the entire drive. this does not work on a 
         * per namespace basis. */
        change_xphy_pw_subopc = 0x01,

        /** lock or unlock files in a namespace. */
        lock_enable_subopc = 0x02,

        /** change policy in a namespace */
        change_policy_subopc = 0x03,

        /** enable or disable encryption in a namespace */
        encryption_enable_subopc = 0x4,

        /** enable or disable namespace protection */
        protect_enable_subopc = 0x5,

        /** initiate secure erase on the flexxon drive */
        secure_erase_subopc = 0x6,

        /** update the signature file in a namespace */
        signature_update_subopc = 0x7,
};

/** \enum vendor_cmd_data_direction
 * In terms of data handling, Flexxon vendor commands are classified under
 * three distinct directions:
 * 1) no data transfer
 * 2) transfer of data from host to drive. (write)
 * 3) transfer of data from drive to host. (read)
 * vendor commands does not support two-way transfer of data in one command.
 */
enum vendor_cmd_data_direction {
        /** no data transfer */
        no_vendor_data = 0x00,

        /** Drive should get data from host memory. This uses the PRP fields 
         * defiend by the NVMe specifiction. */
        vendor_data_to_device = 0x01,

        /** Drive should put data to host memory. This uses the PRP fields 
         * defiend by the NVMe specifiction.*/
        vendor_data_from_device = 0x02,
};

/** \struct discovery_struct 
 * discovery struct is used to determine vendor specific parameters currently
 * being enforced by the drive.
 */
struct discovery_struct {
        uint32_t resv0; /* 03:00 */
        struct {
                uint8_t auth_version; /* 04 */
                uint8_t resv1[3];     /* 07:05 */
        };
        uint8_t resv2[1016]; /* 1023:08 */
};

/** Class RawDevice is used to define the lowlevel interfaces for 
 * device files found in the OS platform.
 */
class RawDevice {
        virtual int open(void) = 0;
        virtual void close(void) = 0;
        virtual int ioctl(int request, ...) = 0;
};

/** Class Device
 * flexxlib representation for a device file found in the OS.
 */
class __flexxlib_api Device : public RawDevice {
public:
        Device(void);
        Device(char* filename);
        Device(const Device& d);
        int setTarget(char* filename);
        ~Device(void);
        int open(void);
        void close(void);
        int ioctl(int request, ...);
private:
        int setTargetFile(char *filename);
        HANDLE hdl;
        char* filename;
};

/** Class NvmeDevice
 * defines the lowlevel interface for NVMe devices found 
 * in the OS platform.
 */
class NvmeDevice {
        virtual uint32_t get_nsid(void) = 0;
        virtual int identify(const uint32_t nsid, const uint32_t cns,
                             void *buffer, const uint32_t sz) = 0;
        virtual int namespace_management(const uint32_t nsid, const uint32_t sel,
                                         void *buffer, const uint32_t sz) = 0;
        virtual int log_page13(const uint32_t nsid, const uint32_t cdw10, const uint32_t cdw11,
                               const uint32_t cdw12, const uint32_t cdw13,
                               void *buffer, const uint32_t size) = 0;
        virtual int feature12(const uint32_t nsid, const uint32_t opcode,
                              const uint32_t cdw10, const uint32_t cdw11,
                              const uint32_t cdw12, void* buffer, const uint32_t sz,
                              uint32_t *cpl_dw0) = 0;
        virtual int fw_download(void *fw_ptr, const uint32_t cdw10,
                                const uint32_t cdw11) = 0;
        virtual int fw_commit(const uint32_t cdw10) = 0;
        virtual int vs(const uint32_t admin, const uint32_t opcode,
                       const uint32_t nsid, const uint32_t data_direction,
                       void *buffer, const uint32_t size,
                       const uint32_t ndm, const uint32_t cdw12,
                       const uint32_t cdw13, const uint32_t cdw14,
                       const uint32_t cdw15) = 0;
};

/** Class NvmeDisk
 * flexxlib represents a generic NVMe drive using this class.
 */
class __flexxlib_api NvmeDisk : public NvmeDevice {
public:
        NvmeDisk(Device &d);
        ~NvmeDisk(void);
        uint32_t get_nsid(void) override;
        int identify(const uint32_t nsid, const uint32_t cns,
                     void *buffer, const uint32_t sz) override;
        int namespace_management(const uint32_t nsid, const uint32_t sel,
                                 void *buffer, const uint32_t sz) override;
        int log_page13(const uint32_t nsid, const uint32_t cdw10, const uint32_t cdw11,
                       const uint32_t cdw12, const uint32_t cdw13,
                       void *buffer, const uint32_t size) override;
        int feature12(const uint32_t nsid, const uint32_t opcode,
                      const uint32_t cdw10, const uint32_t cdw11,
                      const uint32_t cdw12, void* buffer, const uint32_t sz,
                      uint32_t *cpl_dw0) override;
        int fw_download(void *fw_ptr, uint32_t cdw10, const uint32_t cdw11)
                override;
        int fw_commit(const uint32_t cdw10) override;
        int vs(const uint32_t admin, const uint32_t opcode, const uint32_t nsid,
               const uint32_t data_direction, void *buffer,
               const uint32_t size, const uint32_t ndm, 
               const uint32_t cdw12, const uint32_t cdw13,
               const uint32_t cdw14, const uint32_t cdw15);

public: /* overloaded some with implied namespace id given a disk file */

        /* identify */
        int identify(struct identify_controller_struct &idctrl);
        int identify(struct identify_namespace_struct &idns);

        /* namespace management */
        int create_namespace(struct create_ns_template_struct &tmpl,
                             uint32_t &new_nsid);
        int delete_namespace(void);

        /* log pages */
        int logpage(const uint32_t nsid, const uint32_t lid, const uint32_t lsp, uint32_t rae,
                    const uint64_t lpo, void *buffer, const uint32_t size);
        int logpage(std::vector<struct log_err_info_struct> &err_info,
                    const uint32_t count);
        int logpage(const int global, struct log_smart_info_struct &smart);
        int logpage(struct log_fw_slot_info_struct &fw_slot);
        int logpage(struct log_changed_ns_list_struct& cns_list);
        int logpage(struct log_cmds_supported_and_effects_struct& cse);
        int logpage(struct log_device_self_test_struct &selftest);
        int logpage(struct log_host_tlmtry_struct &tlmtry, uint32_t size,
                    uint64_t data_block_index, uint32_t block_count,
                    const uint32_t create_data);
        int logpage(struct log_controller_tlmtry_struct& tlmtry,
                    uint32_t size, uint64_t data_block_index,
                    uint32_t block_count);
        int logpage(struct log_resv_notification_struct& resv_ntfn);
        int logpage(struct log_santize_status_struct& ss);

        int download_firmware(void* fw_ptr, const uint32_t fw_size,
                        const int bs);
        int commit_firmware(const uint32_t bpid, const uint32_t ca,
                        const uint32_t fs);
        /* features */
        int get_feature(const uint32_t nsid, const uint32_t fid,  const uint32_t sel,
                        const uint32_t cdw11, void *buffer,
                        const uint32_t size, uint32_t *cpldw0);
        int set_feature(const uint32_t nsid, const uint32_t fid, const uint32_t save,
                        const uint32_t cdw11, const uint32_t cdw12, void *buffer,
                        const uint32_t size, uint32_t *cpldw0);
        int admin_vs(const uint32_t opcode, const uint32_t nsid,
                     const uint32_t data_direction, void *buffer,
                     const uint32_t size, const uint32_t ndm, 
                     const uint32_t cdw12, const uint32_t cdw13,
                     const uint32_t cdw14, const uint32_t cdw15);
        int nvm_vs(const uint32_t opcode, const uint32_t nsid,
                   const uint32_t data_direction, void *buffer,
                   const uint32_t size, const uint32_t ndm, 
                   const uint32_t cdw12, const uint32_t cdw13,
                   const uint32_t cdw14, const uint32_t cdw15);
private:
        NvmeDisk(void) { }
        Device dev;
private:
        FRIENDLY(identify_controller_ct_assert);
        FRIENDLY(identify_namespace_ct_assert);
        FRIENDLY(create_namespace_ct_assert);
        FRIENDLY(log_page_ct_assert);
};

/** \typedef flexxDriveList
 * std::vector of type NvmeDisk.
 */
typedef std::vector<NvmeDisk> flexxDriveList;

/* version 1 vendor specific commands */
class FlexxonSpecificV1 {
public:
        virtual int discovery(struct discovery_struct& discovery) = 0;
        virtual int change_xphy_passwd(char *password, const int psize,
                                       char *new_password, const int npsize) = 0;

        /* implied nsid */
        virtual int lock(char* password, const int psize,
                         char* filename, const int fsize,
                         const uint32_t enable) = 0;
        virtual int change_policy(const uint32_t policyhi, 
                                  const uint32_t policylo, char *password,
                                  const int psize) = 0;
        virtual int encryption(char *password, const int psize,
                               const int enable_encryption) = 0;
        virtual int protect(char *password, const int psize,
                            const int enable_protection) = 0;
        virtual int secure_erase(char *password, const int psize) = 0;
        virtual int signature_update(char *password, const int psize,
                                     void *signature_data,
                                     const int signature_size) = 0;
};

/* this class is primarily used so that we can send Flexxon vendor specific
 * commands to an NvmeDisk object during runtime */
class __flexxlib_api FlexxDecorator : public FlexxonSpecificV1 {
public:
        FlexxDecorator(NvmeDisk &disk);
        int discovery(struct discovery_struct &discovery);
        int change_xphy_passwd(char *password, const int psize,
                               char *new_password, const int npsize);
        int lock(char* password, const int psize,
                 char* filename, const int fsize,
                 const uint32_t enable);
        int change_policy(const uint32_t policyhi, const uint32_t policylo,
                          char *password, const int psize);
        int encryption(char *password, const int psize,
                       const int enable_encryption);
        int protect(char *password, const int psize,
                    const int enable_protection);
        int secure_erase(char *password, const int psize);
        int signature_update(char *password, const int psize,
                             void *signature_data, const int signature_size);

private:
        NvmeDisk *disk_ptr; // convert this to a smart pointer
};

#ifdef _DEBUG
extern void identify_controller_ct_assert(void);
extern void identify_namespace_ct_assert(void);
extern void create_namespace_ct_assert(void);
extern void log_page_ct_assert(void);
#else
#define identify_controller_ct_assert()
#define identify_namespace_ct_assert()
#define create_namespace_ct_assert()
#define log_page_ct_assert()
#endif

extern __flexxlib_api int flexx_next_device(char* filename, const int len, const int reset);
extern __flexxlib_api int flexx_criteria(Device& d);
extern __flexxlib_api void scan_devices(flexxDriveList & driveList,
        int (*next_device)(char *filename, const int len, const int reset) = flexxon::flexx_next_device,
        int (*criteria)(Device& d) = flexxon::flexx_criteria);

/* helper functions */
static inline int ns_mgmt_supported(struct identify_controller_struct &idcontrol)
{
        unsigned int oacs = idcontrol.oacs;
        return ((oacs & OACS_NS_MANAGEMENT) == OACS_NS_MANAGEMENT) ? 1 : 0;
}

static inline int getlog_lpo_supported(struct identify_controller_struct &idcontrol)
{
        unsigned int lpa = idcontrol.lpa;
        return ((lpa & LPA_GETLOG_EXT_DATA) == LPA_GETLOG_EXT_DATA) ? 1 : 0;
}

static inline int getlog_smart_per_ns_supported(struct identify_controller_struct &idcontrol)
{
        unsigned int lpa = idcontrol.lpa;
        return ((lpa & LPA_SMART_LOG) == LPA_SMART_LOG) ? 1 : 0;
}

static inline int opcode_vendor_specific(const int admin, const uint32_t opcode)
{
        if (admin && ((opcode >= 0xc0) && (opcode <= 0xff))) {
                return 1;
        } else if (!admin && (opcode >= 0x80) && (opcode <= 0xff)) {
                return 1;
        }
        return 0;
}

}; /* flexxon namespace */

#ifdef __cplusplus
}
#endif

#endif /* __FLEXXLIB_H__  */
