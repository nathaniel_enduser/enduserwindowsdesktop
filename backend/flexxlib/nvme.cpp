#include "pch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "flexx-nvme.h"
#include "flexx-winioctl.h"

using namespace flexxon;

NvmeDisk::NvmeDisk(Device& d) : dev(d) { }

NvmeDisk::~NvmeDisk(void)
{ 
        identify_controller_ct_assert();
        identify_namespace_ct_assert();
}

uint32_t NvmeDisk::get_nsid(void)
{
        UCHAR myIoctlBuf[sizeof(NVME_PASS_THROUGH_IOCTL) + sizeof(DWORD)];
        PNVME_PASS_THROUGH_IOCTL     pMyIoctl = (PNVME_PASS_THROUGH_IOCTL)myIoctlBuf;
        DWORD                        dw;
        int err;

        memset(pMyIoctl, 0, sizeof(myIoctlBuf));
        // Set up the SRB IO Control header
        pMyIoctl->SrbIoCtrl.ControlCode = (ULONG)NVME_PASS_THROUGH_NSID;
        memcpy(pMyIoctl->SrbIoCtrl.Signature, NVME_SIG_STR, sizeof(NVME_SIG_STR));
        pMyIoctl->SrbIoCtrl.HeaderLength = (ULONG)sizeof(SRB_IO_CONTROL);
        pMyIoctl->SrbIoCtrl.Timeout = 30;
        pMyIoctl->SrbIoCtrl.Length = sizeof(myIoctlBuf) - sizeof(SRB_IO_CONTROL);
        pMyIoctl->ReturnBufferLen = sizeof(myIoctlBuf);
        pMyIoctl->Direction = NVME_FROM_DEV_TO_HOST;

        dev.open();
        err = dev.ioctl(IOCTL_SCSI_MINIPORT,
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        &dw);
        dev.close();
        if (!err) {
                int* nsidPtr = (int*)pMyIoctl->DataBuffer;
                return *nsidPtr;
        }
        return -1;
}

