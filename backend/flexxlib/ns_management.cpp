#include "pch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "flexx-nvme.h"
#include "flexx-winioctl.h"

using namespace flexxon;

/* create_ns_data_struct is really just an identify controller structure
 * with certain fields marked as reserved and should be zeroed out.
 */
struct create_ns_data_struct {
        uint64_t nsze;          /* 7:0    */
        uint64_t ncap;          /* 15:8   */
        uint8_t reserved0[10];  /* 25:16  */ 
        uint8_t flbas;          /* 26     */
        uint8_t reserved1[2];   /* 28:27  */
        uint8_t dps;            /* 29     */
        uint8_t nmic;           /* 30     */
        uint8_t reserved2[353]; /* 383:31 */
        uint8_t reserved3[640]; /* 1023:384  */
};

int NvmeDisk::namespace_management(const uint32_t nsid, const uint32_t sel,
                                   void *buffer, const uint32_t sz)
{
        UCHAR myIoctlBuf[sizeof(NVME_PASS_THROUGH_IOCTL) +
                         sizeof(struct identify_namespace_struct)];
        PNVME_PASS_THROUGH_IOCTL     pMyIoctl = (PNVME_PASS_THROUGH_IOCTL)myIoctlBuf;
        DWORD                        dw;
        struct nvme_command_packet *cmdPtr;
        int err;

        memset(pMyIoctl, 0, sizeof(myIoctlBuf));

        // Set up the SRB IO Control header
        pMyIoctl->SrbIoCtrl.ControlCode = (ULONG)NVME_PASS_THROUGH_SRB_IO_CODE;
        memcpy(pMyIoctl->SrbIoCtrl.Signature, NVME_SIG_STR, sizeof(NVME_SIG_STR));
        pMyIoctl->SrbIoCtrl.HeaderLength = (ULONG)sizeof(SRB_IO_CONTROL);
        pMyIoctl->SrbIoCtrl.Timeout = 30;
        pMyIoctl->SrbIoCtrl.Length = sizeof(myIoctlBuf) - sizeof(SRB_IO_CONTROL);

        // Set up the NVMe pass through IOCTL buffer
        cmdPtr = (struct nvme_command_packet *)pMyIoctl->NVMeCmd;
        cmdPtr->opcode = admin_namespace_management;
        cmdPtr->nsid = nsid;
        cmdPtr->cdw10 = sel;

        pMyIoctl->QueueId = 0; // Admin queue
        pMyIoctl->DataBufferLen = 0;
        pMyIoctl->Direction = NVME_NO_DATA_TX;
        pMyIoctl->ReturnBufferLen = sizeof(NVME_PASS_THROUGH_IOCTL);
        pMyIoctl->VendorSpecific[0] = (DWORD)0;
        pMyIoctl->VendorSpecific[1] = (DWORD)0;

        if (sel == namespace_mgmt_create) {
                pMyIoctl->Direction = NVME_FROM_HOST_TO_DEV;
                if (!buffer || sz < sizeof(struct identify_namespace_struct)) {
                        return -1;
                }
                memcpy(pMyIoctl->DataBuffer, buffer, sz);
        }
        dev.open();
        err = dev.ioctl(IOCTL_SCSI_MINIPORT, 
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        &dw);
        dev.close();
        return err;   
}

int NvmeDisk::create_namespace(struct create_ns_template_struct &tmpl,
                               uint32_t &new_nsid)
{
        struct identify_controller_struct idcontrol;
        struct identify_namespace_struct idns_common;
        struct create_ns_data_struct *create_ns;

        memset(&idcontrol, 0, sizeof(idcontrol));
        identify(idcontrol);

        /* check if namespace management is supported by controller first */
        if (!ns_mgmt_supported(idcontrol)) {
                return -1;
        }

        /* prepare the create namespace descriptor with common values
         * of all namespace */
        identify(0xffffffff, cns_idns, &idns_common, sizeof(idns_common));
        memset(&idns_common, 0, sizeof(struct create_ns_data_struct));
        create_ns = (struct create_ns_data_struct *)&idns_common;
        create_ns->nsze = tmpl.nsze;
        create_ns->ncap = tmpl.ncap;
        create_ns->flbas = tmpl.flbas;
        create_ns->dps = tmpl.dps;
        create_ns->nmic = tmpl.nmic;
        return namespace_management(0, namespace_mgmt_create, &idns_common, sizeof(idns_common));
}

int NvmeDisk::delete_namespace(void)
{
        struct identify_controller_struct idcontrol;

        memset(&idcontrol, 0, sizeof(idcontrol));
        identify(idcontrol);

        /* check if namespace management is supported by controller first */
        if (!ns_mgmt_supported(idcontrol)) {
                return -1;
        }
        return namespace_management(get_nsid(), namespace_mgmt_delete, NULL, 0);
}

#define CHECK_CREATE_NS_FIELD_OFFSET(field, offset) \
        COMPILE_TIME_ASSERT(offsetof(struct create_ns_data_struct, field) == offset)

#ifdef _DEBUG
namespace flexxon {

void create_namespace_ct_assert(void)
{
        /* provide check so that future modifications will be flagged if they
         * mess up the structure's adherance to the standard */
        COMPILE_TIME_ASSERT(sizeof(struct create_ns_data_struct) == 1024);
        CHECK_CREATE_NS_FIELD_OFFSET(nsze, 0);
        CHECK_CREATE_NS_FIELD_OFFSET(ncap, 8);
        CHECK_CREATE_NS_FIELD_OFFSET(flbas, 26);
        CHECK_CREATE_NS_FIELD_OFFSET(dps, 29);
        CHECK_CREATE_NS_FIELD_OFFSET(nmic, 30);
        return;
}

}; // flexxon namespace
#endif
