#ifndef __FLEXX_FILE_H__
#define __FLEXX_FILE_H__

#include <Windows.h>
#include <string>
#include <vector>
#include <winioctl.h>
#include "flexxlib.h"


__flexxlib_api typedef struct _SECTOR_RANGE
{
        uint64_t        start_sector;
        uint64_t        num_sects;
} SECTOR_RANGE, * PSECTOR_RANGE;


class File
{
public:
        virtual int     open(void) = 0;
        virtual int     ioctl(int request, ...) = 0;
        virtual void    close(void) = 0;
};

class flexxFile : public File
{
private:
        char            *filename;
        char 			volume;
        BOOL			valid;
        HANDLE			Hdl;
        PSECTOR_RANGE   sectors;
        uint64_t        size_on_disk;
        DWORD           num_ranges;

        void            setSectors(void);

public:
        __flexxlib_api  flexxFile(void);
        __flexxlib_api  flexxFile(char* name);
        __flexxlib_api  ~flexxFile(void);
        BOOL			isValid(void) { return valid; }
        char            getVolume(void) { return volume; }
        std::string     getFilename(void) { return filename; }
        HANDLE          getHandle(void) { return Hdl; }
        uint64_t        __flexxlib_api getSizeonDisk(void) { return size_on_disk; }
        int             __flexxlib_api getSectors(PSECTOR_RANGE osector);
        int             __flexxlib_api getRange(void) { return num_ranges; }

        int				open(void);
        int             ioctl(int request, ...);
        void			close(void);
};


#endif //__FLEXX_FILE_H__
