#include "pch.h"

#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>
#include <windows.h>
#include "flexx-file.h"

static HANDLE get_fileobj_handle(std::string FileObj);
static uint64_t get_volume_offset_from_disk(std::string Volume);
static int get_volume_pointer_base(std::string Volume, uint64_t* BasePtr);

int flexxFile::open(void)
{
        Hdl = get_fileobj_handle(filename);
        if (Hdl == INVALID_HANDLE_VALUE)
        {
                return 0;
        }

        return 1;
}


int flexxFile::ioctl(int request, ...)
{
        LPVOID inBuffer;
        DWORD inBufferSize;
        LPVOID outBuffer;
        DWORD outBufferSize;
        LPDWORD bytesReturned;
        va_list args;
        BOOL b;

        va_start(args, request);
        inBuffer = va_arg(args, LPVOID);
        inBufferSize = va_arg(args, DWORD);
        outBuffer = va_arg(args, LPVOID);
        outBufferSize = va_arg(args, DWORD);
        bytesReturned = va_arg(args, LPDWORD);
        va_end(args);

        b = DeviceIoControl(Hdl,
                            request,
                            inBuffer,
                            inBufferSize,
                            outBuffer,
                            outBufferSize,
                            bytesReturned,
                            NULL);

        return b;
}

void flexxFile::close(void)
{
        CloseHandle(Hdl);
}

flexxFile::flexxFile(char* name)
{
        size_t len = strlen(name);

        filename = (char*)malloc(len + 1);
        //memset(filename, 0, len);

        /* Initialize file name  and volume */
        memcpy(filename, name, len);
        filename[len] = '\0';

        volume = name[0];
        Hdl = INVALID_HANDLE_VALUE;

        /*
         * Determine if it's a valid file, because anybody can enter a file name
         * but not necessarily a valid file.
         */
        if (this->open())
        {
                /* It is a valid file */
                valid = 1;
                this->close();
        }
        else
        {
                /* It is not a valid file */
                valid = 0;
        }

        setSectors();
}


flexxFile::flexxFile(void)
{
        valid = 0;
        num_ranges = 0;
        Hdl = INVALID_HANDLE_VALUE;
}

flexxFile::~flexxFile(void)
{
        if (valid) {
                free(filename);
                free(sectors);
        }
}

/* Used to get offset in bytes */
static uint64_t get_volume_offset_from_disk(std::string Volume)
{
        STARTING_VCN_INPUT_BUFFER vcn_buffer;
        HANDLE hdl;
        DWORD dwBytesReturned = 0;
        int buffsize = 1024;
        PVOLUME_DISK_EXTENTS PVol = (PVOLUME_DISK_EXTENTS)malloc(buffsize);
        uint64_t offset = { 0 };

        hdl = CreateFileA(Volume.c_str(),
                            0,
                            0,
                            NULL,
                            OPEN_EXISTING,
                            FILE_FLAG_BACKUP_SEMANTICS, //FILE_ATTRIBUTE_READONLY,
                            NULL);
        if (hdl == INVALID_HANDLE_VALUE) {
                std::cout << "ERROR: opening volume: " << Volume << std::endl;
                return 0;
        }

        vcn_buffer.StartingVcn.QuadPart = 0;

        if (!DeviceIoControl(hdl,
                            IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS,
                            &vcn_buffer,
                            sizeof(STARTING_VCN_INPUT_BUFFER),
                            PVol,
                            buffsize,
                            &dwBytesReturned, NULL))
        {
                std::cout << "ERROR: Get volume offset IOCTL failed." << std::endl;
                return 0;
        }

        if (PVol->NumberOfDiskExtents > 1)
        {
                std::cout << "ERROR: Volume encompassing multiple disks not handled." << std::endl;
                return 0;
        }

        offset = PVol->Extents->StartingOffset.QuadPart;

        free(PVol);
        CloseHandle(hdl);

        return offset;
}


static uint32_t get_volume_sectors_per_cluster(std::string Volume, uint32_t* SectorSize)
{
        int i = 0, buffsize = 4096;
        DWORD sec_per_clustr = 0, bpsect = 0, num_free_clustr = 0, total_clustr = 0;

        if (!GetDiskFreeSpaceA(Volume.c_str(),
                                &sec_per_clustr,
                                &bpsect,
                                &num_free_clustr,
                                &total_clustr))
        {
                std::cout << "ERROR: Failed to query sector per cluster: " << Volume << std::endl;
                return 0;
        }

        *SectorSize = bpsect;

        return sec_per_clustr;
}


static HANDLE get_folder_handle(std::string Folder)
{
        int i = 0, buffsize = 4096;
        HANDLE hdl;

        hdl = CreateFileA(Folder.c_str(),
                            0,
                            0,
                            NULL,
                            OPEN_EXISTING,
                            FILE_FLAG_BACKUP_SEMANTICS,
                            NULL);
        if (hdl == INVALID_HANDLE_VALUE) {
                std::cerr << "ERROR: error opening: " << Folder << std::endl;
                return hdl;
        }

        return hdl;
}


static HANDLE get_file_handle(std::string filename)
{
        int i = 0, buffsize = 4096;
        HANDLE hdl;

        hdl = CreateFileA(filename.c_str(),
                            GENERIC_READ,
                            FILE_SHARE_READ | FILE_SHARE_WRITE,
                            NULL,
                            OPEN_EXISTING,
                            FILE_ATTRIBUTE_READONLY,
                            NULL);
        if (hdl == INVALID_HANDLE_VALUE) {
                std::cerr << "ERROR: error opening file: " << filename << std::endl;
                return hdl;
        }

        return hdl;
}

static HANDLE get_fileobj_handle(std::string FileObj)
{
        if (FileObj.back() == '\\')
        {
                return get_folder_handle(FileObj);
        }

        return get_file_handle(FileObj);
}


static int get_volume_pointer_base(std::string Volume, uint64_t* BasePtr)
{
        HANDLE Hdl;
        STARTING_VCN_INPUT_BUFFER vcn_buffer;
        DWORD dwBytesReturned = 0;
        RETRIEVAL_POINTER_BASE Rp;

        vcn_buffer.StartingVcn.QuadPart = 0;

        Hdl = get_file_handle(Volume);
        if (Hdl == INVALID_HANDLE_VALUE)
        {
                return 0;
        }

        /* Get Volume Base Pointer */
        vcn_buffer.StartingVcn.QuadPart = 0;
        ZeroMemory(&Rp, sizeof(RETRIEVAL_POINTER_BASE));

        if (!DeviceIoControl(Hdl,
                            FSCTL_GET_RETRIEVAL_POINTER_BASE,
                            &vcn_buffer,
                            sizeof(STARTING_VCN_INPUT_BUFFER),
                            &Rp,
                            sizeof(RETRIEVAL_POINTER_BASE),
                            &dwBytesReturned,
                            NULL))
        {
                std::cout << "Error: FSCTL_GET_RETRIEVAL_POINTER_BASE Failed." << std::endl;
                return 0;
        }

        CloseHandle(Hdl);

        *BasePtr = Rp.FileAreaOffset.QuadPart;

        return 1;
}


void flexxFile::setSectors(void)
{
        uint64_t		            base = 0, offsetDisk;
        uint32_t 		            sectperClustr, sectSz;
        int                         ret;
        uint32_t                    Idx;
        char                        ch;
        uint32_t                       dwBytesReturned = 0;
        PRETRIEVAL_POINTERS_BUFFER  Rpb = (PRETRIEVAL_POINTERS_BUFFER)malloc(1024);
        std::string                 Volume;
        STARTING_VCN_INPUT_BUFFER   vcn_buffer;


        if (!isValid())
        {
                std::cout << "ERROR: Not a valid file, no valid sectors to get." << std::endl;
                return;
        }

        //sectors.clear();
        size_on_disk = 0;

        std::cout << "New compute sectors" << std::endl;

        /* Get Drive letter, usually at the beginning of filename */
        ch = getVolume();

        /* Generate Volume string of format "\\\\.\\X: */
        Volume.erase();
        Volume = "\\\\.\\";
        Volume.push_back(ch);
        Volume.push_back(':');

        /* Get C: volume offset in bytes */
        offsetDisk = get_volume_offset_from_disk(Volume);
        if (!offsetDisk)
        {
                return;
        }

        /* Get the Volume Pointer Base */
        ret = get_volume_pointer_base(Volume, &base);
        if (!ret)
        {
                return;
        }

        /* Generate Volume string of format "X:\\" */
        Volume.erase();
        Volume.push_back(ch);
        Volume.push_back(':');
        Volume.push_back('\\');

        sectperClustr = get_volume_sectors_per_cluster(Volume, &sectSz);
        if (!sectperClustr)
        {
                return;
        }

        /* Convert bytes offset from disk to sectors and add to Base */
        offsetDisk /= sectSz;
        base += offsetDisk;

        /* Check if FileObj is folder or string */
        if (!this->open())
        {
                std::cout << "Error opening file " << filename << std::endl;
                return;
        }

        /* Get its LCN */
        ZeroMemory(Rpb, 1024);
        vcn_buffer.StartingVcn.QuadPart = 0;

        if (!ioctl(FSCTL_GET_RETRIEVAL_POINTERS,
                    &vcn_buffer,
                    sizeof(STARTING_VCN_INPUT_BUFFER),
                    Rpb,
                    1024,
                    &dwBytesReturned,
                    NULL))
        {
                std::cout << "Error: FSCTL_GET_RETRIEVAL_POINTERS Failed." << std::endl;
                return;
        }

        sectors = (PSECTOR_RANGE)malloc(sizeof(SECTOR_RANGE) * Rpb->ExtentCount);
        num_ranges = (uint32_t)Rpb->ExtentCount;

        /* convert and print */
        for (Idx = 0; Idx < Rpb->ExtentCount; Idx++)
        {
                //sectors.push_back(SECTOR_RANGE());
                sectors[Idx].start_sector = base + (Rpb->Extents[Idx].Lcn.QuadPart * sectperClustr);

                if (Idx == 0)
                {
                        sectors[Idx].num_sects = (Rpb->Extents[Idx].NextVcn.QuadPart - Rpb->StartingVcn.QuadPart) * sectperClustr;
                }
                else
                {
                        sectors[Idx].num_sects = (Rpb->Extents[Idx].NextVcn.QuadPart - Rpb->Extents[Idx - 1].NextVcn.QuadPart) * sectperClustr;
                }

                size_on_disk += sectors[Idx].num_sects;
        }

        size_on_disk *= sectSz;

        free(Rpb);
        close();

        return;
}


int flexxFile::getSectors(PSECTOR_RANGE osector)
{
        DWORD total_range_len = num_ranges * sizeof(SECTOR_RANGE);

        memcpy(osector, sectors, total_range_len);

        return 1;
}
