#include "pch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "flexx-file.h"
#include "flexx-nvme.h"
#include "flexx-winioctl.h"

using namespace flexxon;

struct __msvc_zero_size_fix __align(8) vendor_data_header {
        uint8_t password[1024];
        uint8_t payload[];
};

static char *set_password_field(struct vendor_data_header *data, char *password,
                                const int psize)
{
        char *dst;
        if (!data || !password || (psize == 0) || (psize >= 1024))
                return NULL;
        dst = (char *)memcpy(data->password, password, psize);
        data->password[psize] = '\0';
        return dst;
}

FlexxDecorator::FlexxDecorator(NvmeDisk &disk)
{
        COMPILE_TIME_ASSERT(sizeof(vendor_data_header) == 1024);
        this->disk_ptr = &disk;
}

int FlexxDecorator::discovery(struct discovery_struct &discovery)
{
        uint32_t nsid = 0; /* nsid field in discovery is reserved */
        uint32_t cdw12 = discovery_subopc;
        uint32_t cdw13 = sizeof(struct discovery_struct);
        return disk_ptr->admin_vs(flexxon_discovery, nsid, vendor_data_from_device,
                                  &discovery, sizeof(discovery), 0,
                                  cdw12, cdw13, 0, 0);
}

int FlexxDecorator::change_xphy_passwd(char *password, const int psize,
                                       char *new_password, const int npsize)
{
        char buffer[2048] = {0};
        char *data_ptr;
        uint32_t nsid = 0; /* nsid field in change xphy password is reserved */
        uint32_t cdw12 = change_xphy_pw_subopc;
        uint32_t cdw13 = sizeof(buffer);

        memcpy(&buffer[sizeof(struct vendor_data_header)], new_password, npsize);
        data_ptr = set_password_field((struct vendor_data_header*)buffer, password, psize);
        return disk_ptr->admin_vs(flexxon_drive_management, nsid,
                                  vendor_data_to_device, data_ptr, sizeof(buffer),
                                  0, cdw12, cdw13, 0, 0);
}

int FlexxDecorator::lock(char* password, const int psize,
                         char* filename, const int fsize,
                         const uint32_t enable)
{ 
        PSECTOR_RANGE plba, plba_data;
        void* psect;
        int nrange, ret, idx;
        flexxFile file(filename);
        char *buffer, *data_ptr;
        uint32_t cdw12 = lock_enable_subopc;
        uint32_t dsize;
        const uint32_t nsid = disk_ptr->get_nsid();

        /* Get the file's LBAs */
        nrange = file.getRange();
        dsize = sizeof(struct vendor_data_header) + sizeof(uint64_t) + (nrange * sizeof(SECTOR_RANGE));
        buffer = (char*)malloc(dsize);
        if (!buffer)
                return -1;

        memset(buffer, 0, dsize);

        /* Set password into data */
        data_ptr = set_password_field((struct vendor_data_header*)buffer, password, psize);
        dsize = sizeof(struct vendor_data_header);

        /* Add number of lba ranges into the data buffer */
        *(uint64_t *)&buffer[dsize] = nrange;
        dsize += sizeof(uint64_t);

        psect = malloc(nrange * sizeof(SECTOR_RANGE));
        if (!psect)
                return -2;

        if (!file.getSectors((PSECTOR_RANGE)psect))
                return -3;

        /* Add the lba ranges into the buffer by walking through the given lba range (psect) */
        plba_data = (PSECTOR_RANGE)&buffer[dsize];
        plba = (PSECTOR_RANGE)psect;
        for (idx = 0; idx < nrange; idx++) {
                plba_data->start_sector = plba->start_sector;
                plba_data->num_sects = plba->num_sects;

                dsize += sizeof(SECTOR_RANGE);
                plba_data++;
                plba++;
        }

        ret = disk_ptr->admin_vs(flexxon_drive_management, nsid,
                                  vendor_data_to_device, data_ptr, dsize,
                                  0, cdw12, dsize, 0, 
                                  (enable) ? 1 : 0);
        free(buffer);
        free(psect);
        return ret;
}

int FlexxDecorator::change_policy(const uint32_t policyhi, 
                                  const uint32_t policylo, char *password,
                                  const int psize)
{
        char *data_ptr;
        struct vendor_data_header data = {0};
        uint32_t cdw12 = change_policy_subopc;
        const uint32_t nsid = disk_ptr->get_nsid();

        data_ptr = set_password_field(&data, password, psize);
        if (!data_ptr)
                return -1;
        return disk_ptr->admin_vs(flexxon_drive_management, nsid, 
                                  vendor_data_to_device, data_ptr, sizeof(data),
                                  0, cdw12, sizeof(data), policylo, policyhi);
}

int FlexxDecorator::encryption(char *password, const int psize,
                               const int enable_encryption)
{
        char *data_ptr;
        struct vendor_data_header data = {0};
        uint32_t cdw12 = encryption_enable_subopc;
        const uint32_t nsid = disk_ptr->get_nsid();

        data_ptr = set_password_field(&data, password, psize);
        if (!data_ptr)
                return -1;
        return disk_ptr->admin_vs(flexxon_drive_management, nsid, 
                                  vendor_data_to_device, data_ptr, sizeof(data),
                                  0, cdw12, sizeof(data), 0,
                                  !!enable_encryption);
}

int FlexxDecorator::protect(char *password, const int psize,
                            const int enable_protection)
{
        char *data_ptr;
        struct vendor_data_header data = {0};
        uint32_t cdw12 = protect_enable_subopc;
        const uint32_t nsid = disk_ptr->get_nsid();

        data_ptr = set_password_field(&data, password, psize);
        if (!data_ptr)
                return -1;
        return disk_ptr->admin_vs(flexxon_drive_management, nsid, 
                                  vendor_data_to_device, data_ptr, sizeof(data),
                                  0, cdw12, sizeof(data), 0,
                                  !!enable_protection);
}

int FlexxDecorator::secure_erase(char *password, const int psize)
{
        char *data_ptr;
        struct vendor_data_header data = {0};
        uint32_t cdw12 = secure_erase_subopc;
        const uint32_t nsid = disk_ptr->get_nsid();

        data_ptr = set_password_field(&data, password, psize);
        if (!data_ptr)
                return -1;
        return disk_ptr->admin_vs(flexxon_drive_management, nsid, 
                                  vendor_data_to_device, data_ptr, sizeof(data),
                                  0, cdw12, sizeof(data), 0, 0);
}

int FlexxDecorator::signature_update(char *password, const int psize,
                                     void *signature_data,
                                     const int signature_size)
{
        char* data_ptr;
        char buffer[2048] = { 0 };
        uint32_t cdw12 = signature_update_subopc;
        const uint32_t nsid = disk_ptr->get_nsid();

        data_ptr = set_password_field((struct vendor_data_header*)buffer, password, psize);
        memcpy(&buffer[sizeof(struct vendor_data_header)], signature_data, signature_size);
    
        return disk_ptr->admin_vs(flexxon_drive_management, nsid,
                                  vendor_data_to_device, data_ptr, sizeof(buffer),
                                  0, cdw12, signature_size, 0, 0);
}

