#include "pch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "flexx-nvme.h"
#include "flexx-winioctl.h"

using namespace flexxon;

int NvmeDisk::vs(const uint32_t admin, const uint32_t opcode,
                 const uint32_t nsid, const uint32_t data_direction,
                 void *buffer, const uint32_t ndt, const uint32_t ndm,
                 const uint32_t cdw12, const uint32_t cdw13,
                 const uint32_t cdw14, const uint32_t cdw15)
{
        ULONG direction;
        unsigned char *myIoctlBuf;
        int ioctlSize = sizeof(NVME_PASS_THROUGH_IOCTL) + (ndt << 2);
        PNVME_PASS_THROUGH_IOCTL pMyIoctl;
        struct nvme_command_packet *cmdPtr;
        DWORD dw;
        void* dataPtr = NULL;
        int err;

        if (!(ndt == 0) && (buffer == NULL))
                return -1;
        if (data_direction == vendor_data_from_device)
                direction = NVME_FROM_DEV_TO_HOST;
        else if (data_direction == vendor_data_to_device)
                direction = NVME_FROM_HOST_TO_DEV;
        else if (data_direction == no_vendor_data)
                direction = NVME_NO_DATA_TX;
        else
                return -1;

        if (!opcode_vendor_specific(admin, opcode))
                return -1;
        
        myIoctlBuf = (unsigned char *)malloc(ioctlSize);
        if (!myIoctlBuf)
                return -1;

        pMyIoctl = (PNVME_PASS_THROUGH_IOCTL)myIoctlBuf;
        memset(pMyIoctl, 0, ioctlSize);

        // Set up the SRB IO Control header
        pMyIoctl->SrbIoCtrl.ControlCode = (ULONG)NVME_PASS_THROUGH_SRB_IO_CODE;
        memcpy(pMyIoctl->SrbIoCtrl.Signature, NVME_SIG_STR, sizeof(NVME_SIG_STR));
        pMyIoctl->SrbIoCtrl.HeaderLength = (ULONG)sizeof(SRB_IO_CONTROL);
        pMyIoctl->SrbIoCtrl.Timeout = 30;
        pMyIoctl->SrbIoCtrl.Length = ioctlSize - sizeof(SRB_IO_CONTROL);

        // Set up the NVMe pass through IOCTL buffer
        cmdPtr = (struct nvme_command_packet*)pMyIoctl->NVMeCmd;
        cmdPtr->opcode = opcode;
        cmdPtr->nsid = nsid;
        cmdPtr->cdw10 = ndt;
        cmdPtr->cdw11 = ndm;
        cmdPtr->cdw12 = cdw12;
        cmdPtr->cdw13 = cdw13;
        cmdPtr->cdw14 = cdw14;
        cmdPtr->cdw15 = cdw15;

        pMyIoctl->QueueId = (admin == 1) ? 0 : 1;
        pMyIoctl->DataBufferLen = 0;
        pMyIoctl->Direction = direction;
        pMyIoctl->ReturnBufferLen = ioctlSize;
        pMyIoctl->VendorSpecific[0] = (DWORD)0;
        pMyIoctl->VendorSpecific[1] = (DWORD)0;
        memset(pMyIoctl->DataBuffer, 0x55, ndt << 2);

        dataPtr = (struct idcntrl_struct*)pMyIoctl->DataBuffer;
        dev.open();
        err = dev.ioctl(IOCTL_SCSI_MINIPORT, 
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        &dw);
        dev.close();
        if (!err) {
                memcpy(buffer, dataPtr, ndt << 2);
        }
        free(myIoctlBuf);
        return err;     
}

int NvmeDisk::admin_vs(const uint32_t opcode, const uint32_t nsid,
                       const uint32_t data_direction, void *buffer,
                       const uint32_t size, const uint32_t ndm,
                       const uint32_t cdw12, const uint32_t cdw13,
                       const uint32_t cdw14, const uint32_t cdw15)
{
        return vs(1, opcode, nsid, data_direction, buffer, size >> 2,
                  ndm, cdw12, cdw13, cdw14, cdw15);
}

int NvmeDisk::nvm_vs(const uint32_t opcode, const uint32_t nsid,
                     const uint32_t data_direction, void *buffer,
                     const uint32_t size, const uint32_t ndm, 
                     const uint32_t cdw12, const uint32_t cdw13,
                     const uint32_t cdw14, const uint32_t cdw15)
{
        return vs(0, opcode, nsid, data_direction, buffer, size >> 2,
                  ndm, cdw12, cdw13, cdw14, cdw15);
}

