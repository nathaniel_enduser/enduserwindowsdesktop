#include "pch.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "winioctl.h"

using namespace flexxon;

Device::Device(void)
{
        filename = NULL;
        hdl = INVALID_HANDLE_VALUE;
}

int Device::setTargetFile(char *filename)
{
        size_t len;
        char *p;
        
        if (!filename)
                return -1;

        len = strlen(filename);
        if (!(p = (char *)malloc(len + 1))) {
                return -1;
        }
        memcpy(p, filename, len);
        p[len] = '\0';
        free(this->filename);
        this->filename = p;
        return 0;    
}

Device::Device(char *filename)
{
        if(setTargetFile(filename) != 0) {
                throw std::bad_alloc();
        }
        hdl = INVALID_HANDLE_VALUE;
}

int Device::setTarget(char *filename)
{
        return setTargetFile(filename);
}

Device::~Device(void)
{
        close();
        free(filename);
}

int Device::open(void)
{
        if (hdl == INVALID_HANDLE_VALUE) {
                hdl = CreateFileA(filename, GENERIC_READ | GENERIC_WRITE,
                                  FILE_SHARE_READ | FILE_SHARE_WRITE,
                                  NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
                                  NULL);
        }
        return (hdl != INVALID_HANDLE_VALUE) ? 0 : -1;
}

void Device::close(void)
{
        if (hdl != INVALID_HANDLE_VALUE) {
                CloseHandle(hdl);
                hdl = INVALID_HANDLE_VALUE;
        }
        return;
}

int Device::ioctl(int request, ...)
{
        LPVOID inBuffer;
        DWORD inBufferSize;
        LPVOID outBuffer;
        DWORD outBufferSize;
        LPDWORD bytesReturned;
        va_list args;
        BOOL b;

        va_start(args, request);
        inBuffer = va_arg(args, LPVOID);
        inBufferSize = va_arg(args, DWORD);
        outBuffer = va_arg(args, LPVOID);
        outBufferSize = va_arg(args, DWORD);
        bytesReturned = va_arg(args, LPDWORD);
        va_end(args);

        b = DeviceIoControl(hdl,
                            request,
                            inBuffer,
                            inBufferSize,
                            outBuffer,
                            outBufferSize,
                            bytesReturned,
                            NULL);

        return b ? 0 : -1;
}

/* need to provide a deep-copy constructor */
Device::Device(const Device& d)
{
        filename = NULL;
        hdl = INVALID_HANDLE_VALUE;
        setTargetFile(d.filename);

        /* no need to provide a deep copy for hdl since we only ever use it
         * when it is needed. e.g. when sending ioctl. most of the time, it 
         * is not referencing any valid handle.
         */
}

int flexxon::flexx_next_device(char *port, const int len, const int reset)
{
        static const int max_devices = MAX_XPHY * MAX_NS_PER_XPHY;
        static int count;

        if (reset)
                count = 0;
        if (count >= max_devices)
                return 0;
        snprintf(port, len ,"%s%d:", "\\\\.\\Scsi", count);
        count++;
        return 1;
}

int flexxon::flexx_criteria(Device &d)
{
        struct identify_controller_struct idcntlr;

        /* are we looking at an nvme device?
         * try issuing an identify controller
         * NVMe command to find out */
        NvmeDisk GenericNVMe(d);
        if (GenericNVMe.identify(idcntlr) != 0) {
                return FLEXX_FAILED;
        }

        /* are we looking at a flexxon nvme device?
         * the identify controller command we issued prior
         * should have a VID and SSVID belonging to
         * Flexxon */
        if ((idcntlr.vid != FLEXXON_VID) ||
            (idcntlr.ssvid != FLEXXON_SSVID)) {
                return FLEXX_FAILED;
        }

        return FLEXX_PASSED;
}

void flexxon::scan_devices(flexxDriveList &driveList,
                  int (*next_device)(char* filename, const int len, const int reset),
                  int (*criteria)(Device &d))
 {
        char ScsiPort[MAX_FILENAME_LEN];
        int reset = 1;

        while (next_device(ScsiPort, sizeof(ScsiPort), reset) == 1) {
                Device d(ScsiPort);
                if (criteria(d) == FLEXX_PASSED) {
                        driveList.emplace_back(d);
                }
                reset = 0;
        }
        return;
}

