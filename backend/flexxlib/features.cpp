#include "pch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "flexx-nvme.h"
#include "flexx-winioctl.h"

using namespace flexxon;

static inline int feature_opcode(const int opcode)
{
        switch(opcode) {
        case admin_get_features:
        case admin_set_features:
                break;
        default:
                return 0;
        }
        return 1;
}

static inline int feature_sel(const int sel)
{
        switch(sel) {
        case sel_current:
        case sel_default:
        case sel_supported_cap:
        case sel_saved:
                break;
        default: 
                return 0;
        }

        return 1;
}

int NvmeDisk::feature12(const uint32_t nsid, const uint32_t opcode,
                        const uint32_t cdw10, const uint32_t cdw11,
                        const uint32_t cdw12, void *buffer, const uint32_t sz,
                        uint32_t *cpl_dw0)
{
        int sel = cdw10 & 0xff;
        int fid = (cdw10 & 0x700) >> 8;
        unsigned char *myIoctlBuf;
        PNVME_PASS_THROUGH_IOCTL     pMyIoctl;
        DWORD                        dw;
        struct nvme_command_packet *cmdPtr;
        void *dataPtr = NULL;
        int err;
        int ioctlSize = sizeof(NVME_PASS_THROUGH_IOCTL) + sz;

        if (!feature_opcode(opcode)) {
                return -1;
        }

        if (!feature_sel(sel)) {
                return -1;
        }

        myIoctlBuf = (unsigned char *)malloc(ioctlSize);
        if (!myIoctlBuf)
                return -1;

        pMyIoctl = (PNVME_PASS_THROUGH_IOCTL)myIoctlBuf;
        memset(pMyIoctl, 0, ioctlSize);

        // Set up the SRB IO Control header
        pMyIoctl->SrbIoCtrl.ControlCode = (ULONG)NVME_PASS_THROUGH_SRB_IO_CODE;
        memcpy(pMyIoctl->SrbIoCtrl.Signature, NVME_SIG_STR, sizeof(NVME_SIG_STR));
        pMyIoctl->SrbIoCtrl.HeaderLength = (ULONG)sizeof(SRB_IO_CONTROL);
        pMyIoctl->SrbIoCtrl.Timeout = 30;
        pMyIoctl->SrbIoCtrl.Length = ioctlSize - sizeof(SRB_IO_CONTROL);

        // Set up the NVMe pass through IOCTL buffer
        cmdPtr = (struct nvme_command_packet*)pMyIoctl->NVMeCmd;
        cmdPtr->opcode = opcode;
        cmdPtr->nsid = nsid;
        cmdPtr->cdw10 = cdw10;
        cmdPtr->cdw11 = cdw11;
        cmdPtr->cdw12 = cdw12;

        pMyIoctl->QueueId = 0; // Admin queue
        pMyIoctl->DataBufferLen = 0;
        if (sz) {
                pMyIoctl->Direction = (opcode == admin_get_features) ? 
                        NVME_FROM_DEV_TO_HOST : 
                        NVME_FROM_HOST_TO_DEV;
                memset(pMyIoctl->DataBuffer, 0x00, sz);
        } else {
                pMyIoctl->Direction = NVME_NO_DATA_TX;
        }
        pMyIoctl->ReturnBufferLen = sz + sizeof(NVME_PASS_THROUGH_IOCTL);
        pMyIoctl->VendorSpecific[0] = (DWORD)0;
        pMyIoctl->VendorSpecific[1] = (DWORD)0;

        dataPtr = (void *)pMyIoctl->DataBuffer;
        dev.open();
        err = dev.ioctl(IOCTL_SCSI_MINIPORT, 
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        &dw);
        dev.close();
        if (!err) {
                if (buffer && sz)
                        memcpy(buffer, dataPtr, sz);

                /* copy completion dw0 back to caller */
                if (cpl_dw0)
                        *cpl_dw0 = pMyIoctl->CplEntry[0];
        }
        free(myIoctlBuf);
        return err;
}

int NvmeDisk::get_feature(const uint32_t nsid, const uint32_t fid,
                          const uint32_t sel, const uint32_t cdw11,
                          void *buffer, const uint32_t size, uint32_t *cpldw0)
{
        uint32_t cdw10 = ((sel & 0x7) << 8) | (fid & 0xff);
        return feature12(nsid, admin_get_features, cdw10, cdw11, 0, buffer,
                         size, cpldw0);
}

int NvmeDisk::set_feature(const uint32_t nsid, const uint32_t fid,
                          const uint32_t save, const uint32_t cdw11,
                          const uint32_t cdw12, void *buffer,
                          const uint32_t size, uint32_t *cpldw0)
{
        uint32_t dw10 = (save) ? (1 << 1) : 0 | (fid & 0xff);
        return feature12(nsid, admin_set_features, dw10, cdw11, cdw12, buffer, 
                         size, cpldw0);
}
