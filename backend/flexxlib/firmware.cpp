#include "pch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <new>
#include "flexxlib.h"
#include "flexx-nvme.h"
#include "flexx-winioctl.h"

using namespace flexxon;


int NvmeDisk::fw_download(void *fw_ptr, const uint32_t cdw10,
                          const uint32_t cdw11)
{
        int ioctlSize;
        unsigned char *myIoctlBuf;
        PNVME_PASS_THROUGH_IOCTL     pMyIoctl;
        DWORD                        dw;
        struct nvme_command_packet *cmdPtr;
        unsigned char *dataPtr;
        int fw_size;
        int err;

        if (!fw_ptr) {
                return -1;
        }

        fw_size = (cdw10+1) << 2;
        ioctlSize = sizeof(NVME_PASS_THROUGH_IOCTL) + fw_size;
        myIoctlBuf = (unsigned char *)malloc(ioctlSize);
        if (!myIoctlBuf)
                return -1;

        pMyIoctl = (PNVME_PASS_THROUGH_IOCTL)myIoctlBuf;
        memset(pMyIoctl, 0, ioctlSize);

        // Set up the SRB IO Control header
        pMyIoctl->SrbIoCtrl.ControlCode = (ULONG)NVME_PASS_THROUGH_SRB_IO_CODE;
        memcpy(pMyIoctl->SrbIoCtrl.Signature, NVME_SIG_STR, sizeof(NVME_SIG_STR));
        pMyIoctl->SrbIoCtrl.HeaderLength = (ULONG)sizeof(SRB_IO_CONTROL);
        pMyIoctl->SrbIoCtrl.Timeout = 30;
        pMyIoctl->SrbIoCtrl.Length = ioctlSize - sizeof(SRB_IO_CONTROL);

        // Set up the NVMe pass through IOCTL buffer
        cmdPtr = (struct nvme_command_packet*)pMyIoctl->NVMeCmd;
        cmdPtr->opcode = admin_fw_img_download;
        cmdPtr->nsid = 0;
        cmdPtr->cdw10 = cdw10;
        cmdPtr->cdw11 = cdw11;

        pMyIoctl->QueueId = 0; // Admin queue
        pMyIoctl->DataBufferLen = 0;
        pMyIoctl->Direction = NVME_FROM_HOST_TO_DEV;
        pMyIoctl->ReturnBufferLen = ioctlSize;
        pMyIoctl->VendorSpecific[0] = (DWORD)0;
        pMyIoctl->VendorSpecific[1] = (DWORD)0;

        dataPtr = (unsigned char *)pMyIoctl->DataBuffer;
        memcpy(dataPtr, fw_ptr, fw_size);
        dev.open();
        err = dev.ioctl(IOCTL_SCSI_MINIPORT, 
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        &dw);
        dev.close();
        free(myIoctlBuf);
        return err;
}

int NvmeDisk::fw_commit(const uint32_t cdw10)
{
        UCHAR myIoctlBuf[sizeof(NVME_PASS_THROUGH_IOCTL)];
        PNVME_PASS_THROUGH_IOCTL     pMyIoctl;
        DWORD                        dw;
        struct nvme_command_packet *cmdPtr;
        int err;

        pMyIoctl = (PNVME_PASS_THROUGH_IOCTL)myIoctlBuf;
        memset(pMyIoctl, 0, sizeof(myIoctlBuf));

        // Set up the SRB IO Control header
        pMyIoctl->SrbIoCtrl.ControlCode = (ULONG)NVME_PASS_THROUGH_SRB_IO_CODE;
        memcpy(pMyIoctl->SrbIoCtrl.Signature, NVME_SIG_STR, sizeof(NVME_SIG_STR));
        pMyIoctl->SrbIoCtrl.HeaderLength = (ULONG)sizeof(SRB_IO_CONTROL);
        pMyIoctl->SrbIoCtrl.Timeout = 30;
        pMyIoctl->SrbIoCtrl.Length = sizeof(myIoctlBuf) - sizeof(SRB_IO_CONTROL);

        // Set up the NVMe pass through IOCTL buffer
        cmdPtr = (struct nvme_command_packet*)pMyIoctl->NVMeCmd;
        cmdPtr->opcode = admin_fw_commit;
        cmdPtr->cdw10 = cdw10;

        pMyIoctl->QueueId = 0; // Admin queue
        pMyIoctl->DataBufferLen = 0;
        pMyIoctl->Direction = NVME_NO_DATA_TX;
        pMyIoctl->ReturnBufferLen = sizeof(myIoctlBuf);
        pMyIoctl->VendorSpecific[0] = (DWORD)0;
        pMyIoctl->VendorSpecific[1] = (DWORD)0;

        dev.open();
        err = dev.ioctl(IOCTL_SCSI_MINIPORT, 
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        pMyIoctl,
                        sizeof(myIoctlBuf),
                        &dw);
        dev.close();
        return err;
}

/* bs (blocksize) is used to specify the largest data chunk that flexxlib will use
 * to transfer the firmware. this api currently transmits the fw binary in its 
 * entirety so bs is currently unused and ignored by the API.
 */
int NvmeDisk::download_firmware(void *fw_ptr, const uint32_t fw_size, const int bs = 0)
{
        uint32_t cdw10;
        uint32_t cdw11;
        uint32_t dwordMask = sizeof(uint32_t) - 1;

        /* we only accept 4 byte aligned binaries */
        if (!fw_ptr || fw_size == 0 || ((fw_size & dwordMask) != 0)) {
                return -1;
        }
        cdw10 = (fw_size >> 2) - 1; // numd is zero-based
        cdw11 = 0;

        return fw_download(fw_ptr, cdw10, cdw11);
}

int NvmeDisk::commit_firmware(const uint32_t bpid, const uint32_t ca, const uint32_t fs)
{
        const uint32_t cdw10 = ((bpid & 0xff) << 31) | ((ca & 0x7) << 3) | 
                         (fs & 0x7);
        return fw_commit(cdw10);
}

