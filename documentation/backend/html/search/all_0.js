var searchData=
[
  ['_5f_5fflexxlib_5fapi_0',['__flexxlib_api',['../flexxlib_8h.html#ab676c6ece3737cfd7fb3d2413ae59480',1,'flexxlib.h']]],
  ['_5fnvme_5fpass_5fthrough_5fioctl_1',['_NVME_PASS_THROUGH_IOCTL',['../struct___n_v_m_e___p_a_s_s___t_h_r_o_u_g_h___i_o_c_t_l.html',1,'']]],
  ['_5fnvme_5fsmart_5fattributes_2',['_NVME_SMART_ATTRIBUTES',['../struct___n_v_m_e___s_m_a_r_t___a_t_t_r_i_b_u_t_e_s.html',1,'']]],
  ['_5fnvme_5fsmart_5fread_5fattributes_5fdata_3',['_NVME_SMART_READ_ATTRIBUTES_DATA',['../struct___n_v_m_e___s_m_a_r_t___r_e_a_d___a_t_t_r_i_b_u_t_e_s___d_a_t_a.html',1,'']]],
  ['_5fnvme_5fsmart_5fread_5fthresholds_5fdata_4',['_NVME_SMART_READ_THRESHOLDS_DATA',['../struct___n_v_m_e___s_m_a_r_t___r_e_a_d___t_h_r_e_s_h_o_l_d_s___d_a_t_a.html',1,'']]],
  ['_5fsector_5frange_5',['_SECTOR_RANGE',['../struct___s_e_c_t_o_r___r_a_n_g_e.html',1,'']]]
];
