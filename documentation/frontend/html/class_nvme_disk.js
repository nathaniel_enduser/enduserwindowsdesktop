var class_nvme_disk =
[
    [ "NvmeDisk", "class_nvme_disk.html#a779043d54211c329abc6abee6f5db1af", null ],
    [ "~NvmeDisk", "class_nvme_disk.html#a8782f8ca57ee128368b6af0c6bc86af5", null ],
    [ "create_namespace", "class_nvme_disk.html#a163f90427766caee5469bd6bfd1374f6", null ],
    [ "delete_namespace", "class_nvme_disk.html#a365a590e7799f286ab678b7c4aaffb80", null ],
    [ "feature12", "class_nvme_disk.html#a7cb4ffbbce190b81f701b6f07f481cf4", null ],
    [ "get_feature", "class_nvme_disk.html#aa1fec9b17aaee22b5d38dce02ace993a", null ],
    [ "get_nsid", "class_nvme_disk.html#ac4461844ff5164631856021bd3ea9e6a", null ],
    [ "identify", "class_nvme_disk.html#ab9ac2dc1e5bc9d381244d32a4fe6c9e3", null ],
    [ "identify", "class_nvme_disk.html#a896c205f9633ebc1248be359fa1b31d6", null ],
    [ "identify", "class_nvme_disk.html#ab3dad940009c3e0924a27eacba975799", null ],
    [ "log_page13", "class_nvme_disk.html#a3e3e6622a41485b30cb2c69fbc447186", null ],
    [ "logpage", "class_nvme_disk.html#a7ad270ead7606db86464d9c820935269", null ],
    [ "logpage", "class_nvme_disk.html#a45ad0ea357d625e6c82ec629f497da77", null ],
    [ "logpage", "class_nvme_disk.html#a5b2abd84b8b7e888d935c7332f1d858e", null ],
    [ "logpage", "class_nvme_disk.html#a464f6210627f8e37570dbb1f23fc933b", null ],
    [ "namespace_management", "class_nvme_disk.html#ac367f3d8247c94c1e2d90a778578adc6", null ],
    [ "set_feature", "class_nvme_disk.html#a8f4d3bd238753826a2b88935b0ade234", null ]
];