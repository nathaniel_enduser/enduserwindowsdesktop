var classprotzone_dialog =
[
    [ "protzoneDialog", "classprotzone_dialog.html#a09bc56b3f203a1d0523ebb0b488cdaa0", null ],
    [ "~protzoneDialog", "classprotzone_dialog.html#a41815b25933595af0a614291bc4b5ae5", null ],
    [ "enadiscombobox", "classprotzone_dialog.html#a2c2bfd8bd020f91ed95e85f0cacbbd7c", null ],
    [ "enadiscomboboxns", "classprotzone_dialog.html#a5328bcbb5d3ed2b6ef596f2ff0552213", null ],
    [ "enadispbutton", "classprotzone_dialog.html#a91ba4bfc4f4052cb95238618cddd30a2", null ],
    [ "enadispbutton2", "classprotzone_dialog.html#a1d0fb894d64e5be65d9fa2fc297e88c3", null ],
    [ "eventsignal", "classprotzone_dialog.html#a4bcdfd398ddba3c64058d43038f862b7", null ],
    [ "savedata", "classprotzone_dialog.html#a8bf8e95a339cf22953edef0efafb078f", null ],
    [ "savenamespace", "classprotzone_dialog.html#a7d719e9d2a194211eb061a1403130c85", null ],
    [ "savephxy", "classprotzone_dialog.html#a9f0389f1b33ce7401a38913cb40da23d", null ],
    [ "setcomboitem", "classprotzone_dialog.html#aaf0b024b650a6a4ee0f92af85b292e63", null ],
    [ "setcombotext", "classprotzone_dialog.html#af48eda58d7189bdecc9904b16c4d821d", null ],
    [ "setcombotextns", "classprotzone_dialog.html#a94b257c3aad3327e7d9de378e91e600c", null ],
    [ "setfocus", "classprotzone_dialog.html#ab775d939f2c49c7c22ff7af4dce66bd6", null ],
    [ "updateprogress", "classprotzone_dialog.html#aa100377c5fcd6a3d9cb7ff50390f0f7e", null ],
    [ "progressval", "classprotzone_dialog.html#ad34a15f2c4b70f64cd60ba4f912a377f", null ],
    [ "protitemns", "classprotzone_dialog.html#ab6acafa4a822062c573b823ef82a7083", null ],
    [ "protitemxphy", "classprotzone_dialog.html#a55cd95be06c460c3b1cb7848ac0a3b4b", null ],
    [ "timer", "classprotzone_dialog.html#a6d8d7ab2851a1316baf643f2d1784b3b", null ]
];