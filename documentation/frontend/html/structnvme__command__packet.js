var structnvme__command__packet =
[
    [ "cdw10", "structnvme__command__packet.html#a15f8dd3152b40e7aeb1a6c2f131661c5", null ],
    [ "cdw11", "structnvme__command__packet.html#aa41af0440ce8192ef5ff546af07384df", null ],
    [ "cdw12", "structnvme__command__packet.html#af638b2ff468db305c763ff4ac77cb2f0", null ],
    [ "cdw13", "structnvme__command__packet.html#aa754c15904c31872a59310030aa591ea", null ],
    [ "cdw14", "structnvme__command__packet.html#a4db8c043dadf78e51c1c68079dbc837a", null ],
    [ "cdw15", "structnvme__command__packet.html#a78eeb661ff19dfcfcfd22928f3b769e1", null ],
    [ "cdw2", "structnvme__command__packet.html#a503da815c63fdc472555c48cc14b6125", null ],
    [ "cid", "structnvme__command__packet.html#a1a6b6ec7d743e90207eba980e62c00e2", null ],
    [ "flags", "structnvme__command__packet.html#a14d4a136f169c32371c67344272a75b5", null ],
    [ "metadata", "structnvme__command__packet.html#a3c0ec319832b48f80f6727f8d0bbdad4", null ],
    [ "nsid", "structnvme__command__packet.html#a40be0d39c0eacb7c28a3143dfaaa8eed", null ],
    [ "opcode", "structnvme__command__packet.html#ab1c5da53e7a3b53e87cc06a1e0da3f6a", null ],
    [ "prp1", "structnvme__command__packet.html#a70a733a504df9f853df1551f13c09674", null ],
    [ "prp2", "structnvme__command__packet.html#a6da187a970539d37a032e597e3ce88ed", null ]
];