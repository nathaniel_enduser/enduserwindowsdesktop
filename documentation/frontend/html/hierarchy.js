var hierarchy =
[
    [ "conf_opt", "structconf__opt.html", null ],
    [ "config_data", "structconfig__data.html", null ],
    [ "create_ns_template_struct", "structcreate__ns__template__struct.html", null ],
    [ "Device", "struct_device.html", null ],
    [ "identify_controller_struct", "structidentify__controller__struct.html", null ],
    [ "identify_namespace_struct", "structidentify__namespace__struct.html", null ],
    [ "lba_formatx_struct", "structlba__formatx__struct.html", null ],
    [ "lockfilestruct", "structlockfilestruct.html", null ],
    [ "lockfolderstruct", "structlockfolderstruct.html", null ],
    [ "log_err_info_struct", "structlog__err__info__struct.html", null ],
    [ "log_fw_slot_info_struct", "structlog__fw__slot__info__struct.html", null ],
    [ "log_smart_info_struct", "structlog__smart__info__struct.html", null ],
    [ "Namespace", "struct_namespace.html", null ],
    [ "nvme_command_packet", "structnvme__command__packet.html", null ],
    [ "NvmeDevice", "class_nvme_device.html", [
      [ "NvmeDisk", "class_nvme_disk.html", null ]
    ] ],
    [ "optstruct", "structoptstruct.html", null ],
    [ "polstruct", "structpolstruct.html", null ],
    [ "power_state_desciptor", "structpower__state__desciptor.html", null ],
    [ "QDialog", null, [
      [ "CReportDialog", "class_c_report_dialog.html", null ],
      [ "encdecDialog", "classencdec_dialog.html", null ],
      [ "genconfigDialog", "classgenconfig_dialog.html", null ],
      [ "lockunlockDialog", "classlockunlock_dialog.html", null ],
      [ "passwordDialog", "classpassword_dialog.html", null ],
      [ "policyDialog", "classpolicy_dialog.html", null ],
      [ "ProgressDialog", "class_progress_dialog.html", null ],
      [ "protzoneDialog", "classprotzone_dialog.html", null ],
      [ "secureeraseDialog", "classsecureerase_dialog.html", null ],
      [ "signupdateDialog", "classsignupdate_dialog.html", null ],
      [ "verifyfwDialog", "classverifyfw_dialog.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QObject", null, [
      [ "CalculationWorker", "class_calculation_worker.html", null ]
    ] ],
    [ "QProxyStyle", null, [
      [ "DarkStyle", "class_dark_style.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "FramelessWindow", "class_frameless_window.html", null ],
      [ "WindowDragger", "class_window_dragger.html", null ]
    ] ],
    [ "RawDevice", "class_raw_device.html", [
      [ "_Device", "class___device.html", null ]
    ] ],
    [ "temp_sensor_data_struct", "structtemp__sensor__data__struct.html", null ],
    [ "Ui_encdecDialog", "class_ui__encdec_dialog.html", [
      [ "Ui::encdecDialog", "class_ui_1_1encdec_dialog.html", null ]
    ] ],
    [ "Ui_FramelessWindow", "class_ui___frameless_window.html", [
      [ "Ui::FramelessWindow", "class_ui_1_1_frameless_window.html", null ]
    ] ],
    [ "Ui_genconfigDialog", "class_ui__genconfig_dialog.html", [
      [ "Ui::genconfigDialog", "class_ui_1_1genconfig_dialog.html", null ]
    ] ],
    [ "Ui_lockunlockDialog", "class_ui__lockunlock_dialog.html", [
      [ "Ui::lockunlockDialog", "class_ui_1_1lockunlock_dialog.html", null ]
    ] ],
    [ "Ui_MainWindow", "class_ui___main_window.html", [
      [ "Ui::MainWindow", "class_ui_1_1_main_window.html", null ]
    ] ],
    [ "Ui_passwordDialog", "class_ui__password_dialog.html", [
      [ "Ui::passwordDialog", "class_ui_1_1password_dialog.html", null ]
    ] ],
    [ "Ui_policyDialog", "class_ui__policy_dialog.html", [
      [ "Ui::policyDialog", "class_ui_1_1policy_dialog.html", null ]
    ] ],
    [ "Ui_protzoneDialog", "class_ui__protzone_dialog.html", [
      [ "Ui::protzoneDialog", "class_ui_1_1protzone_dialog.html", null ]
    ] ],
    [ "Ui_secureeraseDialog", "class_ui__secureerase_dialog.html", [
      [ "Ui::secureeraseDialog", "class_ui_1_1secureerase_dialog.html", null ]
    ] ],
    [ "Ui_signupdateDialog", "class_ui__signupdate_dialog.html", [
      [ "Ui::signupdateDialog", "class_ui_1_1signupdate_dialog.html", null ]
    ] ]
];