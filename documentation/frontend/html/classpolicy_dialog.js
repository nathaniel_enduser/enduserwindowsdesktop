var classpolicy_dialog =
[
    [ "policyDialog", "classpolicy_dialog.html#ad179f2d20cd34d78365e74c2b006ba04", null ],
    [ "~policyDialog", "classpolicy_dialog.html#ae09946a56dbc47d5b7a7d72bc9f27753", null ],
    [ "enadischeckbox", "classpolicy_dialog.html#af99a29c5e98c716c8901a06ab25691c2", null ],
    [ "enadiscombobox", "classpolicy_dialog.html#a202f4946bb32de00327c82f006d64d52", null ],
    [ "enadiscomboboxns", "classpolicy_dialog.html#ac5b997ac50c2e9ca565eb2e6216660f7", null ],
    [ "enadispbutton", "classpolicy_dialog.html#ab10e28a049a26666744b341205214a95", null ],
    [ "enadispbuttonns", "classpolicy_dialog.html#a98f3df26effe8f1c1e69e43ff2f1290f", null ],
    [ "eventsignal", "classpolicy_dialog.html#aec43a4fb9f1fa0ac8dd9afb9d776fbbc", null ],
    [ "savedata", "classpolicy_dialog.html#ab0c4f10196eed7056be9dc779cb8ea6c", null ],
    [ "savenamespace", "classpolicy_dialog.html#a6dfd8fcabd0bc5c320c5c464c3cd1900", null ],
    [ "savephxy", "classpolicy_dialog.html#af7f55d5f72ec44e9a1cb06afcd0d878e", null ],
    [ "setcomboitem", "classpolicy_dialog.html#ac9bc4118fde6b0ecfa86e28c59057c28", null ],
    [ "setcombotext", "classpolicy_dialog.html#ad767d2481761a303a14b16b4509892a3", null ],
    [ "setcombotextns", "classpolicy_dialog.html#af880dee44197ab2a32085c771a576bd9", null ],
    [ "setfocus", "classpolicy_dialog.html#a8c8c994f830f8828c0c7e9bc98f91760", null ],
    [ "updateprogress", "classpolicy_dialog.html#a31fa47c7c9898d456bbbfc5a6adfdf3f", null ],
    [ "policyitemns", "classpolicy_dialog.html#aa509247849d5ce796b3c8253dcd49cee", null ],
    [ "policyitemxphy", "classpolicy_dialog.html#a017e8a6ca8cfc9e2a668985b84968fe2", null ],
    [ "progressval", "classpolicy_dialog.html#a5d3e126a697c23b8b172a05ba8b55783", null ],
    [ "timer", "classpolicy_dialog.html#aec6a095ce02bef155452915619716754", null ]
];