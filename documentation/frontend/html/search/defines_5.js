var searchData=
[
  ['filelock_5fcount_1463',['FILELOCK_COUNT',['../enduserdesktop_8h.html#afb2e90fa013c47734123678f0fbbfde2',1,'enduserdesktop.h']]],
  ['flbas_5fmdata_5fxfer_1464',['FLBAS_MDATA_XFER',['../flexx-nvme_8h.html#a62b1aa0353db4697d72207890c52bef3',1,'flexx-nvme.h']]],
  ['flbas_5fsupported_5fform_5fmask_1465',['FLBAS_SUPPORTED_FORM_MASK',['../flexx-nvme_8h.html#af23ef06a4e22c7c6de38ef27dae0ae37',1,'flexx-nvme.h']]],
  ['flexx_5ffailed_1466',['FLEXX_FAILED',['../flexxlib_8h.html#ab0d4b349032a20966bbf8f321201ff89',1,'flexxlib.h']]],
  ['flexx_5fpassed_1467',['FLEXX_PASSED',['../flexxlib_8h.html#a820d98b2e703f5a3bb48545270aca07c',1,'flexxlib.h']]],
  ['flexxon_5fssvid_1468',['FLEXXON_SSVID',['../flexxlib_8h.html#afc4d8926e9835e277a77c9c894a0c20d',1,'flexxlib.h']]],
  ['flexxon_5fvid_1469',['FLEXXON_VID',['../flexxlib_8h.html#a98e20afd8842d1a14fd190a5dab0defc',1,'flexxlib.h']]],
  ['fna_5fcrypt_5ferase_1470',['FNA_CRYPT_ERASE',['../flexx-nvme_8h.html#a47577ebbaaed7dc15740d6a3b54cefcc',1,'flexx-nvme.h']]],
  ['fna_5ferase_1471',['FNA_ERASE',['../flexx-nvme_8h.html#ab72e9e4a880eb17ebc97c45af3c97785',1,'flexx-nvme.h']]],
  ['fna_5fformat_1472',['FNA_FORMAT',['../flexx-nvme_8h.html#a63803ae328a66b5a38d3823565b015c1',1,'flexx-nvme.h']]],
  ['folderlock_5fcount_1473',['FOLDERLOCK_COUNT',['../enduserdesktop_8h.html#aaba355b4ab54646638964d7175355025',1,'enduserdesktop.h']]],
  ['fpi_5fprogress_5findicator_5fmask_1474',['FPI_PROGRESS_INDICATOR_MASK',['../flexx-nvme_8h.html#a210d2be718abc1de88d0b14d4172b443',1,'flexx-nvme.h']]],
  ['fpi_5fprogress_5fsupported_1475',['FPI_PROGRESS_SUPPORTED',['../flexx-nvme_8h.html#a9f25889480106dcb96d276894dc04578',1,'flexx-nvme.h']]],
  ['friendly_1476',['FRIENDLY',['../flexxlib_8h.html#a7c381815261a12774447808c9da014fe',1,'flexxlib.h']]],
  ['fuses_5fcomp_5fwrite_5ffused_1477',['FUSES_COMP_WRITE_FUSED',['../flexx-nvme_8h.html#ac4f6a83d7e0bc2806b05d6442ed4086d',1,'flexx-nvme.h']]]
];
