var searchData=
[
  ['cns_5fidcntrl_1388',['cns_idcntrl',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7abb5b854bb0ebfab3716f0d361f54d65a',1,'flexx-nvme.h']]],
  ['cns_5fidns_1389',['cns_idns',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a8c0df61259cfa9a8fa910d54d9635a8e',1,'flexx-nvme.h']]],
  ['cns_5fmgmt_5fattached_5fcntrl_5flist_1390',['cns_mgmt_attached_cntrl_list',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a3f0d56fdb44f46c346ddb46b1f930765',1,'flexx-nvme.h']]],
  ['cns_5fmgmt_5fcntrl_5flist_1391',['cns_mgmt_cntrl_list',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a6d0d80c1f502c66168aae296d57759c5',1,'flexx-nvme.h']]],
  ['cns_5fmgmt_5fidns_1392',['cns_mgmt_idns',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7af19e95225a47923dba53818ce5ae9f88',1,'flexx-nvme.h']]],
  ['cns_5fmgmt_5fnsid_5flist_1393',['cns_mgmt_nsid_list',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a0974596875e7790adba726f9d8049b0d',1,'flexx-nvme.h']]],
  ['cns_5fmgmt_5fprimary_5fcntrl_1394',['cns_mgmt_primary_cntrl',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a5c2e38acfa9898c9ad25fccb7299c93b',1,'flexx-nvme.h']]],
  ['cns_5fmgmt_5fsecondary_5fcntrl_1395',['cns_mgmt_secondary_cntrl',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a3cd9f7f313474041596084a960b41b62',1,'flexx-nvme.h']]],
  ['cns_5fnsid_5fdesc_1396',['cns_nsid_desc',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a5d1fbd151b999372e0fe642cf3477853',1,'flexx-nvme.h']]],
  ['cns_5fnsid_5flist_1397',['cns_nsid_list',['../flexx-nvme_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a847d6a69d8f8b0b868c64840ad68f140',1,'flexx-nvme.h']]]
];
