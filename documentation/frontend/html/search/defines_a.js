var searchData=
[
  ['namespace_5fcount_1503',['NAMESPACE_COUNT',['../enduserdesktop_8h.html#a2fd1440048958de85fd84b2b15996d63',1,'enduserdesktop.h']]],
  ['namespace_5fname_1504',['NAMESPACE_NAME',['../enduserdesktop_8h.html#a334f501017e0bf21f68003cd654d943d',1,'enduserdesktop.h']]],
  ['nmic_5fshared_1505',['NMIC_SHARED',['../flexx-nvme_8h.html#a7fb09616ab866c0eab29d3eebc4792dc',1,'flexx-nvme.h']]],
  ['nop_5fstate_5fmode_1506',['NOP_STATE_MODE',['../flexx-nvme_8h.html#a1233c48095cb5f85203c9d775ec0e2ca',1,'flexx-nvme.h']]],
  ['nops_5fmxps_5fmxps_1507',['NOPS_MXPS_MXPS',['../flexx-nvme_8h.html#aef2c01beebb76f8ee50628f782389af7',1,'flexx-nvme.h']]],
  ['nops_5fmxps_5fnops_1508',['NOPS_MXPS_NOPS',['../flexx-nvme_8h.html#a8ae0d19979fb902698e04023f010014a',1,'flexx-nvme.h']]],
  ['nsfeat_5fdealloc_1509',['NSFEAT_DEALLOC',['../flexx-nvme_8h.html#aee0510dc96a84ee516d01bd5482b96c9',1,'flexx-nvme.h']]],
  ['nsfeat_5fnzero_5ffields_1510',['NSFEAT_NZERO_FIELDS',['../flexx-nvme_8h.html#a241a7b4210be3142e4ba6b7dd9dfedcc',1,'flexx-nvme.h']]],
  ['nsfeat_5fopts_1511',['NSFEAT_OPTS',['../flexx-nvme_8h.html#a27e00bec25eafa3c3aeb93644aa9d256',1,'flexx-nvme.h']]],
  ['nsfeat_5fthin_5fprov_1512',['NSFEAT_THIN_PROV',['../flexx-nvme_8h.html#a058759262ba4468401bd26899b5e52dc',1,'flexx-nvme.h']]]
];
