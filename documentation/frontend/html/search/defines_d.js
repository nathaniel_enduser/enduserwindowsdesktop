var searchData=
[
  ['report_5fcontents_1534',['REPORT_CONTENTS',['../enduserdesktop_8h.html#a6dd72eda0a41709952d9b5d0c0e8f9c1',1,'enduserdesktop.h']]],
  ['report_5fheader_1535',['REPORT_HEADER',['../enduserdesktop_8h.html#a0fdb42c9fc03214e7433b8695c64c277',1,'enduserdesktop.h']]],
  ['rescap_5fexaccess_5fall_5fregistrants_1536',['RESCAP_EXACCESS_ALL_REGISTRANTS',['../flexx-nvme_8h.html#a45f4b3577f3c6e63b7b6dcc625117fd1',1,'flexx-nvme.h']]],
  ['rescap_5fexaccess_5fregistrants_1537',['RESCAP_EXACCESS_REGISTRANTS',['../flexx-nvme_8h.html#a511d069a8e0d82d0d45099827d64cfc9',1,'flexx-nvme.h']]],
  ['rescap_5fexaccess_5freservation_1538',['RESCAP_EXACCESS_RESERVATION',['../flexx-nvme_8h.html#a6b3792a3b5bb84f83a0d4a820db5e19e',1,'flexx-nvme.h']]],
  ['rescap_5fignore_5fexisting_5fkey_1539',['RESCAP_IGNORE_EXISTING_KEY',['../flexx-nvme_8h.html#a966c59591ad9e006fca9bc89127d0b14',1,'flexx-nvme.h']]],
  ['rescap_5fpersist_5fpower_5floss_1540',['RESCAP_PERSIST_POWER_LOSS',['../flexx-nvme_8h.html#a02ffd9a5e8fd12094f88d88fb188f0a0',1,'flexx-nvme.h']]],
  ['rescap_5fwrex_5fall_5fregistrants_1541',['RESCAP_WREX_ALL_REGISTRANTS',['../flexx-nvme_8h.html#a4e7149a943010afa9439d1d25b2512f3',1,'flexx-nvme.h']]],
  ['rescap_5fwrex_5fregistrants_1542',['RESCAP_WREX_REGISTRANTS',['../flexx-nvme_8h.html#a7f2d6efe27ca77e6bbca6a70ce3d0ea3',1,'flexx-nvme.h']]],
  ['rescap_5fwrex_5freservation_1543',['RESCAP_WREX_RESERVATION',['../flexx-nvme_8h.html#a784598fa747e2346cb07e476880ed8c3',1,'flexx-nvme.h']]],
  ['rp_5fbest_5fperformance_1544',['RP_BEST_PERFORMANCE',['../flexx-nvme_8h.html#ab38cd9c2e18bd3141db3f45ba3d5bbaf',1,'flexx-nvme.h']]],
  ['rp_5fbetter_5fperformance_1545',['RP_BETTER_PERFORMANCE',['../flexx-nvme_8h.html#af219c1c327c3d73165abdde89d263450',1,'flexx-nvme.h']]],
  ['rp_5fdegraded_5fperformance_1546',['RP_DEGRADED_PERFORMANCE',['../flexx-nvme_8h.html#a2214fb692f0c8addc413555bc109665e',1,'flexx-nvme.h']]],
  ['rp_5fgood_5fperformance_1547',['RP_GOOD_PERFORMANCE',['../flexx-nvme_8h.html#a41681f4b8d7c81e066fef2c01a277238',1,'flexx-nvme.h']]]
];
