var searchData=
[
  ['major_5fver_5f1_1493',['MAJOR_VER_1',['../flexx-nvme_8h.html#a5e0e57c752ed2ea2c5c23c3deed29ea5',1,'flexx-nvme.h']]],
  ['max_5ffilename_5flen_1494',['MAX_FILENAME_LEN',['../flexxlib_8h.html#acc9794f655f61a3246ba3fd5c9c67e14',1,'flexxlib.h']]],
  ['max_5fns_5fper_5fxphy_1495',['MAX_NS_PER_XPHY',['../flexxlib_8h.html#a8d7738f9f013edfd8484535f4551f180',1,'flexxlib.h']]],
  ['max_5fxphy_1496',['MAX_XPHY',['../flexxlib_8h.html#a98a8204d1e511fa393f1fe2a3359538f',1,'flexxlib.h']]],
  ['mc_5fmdata_5fextdata_1497',['MC_MDATA_EXTDATA',['../flexx-nvme_8h.html#a02c949c005d892137fb1e6cf692d7e65',1,'flexx-nvme.h']]],
  ['mc_5fmdata_5fseparate_1498',['MC_MDATA_SEPARATE',['../flexx-nvme_8h.html#a74cb93f7b72fb0a6156b305e67e19eea',1,'flexx-nvme.h']]],
  ['minor_5fver_5f0_1499',['MINOR_VER_0',['../flexx-nvme_8h.html#a4fa1f8307c14221221f7a73459d8dba6',1,'flexx-nvme.h']]],
  ['minor_5fver_5f1_1500',['MINOR_VER_1',['../flexx-nvme_8h.html#a3faa3aa7b24394b81fa5210adb0a2b31',1,'flexx-nvme.h']]],
  ['minor_5fver_5f2_1501',['MINOR_VER_2',['../flexx-nvme_8h.html#a845547ba652d04232cf2e8278e361127',1,'flexx-nvme.h']]],
  ['minor_5fver_5f3_1502',['MINOR_VER_3',['../flexx-nvme_8h.html#a71ea6aaeb969447f851443246900a976',1,'flexx-nvme.h']]]
];
