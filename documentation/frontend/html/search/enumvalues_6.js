var searchData=
[
  ['lba_5frange_5ftype_5ffeat_1405',['lba_range_type_feat',['../flexx-nvme_8h.html#abc6126af1d45847bc59afa0aa3216b04ad5880b336716359e4d36afc87d0872f8',1,'flexx-nvme.h']]],
  ['lpid_5fchanged_5fns_5flist_1406',['lpid_changed_ns_list',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9eae289ab4791f98cfcd2573a7e8f90d559',1,'flexx-nvme.h']]],
  ['lpid_5fcmds_5fsupported_5fand_5feffects_1407',['lpid_cmds_supported_and_effects',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9ead9d02ed0aa8b873b0456b459d00d1521',1,'flexx-nvme.h']]],
  ['lpid_5fdevice_5fself_5ftest_1408',['lpid_device_self_test',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9eaad660c91362e2cb136aadb67a3c8f7f6',1,'flexx-nvme.h']]],
  ['lpid_5fdiscover_1409',['lpid_discover',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9ea712241eb02623c2eece8c563a3e2d5c2',1,'flexx-nvme.h']]],
  ['lpid_5ferror_5finfo_1410',['lpid_error_info',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9ea51392dcc7aadfb09fe357d4718db74cb',1,'flexx-nvme.h']]],
  ['lpid_5ffirmware_5fslot_5finfo_1411',['lpid_firmware_slot_info',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9ea595aa8f8a9dc2dbded7cc74b629e670e',1,'flexx-nvme.h']]],
  ['lpid_5fresv_5fnotification_1412',['lpid_resv_notification',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9eac02c2829ab0ff291542d7485bb11b6f0',1,'flexx-nvme.h']]],
  ['lpid_5fsanitize_5fstatus_1413',['lpid_sanitize_status',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9eaf9b4d095c514e4efaad48c7299f7a691',1,'flexx-nvme.h']]],
  ['lpid_5fsmart_5fhealth_5finfo_1414',['lpid_smart_health_info',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9ea5a2c14a03aa5ae58e20da7b079b89d5c',1,'flexx-nvme.h']]],
  ['lpid_5ftelemetry_5fcontroller_1415',['lpid_telemetry_controller',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9eac5d34ae69c8d568dfd869b49c1e46ca5',1,'flexx-nvme.h']]],
  ['lpid_5ftelemetry_5fhost_1416',['lpid_telemetry_host',['../flexx-nvme_8h.html#a61dadd085c1777f559549e05962b2c9ea471df7efe108edeaee39e8da2a113b4f',1,'flexx-nvme.h']]]
];
