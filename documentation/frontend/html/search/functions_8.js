var searchData=
[
  ['identify_885',['identify',['../class_nvme_disk.html#ab9ac2dc1e5bc9d381244d32a4fe6c9e3',1,'NvmeDisk::identify(const uint32_t nsid, const uint32_t cns, void *buffer, const uint32_t sz) override'],['../class_nvme_disk.html#a896c205f9633ebc1248be359fa1b31d6',1,'NvmeDisk::identify(struct identify_controller_struct &amp;idctrl)'],['../class_nvme_disk.html#ab3dad940009c3e0924a27eacba975799',1,'NvmeDisk::identify(struct identify_namespace_struct &amp;idns)']]],
  ['initializebackend_886',['initializebackend',['../class_main_window.html#a7c9e94aa11e4dcd244e3c4c37b212de6',1,'MainWindow']]],
  ['ioctl_887',['ioctl',['../class___device.html#a4ac49ec66e5e4b079d84da787a63227c',1,'_Device']]],
  ['is_5fsame_5fxphy_888',['is_same_xphy',['../flexxlib_8h.html#a9516d6c61162d11db83c4de7a1a0c8a5',1,'flexxlib.h']]]
];
