var searchData=
[
  ['data1_1007',['Data1',['../struct_namespace.html#aa5bac0460caa76f43f8f2d6e828aee91',1,'Namespace']]],
  ['data2_1008',['Data2',['../struct_namespace.html#a7d294799d3b96c870727f790c43e9a6d',1,'Namespace']]],
  ['data3_1009',['Data3',['../struct_namespace.html#a6d0d6ff2d62c3e384c4e0c4df326198a',1,'Namespace']]],
  ['data_5fintegrity_5ferrors_1010',['data_integrity_errors',['../structlog__smart__info__struct.html#aaf7f2659023bb09d14751e4798c50847',1,'log_smart_info_struct']]],
  ['data_5funits_5fread_1011',['data_units_read',['../structlog__smart__info__struct.html#a436811ec6ce852745fa69157cdbbdc04',1,'log_smart_info_struct']]],
  ['data_5funits_5fwritten_1012',['data_units_written',['../structlog__smart__info__struct.html#ac9bfab182da3467d08a1f3e7c4c78f25',1,'log_smart_info_struct']]],
  ['dlfeat_1013',['dlfeat',['../structidentify__namespace__struct.html#a79dcdd299b8d541660cdbb4dd89c1677',1,'identify_namespace_struct']]],
  ['dpc_1014',['dpc',['../structidentify__namespace__struct.html#ae3081bd25dcd5c37df461545b60cd8e9',1,'identify_namespace_struct']]],
  ['dps_1015',['dps',['../structidentify__namespace__struct.html#af8236b79342f76b2a859643a737ffa2d',1,'identify_namespace_struct::dps()'],['../structcreate__ns__template__struct.html#a71cf5944bd36a11c722f67d6f84b11b1',1,'create_ns_template_struct::dps()']]],
  ['dsto_1016',['dsto',['../structidentify__controller__struct.html#a971f471c0a6f6a0c6c23d33ec976f3d9',1,'identify_controller_struct']]]
];
