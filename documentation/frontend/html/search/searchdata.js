var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwx~",
  1: "_cdefgilmnoprstuvw",
  2: "u",
  3: "cdefglmpsuvw",
  4: "_abcdefgilmnoprsuvw~",
  5: "abcdefghiklmnoprstuvwx",
  6: "cdflnop",
  7: "acehiklnprstvw",
  8: "g",
  9: "_abcdfhilmnoprstuvx"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enumvalues",
  8: "related",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerator",
  8: "Friends",
  9: "Macros"
};

