var searchData=
[
  ['oacs_502',['oacs',['../structidentify__controller__struct.html#aff38b89b021bf8bed4d000640d537018',1,'identify_controller_struct']]],
  ['oacs_5fdevice_5fself_5ftest_503',['OACS_DEVICE_SELF_TEST',['../flexx-nvme_8h.html#afd332a484a1a3ba9762bea5695ad6c81',1,'flexx-nvme.h']]],
  ['oacs_5fdirectives_504',['OACS_DIRECTIVES',['../flexx-nvme_8h.html#a01202115855608b4827ed84c6e374727',1,'flexx-nvme.h']]],
  ['oacs_5fdoorbell_5fbuffer_5fconfig_505',['OACS_DOORBELL_BUFFER_CONFIG',['../flexx-nvme_8h.html#a5a56303b7b8a4cd84b1495b461bad232',1,'flexx-nvme.h']]],
  ['oacs_5fformat_5fnvm_506',['OACS_FORMAT_NVM',['../flexx-nvme_8h.html#a00381a700222bd3015302c6f066ef628',1,'flexx-nvme.h']]],
  ['oacs_5ffw_5fcommit_5fdownload_507',['OACS_FW_COMMIT_DOWNLOAD',['../flexx-nvme_8h.html#a7dc15cf2ddbb572012825520cc2333b1',1,'flexx-nvme.h']]],
  ['oacs_5fns_5fmanagement_508',['OACS_NS_MANAGEMENT',['../flexx-nvme_8h.html#a1166d17496bc91cd22239e5fb5723e2f',1,'flexx-nvme.h']]],
  ['oacs_5fnvmemi_5fsupported_509',['OACS_NVMEMI_SUPPORTED',['../flexx-nvme_8h.html#a29860bb06e96795658a564c0c27183d3',1,'flexx-nvme.h']]],
  ['oacs_5fsecurity_5fsend_5frecv_510',['OACS_SECURITY_SEND_RECV',['../flexx-nvme_8h.html#a9525e6fe313051521c946d9b98ad53c9',1,'flexx-nvme.h']]],
  ['oacs_5fvirt_5fmanagement_511',['OACS_VIRT_MANAGEMENT',['../flexx-nvme_8h.html#addbeb5793426604061f04af1c693be90',1,'flexx-nvme.h']]],
  ['oaes_512',['oaes',['../structidentify__controller__struct.html#af8385ae2569bccbfe8be3037e69bb872',1,'identify_controller_struct']]],
  ['oaes_5ffw_5factivation_5fnotice_513',['OAES_FW_ACTIVATION_NOTICE',['../flexx-nvme_8h.html#ac1a4269c9f2b980bc242fef33f25de03',1,'flexx-nvme.h']]],
  ['oaes_5fns_5fattribute_5fnotice_514',['OAES_NS_ATTRIBUTE_NOTICE',['../flexx-nvme_8h.html#a4141e2d2d48e66d15d4e5642c36335f1',1,'flexx-nvme.h']]],
  ['oncs_515',['oncs',['../structidentify__controller__struct.html#ae5b5fed26357f0ad0efd5779230415b3',1,'identify_controller_struct']]],
  ['oncs_5fcompare_516',['ONCS_COMPARE',['../flexx-nvme_8h.html#a3f44fd878f523eabcd7f96df98ef6e1b',1,'flexx-nvme.h']]],
  ['oncs_5fdataset_5fmgmt_517',['ONCS_DATASET_MGMT',['../flexx-nvme_8h.html#acf0d2b6ae93b9143b9290cb1adb9db6d',1,'flexx-nvme.h']]],
  ['oncs_5freservations_518',['ONCS_RESERVATIONS',['../flexx-nvme_8h.html#a2c4a8479c27749a05a4ba15a86be4479',1,'flexx-nvme.h']]],
  ['oncs_5fsave_5ffield_5fnon_5fzero_519',['ONCS_SAVE_FIELD_NON_ZERO',['../flexx-nvme_8h.html#afef57a860de6d158b6ceadc705cd38c7',1,'flexx-nvme.h']]],
  ['oncs_5ftimestamp_520',['ONCS_TIMESTAMP',['../flexx-nvme_8h.html#a48d99efdbd8dd784ed2ff4248113bf5f',1,'flexx-nvme.h']]],
  ['oncs_5fwr_5funcorr_521',['ONCS_WR_UNCORR',['../flexx-nvme_8h.html#a3a7a0ef138e140942a7b87d9781fb867',1,'flexx-nvme.h']]],
  ['oncs_5fwr_5fzeroes_522',['ONCS_WR_ZEROES',['../flexx-nvme_8h.html#ae9124cecac537116d1a9f4bc5f74c448',1,'flexx-nvme.h']]],
  ['opcode_523',['opcode',['../structnvme__command__packet.html#ab1c5da53e7a3b53e87cc06a1e0da3f6a',1,'nvme_command_packet']]],
  ['open_524',['open',['../class___device.html#a6d0348315eb626a91cac4b0cea82b9bc',1,'_Device']]],
  ['opt_525',['OPT',['../enduserdesktop_8h.html#a27b4b50722f28f0c19c0c94660ec95ff',1,'enduserdesktop.h']]],
  ['optstruct_526',['optstruct',['../structoptstruct.html',1,'']]]
];
