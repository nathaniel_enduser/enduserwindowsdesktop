var structpower__state__desciptor =
[
    [ "actp", "structpower__state__desciptor.html#aa9e79d5cc86c82b2b98ef95fd035f1ce", null ],
    [ "aps_apw", "structpower__state__desciptor.html#aacfc38e9b52589aca33e46832caad090", null ],
    [ "enlat", "structpower__state__desciptor.html#aa3d890b3b3a786c2f07862e3a50fbba1", null ],
    [ "exlat", "structpower__state__desciptor.html#a31bcf57ee4f634b4584a780635878dfb", null ],
    [ "idlp", "structpower__state__desciptor.html#aa398466358690f98860488e4c436d32a", null ],
    [ "ips", "structpower__state__desciptor.html#a05fda3144fb7810d60df53731da1795b", null ],
    [ "mp", "structpower__state__desciptor.html#a11803d2658c9f88484874d2b112bc65d", null ],
    [ "nops_mxps", "structpower__state__desciptor.html#aa19621363c9d79356c95724aab99d3c0", null ],
    [ "reserved0", "structpower__state__desciptor.html#aae45a855c2c9828e359011f298f66f35", null ],
    [ "reserved2", "structpower__state__desciptor.html#a6ccf50dbf8418d0ee9abfb5bd0fd6571", null ],
    [ "reserverd1", "structpower__state__desciptor.html#aa96f69dbcee37b917e8f834a08edc260", null ],
    [ "rrl", "structpower__state__desciptor.html#a80abb1e6ed65ddd329962d18c86640ef", null ],
    [ "rrt", "structpower__state__desciptor.html#a4f50597fa4c4ce6fb763966a09e79708", null ],
    [ "rwl", "structpower__state__desciptor.html#aeb49386fc1932f134485385d0fcad0f8", null ],
    [ "rwt", "structpower__state__desciptor.html#a847b1018258241e085669b286d357117", null ]
];