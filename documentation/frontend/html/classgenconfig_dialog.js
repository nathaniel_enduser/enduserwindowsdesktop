var classgenconfig_dialog =
[
    [ "genconfigDialog", "classgenconfig_dialog.html#afd6b7cfb8d78a1eabf61ab284368e2fc", null ],
    [ "~genconfigDialog", "classgenconfig_dialog.html#a4eb44571ef56bd2a3fea6f5da37607d9", null ],
    [ "eventsignal", "classgenconfig_dialog.html#a9aeea1f57068f3341c07933422c21130", null ],
    [ "returnData", "classgenconfig_dialog.html#a39e0377f9cea00cd0e4f0b61f1525f15", null ],
    [ "savedata", "classgenconfig_dialog.html#a257da094d9961e18dd56bda41630b646", null ],
    [ "savelogpolicy", "classgenconfig_dialog.html#a953e51286d8e776ca2a4e924dc946a0c", null ],
    [ "setcombostate", "classgenconfig_dialog.html#aed6c380ed1d3d91b651ef9b417667cd0", null ],
    [ "setcombotext", "classgenconfig_dialog.html#a372a24769174d79eea082a2e8618b95b", null ],
    [ "setcombotextns", "classgenconfig_dialog.html#a78d7acb939efa38974b5651bd50259cc", null ],
    [ "setfocus", "classgenconfig_dialog.html#a50c31cd52150bb90961c9a501a7ed001", null ],
    [ "setgroupboxns", "classgenconfig_dialog.html#ac882cbba6620654d35dc281f798774ae", null ],
    [ "setgroupboxpol", "classgenconfig_dialog.html#acf328f3ff30b1eb8efbc11d7d3d39817", null ],
    [ "setpushbuttoncreapw", "classgenconfig_dialog.html#ac6522081079de9f3dea7e8e87a937397", null ],
    [ "setpushbuttonupdcfg", "classgenconfig_dialog.html#a51e420d8959da53b407982215f548daf", null ],
    [ "configdata", "classgenconfig_dialog.html#a320a514b687666dc7c5fdcd9f8c234a2", null ]
];