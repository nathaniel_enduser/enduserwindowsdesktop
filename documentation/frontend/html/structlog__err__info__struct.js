var structlog__err__info__struct =
[
    [ "cid", "structlog__err__info__struct.html#a00d7fec4e382ba140bbb0f201a76140d", null ],
    [ "cmd_specific_info", "structlog__err__info__struct.html#a8c6498a6ff6a64baac9416afc17b6dc3", null ],
    [ "error_count", "structlog__err__info__struct.html#a97f3181b6239851193778718a008dbd8", null ],
    [ "lba", "structlog__err__info__struct.html#afcb5044b6d5dae8fee5095dc273ff7e2", null ],
    [ "namespc", "structlog__err__info__struct.html#a5da9a1f4d7374d05ca09845dc37da45c", null ],
    [ "param_err_loc", "structlog__err__info__struct.html#a8b2f27016483a3c69553c713509ad152", null ],
    [ "resv0", "structlog__err__info__struct.html#a1d2a3c80ce266664c833efdcbbbc5a3c", null ],
    [ "resv1", "structlog__err__info__struct.html#a3bb5ac99438a34388a01b8ed99a6a0bd", null ],
    [ "sqid", "structlog__err__info__struct.html#a4c01c9f13b6d6e3bae25c0dca8b8442d", null ],
    [ "status_field", "structlog__err__info__struct.html#a7d7429ee7e5f1f7f1b110689ae92a4f6", null ],
    [ "vendor_info_available", "structlog__err__info__struct.html#a316d5fce35cb57a72ef44f5dce72d33a", null ]
];