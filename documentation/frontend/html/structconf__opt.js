var structconf__opt =
[
    [ "currnsidx", "structconf__opt.html#a03156379a7a118219b872547026dc9d9", null ],
    [ "enablefeature", "structconf__opt.html#adf35b665738bf486eb0f2b2a73de32f9", null ],
    [ "featurelock", "structconf__opt.html#aedc145f5396a9541863619e6dede9663", null ],
    [ "filefolder", "structconf__opt.html#aae1718a40a8b4bcb36cf59b3aef2be8e", null ],
    [ "lockedfile", "structconf__opt.html#a7ff87fee696c6a982717006f279f3cd5", null ],
    [ "lockedfolder", "structconf__opt.html#a0b96ff0a0ed7ce29418287ad987058b3", null ],
    [ "nspace", "structconf__opt.html#aa0e89bfc4ff295db679b74d633472026", null ],
    [ "password", "structconf__opt.html#aa99bc2089951284e824834c63b4eba6e", null ],
    [ "policies", "structconf__opt.html#a888944de1818d2eed90f68eb21df6d4b", null ],
    [ "protectnamespace", "structconf__opt.html#a42c9c48b81d00046963bc673e2e79f0f", null ]
];