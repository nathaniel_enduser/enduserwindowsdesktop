var class_ui___frameless_window =
[
    [ "retranslateUi", "class_ui___frameless_window.html#ac735ed5a8860f5b0300ee79448a4fede", null ],
    [ "setupUi", "class_ui___frameless_window.html#aee6a3f7f858eda406c20e3c8991bcfd9", null ],
    [ "closeButton", "class_ui___frameless_window.html#a65501f876326e124b16ccb6ef7b0bb8b", null ],
    [ "horizontalLayout", "class_ui___frameless_window.html#a9bdc2c79ee16dc747fea3989a30c649d", null ],
    [ "icon", "class_ui___frameless_window.html#ab55e25e85aa2312bb2bb0ba9e687bd85", null ],
    [ "maximizeButton", "class_ui___frameless_window.html#aa8d4c865d61ef52dd6a27559755ea673", null ],
    [ "minimizeButton", "class_ui___frameless_window.html#a2ec960ac32fe251a1dc79c31ccbd55d1", null ],
    [ "restoreButton", "class_ui___frameless_window.html#a519dd2ac6df42c8d6675427dbcf643a0", null ],
    [ "spacer", "class_ui___frameless_window.html#af84932fcac4c39736899d39fb4f708ba", null ],
    [ "titleText", "class_ui___frameless_window.html#a8b3d8d23de44463d01c3c44e31ce3e01", null ],
    [ "verticalLayout", "class_ui___frameless_window.html#ac54012b76ed9c8ede2fb92d280c46288", null ],
    [ "verticalLayout_2", "class_ui___frameless_window.html#a8e11af3e539a31605ca9b80b5c7d8638", null ],
    [ "verticalLayout_3", "class_ui___frameless_window.html#a24f2c6480c6545383d20b890839c50df", null ],
    [ "windowContent", "class_ui___frameless_window.html#a8e50bb734851d4912a6cbc7b1dc22027", null ],
    [ "windowFrame", "class_ui___frameless_window.html#a539b2e2408b4e102d1ff86f97546ca0a", null ],
    [ "windowTitlebar", "class_ui___frameless_window.html#a97e58bd576178396d1ab644021c51848", null ]
];