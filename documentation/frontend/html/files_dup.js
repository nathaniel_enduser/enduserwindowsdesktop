var files_dup =
[
    [ "calculationworker.h", "calculationworker_8h.html", [
      [ "CalculationWorker", "class_calculation_worker.html", "class_calculation_worker" ]
    ] ],
    [ "creportdialog.h", "creportdialog_8h.html", [
      [ "CReportDialog", "class_c_report_dialog.html", "class_c_report_dialog" ]
    ] ],
    [ "darkstyle.h", "darkstyle_8h.html", [
      [ "DarkStyle", "class_dark_style.html", "class_dark_style" ]
    ] ],
    [ "encdecdialog.h", "encdecdialog_8h.html", [
      [ "encdecDialog", "classencdec_dialog.html", "classencdec_dialog" ]
    ] ],
    [ "enduserdesktop.h", "enduserdesktop_8h.html", "enduserdesktop_8h" ],
    [ "flexx-nvme.h", "flexx-nvme_8h.html", "flexx-nvme_8h" ],
    [ "flexxlib.h", "flexxlib_8h.html", "flexxlib_8h" ],
    [ "framelesswindow.h", "framelesswindow_8h.html", [
      [ "FramelessWindow", "class_frameless_window.html", "class_frameless_window" ]
    ] ],
    [ "genconfigdialog.h", "genconfigdialog_8h.html", [
      [ "genconfigDialog", "classgenconfig_dialog.html", "classgenconfig_dialog" ]
    ] ],
    [ "lockunlockdialog.h", "lockunlockdialog_8h.html", [
      [ "lockunlockDialog", "classlockunlock_dialog.html", "classlockunlock_dialog" ]
    ] ],
    [ "mainwindow.h", "mainwindow_8h.html", [
      [ "MainWindow", "class_main_window.html", "class_main_window" ]
    ] ],
    [ "passworddialog.h", "passworddialog_8h.html", [
      [ "passwordDialog", "classpassword_dialog.html", "classpassword_dialog" ]
    ] ],
    [ "policydialog.h", "policydialog_8h.html", [
      [ "policyDialog", "classpolicy_dialog.html", "classpolicy_dialog" ]
    ] ],
    [ "progressdialog.h", "progressdialog_8h.html", [
      [ "ProgressDialog", "class_progress_dialog.html", "class_progress_dialog" ]
    ] ],
    [ "protzonedialog.h", "protzonedialog_8h.html", [
      [ "protzoneDialog", "classprotzone_dialog.html", "classprotzone_dialog" ]
    ] ],
    [ "secureerasedialog.h", "secureerasedialog_8h.html", [
      [ "secureeraseDialog", "classsecureerase_dialog.html", "classsecureerase_dialog" ]
    ] ],
    [ "signupdatedialog.h", "signupdatedialog_8h.html", [
      [ "signupdateDialog", "classsignupdate_dialog.html", "classsignupdate_dialog" ]
    ] ],
    [ "ui_encdecdialog.h", "ui__encdecdialog_8h.html", [
      [ "Ui_encdecDialog", "class_ui__encdec_dialog.html", "class_ui__encdec_dialog" ],
      [ "encdecDialog", "class_ui_1_1encdec_dialog.html", null ]
    ] ],
    [ "ui_framelesswindow.h", "ui__framelesswindow_8h.html", [
      [ "Ui_FramelessWindow", "class_ui___frameless_window.html", "class_ui___frameless_window" ],
      [ "FramelessWindow", "class_ui_1_1_frameless_window.html", null ]
    ] ],
    [ "ui_genconfigdialog.h", "ui__genconfigdialog_8h.html", [
      [ "Ui_genconfigDialog", "class_ui__genconfig_dialog.html", "class_ui__genconfig_dialog" ],
      [ "genconfigDialog", "class_ui_1_1genconfig_dialog.html", null ]
    ] ],
    [ "ui_lockunlockdialog.h", "ui__lockunlockdialog_8h.html", [
      [ "Ui_lockunlockDialog", "class_ui__lockunlock_dialog.html", "class_ui__lockunlock_dialog" ],
      [ "lockunlockDialog", "class_ui_1_1lockunlock_dialog.html", null ]
    ] ],
    [ "ui_mainwindow.h", "ui__mainwindow_8h.html", [
      [ "Ui_MainWindow", "class_ui___main_window.html", "class_ui___main_window" ],
      [ "MainWindow", "class_ui_1_1_main_window.html", null ]
    ] ],
    [ "ui_passworddialog.h", "ui__passworddialog_8h.html", [
      [ "Ui_passwordDialog", "class_ui__password_dialog.html", "class_ui__password_dialog" ],
      [ "passwordDialog", "class_ui_1_1password_dialog.html", null ]
    ] ],
    [ "ui_policydialog.h", "ui__policydialog_8h.html", [
      [ "Ui_policyDialog", "class_ui__policy_dialog.html", "class_ui__policy_dialog" ],
      [ "policyDialog", "class_ui_1_1policy_dialog.html", null ]
    ] ],
    [ "ui_protzonedialog.h", "ui__protzonedialog_8h.html", [
      [ "Ui_protzoneDialog", "class_ui__protzone_dialog.html", "class_ui__protzone_dialog" ],
      [ "protzoneDialog", "class_ui_1_1protzone_dialog.html", null ]
    ] ],
    [ "ui_secureerasedialog.h", "ui__secureerasedialog_8h.html", [
      [ "Ui_secureeraseDialog", "class_ui__secureerase_dialog.html", "class_ui__secureerase_dialog" ],
      [ "secureeraseDialog", "class_ui_1_1secureerase_dialog.html", null ]
    ] ],
    [ "ui_signupdatedialog.h", "ui__signupdatedialog_8h.html", [
      [ "Ui_signupdateDialog", "class_ui__signupdate_dialog.html", "class_ui__signupdate_dialog" ],
      [ "signupdateDialog", "class_ui_1_1signupdate_dialog.html", null ]
    ] ],
    [ "verifypwdialog.h", "verifypwdialog_8h.html", [
      [ "verifyfwDialog", "classverifyfw_dialog.html", "classverifyfw_dialog" ]
    ] ],
    [ "windowdragger.h", "windowdragger_8h.html", [
      [ "WindowDragger", "class_window_dragger.html", "class_window_dragger" ]
    ] ]
];