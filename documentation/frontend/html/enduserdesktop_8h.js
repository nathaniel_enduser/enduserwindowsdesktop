var enduserdesktop_8h =
[
    [ "Namespace", "struct_namespace.html", "struct_namespace" ],
    [ "Device", "struct_device.html", "struct_device" ],
    [ "optstruct", "structoptstruct.html", "structoptstruct" ],
    [ "polstruct", "structpolstruct.html", "structpolstruct" ],
    [ "lockfilestruct", "structlockfilestruct.html", "structlockfilestruct" ],
    [ "lockfolderstruct", "structlockfolderstruct.html", "structlockfolderstruct" ],
    [ "conf_opt", "structconf__opt.html", "structconf__opt" ],
    [ "config_data", "structconfig__data.html", "structconfig__data" ],
    [ "BACKEND_ENABLE", "enduserdesktop_8h.html#ae8070422eac99d63b1a291dacc05cba2", null ],
    [ "BUFFERSIZE", "enduserdesktop_8h.html#ac3146f1e9227301bb4aa518f4d336cee", null ],
    [ "DEVICE_COUNT", "enduserdesktop_8h.html#a268c0726d6884c56c5c4d0b96ac162cd", null ],
    [ "FILELOCK_COUNT", "enduserdesktop_8h.html#afb2e90fa013c47734123678f0fbbfde2", null ],
    [ "FOLDERLOCK_COUNT", "enduserdesktop_8h.html#aaba355b4ab54646638964d7175355025", null ],
    [ "LOCKED", "enduserdesktop_8h.html#a00de8f7e0b615f88335573ba3909583d", null ],
    [ "NAMESPACE_COUNT", "enduserdesktop_8h.html#a2fd1440048958de85fd84b2b15996d63", null ],
    [ "NAMESPACE_NAME", "enduserdesktop_8h.html#a334f501017e0bf21f68003cd654d943d", null ],
    [ "PASSWORDTRIES", "enduserdesktop_8h.html#a2a7760567f904f83f9c56b2647b099c6", null ],
    [ "PWORD_DEFAULT1", "enduserdesktop_8h.html#af3f7bedda754584357eb84f2b4b537c2", null ],
    [ "PWORD_DEFAULT2", "enduserdesktop_8h.html#a791bae5f2a856df314b2540897fe95ef", null ],
    [ "REPORT_CONTENTS", "enduserdesktop_8h.html#a6dd72eda0a41709952d9b5d0c0e8f9c1", null ],
    [ "REPORT_HEADER", "enduserdesktop_8h.html#a0fdb42c9fc03214e7433b8695c64c277", null ],
    [ "SPLASHSCREEN", "enduserdesktop_8h.html#ae8eb41fae823a0f065e8e6275d6b80b5", null ],
    [ "UNLOCKED", "enduserdesktop_8h.html#af0591d953a49374b660c9de8964825fe", null ],
    [ "XPHY_NAME", "enduserdesktop_8h.html#a824f653d67e6e07b458a642224d2663d", null ],
    [ "CFG_OPT", "enduserdesktop_8h.html#a86ab361f1b4a19ef7c1ee0489024c53b", null ],
    [ "CONFIG_DATA", "enduserdesktop_8h.html#aa0ef434f46f3ec6c95d9f251c765f63a", null ],
    [ "DEVICE_DATA", "enduserdesktop_8h.html#acfbbf45a894b3af90748a94ce205003a", null ],
    [ "LOCKFILESTRUCT", "enduserdesktop_8h.html#a8dbde2366283767bc4bed0ec13170199", null ],
    [ "LOCKFOLDERSTRUCT", "enduserdesktop_8h.html#a22c7d72213f04c848d28e02c4e1552d0", null ],
    [ "NAMESPACE_DATA", "enduserdesktop_8h.html#a220292cad95a0795feac8cb08ea22922", null ],
    [ "OPT", "enduserdesktop_8h.html#a27b4b50722f28f0c19c0c94660ec95ff", null ],
    [ "POLICY", "enduserdesktop_8h.html#a4259c4d8690ffa6504030862244f7dc6", null ]
];