#include "secureerasedialog.h"
#include "ui_secureerasedialog.h"
#include <QMessageBox>
#include <QDateTime>
#include <QMediaPlayer>
#include <QFile>
#include <QMutex>

secureeraseDialog::secureeraseDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::secureeraseDialog)
{
    ui->setupUi(this);

    progressbar = new QProgressBar();
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->hide();
    progressval = 0;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(updateprogress()));

    timer->start(1000);

}

secureeraseDialog::~secureeraseDialog()
{
    delete ui;
}

void secureeraseDialog::savenamespace(QString ns)
{
    seceraseitemns = ns;
}

void secureeraseDialog::savephxy(QString xphy)
{
    seceraseitemns = xphy;

    if (xphy == "XPHY1")
    {
        setcomboitem(0);
    }
    else
    {
        setcomboitem(1);
    }
}

void secureeraseDialog::setcomboitem(int idx)
{
    ui->comboBox->setCurrentIndex(idx);
}

void secureeraseDialog::saveData(CONFIG_DATA *ConfigData)
{
    this->ConfigData = ConfigData;
}

void secureeraseDialog::enadiscombobox(bool val)
{
    ui->comboBox->setEnabled(val);
}

void secureeraseDialog::enadiscomboboxns(bool val)
{
    ui->comboBox_2->setEnabled(val);
}

void secureeraseDialog::enadispbutton(bool val)
{
    ui->pushButton->setEnabled(val);
}

void secureeraseDialog::enadispbutton2(bool val)
{
    ui->pushButton_2->setEnabled(val);
}

void secureeraseDialog::setcombotext(QString str)
{
    ui->comboBox->addItem(str);
}

void secureeraseDialog::setcombotextns(QString str)
{
    ui->comboBox_2->addItem(str);
}

void secureeraseDialog::setfocus(void)
{
    ui->lineEdit->setFocus();
}

#include "flexxlib.h"
extern flexxon::flexxDriveList driveList;

void secureeraseDialog::on_pushButton_clicked()
{
    QString         FileReport = QString("../../reports/") + "report.csv";
    QFile           ReportFile(FileReport);
    QTextStream     Data(&ReportFile);
    static QMutex   wait;


    if (ui->comboBox != NULL)
    {
        seceraseitemxphy = ui->comboBox->currentText();
    }

    if (seceraseitemxphy == "XPHY1")
    {
        if (ConfigData->cfgoption[0].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            ui->comboBox_2->setEnabled(true);
            ui->pushButton_2->setEnabled(true);

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            pw = "Password verified passed!";
            logmsg = QString(logmsg).append("--> "+pw);
            logmsg = QString(logmsg).append("  "+time);
            logmsg = QString(logmsg).append("  "+date);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(seceraseitemxphy)
                                                .arg(seceraseitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }

            flexxon::FlexxDecorator flexxdisk(driveList[0]);
            char temp_pass[] = "password";

            wait.lock();
            flexxdisk.secure_erase(temp_pass, strlen(temp_pass));
            wait.unlock();

        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(seceraseitemxphy)
                                                .arg(seceraseitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }
    else
    {
        if (ConfigData->cfgoption[1].password == ui->lineEdit->text())
        {                        
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            // call api here for secure erase

            ui->comboBox_2->setEnabled(true);
            ui->pushButton_2->setEnabled(true);

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(seceraseitemxphy)
                                                .arg(seceraseitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(seceraseitemxphy)
                                                .arg(seceraseitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }

    ui->lineEdit->clear();
    ui->lineEdit->setFocus();
}

void secureeraseDialog::on_pushButton_2_clicked()
{
    // secure
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;

    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    nsstr = ui->comboBox_2->currentText();          // get ns
    nsno = nsstr.right(nsstr.size()-9).toInt();     // get string number only

    xphystr = ui->comboBox->currentText();          // xphy
    xphyno = xphystr.right(xphystr.size()-4).toInt();

    QMediaPlayer *music = new QMediaPlayer(this);
    music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
    music->setVolume(50);
    music->play();

    logmsg = ui->comboBox->currentText();
    QString date = QDate::currentDate().toString("MM/dd/yyyy");
    QString time = QTime::currentTime().toString("hh:mm:ss");
    logmsg = QString(logmsg).append("--> "+date);
    logmsg = QString(logmsg).append("  "+time);
    logmsg = QString(logmsg).append("  Secure Erase has been executed to this");
    logmsg = QString(logmsg).append("  "+nsstr);
    emit eventsignal(logmsg);

    if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        Data << QString(REPORT_CONTENTS).arg(date).arg(time).arg(xphystr)
                                        .arg(nsstr)
                                        .arg("Secure Erase has been executed to this "+nsstr);

        ReportFile.close();
    }
    else
    {
        logmsg = "Failed to create a report file!  ";
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  "+date);
        emit eventsignal(logmsg);
    }

    ui->comboBox_2->setEnabled(false);
    ui->pushButton_2->setEnabled(false);
}

void secureeraseDialog::on_comboBox_2_activated(const QString &arg1)
{
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;

    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    nsstr = ui->comboBox_2->currentText();          // get ns
    nsno = arg1.right(arg1.size()-9).toInt();       // get string number only

    xphystr = ui->comboBox->currentText();          // xphy
    xphyno = xphystr.right(xphystr.size()-4).toInt();

    if (ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].secerase == false)
    {
        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
        music->setVolume(50);
        music->play();

        ui->pushButton_2->setEnabled(false);
        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        logmsg = ui->comboBox->currentText();
        logmsg = QString(logmsg).append("  Secure Erase is not available to this");
        logmsg = QString(logmsg).append("  "+nsstr);
        emit eventsignal(logmsg);

        if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            Data << QString(REPORT_CONTENTS).arg(date).arg(time).arg(xphystr)
                                            .arg(nsstr)
                                            .arg("Secure Erase is not available to this "+nsstr);

            ReportFile.close();
        }
        else
        {
            logmsg = "Failed to create a report file!  ";
            logmsg = QString(logmsg).append("  "+time);
            logmsg = QString(logmsg).append("  "+date);
            emit eventsignal(logmsg);
        }

        return;
    }

    ui->pushButton_2->setEnabled(true);
}

void secureeraseDialog::updateprogress()
{
/*    ui->progressBar->show();

    progressval++;
    ui->progressBar->setValue(progressval);*/
}
