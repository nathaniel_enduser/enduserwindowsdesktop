#include "darkstyle.h"

DarkStyle::DarkStyle() : DarkStyle(styleBase()) {}

DarkStyle::DarkStyle(QStyle *style) : QProxyStyle(style) {}

QStyle *DarkStyle::styleBase(QStyle *style) const
{
    static QStyle *base =
            !style ? QStyleFactory::create(QStringLiteral("Fusion")) : style;
    return base;
}

QStyle *DarkStyle::baseStyle() const
{
    return styleBase();
}

void DarkStyle::polish(QPalette &palette)
{
    // modify palette to dark
//    palette.setColor(QPalette::Window, QColor(51, 156, 255));
    palette.setColor(QPalette::Window, QColor(42, 130, 218));
    palette.setColor(QPalette::WindowText, Qt::white);
    palette.setColor(QPalette::Disabled, QPalette::WindowText,
                     QColor(127, 127, 127));
    palette.setColor(QPalette::Base, QColor(50, 100, 165));
    palette.setColor(QPalette::AlternateBase, QColor(125, 51, 255));
    palette.setColor(QPalette::ToolTipBase, Qt::white);
    palette.setColor(QPalette::ToolTipText, QColor(53, 53, 53));
    palette.setColor(QPalette::Text, QColor(51, 156, 255));
    palette.setColor(QPalette::Disabled, QPalette::Text, QColor(127, 127, 127));
    palette.setColor(QPalette::Dark, QColor(125, 51, 255));
    palette.setColor(QPalette::Shadow, QColor(50, 100, 165));
    palette.setColor(QPalette::Button, QColor(53, 53, 53));
    palette.setColor(QPalette::ButtonText, Qt::white);
    palette.setColor(QPalette::Disabled, QPalette::ButtonText,
                   QColor(127, 127, 127));
    palette.setColor(QPalette::BrightText, Qt::red);
    palette.setColor(QPalette::Link, QColor(42, 130, 218));
//    palette.setColor(QPalette::Link, QColor(50, 100, 165));//
    palette.setColor(QPalette::Highlight, QColor(42, 130, 218));
    palette.setColor(QPalette::Disabled, QPalette::Highlight, QColor(80, 80, 80));
//    palette.setColor(QPalette::HighlightedText, Qt::white);
    palette.setColor(QPalette::HighlightedText, QColor(125, 51, 255));
    palette.setColor(QPalette::Disabled, QPalette::HighlightedText,
                   QColor(127, 127, 127));

    return;
}

void DarkStyle::polish(QApplication *app)
{
    if (!app) return;

    // increase font size for better reading,
    // setPointSize was reduced from +2 because when applied this way in Qt5, the
    // font is larger than intended for some reason
    QFont defaultFont = QApplication::font();
    defaultFont.setPointSize(defaultFont.pointSize() + 1);
    app->setFont(defaultFont);

    // loadstylesheet
    QFile qfDarkstyle(QStringLiteral(":/css/css/darkstyle.qss"));
    if (qfDarkstyle.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // set stylesheet
        QString qsStylesheet = QString::fromLatin1(qfDarkstyle.readAll());
        app->setStyleSheet(qsStylesheet);
        qfDarkstyle.close();
    }

    return;
}
