#include "protzonedialog.h"
#include "ui_protzonedialog.h"
#include <QMessageBox>
#include <QDateTime>
#include <QMediaPlayer>
#include <QFile>
#include <QMutex>

bool    protectns = false;
bool    protinit = false;

protzoneDialog::protzoneDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::protzoneDialog)
{
    ui->setupUi(this);

    progressbar = new QProgressBar();
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->hide();
    progressval = 0;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(updateprogress()));

    timer->start(1000);

//    protns = false;

    QFile file("../../reports/config.csv");
    QStringList wordlist;

    if (protinit == false)
    {
        protinit = true;

        if (!file.open(QIODevice::ReadOnly))
        {
            qDebug() << file.errorString();
            while (1);
        }

        QByteArray line;
        QString string ;
        QString protdata[1000];
        int index = 0;
        int protstart = 0;

        while (!file.atEnd())
        {
            line = file.readLine();
            string = line.data();
            protdata[index] = line.data();

            if (string[0] == "e")
            {
                index++;
                continue;
            }

            if (string[8] == "l")
            {
                index++;
                continue;
            }

            if (string[18] == "p")
            {
                index++;
                continue;
            }

            if (string[36] == "z")
            {
                index++;
                continue;
            }
            else
            {
                break;
            }
            index++;
        }

        QString str = protdata[index-1]; // get the recent setup;
        protstart = index-1;

        while (str[36] == "z")  // all unlock data have been collected
        {
            QChar prottns = str[46];
            QString polen = prottns;
            protectns = polen.toInt();

            protstart++;
            str = protdata[protstart];
        }

        file.close();
    }

}

protzoneDialog::~protzoneDialog()
{
    delete ui;
}


void protzoneDialog::savenamespace(QString ns)
{
    protitemns = ns;
}

void protzoneDialog::savephxy(QString xphy)
{
    protitemxphy = xphy;

    if (xphy == "XPHY1")
    {
        setcomboitem(0);
    }
    else
    {
        setcomboitem(1);
    }
}

void protzoneDialog::setcomboitem(int idx)
{
    ui->comboBox->setCurrentIndex(idx);
}

void protzoneDialog::savedata(CONFIG_DATA *ConfigData)
{
    this->ConfigData = ConfigData;
}

void protzoneDialog::enadiscombobox(bool val)
{
    ui->comboBox->setEnabled(val);
}

void protzoneDialog::enadiscomboboxns(bool val)
{
    ui->comboBox_2->setEnabled(val);
}

void protzoneDialog::enadispbutton(bool val)
{
    ui->pushButton->setEnabled(val);
}

void protzoneDialog::enadispbutton2(bool val)
{
    ui->pushButton_2->setEnabled(val);
}
/*
void protzoneDialog::enadispbutton3(bool val)
{
    //ui->pushButton_3->setEnabled(val);
}
*/
void protzoneDialog::setcombotext(QString str)
{
    ui->comboBox->addItem(str);
}

void protzoneDialog::setcombotextns(QString str)
{
    ui->comboBox_2->addItem(str);
}

void protzoneDialog::setfocus(void)
{
    ui->lineEdit->setFocus();
}

#include "flexxlib.h"
extern flexxon::flexxDriveList driveList;

void protzoneDialog::on_pushButton_clicked()
{
    QString         FileReport = QString("../../reports/") + "report.csv";
    QFile           ReportFile(FileReport);
    QTextStream     Data(&ReportFile);
    static QMutex   wait;


    if (ui->comboBox != NULL)
    {
        protitemxphy = ui->comboBox->currentText();
    }

    if (protitemxphy == "XPHY1")
    {
        if (ConfigData->cfgoption[0].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            if (protectns == false)
            {
                QMessageBox::information(this,
                                         "Namespace is not protected",
                                         " Would you like to Protect it?");
                ui->comboBox_2->setEnabled(true);
                ui->pushButton_2->setEnabled(true);
                //ui->pushButton_3->setEnabled(false);
                ui->pushButton_2->setText("Protect Namespace");
            }
            else
            {
                QMessageBox::information(this,
                                         "Namespace is protected",
                                         " Would you like to Un-Protect it?");
                ui->comboBox_2->setEnabled(true);
                ui->pushButton_2->setEnabled(true);
                ui->pushButton_2->setText("UnProtect Namespace");
//                ui->pushButton_3->setEnabled(true);
            }

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");            
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(protitemxphy)
                                                .arg(protitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }

//            flexxon::flexxDriveList driveList;

            flexxon::FlexxDecorator flexxdisk(driveList[0]);
            char temp_pass[] = "password";

            wait.lock();
            flexxdisk.protect(temp_pass,
                              strlen(temp_pass),
                              1);
            wait.unlock();

        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(protitemxphy)
                                                .arg(protitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }
    else
    {
        if (ConfigData->cfgoption[1].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            // call api here for protect zone/namespace
            if (protectns == false)
            {
                ui->comboBox_2->setEnabled(true);
                ui->pushButton_2->setEnabled(true);
                //ui->pushButton_3->setEnabled(false);
            }

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");            
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(protitemxphy)
                                                .arg(protitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(protitemxphy)
                                                .arg(protitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }

    ui->lineEdit->clear();
    ui->lineEdit->setFocus();
}

void protzoneDialog::on_pushButton_2_clicked()
{
    // check if namespace selected feature is disabled
    // if disabled, protection cannot be used.
    // code below assumed namespace protection is enabled
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;

    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    // namespace unprotected
    if (protectns == false)
    {
        // protected
        protectns = true;

        nsstr = ui->comboBox_2->currentText();          // get ns
        nsno = nsstr.right(nsstr.size()-9).toInt();     // get string number only

        xphystr = ui->comboBox->currentText();          // xphy
        xphyno = xphystr.right(xphystr.size()-4).toInt();

        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        QString xphy= ui->comboBox->currentText();

        ConfigData->xphy = ui->comboBox->currentText();
        ConfigData->cfgoption[xphyno-1].nspace[nsno-1] = ui->comboBox_2->currentText();
        ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].protzone = true;
    //    ConfigData->cfgoption[xphyno-1].protectzone[nsno-1] = true;

        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
        music->setVolume(50);
        music->play();

        logmsg = xphystr;
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  "+ui->comboBox_2->currentText());
        logmsg = QString(logmsg).append("  has been protected");
        emit eventsignal(logmsg);

        // call protect namespace api here...

        if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            Data << QString(REPORT_CONTENTS).arg(date)
                                            .arg(time)
                                            .arg(xphystr)
                                            .arg(nsstr)
                                            .arg(nsstr+" Has been protected");

            ReportFile.close();
        }
        else
        {
            logmsg = "Failed to create a report file!  ";
            logmsg = QString(logmsg).append("  "+time);
            logmsg = QString(logmsg).append("  "+date);
            emit eventsignal(logmsg);
        }

//        enadiscomboboxns(false);
        ui->lineEdit->setEnabled(true);
        ui->pushButton->setEnabled(true);
        ui->comboBox_2->setEnabled(false);
        ui->pushButton_2->setEnabled(false);
//        ui->pushButton_3->setEnabled(false);
    }
    else
    {
        // un-protected
        protectns = false;

        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        xphystr = ui->comboBox->currentText();
        logmsg = xphystr;
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  "+ui->comboBox_2->currentText());
        logmsg = QString(logmsg).append("  is un-protected");
        emit eventsignal(logmsg);
    }

    QString     config = QString("../../reports/") + "config.csv";
    QFile       configfile(config);
    QTextStream PROTECT(&configfile);

    if (configfile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        QString feature = "zoneprot";

        PROTECT << QString(PROTECT_CONTENTS).arg(feature)
                                            .arg(protectns);
        configfile.close();
    }

    ui->lineEdit->setEnabled(true);
    ui->pushButton->setEnabled(true);
    ui->comboBox_2->setEnabled(false);
    ui->pushButton_2->setEnabled(false);
}

void protzoneDialog::on_comboBox_2_activated(const QString &arg1)
{
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;

    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    nsstr = ui->comboBox_2->currentText();          // get ns
    nsno = arg1.right(arg1.size()-9).toInt();     // get string number only

    xphystr = ui->comboBox->currentText();          // xphy
    xphyno = xphystr.right(xphystr.size()-4).toInt();

    if (ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].protzone == false)
    {
        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
        music->setVolume(50);
        music->play();

        ui->pushButton_2->setEnabled(false);
        logmsg = ui->comboBox->currentText();
        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  Protect zone is not available to this");
        logmsg = QString(logmsg).append("  "+nsstr);
        emit eventsignal(logmsg);

        if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            Data << QString(REPORT_CONTENTS).arg(date)
                                            .arg(time)
                                            .arg(xphystr)
                                            .arg(nsstr)
                                            .arg("Protect zone is not available to this "+nsstr);

            ReportFile.close();
        }
        else
        {
            logmsg = "Failed to create a report file!  ";
            logmsg = QString(logmsg).append("  "+time);
            logmsg = QString(logmsg).append("  "+date);
            emit eventsignal(logmsg);
        }

        return;
    }

    ui->pushButton_2->setEnabled(true);
}

void protzoneDialog::updateprogress()
{
/*    ui->progressBar->show();

    progressval++;
    ui->progressBar->setValue(progressval);*/
}
/*
void protzoneDialog::on_pushButton_3_clicked()
{
    return;

//    ui->pushButton_3->setEnabled(false);

    QString xphystr, logmsg;

    //ui->comboBox_2->setEnabled(val);
    QString date = QDate::currentDate().toString("MM/dd/yyyy");
    QString time = QTime::currentTime().toString("hh:mm:ss");
    xphystr = ui->comboBox->currentText();
    logmsg = xphystr;
    logmsg = QString(logmsg).append("--> "+date);
    logmsg = QString(logmsg).append("  "+time);
    logmsg = QString(logmsg).append("  "+ui->comboBox_2->currentText());
    logmsg = QString(logmsg).append("  is un-protected");
    emit eventsignal(logmsg);

    ui->comboBox_2->setEnabled(true);
    ui->pushButton_2->setEnabled(false);
    ui->pushButton_3->setEnabled(false);

    protectns = false;

}
*/
