#include "passworddialog.h"
#include "ui_passworddialog.h"
#include <QMessageBox>
#include <QDateTime>
#include <QMediaPlayer>
#include <QFile>
#include <QMutex>

passwordDialog::passwordDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::passwordDialog)
{
    ui->setupUi(this);

    progressbar = new QProgressBar();
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->hide();
    progressval = 0;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(updateprogress()));

    timer->start(1000);

    return;
}

passwordDialog::~passwordDialog()
{
    delete ui;
}

void passwordDialog::setLabelText(QString str)
{
    ui->lineEdit->setText(str);
}

void passwordDialog::SetFocus(void)
{
    ui->lineEdit->setFocus();
}

void passwordDialog::savenamespace(QString nspace)
{
    pwitemnamespace = nspace;
}

void passwordDialog::savexphy(QString xphy)
{
    pwitemxphy = xphy;

    if (xphy == "XPHY1")
    {
        setcomboitem(0);
    }
    else
    {
        setcomboitem(1);
    }
}

void passwordDialog::saveData(CONFIG_DATA *ConfigData)
{
    this->ConfigData =  ConfigData;

    return;
}

void passwordDialog::enadiscombobox(bool val)
{
    ui->comboBox->setEnabled(val);
}

void passwordDialog::addcombotext(QString str)
{
    ui->comboBox->addItem(str);
}

void passwordDialog::setcomboitem(int idx)
{
    ui->comboBox->setCurrentIndex(idx);
}

void passwordDialog::setfocus()
{
    ui->lineEdit->setFocus();
}

#include "flexxlib.h"
extern flexxon::flexxDriveList driveList;

void passwordDialog::on_pushButton_clicked()
{     
    QString         FileReport = QString("../../reports/") + "report.csv";
    QFile           ReportFile(FileReport);
    QTextStream     Data(&ReportFile);
    static QMutex   wait;


    if (ui->comboBox != NULL)
    {
        pwitemxphy = ui->comboBox->currentText();
    }

    if (pwitemxphy == "XPHY1")
    {
        if (ConfigData->cfgoption[0].password == ui->lineEdit->text()) // check pw from old text against from config
        {
            ConfigData->xphy = "XPHY1";
            ConfigData->cfgoption[0].password = ui->lineEdit_2->text();    // new password,

            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password has been successfully changed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(pwitemxphy)
                                                .arg(pwitemnamespace)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }

            flexxon::FlexxDecorator flexxdisk(driveList[0]);
//            char temp_pass[] = "password";

            QString str1 = ui->lineEdit->text();
            QString str2 = ui->lineEdit_2->text();
            QByteArray ba = str1.toLocal8Bit();
            QByteArray ba2 = str1.toLocal8Bit();
            char *old = ba.data();
            char *replcae = ba2.data();

            wait.lock();
            flexxdisk.change_xphy_passwd(old, str1.size(),
                                         replcae, str2.size());
            wait.unlock();
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = " Password change failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date).arg(time).arg(pwitemxphy)
                                                .arg(pwitemnamespace)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }
    else
    {
        if (ConfigData->cfgoption[1].password == ui->lineEdit->text()) // check pw from old text against from config
        {
            ConfigData->xphy = "XPHY2";
            ConfigData->cfgoption[1].password = ui->lineEdit_2->text();    // new password,

            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            // call api here for change password

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = " Password has been successfully changed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date).arg(time).arg(pwitemxphy)
                                                .arg(pwitemnamespace)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            QString xphy= ui->comboBox->currentText();
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = " Password change failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date).arg(time).arg(pwitemxphy)
                                                .arg(pwitemnamespace)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }

    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit->setFocus();

    return;

}

void passwordDialog::updateprogress()
{
/*    ui->progressBar->show();

    progressval++;
    ui->progressBar->setValue(progressval);*/
}
