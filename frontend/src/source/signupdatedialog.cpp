#include "signupdatedialog.h"
#include "ui_signupdatedialog.h"
#include <QMessageBox>
#include <QDateTime>
#include <QMediaPlayer>
#include <QFile>
#include <QMutex>

signupdateDialog::signupdateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::signupdateDialog)
{
    ui->setupUi(this);

    progressbar = new QProgressBar();
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->hide();
    progressval = 0;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(updateprogress()));

    timer->start(1000);

}

signupdateDialog::~signupdateDialog()
{
    delete ui;
}

void signupdateDialog::savenamespace(QString ns)
{
    signitemns = ns;
}

void signupdateDialog::savexphy(QString xphy)
{
    signitemxphy = xphy;

    if (xphy == "XPHY1")
    {
        setcomboitem(0);
    }
    else
    {
        setcomboitem(1);
    }
}

void signupdateDialog::savefilename(QString filename, QString xphy)
{
    if (xphy == "XPHY1")
    {
        ConfigData->cfgoption[0].filename = filename;
    }
    else
    {
        ConfigData->cfgoption[1].filename = filename;
    }
}

void signupdateDialog::setcomboitem(int idx)
{
    ui->comboBox->setCurrentIndex(idx);
}

void signupdateDialog::saveData(CONFIG_DATA *ConfigData)
{
    this->ConfigData = ConfigData;
}

void signupdateDialog::enadiscombobox(bool val)
{
    ui->comboBox->setEnabled(val);
}

void signupdateDialog::enadiscomboboxns(bool val)
{
    ui->comboBox_2->setEnabled(val);
}

void signupdateDialog::setcombotext(QString str)
{
    ui->comboBox->addItem(str);
}

void signupdateDialog::setcombotextns(QString str)
{
    ui->comboBox_2->addItem(str);
}

void signupdateDialog::enadispbutton2(bool val)
{
    ui->pushButton_2->setEnabled(val);
}

void signupdateDialog::setfocus(void)
{
    ui->lineEdit->setFocus();
}

#include "flexxlib.h"
extern flexxon::flexxDriveList driveList;

void signupdateDialog::on_pushButton_clicked()
{
    QString         FileReport = QString("../../reports/") + "report.csv";
    QFile           ReportFile(FileReport);
    QTextStream     Data(&ReportFile);
    static QMutex   wait;


    if (ui->comboBox != NULL)
    {
        signitemxphy = ui->comboBox->currentText();
    }

    if (signitemxphy == "XPHY1")
    {
        if (ConfigData->cfgoption[0].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            ui->comboBox_2->setEnabled(true);
            ui->pushButton_2->setEnabled(true);

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(signitemxphy)
                                                .arg(signitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
/*
            flexxon::FlexxDecorator flexxdisk(driveList[0]);
            char temp_pass[] = "password";

            wait.lock();

            flexxdisk.signature_update(temp_pass,
                                       strlen(temp_pass),
                                       void *signature_data,
                                       const int signature_size)
            wait.unlock();
*/
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(signitemxphy)
                                                .arg(signitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }
    else
    {
        if (ConfigData->cfgoption[1].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            // call api here for secure erase

            ui->comboBox_2->setEnabled(true);
            ui->pushButton_2->setEnabled(true);

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(signitemxphy)
                                                .arg(signitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            ui->comboBox_2->setEnabled(false);
            ui->pushButton_2->setEnabled(false);

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(signitemxphy)
                                                .arg(signitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }

    ui->lineEdit->clear();
    ui->lineEdit->setFocus();
}

void signupdateDialog::on_pushButton_2_clicked()
{
    // sign update
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;

    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    nsstr = ui->comboBox_2->currentText();          // get ns
    nsno = nsstr.right(nsstr.size()-9).toInt();     // get string number only

    xphystr = ui->comboBox->currentText();          // xphy
    xphyno = xphystr.right(xphystr.size()-4).toInt();

    // get the object from file explorer then send thru vendor specific

    QMediaPlayer *music = new QMediaPlayer(this);
    music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
    music->setVolume(50);
    music->play();  

    logmsg = ui->comboBox->currentText();
    QString date = QDate::currentDate().toString("MM/dd/yyyy");
    QString time = QTime::currentTime().toString("hh:mm:ss");
    logmsg = QString(logmsg).append("--> "+date);
    logmsg = QString(logmsg).append("  "+time);
//    logmsg = QString(logmsg).append("  Signature update has been executed to this");
    logmsg = QString(logmsg).append("  Signature has been executed to this");
    logmsg = QString(logmsg).append("  "+nsstr);
    emit eventsignal(logmsg);

    QString filename = ConfigData->cfgoption[0].filename;
    emit eventsignal(filename);

    if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        Data << QString(REPORT_CONTENTS).arg(date)
                                        .arg(time)
                                        .arg(signitemxphy)
                                        .arg(signitemns)
                                        .arg("Signature update has been executed to this "+nsstr);

        ReportFile.close();
    }
    else
    {
        logmsg = "Failed to create a report file!  ";
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  "+date);
        emit eventsignal(logmsg);
    }

    ui->comboBox_2->setEnabled(false);
    ui->pushButton_2->setEnabled(false);
}

void signupdateDialog::on_comboBox_2_activated(const QString &arg1)
{
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;

    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    nsstr = ui->comboBox_2->currentText();          // get ns
    nsno = arg1.right(arg1.size()-9).toInt();     // get string number only

    xphystr = ui->comboBox->currentText();          // xphy
    xphyno = xphystr.right(xphystr.size()-4).toInt();

    if (ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].signupdate == false)
    {
        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
        music->setVolume(50);
        music->play();

        ui->pushButton_2->setEnabled(false);
        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        logmsg = ui->comboBox->currentText();
        logmsg = QString(logmsg).append("  Signature Update is not available to this");
        logmsg = QString(logmsg).append("  "+nsstr);
        emit eventsignal(logmsg);

        if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            Data << QString(REPORT_CONTENTS).arg(date).arg(time).arg(xphystr)
                                            .arg(nsstr)
                                            .arg("Signature Update is not available to this "+nsstr);

            ReportFile.close();
        }
        else
        {
            logmsg = "Failed to create a report file!  ";
            logmsg = QString(logmsg).append("  "+time);
            logmsg = QString(logmsg).append("  "+date);
            emit eventsignal(logmsg);
        }

        return;
    }

    ui->pushButton_2->setEnabled(true);
}

void signupdateDialog::updateprogress()
{
/*    ui->progressBar->show();

    progressval++;
    ui->progressBar->setValue(progressval);*/
}
