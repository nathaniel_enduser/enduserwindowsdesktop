#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QTextStream>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSet>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>
#include <QDateTime>
#include <QMediaPlayer>
#include <QFileDialog>
#include <QStackedBarSeries>

#include "passworddialog.h"
#include "protzonedialog.h"
#include "policydialog.h"
#include "lockunlockdialog.h"
#include "encdecdialog.h"
#include "secureerasedialog.h"
#include "signupdatedialog.h"


#include <flexxlib.h>
flexxon::flexxDriveList driveList;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

//    QString reportDate = QDate::currentDate().toString("MM/dd/yyyy");
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
    bool FileExist = false;

    if (ReportFile.exists())
    {
        FileExist = true;
    }

    if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        if (!FileExist)
        {
            Data << REPORT_HEADER;
        }

        ReportFile.close();
    }

    configDialog = new genconfigDialog(this);

    connect(configDialog,
            &genconfigDialog::eventsignal,
            this,
            &MainWindow::log_event);

    xphydetected = false;

    #if BACKEND_ENABLE
    initializebackend();
    #endif

    // get initial information from the drive
    // these will be use for verification and display
    // password for xphy1, xpy2
    // get lock, policy, enc/dec, protect file, health graph

    // use genconfigdialog for all the information acquired from the drive and use it all through out.
    // maintain this copy so that after reboot, this copy will be regenerated

    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->treeWidget->topLevelItem(0)->setData(0,Qt::UserRole,"toplevel");

    for (int xphy=0; xphy < DEVICE_COUNT; xphy++)
    {
        for (int locked = 0; locked < NAMESPACE_COUNT; locked++)
        {
            configDialog->configdata->cfgoption[xphy].featurelock[locked] = false;
        }
    }

    connect(ui->treeWidget,
            &QTreeWidget::customContextMenuRequested,
            this,
            &MainWindow::right_click_menu);

    connect(ui->treeWidget,
            &QTreeWidget::itemPressed,
            this,
            &MainWindow::item_pressed);

    connect(ui->treeWidget,
            &QTreeWidget::itemClicked,
            this,
            &MainWindow::item_clicked);

    connect(ui->pushButton,
            &QPushButton::clicked,
            this,
            &MainWindow::execute_lockunlock);

    create_actions();
    create_chart();
    create_tray_icon();
    set_hardcoded_data();
    set_tree_widget();
    set_tree_directory();
    hide_displays();

    device_health();

    currentrecord = 0;

    reloadluparam();


    return;
}

MainWindow::~MainWindow()
{
    delete DeviceDataHash;
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event)
    return;
}

void MainWindow::reloadluparam()
{
    QFile file("../../reports/config.csv");
    QStringList wordlist;

    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug() << file.errorString();
        while (1);
    }

    QByteArray line;
    QString string;
    QString lockdata[1000];
    int index = 0;
    int lockstart = 0;

    while (!file.atEnd())
    {
        line = file.readLine();
        string = line.data();
        lockdata[index] = line.data();

        if (string[8] == "l")
        {
            if (lockstart == 0)
            {
                lockstart = index;
            }
        }
        index++;
    }

    QString feature, currrecord, filerec, lockstr;
    QString str = lockdata[lockstart];
    int lockdatacnt = lockstart;
    int i = 0;

    while (str[8] == "l")  // all unlock data have been collected
    {
        QChar num = str[20];
        QString cur = num;
        currentrecord = cur.toInt();

        if (str[23] == ",")     // file
        {
            if (str[25] == "f") // lock state
            {
                lock = false;
            }
            else
            {
                lock = true;
            }
            lockdatacnt++;
            str = lockdata[lockdatacnt];
            i++;
            continue;
        }
        else
        {
            QString lockfile;
            int stridx = 23;

            while (str[stridx] != ",")
            {
                lockfile = QString(lockfile).append(str[stridx]);
                stridx++;
            }
            lockfilerecord[i] = lockfile;
            lockdatacnt++;
            str = lockdata[lockdatacnt];
            i++;
        }
    }

    file.close();
}

void MainWindow::create_actions()
{
    //Namespace Menu Options
    NamespaceMenu = new QMenu(this);

/*    Password = new QAction(QIcon(":/icons/icons/changepassword.png"),
                           tr("Change Password"), this);*/
    Protect = new QAction(QIcon(":/icons/icons/protectzone.png"),
                          tr("Protect Zone"), this);
    Policy = new QAction(QIcon(":/icons/icons/changepolicy.png"),
                         tr("Change Policy"), this);
    EncDec = new QAction(QIcon(":/icons/icons/encryption.png"),
                         tr("Enc/Dec"), this);
    SecErase = new QAction(QIcon(":/icons/icons/secureerase.jpg"),
                           tr("Secure Erase"), this);
    SignUpdate = new QAction(QIcon(":/icons/icons/signatureupdate.png"),
                             tr("Signature Update"), this);

//    Password->setStatusTip("Change Password...");
    Protect->setStatusTip("Protect/Lock the selected Namespace...");
    Policy->setStatusTip("Change Policy the selected Namespace...");
    EncDec->setStatusTip("Enable/Disable Enc/Dec for this Namespace");
    SecErase->setStatusTip("Secure Erase the selected Namespace...");
    SignUpdate->setStatusTip("A Signature object will be transferred to the selected Namespace...");

/*    connect(Password,
            &QAction::triggered,
            this,
            &MainWindow::changepassword);*/
    connect(Protect,
            &QAction::triggered,
            this,
            &MainWindow::protectzone);
    connect(Policy,
            &QAction::triggered,
            this,
            &MainWindow::changepolicy);
    connect(EncDec,
            &QAction::triggered,
            this,
            &MainWindow::encryptdecryptenable);
    connect(SecErase,
            &QAction::triggered,
            this,
            &MainWindow::secureerase);
    connect(SignUpdate,
            &QAction::triggered,
            this,
            &MainWindow::signatureupdate);

//    NamespaceMenu->addAction(Password);
    NamespaceMenu->addAction(Protect);
    NamespaceMenu->addAction(Policy);
    NamespaceMenu->addAction(EncDec);
    NamespaceMenu->addAction(SecErase);
    NamespaceMenu->addAction(SignUpdate);

    //Device Menu Options
    DeviceMenu = new QMenu(this);

/*    Create = new QAction(tr("Create Namespace"), this);
    Delete = new QAction(tr("Delete Namespace"), this);
    Attach = new QAction(tr("Attach Namespace"), this);*/
    Create = new QAction(QIcon(":/icons/icons/drive.png"),
                         tr("Create Namespace"), this);
    Delete = new QAction(QIcon(":/icons/icons/drive.png"),
                         tr("Delete Namespace"), this);
    Attach = new QAction(QIcon(":/icons/icons/drive.png"),
                         tr("Attach Namespace"), this);
    Detach = new QAction(QIcon(":/icons/icons/drive.png"),
                         tr("Detach Namespace"), this);
    Password = new QAction(QIcon(":/icons/icons/changepassword.png"),
                           tr("Change Password"), this);
    FwDownload = new QAction(QIcon(":/icons/icons/updatefirmware.png"),
                           tr("Update Firmware"), this);


    Create->setStatusTip("Create Namespace...");
    Delete->setStatusTip("Delete Namespace...");
    Attach->setStatusTip("Attach Namespace...");
    Detach->setStatusTip("Detach Namespace...");
    Password->setStatusTip("Change Password...");
    FwDownload->setStatusTip("Update Firmware...");

    connect(Create,
            &QAction::triggered,
            this,
            &MainWindow::create_namespace);
    connect(Delete,
            &QAction::triggered,
            this,
            &MainWindow::delete_namespace);
    connect(Attach,
            &QAction::triggered,
            this,
            &MainWindow::attach_namespace);
    connect(Detach,
            &QAction::triggered,
            this,
            &MainWindow::detach_namespace);
    connect(Password,
            &QAction::triggered,
            this,
            &MainWindow::changepassword);
    connect(FwDownload,
            &QAction::triggered,
            this,
            &MainWindow::fwdownload);

    DeviceMenu->addAction(Create);
    DeviceMenu->addAction(Delete);
    DeviceMenu->addAction(Attach);
    DeviceMenu->addAction(Detach);
    DeviceMenu->addAction(Password);

    //    ui->statusbar->setFont()
//    ui->statusbar->setStyleSheet("color: blue");
    ui->statusbar->setStyleSheet("color: white");
    ui->statusbar->showMessage("Right-Click to XPHY or Namespace objects to access the features!");

    return;
}

void MainWindow::create_chart()
{/*
    BarChart = new QChart();
    BarChart->setTitle("XPHY NVME Device Health");
//    BarChart->setAnimationOptions(QChart::SeriesAnimations);
    BarChart->legend()->hide();
//    BarChart->legend()->setAlignment(Qt::AlignBottom);

    QStringList categories;
//    categories << "availspare" << "avail thresh" << "cntl bus time" << "power cycle" << "unsafe shutdown" << "numberlogerr";
    categories << "avail thold" << "cntl busy" << "pwr cycle" << "unsafe sd";// << "info error" << "temp" << "asdf";
//    categories << "avail thold" << "cntl busy" << "pwr cycle" << "unsafe sd" << "info error";

    QBarCategoryAxis *axisX = new QBarCategoryAxis();   //
    axisX->append(categories);
    BarChart->addAxis(axisX, Qt::AlignBottom);

    QValueAxis *axisY = new QValueAxis();
    axisY->setRange(0, 100);
    BarChart->addAxis(axisY, Qt::AlignLeft);

    QChartView *BarChartView = new QChartView(BarChart);
    BarChartView->setRenderHint(QPainter::Antialiasing);

    ui->gridLayout_3->addWidget(BarChartView, 0, 0);
*/
    return;
}

void MainWindow::create_tray_icon()
{
    TrayMenu = new QMenu(this);
    Open = new QAction(tr("Open"), this);
    Exit = new QAction(tr("Exit"), this);
    QWidget *Parent = (QWidget*)this->parent();

    connect(Open,
            &QAction::triggered,
            Parent,
            &QWidget::show);
    connect(Exit,
            &QAction::triggered,
            qApp,
            &QCoreApplication::quit);

    TrayMenu->addAction(Open);
    TrayMenu->addAction(Exit);

    TrayIcon = new QSystemTrayIcon(QIcon(":/icons/icons/flexxon.png"), this);
    TrayIcon->setContextMenu(TrayMenu);
    TrayIcon->show();

    return;
}

void MainWindow::set_hardcoded_data()
{

    DeviceDataHash = new QHash<QString, DEVICE_DATA>;
    QString Key;
    QString nspace;
    DEVICE_DATA DevData;

//    for (int i = 0; i < DEVICE_COUNT; i++)
        for (int i = 0; i < 1; i++)
    {
        Key = QString(XPHY_NAME).append(QString::number(i+1));
        DevData.NsCount = NAMESPACE_COUNT;

//        for (int j = 0; j < NAMESPACE_COUNT; j++)
            for (int j = 0; j < 1; j++)
        {
            nspace = NAMESPACE_NAME;
            DevData.NS[j].NsName = QString(nspace).append(QString::number(j+1));
            DevData.NS[j].Data1 = QString(DevData.NS[j].NsName)\
                                      .append("_data1");
            DevData.NS[j].Data2 = QString(DevData.NS[j].NsName)\
                                      .append("_data2");
            DevData.NS[j].Data3 = QString(DevData.NS[j].NsName)\
                                      .append("_data3");
        }
        DeviceDataHash->insert(Key, DevData);
    }

    return;
}

/*
struct xphy_device {
    char subnqn[256];
};
*/
void MainWindow::set_tree_widget()
{
    QTreeWidgetItem *Children;
    QTreeWidgetItem *Children2;
    DEVICE_DATA DeviceData;
    QStringList Keys = DeviceDataHash->keys();
    Keys.sort();
/*
    // creat xphy vector
    std::vector<struct xphy_device> xphy_list;

    for (disk = driveList.begin(); disk < driveList.end(); disk++)
    {
        // send identify controller to disk
        // if xphy vector.size is zero, create new parent,add this to the xphy_list
        // else get correct parent from xphy vector

        // at this point there should be a parent pointer


        // send identify namespace to disk
        // create new child for this namespace
        // add this child to the parent


    }
*/
#if 0
//    DeviceData = DeviceDataHash->value(Name);
    DeviceData = DeviceDataHash->value("XPHY");
    Children = new QTreeWidgetItem(ui->treeWidget->topLevelItem(0));
    Children->setText(0,"XPHY1");
    Children->setIcon(0,QIcon(":/icons/icons/ssd.png"));
    Children->setData(0,Qt::UserRole,"devicename");

    Children2 = new QTreeWidgetItem(Children);
//    Children2->setText(0,DeviceData.NS[j].NsName);
    Children2->setText(0,DeviceData.NS[0].NsName);
    Children2->setIcon(0,QIcon(":/icons/icons/drive.png"));
    Children2->setData(0,Qt::UserRole,"namespace");
#endif

    foreach (QString Name, Keys)
    {
        DeviceData = DeviceDataHash->value(Name);
        Children = new QTreeWidgetItem(ui->treeWidget->topLevelItem(0));
        Children->setText(0,Name);
        Children->setIcon(0,QIcon(":/icons/icons/ssd.png"));
        Children->setData(0,Qt::UserRole,"devicename");

//        for (int j = 0; j < DeviceData.NsCount; j++)
            for (int j = 0; j < 1; j++)
        {
            Children2 = new QTreeWidgetItem(Children);
            Children2->setText(0,DeviceData.NS[j].NsName);
            Children2->setIcon(0,QIcon(":/icons/icons/drive.png"));
            Children2->setData(0,Qt::UserRole,"namespace");
        }
    }

    return;
}

void MainWindow::set_tree_directory()
{
    SystemModel = new QFileSystemModel(this);
    ui->treeView->setModel(SystemModel);

    connect(ui->treeView,
            &QTreeView::clicked,
            this,
            &MainWindow::lock_unlock);
/*
    connect(ui->treeView,
            &QTreeView::doubleClicked,
            this,
            &MainWindow::lock_unlock_dclick);
*/
    return;
}

void MainWindow::hide_displays()
{
//    ui->wgtXPHYInfo->hide();
//    ui->wgtHealthInfo->hide();
    ui->wgtLockUnlock->hide();

    return;
}

void MainWindow::device_clicked()
{
/*
    if (xphydetected == false)
    {
        return;
    }
*/
    if (TrayIcon->isVisible())
    {
        static flexxon::flexxDriveList::iterator disk;
        struct identify_controller_struct id_control;
        struct identify_namespace_struct id_namespace;
        struct log_smart_info_struct smart_info_global;

        scan_devices(driveList);

        for (disk = driveList.begin(); disk < driveList.end(); disk++)
        {
            memset(&id_control, 0, sizeof(id_control));
            memset(&id_namespace, 0, sizeof(id_namespace));
            disk->identify(id_control);
            disk->identify(id_namespace);
            disk->logpage(1, smart_info_global);
        }

        if (configDialog->configdata->cfgoption[0].policies[0].policy1 == true)
        {
            // check total capacity
            double totcapacity = (512 * 1024) * 1024;
            double cap75consumed;

            cap75consumed = totcapacity * .25;

            // free space - check 25% remaining capacity
            // id_control - check total capacity
            if ((totcapacity * .25) < cap75consumed)    // .25 remaining of the drive is warning. (.20 remaining of the drive is critical)
            {
                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
                music->setVolume(50);
                music->play();

                TrayIcon->showMessage(windowTitle(),
                                      tr("Warning!..... "
                                         "Disk Free Space is below 75% usage"));
            }
        }

        if (configDialog->configdata->cfgoption[0].policies[0].policy2 == true)
        {
            uint16_t temp = (smart_info_global.temperature[1] << 8) | smart_info_global.temperature[0];
            float comptemp = temp - 273.15;

            if (comptemp > 75)  // 80 critical
            {
                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
                music->setVolume(50);
                music->play();

                TrayIcon->showMessage(windowTitle(),
                                      tr("Warning!..... "
                                         "Tempature of the drive is above 75 'C"));
            }
        }

        if (configDialog->configdata->cfgoption[0].policies[0].policy3 == true)
        {
            xphyhealth[0].unsafe_shutdowns.lo = smart_info_global.unsafe_shutdowns.lo;
            uint64_t unsafeshutdown = xphyhealth[0].unsafe_shutdowns.lo;

            if (unsafeshutdown > 100)   // 130 critical
            {
                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
                music->setVolume(50);
                music->play();

                TrayIcon->showMessage(windowTitle(),
                                      tr("Warning!..... "
                                         "Unsafe shutdown of the drive is around 100"));
            }
        }

        if (configDialog->configdata->cfgoption[0].policies[0].policy4 == true)
        {
            xphyhealth[0].data_integrity_errors.lo = smart_info_global.data_integrity_errors.lo;
            uint64_t datainterror = xphyhealth[0].data_integrity_errors.lo;

            if (datainterror > 50)   //
            {
                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
                music->setVolume(50);
                music->play();

                TrayIcon->showMessage(windowTitle(),
                                      tr("Warning!..... "
                                         "Media and Data integrity error"));
            }
        }

        if (configDialog->configdata->cfgoption[0].policies[0].policy5 == true)
        {
            xphyhealth[0].number_of_error_info_log_entries.lo = smart_info_global.number_of_error_info_log_entries.lo;
            uint64_t numberlogerr = xphyhealth[0].number_of_error_info_log_entries.lo;

            if (numberlogerr > 50)
            {
                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
                music->setVolume(50);
                music->play();

                TrayIcon->showMessage(windowTitle(),
                                      tr("Warning!..... "
                                         "Number of Error Information Log Entries"));
            }
        }

        if (configDialog->configdata->cfgoption[0].policies[0].policy6 == true)
        {
            uint8_t critwarning = xphyhealth[0].criticalWarn;

            if (critwarning > 0)   // 1 means critical warning
            {
                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
                music->setVolume(50);
                music->play();

                TrayIcon->showMessage(windowTitle(),
                                      tr("Warning!..... "
                                         "Critical Warning"));
            }
        }
    }
    else
    {
        close();
    }

    return;
}

void MainWindow::namespace_clicked()
{
    QMessageBox::information(this,
                             "Deslaimer: File to lock must be greater than 1024B from the nature of ntfs",
                             "Due to ntfs issue, the file locking must be above 1k (1024B)."
                             " A file lower than 1k (1024B) is prohibited according to ntfs."
                             " A folder cannot also be locked since there is multiple folder under the initial folder");

    QString Path = "C:";
    SystemModel->setRootPath(Path);
    ui->treeView->setRootIndex(SystemModel->index(Path));
    ui->wgtLockUnlock->show();
    ui->pushButton->setEnabled(false);

    return;
}

void MainWindow::device_health()
{
#if 0
    QString xphystr;
    int xphyno;

    CurrentXphyName = "XPHY1";

    if (CurrentXphyName == "XPHY1")
    {
        xphystr = CurrentXphyName;
        xphyno = xphystr.right(xphystr.size()-4).toInt();

        QBarSeries *BarSeries = new QBarSeries();
        QBarSet *smartdata = new QBarSet("");
        BarChart->removeAllSeries();

        //uint32_t availspare = xphyhealth[0].info1;
        //availspare = (availspare & INFO1_AVAIL_SPARE_MAKE) >> 24;

//        uint8_t temp = xphyhealth[0].info1;
//        temp = ((temp & INFO1_COMPOSITE_TEMP_MASK) >> 8) - 273.15;

        //        float temp = tempa - 273.15; // kelvin to celcius
//        QString compositetemp = QString::number(temp);

        uint8_t availthresh = xphyhealth[0].avail_spare_threshold;

        uint64_t cntlbusytimehi = xphyhealth[1].controller_busy_time.hi;
        uint64_t cntlbusytimelo = xphyhealth[0].controller_busy_time.lo;
        uint64_t cntlbusytime = ((cntlbusytimehi << 8) | cntlbusytimelo);

        uint64_t powercycle = xphyhealth[0].power_cycles.lo;

        uint64_t unsafeshutdown = xphyhealth[0].unsafe_shutdowns.lo;

        uint64_t numberlogerr = xphyhealth[0].number_of_error_info_log_entries.lo;


//        *smartdata << availthresh << cntlbusytime << powercycle << unsafeshutdown << numberlogerr;// << temp ;
//        *smartdata << 23 << 5 << 9 << 1 << 4 << 3;// << uint64_t(35/10);
        *smartdata << 4 << 3 << 2 << 1;// << uint64_t(35/10);
//        *smartdata << uint8_t(23) << uint64_t(5) << 9 << 1 << 35;

        BarSeries->append(smartdata);
        BarChart->addSeries(BarSeries);

    }
    else if (CurrentXphyName == "XPHY2")
    {
        // xphy2
    }
    else
    {
        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
        music->setVolume(50);
        music->play();

        log_event("unkown device id");
    }
#endif
    return;
}

void MainWindow::right_click_menu(const QPoint &pos)
{
    QModelIndex Index = ui->treeWidget->indexAt(pos);

    if (Index.isValid())
    {
        if (ItemType == "namespace")
        {
            NamespaceMenu->exec(ui->treeWidget->mapToGlobal(pos));
        }
        else if (ItemType == "devicename")
        {
            DeviceMenu->exec(ui->treeWidget->mapToGlobal(pos));
        }
    }

    return;
}

void MainWindow::item_pressed(QTreeWidgetItem *item, int Col)
{    
    CurrentItem = item;
    ItemName = CurrentItem->text(Col);
    ItemType = CurrentItem->data(0,Qt::UserRole).toString();

    if (ItemType == "namespace")
    {
        CurrentXphyName = CurrentItem->parent()->text(Col);
    }

    return;
}

void MainWindow::item_clicked(QTreeWidgetItem *item, int Col)
{
    Q_UNUSED(item)
    Q_UNUSED(Col)

    //initially hide all displays
    hide_displays();

    if (ItemType == "devicename")
    {
        ui->tabWidget->setCurrentIndex(0);
        ui->wgtXPHYInfo->show();
        device_clicked();
    }
    else if (ItemType == "namespace")
    {
        ui->tabWidget->setCurrentIndex(2);
        ui->wgtLockUnlock->show();
        ui->wgtHealthInfo->show();
        namespace_clicked();
        device_health();
    }

    return;
}

void MainWindow::log_event(QString str)
{
    ui->textBrowser->append(str);

    return;
}

QString MainWindow::get_log(void)
{
    QString text = ui->textBrowser->toPlainText();

    return text;
}

void MainWindow::generatereport()
{
    QString reportDate = QDate::currentDate().toString("MM/dd/yyyy");
//    QTreeWidgetItem *ChildItem;
    //    QString FileReport = QString("../../reports/") + reportDate + "_Report.csv";
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    QString Date = QDate::currentDate().toString("MM/dd/yyyy");
//    QString Time = QTime::currentTime().toString("hh:mm:ss");
//    DEVICE_DATA DevData;
//    NAMESPACE_DATA NsData;
//    QString DeviceName, logmsg;
    QString logmsg;
    bool FileExist = false;
//    int NsIndex = 0;

/*    QString loginfo = get_log();

    if (ReportFile.exists())
    {
        FileExist = true;
    }
*/
    if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        if (!FileExist)
        {
            Data << REPORT_HEADER;
        }

        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = "Generate report has been requested!  ";
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  "+date);
        log_event(logmsg);

        Data << QString(REPORT_CONTENTS).arg(date)
                                        .arg(time)
                                        .arg("XPHY")
                                        .arg("Namespace")
                                        .arg("Generate report has been requested!");

        ReportFile.close();
    }
    else
    {
        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = "Failed to create a report file!  ";
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  "+date);
        log_event(logmsg);
    }

    return;
}

void MainWindow::changepassword()
{
    if (xphydetected == false)
    {
        return;
    }

    passwordDialog Password(this);

//    Password.savenamespace(ItemName);
    Password.addcombotext("XPHY1");
//    Password.addcombotext("XPHY2");
    Password.savexphy(ItemName);
    Password.saveData(configDialog->returnData());
    Password.enadiscombobox(false);
    Password.setfocus();

    connect(&Password,
            &passwordDialog::eventsignal,
            this,
            &MainWindow::log_event);

    Password.exec();

    return;
}

void MainWindow::fwdownload()
{
    static flexxon::flexxDriveList::iterator    disk;
    struct identify_controller_struct           id_control;
    struct identify_namespace_struct            id_namespace;
    struct log_smart_info_struct                smart_info_global;

    scan_devices(driveList);

    for (disk = driveList.begin(); disk < driveList.end(); disk++)
    {
        memset(&id_control, 0, sizeof(id_control));
        memset(&id_namespace, 0, sizeof(id_namespace));
        disk->identify(id_control);
        disk->identify(id_namespace);
        disk->logpage(1, smart_info_global);
    }

    disk = driveList.begin();
    disk->identify(0xffffffff, cns_idns, &id_namespace, sizeof(id_namespace));
    void *p;    // fw_ptr
    uint32_t fwsize;
    const int bs = 0;
    disk->fw_download(p, fwsize, bs);


#if 0
    const char *desc = "Copy all or part of a firmware image to "\
        "a controller for future update. Optionally, specify how "\
        "many KiB of the firmware to transfer at once. The offset will "\
        "start at 0 and automatically adjust based on xfer size "\
        "unless fw is split across multiple files. May be submitted "\
        "while outstanding commands exist on the Admin and IO "\
        "Submission Queues. Activate downloaded firmware with "\
        "fw-activate, and then reset the device to apply the downloaded firmware.";
    const char *fw = "firmware file (required)";
    const char *xfer = "transfer chunksize limit";
    const char *offset = "starting dword offset, default 0";
    int err, fd, fw_fd = -1;
    unsigned int fw_size;
    struct stat sb;
//    void *fw_buf, *buf;
    void *buf;
    char *fw_buf;
    bool huge;

    struct config {
        char  *fw;
        uint32_t    xfer;
        uint32_t    offset;
    };
/*
    struct config cfg = {
        .fw     = "",
        .xfer   = 4096,
        .offset = 0,
    };
*/
    struct config cfg;
    cfg.fw = 0;
    cfg.xfer = 4096;
    cfg.offset = 0;

    /*
    OPT_ARGS(opts) = {
        OPT_FILE("fw",     'f', &cfg.fw,     fw),
        OPT_UINT("xfer",   'x', &cfg.xfer,   xfer),
        OPT_UINT("offset", 'o', &cfg.offset, offset),
        OPT_END()
    };
*/

//    err = fd = parse_and_open(argc, argv, desc, opts);
    if (fd < 0)
        goto ret;

 //   fw_fd = open(cfg.fw, O_RDONLY);
    cfg.offset <<= 2;
    if (fw_fd < 0) {
        fprintf(stderr, "Failed to open firmware file %s: %s\n",
                cfg.fw, strerror(errno));
        err = -EINVAL;
        goto close_fd;
    }

    err = fstat(fw_fd, &sb);
    if (err < 0) {
        perror("fstat");
        goto close_fw_fd;
    }

  //  fw_size = sb.st_size;
    if (fw_size & 0x3) {
        fprintf(stderr, "Invalid size:%d for f/w image\n", fw_size);
        err = -EINVAL;
        goto close_fw_fd;
    }

//    fw_buf = nvme_alloc(fw_size, &huge);
    if (!fw_buf) {
        fprintf(stderr, "No memory for f/w size:%d\n", fw_size);
        err = -ENOMEM;
        goto close_fw_fd;
    }

    buf = fw_buf;
    if (cfg.xfer == 0 || cfg.xfer % 4096)
        cfg.xfer = 4096;
 //   if (read(fw_fd, fw_buf, fw_size) != ((ssize_t)(fw_size)))
    {
        err = -errno;
        fprintf(stderr, "read :%s :%s\n", cfg.fw, strerror(errno));
        goto free;
    }

    while (fw_size > 0) {
      //  cfg.xfer = min(cfg.xfer, fw_size);

   //     err = nvme_fw_download(fd, cfg.offset, cfg.xfer, fw_buf);
        if (err < 0) {
            perror("fw-download");
            break;
        } else if (err != 0) {
  //          nvme_show_status(err);
            break;
        }
        fw_buf     += cfg.xfer;
        fw_size    -= cfg.xfer;
        cfg.offset += cfg.xfer;
    }
    if (!err)
        printf("Firmware download success\n");

free:
    //nvme_free(buf, huge);
close_fw_fd:
    //close(fw_fd);
close_fd:
    //close(fd);
ret:
    //return nvme_status_to_errno(err, false);
#endif

   return;
}

void MainWindow::fwcommit()
{
    static flexxon::flexxDriveList::iterator    disk;
    struct identify_controller_struct           id_control;
    struct identify_namespace_struct            id_namespace;
    struct log_smart_info_struct                smart_info_global;

    scan_devices(driveList);

    for (disk = driveList.begin(); disk < driveList.end(); disk++)
    {
        memset(&id_control, 0, sizeof(id_control));
        memset(&id_namespace, 0, sizeof(id_namespace));
        disk->identify(id_control);
        disk->identify(id_namespace);
        disk->logpage(1, smart_info_global);
    }

    disk = driveList.begin();
    disk->identify(0xffffffff, cns_idns, &id_namespace, sizeof(id_namespace));
    uint32_t bpid;
    uint32_t ca;
    uint32_t fs;

    disk->commit_firmware(bpid, ca, fs);


#if 0
    const char *desc = "Verify downloaded firmware image and "\
        "commit to specific firmware slot. Device is not automatically "\
        "reset following firmware activation. A reset may be issued "\
        "with an 'echo 1 > /sys/class/nvme/nvmeX/reset_controller'. "\
        "Ensure nvmeX is the device you just activated before reset.";
    const char *slot = "[0-7]: firmware slot for commit action";
    const char *action = "[0-7]: commit action";
    const char *bpid = "[0,1]: boot partition identifier, if applicable (default: 0)";
    int err, fd;

    struct config {
        uint8_t slot;
        uint8_t action;
        uint8_t bpid;
    };
/*
    struct config cfg = {
        .slot   = 0,
        .action = 0,
        .bpid   = 0,
    };
*/
    struct config cfg;
    cfg.slot = 0;
    cfg.action = 0;
    cfg.bpid = 0;

    /*
    OPT_ARGS(opts) = {
        OPT_BYTE("slot",   's', &cfg.slot,   slot),
        OPT_BYTE("action", 'a', &cfg.action, action),
        OPT_BYTE("bpid",   'b', &cfg.bpid,   bpid),
        OPT_END()
    };
*/
//    err = fd = parse_and_open(argc, argv, desc, opts);
    if (fd < 0)
        goto ret;

    if (cfg.slot > 7) {
        fprintf(stderr, "invalid slot:%d\n", cfg.slot);
        err = -EINVAL;
        goto close_fd;
    }
    if (cfg.action > 7 || cfg.action == 4 || cfg.action == 5) {
        fprintf(stderr, "invalid action:%d\n", cfg.action);
        err = -EINVAL;
        goto close_fd;
    }
    if (cfg.bpid > 1) {
        fprintf(stderr, "invalid boot partition id:%d\n", cfg.bpid);
        err = -EINVAL;
        goto close_fd;
    }

//    err = nvme_fw_commit(fd, cfg.slot, cfg.action, cfg.bpid);
    if (err < 0)
        perror("fw-commit");
    else if (err != 0)
        switch (err & 0x3ff) {
   //     case NVME_SC_FW_NEEDS_CONV_RESET:
   //     case NVME_SC_FW_NEEDS_SUBSYS_RESET:
   //     case NVME_SC_FW_NEEDS_RESET:
            printf("Success activating firmware action:%d slot:%d",
                   cfg.action, cfg.slot);
            if (cfg.action == 6 || cfg.action == 7)
                printf(" bpid:%d", cfg.bpid);
   //         printf(", but firmware requires %s reset\n", nvme_fw_status_reset_type(err));
            break;
        default:
    //        nvme_show_status(err);
            break;
        }
    else {
        printf("Success committing firmware action:%d slot:%d",
               cfg.action, cfg.slot);
        if (cfg.action == 6 || cfg.action == 7)
            printf(" bpid:%d", cfg.bpid);
        printf("\n");
    }

close_fd:
//	close(fd);
ret:
//	return nvme_status_to_errno(err, false);

#endif

    return;
}

void MainWindow::fwstatusreset(uint32_t status)
{
    switch (status & 0x3ff) {
//	case NVME_SC_FW_NEEDS_CONV_RESET:	return "conventional";
//	case NVME_SC_FW_NEEDS_SUBSYS_RESET:	return "subsystem";
//	case NVME_SC_FW_NEEDS_RESET:		return "any controller";
//    default:
//        return "unknown";
    }
}

void MainWindow::protectzone()
{
    if (xphydetected == false)
    {
        return;
    }

    protzoneDialog protect(this);

    protect.setcombotext("XPHY1");
//    protect.setcombotext("XPHY2");
    protect.savenamespace(ItemName);
    protect.savephxy(CurrentXphyName);
    protect.savedata(configDialog->returnData());
    protect.enadiscombobox(false);
    protect.enadiscomboboxns(false);
    protect.enadispbutton(true);
    protect.enadispbutton2(false);
//    protect.enadispbutton3(false);
    protect.setcombotextns("namespace1");
/*    protect.setcombotextns("namespace2");
    protect.setcombotextns("namespace3");
    protect.setcombotextns("namespace4");
    protect.setcombotextns("namespace5");
    protect.setcombotextns("namespace6");
    protect.setcombotextns("namespace7");
    protect.setcombotextns("namespace8");*/
    protect.setfocus();

    connect(&protect,
            &protzoneDialog::eventsignal,
            this,
            &MainWindow::log_event);

    protect.exec();

    return;
}

void MainWindow::changepolicy()
{
    if (xphydetected == false)
    {
        return;
    }

    policyDialog policy(this);

    policy.setcombotext("XPHY1");
//    policy.setcombotext("XPHY2");
    policy.savenamespace(ItemName);
    policy.savephxy(CurrentXphyName);
    policy.savedata(configDialog->returnData());
    policy.enadiscombobox(true);
    policy.enadiscomboboxns(false);
    policy.enadispbutton(true);
    policy.enadispbuttonns(false);
    policy.enadischeckbox(false);
    policy.setcombotextns("namespace1");
/*    policy.setcombotextns("namespace2");
    policy.setcombotextns("namespace3");
    policy.setcombotextns("namespace4");
    policy.setcombotextns("namespace5");
    policy.setcombotextns("namespace6");
    policy.setcombotextns("namespace7");
    policy.setcombotextns("namespace8");*/
    policy.setfocus();

    connect(&policy,
            &policyDialog::eventsignal,
            this,
            &MainWindow::log_event);

    policy.exec();

    return;
}

void MainWindow::lockfile()
{
/*    lockunlockDialog Lock(this);

    Lock.savenamespace(ItemName);
    Lock.savexphy(CurrItemName);
    Lock.saveData(configDialog->returnData());
    Lock.enadiscombobox(true);
    Lock.setcombotext("XPHY1");
    Lock.setcombotext("XPHY2");
    Lock.enadiscomboboxns(false);
    Lock.enadispbutton2(false);
    Lock.setcombotextns("namespace1");
    Lock.setcombotextns("namespace2");
    Lock.setcombotextns("namespace3");
    Lock.setcombotextns("namespace4");
    Lock.setcombotextns("namespace5");
    Lock.setcombotextns("namespace6");
    Lock.setcombotextns("namespace7");
    Lock.setcombotextns("namespace8");
    Lock.setfocus();

    connect(&Lock,
            &lockunlockDialog::eventsignal,
            this,
            &MainWindow::log_event);

    Lock.exec();
*/
    return;
}

void MainWindow::encryptdecryptenable()
{
    if (xphydetected == false)
    {
        return;
    }

    encdecDialog encdec(this);

    encdec.setcombotext("XPHY1");
//    encdec.setcombotext("XPHY2");
    encdec.savenamespace(ItemName);
    encdec.savexphy(CurrentXphyName);
    encdec.saveData(configDialog->returnData());
    encdec.enadiscombobox(true);
    encdec.enadiscomboboxns(false);
    encdec.setcombotextns("namespace1");
/*    encdec.setcombotextns("namespace2");
    encdec.setcombotextns("namespace3");
    encdec.setcombotextns("namespace4");
    encdec.setcombotextns("namespace5");
    encdec.setcombotextns("namespace6");
    encdec.setcombotextns("namespace7");
    encdec.setcombotextns("namespace8");
*/
    encdec.enadispbutton2(false);
    encdec.disableradioall();
    encdec.setfocus();

    connect(&encdec,
            &encdecDialog::eventsignal,
            this,
            &MainWindow::log_event);

    encdec.exec();

    return;
}

void MainWindow::secureerase()
{
    if (xphydetected == false)
    {
        return;
    }

    secureeraseDialog secerase(this);

    secerase.setcombotext("XPHY1");
//    secerase.setcombotext("XPHY2");
    secerase.savenamespace(ItemName);
    secerase.savephxy(CurrentXphyName);
    secerase.saveData(configDialog->returnData());
    secerase.enadiscombobox(true);
    secerase.enadiscomboboxns(false);
    secerase.setcombotextns("namespace1");
/*    secerase.setcombotextns("namespace2");
    secerase.setcombotextns("namespace3");
    secerase.setcombotextns("namespace4");
    secerase.setcombotextns("namespace5");
    secerase.setcombotextns("namespace6");
    secerase.setcombotextns("namespace7");
    secerase.setcombotextns("namespace8");
*/
    secerase.enadispbutton2(false);
    secerase.setfocus();

    connect(&secerase,
            &secureeraseDialog::eventsignal,
            this,
            &MainWindow::log_event);

    secerase.exec();

    return;
}

void MainWindow::signatureupdate()
{
    if (xphydetected == false)
    {
        return;
    }

    QString filename = QFileDialog::getOpenFileName(this, "Open a File", QDir::homePath());
/*    QMessageBox::information(this, "..",filename);
    QString text;
    QMessageBox box;
    box.setText(filename);
*/
/*
    QString logmsg, message;
    QString date = QDate::currentDate().toString("MM/dd/yyyy");
    QString time = QTime::currentTime().toString("hh:mm:ss");
    logmsg = "XPHY1";
    logmsg = QString(logmsg).append("--> "+date);
    logmsg = QString(logmsg).append("  "+time);
    logmsg = QString(logmsg).append(" Namespace1 ");
    logmsg = QString(logmsg).append(filename + " Sent this signature update file to xphy device");
    log_event(logmsg);
*/


    signupdateDialog sign(this);

    sign.setcombotext("XPHY1");
//    sign.setcombotext("XPHY2");
    sign.savenamespace(ItemName);
    sign.savexphy(CurrentXphyName);
    sign.saveData(configDialog->returnData());
    sign.enadiscombobox(true);
    sign.enadiscomboboxns(false);
    sign.savefilename(filename, CurrentXphyName);
    sign.setcombotextns("namespace1");    
/*
    sign.setcombotextns("namespace2");
    sign.setcombotextns("namespace3");
    sign.setcombotextns("namespace4");
    sign.setcombotextns("namespace5");
    sign.setcombotextns("namespace6");
    sign.setcombotextns("namespace7");
    sign.setcombotextns("namespace8");
*/
    sign.enadispbutton2(false);
    sign.setfocus();

    connect(&sign,
            &signupdateDialog::eventsignal,
            this,
            &MainWindow::log_event);

    sign.exec();


    return;
}

void MainWindow::genconfiguration()
{
    if (xphydetected == false)
    {
        return;
    }

    // during initial launch policy must be none
    // second boot, must apply previous policy settings

    configDialog->setcombotext("XPHY1");
    configDialog->setcombotext("XPHY2");
    configDialog->setgroupboxns(false);
    configDialog->setgroupboxpol(false);
    configDialog->setfocus();
    configDialog->setcombotextns("namespace1");
    configDialog->setcombotextns("namespace2");
    configDialog->setcombotextns("namespace3");
    configDialog->setcombotextns("namespace4");
    configDialog->setcombotextns("namespace5");
    configDialog->setcombotextns("namespace6");
    configDialog->setcombotextns("namespace7");
    configDialog->setcombotextns("namespace8");
    configDialog->setpushbuttonupdcfg(false);
    configDialog->setcombostate(false);

    configDialog->exec();

    return;
}

void MainWindow::updatefirmware()
{
    if (xphydetected == false)
    {
        return;
    }

    return;
}

void MainWindow::about()
{
    return;
}

#define NVME_IOCTL_TIMEOUT  120000   // in milliseconds
#define NVME_NSID_ALL		0xffffffff

void MainWindow::create_namespace()
{
    static const int namespace_size = 1*1024*1024*1024; // 1GB
    static flexxon::flexxDriveList::iterator    disk;
    struct identify_controller_struct           id_control;
    struct identify_namespace_struct            id_namespace;
    struct log_smart_info_struct                smart_info_global;
    struct lba_formatx_struct                   *flbaPtr;
    struct create_ns_template_struct            create_ns;
    uint32_t                                    new_nsid;

    scan_devices(driveList);

    for (disk = driveList.begin(); disk < driveList.end(); disk++)
    {
        memset(&id_control, 0, sizeof(id_control));
        memset(&id_namespace, 0, sizeof(id_namespace));
        disk->identify(id_control);
        disk->identify(id_namespace);
        disk->logpage(1, smart_info_global);
    }

    disk = driveList.begin();
    disk->identify(0xffffffff, cns_idns, &id_namespace, sizeof(id_namespace));
    flbaPtr = &id_namespace.lbaf[0];
    create_ns.nsze = namespace_size / (1 << flbaPtr->lbads);
    create_ns.ncap = create_ns.nsze;
    create_ns.flbas = 0;
    disk->create_namespace(create_ns, new_nsid);


#if 0
//    struct identify_namespace_struct id_namespace;
    struct identify_namespace_struct ns;

    const char *nsze = "size of ns";
    const char *ncap = "capacity of ns";
    const char *flbas = "FLAB size";
    const char *dps = "data protection capabilities";
    const char *nmic = "multipath and sharing capabilities";
    const char *anagrpid = "ANA Group Identifier";
    const char *nvmsetid = "NVM Set Identifier";
    const char *timeout = "timeout value, in milliseconds";
    const char *bs = "target block size";

    int err = 0, fd, i;
    uint32_t nsid;

    struct config
    {
        uint64_t    nsze;
        uint64_t    ncap;
        uint8_t     flbas;
        uint8_t     dps;
        uint8_t     nmic;
        uint32_t    anagrpid;
        uint16_t    nvmsetid;
        uint64_t    bs;
        uint32_t    timeout;
    };

/*
    struct config cfg = {
        .flbas      = 0xff,
        .anagrpid   = 0,
        .nvmsetid   = 0,
        .bs         = 0x00,
        .timeout    = NVME_IOCTL_TIMEOUT,
    };
*/
    struct config cfg;
    cfg.flbas = 0xff;
    cfg.anagrpid = 0;
    cfg.nvmsetid = 0;
    cfg.bs = 0x00;
    cfg.timeout = NVME_IOCTL_TIMEOUT;


    if (cfg.flbas != 0xff && cfg.bs != 0x00)
    {
        QMessageBox::information(this,
                                 "Invalid specs of both FLBAs and Block Size",
                                 "Please specify only one");
        //fprintf(stderr,
        //"Invalid specification of both FLBAS and Block Size, please specify only one\n");
        err = -EINVAL;
        goto close_fd;
    }
    if (cfg.bs)
    {
        if ((cfg.bs & (~cfg.bs + 1)) != cfg.bs)
        {
            QMessageBox::information(this,
                                     "Invalid value for block size",
                                     "Block size must be a power of two");
            //fprintf(stderr,
            //    "Invalid value for block size (%"PRIu64"). Block size must be a power of two\n",
            //    (uint64_t)cfg.bs);
            err = -EINVAL;
            goto close_fd;
        }
//        err = nvme_identify_ns(fd, NVME_NSID_ALL, 0, &ns);
        if (err)
        {
            if (err < 0)
                perror("identify-namespace");
            else
            {
                QMessageBox::information(this,
                                         "Identify failed",
                                         "Identify failed");
                //fprintf(stderr, "identify failed\n");
                //nvme_show_status(err);
            }
            goto close_fd;
        }
        for (i = 0; i < 16; ++i)
        {
//            if ((1 << ns.lbaf[i].ds) == cfg.bs && ns.lbaf[i].ms == 0)
                if ((1 << ns.lbaf[i].lbads) == cfg.bs && ns.lbaf[i].ms == 0)
            {
                cfg.flbas = (uint8_t)i;
                break;
            }
        }

    }
    if (cfg.flbas == 0xff)
    {
        QMessageBox::information(this,
                                 "FLBAS corresponding to block size not found",
                                 "Please correct block size, or specify FLBS directly");

        //fprintf(stderr,
        //    "FLBAS corresponding to block size %"PRIu64" not found\n",
        //    (uint64_t)cfg.bs);
        //fprintf(stderr,
        //    "Please correct block size, or specify FLBAS directly\n");

        err = -EINVAL;
        goto close_fd;
    }

//    err = nvme_ns_create(fd, cfg.nsze, cfg.ncap, cfg.flbas, cfg.dps, cfg.nmic,
//                         cfg.anagrpid, cfg.nvmsetid, cfg.timeout, &nsid);
    /*
    if (!err)
        printf("%s: Success, created nsid:%d\n", cmd->name, nsid);
    else if (err > 0)
        nvme_show_status(err);
    else
        perror("create namespace");
    */
close_fd:
//    close(fd);
ret:
//    return nvme_status_to_errno(err, false);
#endif

    return;
}

void MainWindow::delete_namespace()
{

    static flexxon::flexxDriveList::iterator    disk;
    struct identify_controller_struct           id_control;
    struct identify_namespace_struct            id_namespace;
    struct log_smart_info_struct                smart_info_global;

    scan_devices(driveList);

    for (disk = driveList.begin(); disk < driveList.end(); disk++)
    {
        memset(&id_control, 0, sizeof(id_control));
        memset(&id_namespace, 0, sizeof(id_namespace));
        disk->identify(id_control);
        disk->identify(id_namespace);
        disk->logpage(1, smart_info_global);
    }

    disk = driveList.begin();
    disk->identify(0xffffffff, cns_idns, &id_namespace, sizeof(id_namespace));
    disk->delete_namespace();


#if 0
    const char *desc = "Delete the given namespace by "\
        "sending a namespace management command to "\
        "the provided device. All controllers should be detached from "\
        "the namespace prior to namespace deletion. A namespace ID "\
        "becomes inactive when that namespace is detached or, if "\
        "the namespace is not already inactive, once deleted.";
    const char *namespace_id = "namespace to delete";
    const char *timeout = "timeout value, in milliseconds";
    int err, fd;

    struct config {
        uint32_t namespace_id;
        uint32_t timeout;
    };
/*
    struct config cfg = {
        .namespace_id	= 0,
        .timeout	= NVME_IOCTL_TIMEOUT,
    };
*/
    struct config cfg;
    cfg.namespace_id = 0;
    cfg.timeout = NVME_IOCTL_TIMEOUT;

//	err = fd = parse_and_open(argc, argv, desc, opts);
    if (fd < 0)
        goto ret;

    if (!cfg.namespace_id) {
        //cfg.namespace_id = nvme_get_nsid(fd);
        if (cfg.namespace_id == 0) {
            err = -EINVAL;
            goto close_fd;
        }
        else if (cfg.namespace_id < 0) {
            err = cfg.namespace_id;
            goto close_fd;
        }
    }

//	err = nvme_ns_delete(fd, cfg.namespace_id, cfg.timeout);
    if (!err)
        //printf("%s: Success, deleted nsid:%d\n", cmd->name,
        //						cfg.namespace_id);
        QMessageBox::information(this,
                                 "Success",
                                 "Deleted namespace");
/*
    else if (err > 0)
        nvme_show_status(err);
    else
        perror("delete namespace");
*/
close_fd:
    //close(fd);
ret:
    //return nvme_status_to_errno(err, false);

#endif

    return;
}

void MainWindow::attach_namespace()
{
    int err, num, i, fd, list[2048];
    uint16_t ctrlist[2048];

    const char *namespace_id = "namespace to attach";
    const char *cont = "optional comma-sep controller id list";

    struct config
    {
        char        *cntlist;
        uint32_t    namespace_id;
    };
/*
    struct config cfg = {
        .cntlist = "",
        .namespace_id = 0,
    };
*/
    struct config cfg;
    cfg.cntlist = 0;
    cfg.namespace_id = 0;

//    err = fd = parse_and_open(argc, argv, desc, opts);
    if (fd < 0)
        goto ret;

    if (!cfg.namespace_id)
    {
        QMessageBox::information(this,
                                 "Namespace-id parameter required",
                                 "namespace-id parameter is required");

        //fprintf(stderr, "%s: namespace-id parameter required\n",
        //				cmd->name);
        err = -EINVAL;
        goto close_fd;
    }

//    num = argconfig_parse_comma_sep_array(cfg.cntlist, list, 2047);
    if (!num)
    {
//        fprintf(stderr, "warning: empty controller-id list will result in no actual change in namespace attachment\n");
        QMessageBox::information(this,
                                 "warning: emptry controller-id",
                                 "empty controller-id list will result"
                                 " in no actual change in namespace attachment");
    }

    if (num == -1)
    {
        //fprintf(stderr, "%s: controller id list is malformed\n",
                        //cmd->name);
        QMessageBox::information(this,
                                 "controller id list is malformed",
                                 "controller id list is malformed");
        err = -EINVAL;
        goto close_fd;
    }

    for (i = 0; i < num; i++)
        ctrlist[i] = (uint16_t)list[i];

//    err = nvme_ns_attachment(fd, cfg.namespace_id, num, ctrlist, attach);
/*
    if (!err)
        printf("%s: Success, nsid:%d\n", cmd->name, cfg.namespace_id);
    else if (err > 0)
        nvme_show_status(err);
    else
        perror(attach ? "attach namespace" : "detach namespace");
*/
close_fd:
//	close(fd);
ret:
//	return nvme_status_to_errno(err, false);

    return;
}

void MainWindow::detach_namespace()
{
    const char *desc = "Detach the given namespace from the "\
        "given controller; de-activates the given namespace's ID. A "\
        "namespace must be attached to a controller before IO "\
        "commands may be directed to that namespace.";
//	return nvme_attach_ns(argc, argv, 0, desc, cmd);

    return;
}

void MainWindow::lock_unlock(const QModelIndex &index)
{
    if (xphydetected == false)
    {
        return;
    }

    ui->pushButton->setEnabled(true);
    SelectedFile = SystemModel->filePath(index);

    for (int i = 0; i < currentrecord; i++)
    {
        if (lockfilerecord[i] == SelectedFile)
        {
            lockfilerecord[i] = "";
            ui->pushButton->setText("UnLock");
            lock = false;
            QMessageBox::information(this,
                         "File already locked",
                         "This file is already locked!."
                         " Would you want to unlock it or lock another file?");

            // new file has a record from previous
            // would you like to unlock?
            goto skiplocktrue;
            //return;
        }
    }

    lock = true;    // locked
    ui->pushButton->setText("Lock");
    lockfilerecord[currentrecord] = SelectedFile;

skiplocktrue:

    QString     config = QString("../../reports/") + "config.csv";
    QFile       configfile(config);
    QTextStream LOCK(&configfile);

    if (configfile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        QString feature = "lockunlock";
        QString currrec = QString::number(currentrecord);
        QString lockstr;
        QString lockfilerec = lockfilerecord[currentrecord];

        int i = lockfilerec.count();

        if (lock == true)
        {
            lockstr = "true";
        }
        else
        {
            lockstr = "false";
        }

        LOCK << QString(LOCK_CONTENTS).arg(feature)
                                        .arg(currrec)
                                        .arg(lockfilerec)
                                        .arg(lockstr);

        configfile.close();
    }

    if (lock == true)
    {
        currentrecord++;
    }

    return;
}

void MainWindow::lock_unlock_dclick(const QModelIndex &index)
{
    lock_unlock(index);

    if (!QDir(SelectedFile).exists())
    {
        execute_lockunlock();
    }

    return;
}

void MainWindow::execute_lockunlock()
{
    if (xphydetected == false)
    {
        return;
    }

    QString Message = QString("Lock / Unlock this file or folder?\n\n").append(SelectedFile);
//    int Ret = QMessageBox::question(this, tr("Lock / Unlock"), Message);

    int xphyno, nsno;

    xphyno = CurrentXphyName.right(CurrentXphyName.size()-4).toInt();
    nsno = ItemName.right(ItemName.size()-9).toInt();

    if (configDialog->configdata->cfgoption[xphyno-1].featurelock[nsno-1] == false)
    {
        lockunlockDialog Lock(this);

        //do lock/unlock here
        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
        music->setVolume(50);
        music->play();

        Lock.setcombotext("XPHY1");
//        Lock.setcombotext("XPHY2");
        Lock.saveData(configDialog->returnData());
        Lock.savenamespace(ItemName, CurrentXphyName);
        Lock.savexphy(CurrentXphyName);
        Lock.savefilefolder(SelectedFile, CurrentXphyName);
        Lock.enadispbutton(true);
        Lock.lockfile(lock);
        Lock.setfocus();

        connect(&Lock,
                &lockunlockDialog::eventsignal,
                this,
                &MainWindow::log_event);

        Lock.exec();
    }
    else
    {
        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/SmokeAlarm.wav"));
        music->setVolume(50);
        music->play();

        QString logmsg, pw;
        logmsg = CurrentXphyName;
        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        pw = "Lock/Unlock feature, Invalid Password has reached its limit!";
        logmsg = QString(logmsg).append("  "+pw);
        log_event(logmsg);
    }

    return;
}

#if BACKEND_ENABLE
//#include <flexxlib.h>
//flexxon::flexxDriveList driveList;

//static const int namespace_size = 1*1024*1024*1024; // 1GB

void MainWindow::initializebackend(void)
{
    static flexxon::flexxDriveList::iterator disk;
    struct identify_controller_struct id_control;
    struct identify_namespace_struct id_namespace;
//    struct create_ns_template_struct create_ns;
//    struct lba_formatx_struct *flbaPtr;
//    uint32_t new_nsid;/
//    std::vector<struct log_err_info_struct> err_info;
    struct log_smart_info_struct smart_info_global;
//    struct log_smart_info_struct smart_info_ns;
//    struct log_fw_slot_info_struct fw_slot;

    scan_devices(driveList);

    for (disk = driveList.begin(); disk < driveList.end(); disk++) {

            xphydetected = true;
            memset(&id_control, 0, sizeof(id_control));
            memset(&id_namespace, 0, sizeof(id_namespace));

            disk->identify(id_control);
            disk->identify(id_namespace);
            //disk->logpage(err_info, 1);
            disk->logpage(1, smart_info_global);
//            disk->logpage(0, smart_info_ns);
            //disk->logpage(fw_slot);
    }

    if (xphydetected == false)
    {
        return;
    }

    display_miscdata(id_namespace);

    get_id_namespace(id_namespace);
    get_smart_health_info(smart_info_global);
    get_identify_controller(id_control);

#if 0
    /* try sending create namespace once to the drive */
    disk = driveList.begin();
    disk->identify(0xffffffff, cns_idns, &id_namespace, sizeof(id_namespace));
    flbaPtr = &id_namespace.lbaf[0];
    create_ns.nsze = namespace_size / (1 << flbaPtr->lbads);
    create_ns.ncap = create_ns.nsze;
    create_ns.flbas = 0;
    disk->create_namespace(create_ns, new_nsid);
#endif
}

void MainWindow::get_identify_controller(identify_controller_struct id_control)
{

    // check xphy or device name this identify controller is
    // scsi1, or scsi2
    // CurrentXphyName

    identifycontlr[0].vid = id_control.vid;
    uint16_t vid = identifycontlr[0].vid;
    QString vidstr = QString::number(vid);
    ui->label_42->setStyleSheet("color: yellow");
    ui->label_42->setText(vidstr);

    identifycontlr[0].ssvid = id_control.ssvid;
    uint16_t ssvid = identifycontlr[0].ssvid;
    QString ssvidstr = QString::number(ssvid);
    ui->label_34->setStyleSheet("color: yellow");
    ui->label_34->setText(ssvidstr);

    QString serialnum;
    for (int i = 0; i < 20; i++)
    {
        identifycontlr[0].sn[i] = id_control.sn[i];
        uint8_t sn = identifycontlr[0].sn[i];
        char serialno = static_cast<char>(sn);
        serialnum[i] = serialno;
    }
    ui->label_36->setStyleSheet("color: yellow");
    ui->label_36->setText(serialnum);

    QString modelnum;
    for (int i = 0; i < 40; i++)
    {
        identifycontlr[0].mn[i] = id_control.mn[i];
        uint8_t mn = identifycontlr[0].mn[i];
        char modelno = static_cast<char>(mn);
        modelnum[i] = modelno;
    }
    ui->label_17->setStyleSheet("color: yellow");
    ui->label_17->setText(modelnum);

    QString firmrev;
    for (int i = 0; i < 8; i++)
    {        
        identifycontlr[0].fr[i] = id_control.fr[i];
        uint8_t fr = identifycontlr[0].fr[i];
        char firm = static_cast<char>(fr);
        firmrev[i] = firm;
    }
    ui->label_16->setStyleSheet("color: yellow");
    ui->label_16->setText(firmrev);

    identifycontlr[0].rab = id_control.rab;
    uint16_t rab = identifycontlr[0].rab;
    QString rabstr = QString::number(rab);
    ui->label_27->setStyleSheet("color: yellow");
    ui->label_27->setText(rabstr);

    QString ieeestr;
    for (int i = 0; i < 3; i++)
    {
        identifycontlr[0].ieee[i] = id_control.ieee[i];

        uint8_t ieee = identifycontlr[0].ieee[i];
        char ieeest = static_cast<char>(ieee);
        ieeestr[i] = ieeest;
    }
    ui->label_37->setStyleSheet("color: yellow");
    ui->label_37->setText(ieeestr);

    identifycontlr[0].cmic = id_control.cmic;
    uint8_t cmic =  identifycontlr[0].cmic;
    QString cmicshare = QString::number(cmic);
    ui->label_173->setStyleSheet("color: yellow");
    ui->label_173->setText(cmicshare);

    identifycontlr[0].mdts = id_control.mdts;
    uint8_t mdts = identifycontlr[0].mdts;
    QString mdtsize = QString::number(mdts);
    ui->label_191->setStyleSheet("color: yellow");
    ui->label_191->setText(mdtsize);

    identifycontlr[0].cntlid = id_control.cntlid;
    uint16_t cid = identifycontlr[0].cntlid;
    QString cntlid = QString::number(cid);
    ui->label_18->setStyleSheet("color: yellow");
    ui->label_18->setText(cntlid);

    identifycontlr[0].ver.mjr = id_control.ver.mjr;
    identifycontlr[0].ver.mnr = id_control.ver.mnr;
    identifycontlr[0].ver.ter = id_control.ver.ter;

    QString major, minor, terstr;
    uint16_t mjr = identifycontlr[0].ver.mjr;
    uint8_t mnr = identifycontlr[0].ver.mnr;
    uint8_t tert = identifycontlr[0].ver.ter;
    major = QString::number(mjr);
    minor = QString::number(mnr);
    terstr = QString::number(tert);
    QString version = QString(major).append(minor).append(terstr);
    ui->label_31->setStyleSheet("color: yellow");
    ui->label_31->setText(version);

    identifycontlr[0].rtd3e = id_control.rtd3e;
    uint32_t rtd3e = identifycontlr[0].rtd3e;
    QString rtd3estr = QString::number(rtd3e);
    ui->label_26->setStyleSheet("color: yellow");
    ui->label_26->setText(rtd3estr);

    identifycontlr[0].rtd3r = id_control.rtd3r;
    uint32_t rtd3r = identifycontlr[0].rtd3r;
    QString rtd3rstr = QString::number(rtd3r);
    ui->label_23->setStyleSheet("color: yellow");
    ui->label_23->setText(rtd3rstr);

    identifycontlr[0].oaes = id_control.oaes;
    uint32_t oaes = identifycontlr[0].oaes;
    QString oaesstr = QString::number(oaes);
    ui->label_22->setStyleSheet("color: yellow");
    ui->label_22->setText(oaesstr);

    identifycontlr[0].oacs = id_control.oacs;
    uint16_t oacs = identifycontlr[0].oacs;
    QString oacsstr = QString::number(oacs);
    ui->label_21->setStyleSheet("color: yellow");
    ui->label_21->setText(oacsstr);

    identifycontlr[0].acl= id_control.acl;
    uint8_t acl =  identifycontlr[0].acl;
    QString abortcmdlimit = QString::number(acl);
    ui->label_72->setStyleSheet("color: yellow");
    ui->label_72->setText(abortcmdlimit);

    identifycontlr[0].aerl= id_control.aerl;
    uint8_t aerl =  identifycontlr[0].aerl;
    QString asyncherL = QString::number(aerl);
    ui->label_43->setStyleSheet("color: yellow");
    ui->label_43->setText(asyncherL);

    identifycontlr[0].frmw = id_control.frmw;
    uint8_t frmw =  identifycontlr[0].aerl;
    QString frmwupd = QString::number(frmw);
    ui->label_193->setStyleSheet("color: yellow");
    ui->label_193->setText(frmwupd);

    identifycontlr[0].lpa = id_control.lpa;
    uint16_t lpa = identifycontlr[0].lpa;
    QString lpastr = QString::number(lpa);
    ui->label_38->setStyleSheet("color: yellow");
    ui->label_38->setText(lpastr);

    identifycontlr[0].elpe= id_control.elpe;
    uint8_t elpe =  identifycontlr[0].elpe;
    QString errorlpe = QString::number(elpe);
    ui->label_45->setStyleSheet("color: yellow");
    ui->label_45->setText(errorlpe);

    identifycontlr[0].npss = id_control.npss;
    uint8_t npss = identifycontlr[0].npss;
    QString npssstr = QString::number(npss);
    ui->label_20->setStyleSheet("color: yellow");
    ui->label_20->setText(npssstr);

    identifycontlr[0].avscc= id_control.avscc;
    uint8_t avscc =  identifycontlr[0].avscc;
    QString adminvscc = QString::number(avscc);
    ui->label_49->setStyleSheet("color: yellow");
    ui->label_49->setText(adminvscc);

    identifycontlr[0].apsta = id_control.apsta;
    uint8_t apsta = identifycontlr[0].apsta;
    QString apstatrans = QString::number(apsta);
    ui->label_194->setStyleSheet("color: yellow");
    ui->label_194->setText(apstatrans);

    identifycontlr[0].wctemp = id_control.wctemp;
    uint16_t wctemp = identifycontlr[0].wctemp;
    QString wcomptemp = QString::number(wctemp);
    ui->label_195->setStyleSheet("color: yellow");
    ui->label_195->setText(wcomptemp);

    identifycontlr[0].cctemp = id_control.cctemp;
    uint16_t cctemp = identifycontlr[0].cctemp;
    QString ccomptemp = QString::number(cctemp);
    ui->label_196->setStyleSheet("color: yellow");
    ui->label_196->setText(ccomptemp);

    identifycontlr[0].mtfa = id_control.mtfa;
    uint16_t mtfa = identifycontlr[0].mtfa;
    QString mtimefa = QString::number(mtfa);
    ui->label_197->setStyleSheet("color: yellow");
    ui->label_197->setText(mtimefa);

    identifycontlr[0].hmpre = id_control.hmpre;
    uint32_t hmpre = identifycontlr[0].hmpre;
    QString hmbuffpre = QString::number(hmpre);
    ui->label_198->setStyleSheet("color: yellow");
    ui->label_198->setText(hmbuffpre);

    identifycontlr[0].hmmin = id_control.hmmin;
    uint32_t hmmin = identifycontlr[0].hmmin;
    QString hmbuffmin = QString::number(hmmin);
    ui->label_199->setStyleSheet("color: yellow");
    ui->label_199->setText(hmbuffmin);

    QString tnvmcaphi, tnvmcaplo;
    identifycontlr[0].tnvmcap.hi = id_control.tnvmcap.hi;
    identifycontlr[0].tnvmcap.lo = id_control.tnvmcap.lo;
    uint64_t hi = identifycontlr[0].tnvmcap.hi;
    uint64_t lo = identifycontlr[0].tnvmcap.lo;
    tnvmcaphi = QString::number(hi);
    tnvmcaplo = QString::number(lo);
    QString tnvmhi = QString(tnvmcaphi).append(tnvmcaplo);
    ui->label_32->setStyleSheet("color: yellow");
    ui->label_32->setText(rtd3rstr);

    QString unvmcaphi, unvmcaplo;
    identifycontlr[0].unvmcap.hi = id_control.unvmcap.hi;
    identifycontlr[0].unvmcap.lo = id_control.unvmcap.lo;
    uint64_t unvmhi = identifycontlr[0].unvmcap.hi;
    uint64_t unvmlo = identifycontlr[0].unvmcap.lo;
    unvmcaphi = QString::number(unvmhi);
    unvmcaplo = QString::number(unvmlo);
    QString unvm = QString(unvmcaphi).append(unvmcaplo);
    ui->label_30->setStyleSheet("color: yellow");
    ui->label_30->setText(unvm);

    identifycontlr[0].rpmbs = id_control.rpmbs;
    uint32_t rpmbs = identifycontlr[0].rpmbs;
    QString rpmbsstr = QString::number(rpmbs);
    ui->label_28->setStyleSheet("color: yellow");
    ui->label_28->setText(rpmbsstr);

    identifycontlr[0].sqes = id_control.sqes;
    uint8_t sqes = identifycontlr[0].sqes;
    QString sqesstr = QString::number(sqes);
    ui->label_35->setStyleSheet("color: yellow");
    ui->label_35->setText(sqesstr);

    identifycontlr[0].cqes = id_control.cqes;
    uint8_t cqes = identifycontlr[0].cqes;
    QString compqes = QString::number(cqes);
    ui->label_204->setStyleSheet("color: yellow");
    ui->label_204->setText(compqes);

    identifycontlr[0].nn = id_control.nn;
    uint32_t nn = identifycontlr[0].nn;
    QString nnstr = QString::number(nn);
    ui->label_19->setStyleSheet("color: yellow");
    ui->label_19->setText(nnstr);

    identifycontlr[0].oncs = id_control.oncs;
    uint16_t oncs = identifycontlr[0].oncs;
    QString oncsstr = QString::number(oncs);
    ui->label_29->setStyleSheet("color: yellow");
    ui->label_29->setText(oncsstr);

    identifycontlr[0].fuses = id_control.fuses;
    uint16_t fuses = identifycontlr[0].fuses;
    QString fusesupp = QString::number(fuses);
    ui->label_205->setStyleSheet("color: yellow");
    ui->label_205->setText(fusesupp);

    identifycontlr[0].fna = id_control.fna;
    uint8_t fna = identifycontlr[0].fna;
    QString fnatt = QString::number(fna);
    ui->label_206->setStyleSheet("color: yellow");
    ui->label_206->setText(fnatt);

    identifycontlr[0].vwc = id_control.vwc;
    uint32_t vwc = identifycontlr[0].vwc;
    QString vwcstr = QString::number(vwc);
    ui->label_41->setStyleSheet("color: yellow");
    ui->label_41->setText(vwcstr);

    identifycontlr[0].awun = id_control.awun;
    uint16_t awun = identifycontlr[0].awun;
    QString awunorm = QString::number(awun);
    ui->label_207->setStyleSheet("color: yellow");
    ui->label_207->setText(awunorm);

    identifycontlr[0].awupf = id_control.awupf;
    uint16_t awupf = identifycontlr[0].awupf;
    QString awupower = QString::number(awupf);
    ui->label_211->setStyleSheet("color: yellow");
    ui->label_211->setText(awupower);

    identifycontlr[0].nvscc = id_control.nvscc;
    uint8_t nvncc = identifycontlr[0].nvscc;
    QString nvnccstr = QString::number(nvncc);
    ui->label_24->setStyleSheet("color: yellow");
    ui->label_24->setText(nvnccstr);

    identifycontlr[0].acwu = id_control.acwu;
    uint16_t acwu = identifycontlr[0].acwu;
    QString acwunit = QString::number(acwu);
    ui->label_212->setStyleSheet("color: yellow");
    ui->label_212->setText(acwunit);

    identifycontlr[0].sgls = id_control.sgls;
    uint32_t sgls = identifycontlr[0].sgls;
    QString sglsupp = QString::number(sgls);
    ui->label_213->setStyleSheet("color: yellow");
    ui->label_213->setText(sglsupp);

    QString subnqnstr;
    for (int i = 0; i < 32; i++) // 256 orig, should be nvme 1.3
    {
        identifycontlr[0].subnqn[i] = id_control.subnqn[i];

        uint8_t subnqn = identifycontlr[0].subnqn[i];
        char subnqno = static_cast<char>(subnqn);
        subnqnstr[i] = subnqno;
    }
    ui->label_33->setStyleSheet("color: yellow");
    ui->label_33->setText(subnqnstr);

    int i;
    for (i = 0; i < 3; i++) // 32 orig
    {
        identifycontlr[0].psdx[i].mp = id_control.psdx[i].mp;
        identifycontlr[0].psdx[i].mps = id_control.psdx[i].mps;
        identifycontlr[0].psdx[i].nops = id_control.psdx[i].nops;
        identifycontlr[0].psdx[i].enlat = id_control.psdx[i].enlat;
        identifycontlr[0].psdx[i].exlat = id_control.psdx[i].exlat;
        identifycontlr[0].psdx[i].rrt = id_control.psdx[i].rrt;
        identifycontlr[0].psdx[i].rrl = id_control.psdx[i].rrl;
        identifycontlr[0].psdx[i].rwt= id_control.psdx[i].rwt;
        identifycontlr[0].psdx[i].rwl = id_control.psdx[i].rwl;
        identifycontlr[0].psdx[i].idlp = id_control.psdx[i].idlp;
        identifycontlr[0].psdx[i].ips = id_control.psdx[i].ips;
        identifycontlr[0].psdx[i].actp = id_control.psdx[i].actp;
        identifycontlr[0].psdx[i].apw = id_control.psdx[i].apw;
        identifycontlr[0].psdx[i].aps = id_control.psdx[i].aps;
    }

    i = 0;
    uint16_t mp0 = identifycontlr[0].psdx[i].mp;
    QString mpower0 = QString::number(mp0);
    ui->label_47->setStyleSheet("color: yellow");
    ui->label_47->setText("MP0: "+mpower0);

    uint16_t mps0 = identifycontlr[0].psdx[i].mps;
    QString mpscale0 = QString::number(mps0);
    ui->label_48->setStyleSheet("color: yellow");
    ui->label_48->setText("MPS0: "+mpscale0);

    uint16_t nops0 = identifycontlr[0].psdx[i].nops;
    QString nopstate0 = QString::number(nops0);
    ui->label_175->setStyleSheet("color: yellow");
    ui->label_175->setText("NOPS0: "+nopstate0);

    uint32_t enlat0 = identifycontlr[0].psdx[i].enlat;
    QString enlatency0 = QString::number(enlat0);
    ui->label_183->setStyleSheet("color: yellow");
    ui->label_183->setText("ENLAT0: "+enlatency0);

    uint32_t exlat0 = identifycontlr[0].psdx[i].exlat;
    QString exlatency0 = QString::number(exlat0);
    ui->label_184->setStyleSheet("color: yellow");
    ui->label_184->setText("EXLAT0: "+exlatency0);

    uint8_t rrt0 = identifycontlr[0].psdx[i].rrt;
    QString rrthrough0 = QString::number(rrt0);
    ui->label_185->setStyleSheet("color: yellow");
    ui->label_185->setText("RRT0: "+rrthrough0);

    uint8_t rrl0 = identifycontlr[0].psdx[i].rrl;
    QString rrlatency0 = QString::number(rrl0);
    ui->label_186->setStyleSheet("color: yellow");
    ui->label_186->setText("RRL0: "+rrlatency0);

    uint8_t rwt0 = identifycontlr[0].psdx[i].rwt;
    QString rwthrough0 = QString::number(rwt0);
    ui->label_192->setStyleSheet("color: yellow");
    ui->label_192->setText("RWT0: "+rwthrough0);

    uint8_t rwl0 = identifycontlr[0].psdx[i].rwl;
    QString rwlatency0 = QString::number(rwl0);
    ui->label_200->setStyleSheet("color: yellow");
    ui->label_200->setText("RWL0: "+rwlatency0);

    uint16_t idlp0 = identifycontlr[0].psdx[i].idlp;
    QString idlpower0 = QString::number(idlp0);
    ui->label_201->setStyleSheet("color: yellow");
    ui->label_201->setText("IDLP0: "+idlpower0);

    uint16_t ips0 = identifycontlr[0].psdx[i].ips;
    QString ipscale0 = QString::number(ips0);
    ui->label_202->setStyleSheet("color: yellow");
    ui->label_202->setText("IPS0: "+ipscale0);

    uint16_t actp0 = identifycontlr[0].psdx[i].actp;
    QString actpower0 = QString::number(actp0);
    ui->label_218->setStyleSheet("color: yellow");
    ui->label_218->setText("ACTP0: "+actpower0);

    uint8_t apw0 = identifycontlr[0].psdx[i].apw;
    QString apworkload0 = QString::number(apw0);
    ui->label_219->setStyleSheet("color: yellow");
    ui->label_219->setText("APW0: "+apworkload0);

    uint8_t aps0 = identifycontlr[0].psdx[i].aps;
    QString apscale0 = QString::number(aps0);
    ui->label_220->setStyleSheet("color: yellow");
    ui->label_220->setText("APS0: "+apscale0);

    i++;
    uint16_t mp1 = identifycontlr[0].psdx[i].mp;
    QString mpower1 = QString::number(mp1);
    ui->label_203->setStyleSheet("color: yellow");
    ui->label_203->setText("MP1: "+mpower1);

    uint16_t mps1 = identifycontlr[0].psdx[i].mps;
    QString mpscale1 = QString::number(mps1);
    ui->label_214->setStyleSheet("color: yellow");
    ui->label_214->setText("MPS1: "+mpscale1);

    uint16_t nops1 = identifycontlr[0].psdx[i].nops;
    QString nopstate1 = QString::number(nops1);
    ui->label_215->setStyleSheet("color: yellow");
    ui->label_215->setText("NOPS1: "+nopstate1);

    uint32_t enlat1 = identifycontlr[0].psdx[i].enlat;
    QString enlatency1 = QString::number(enlat1);
    ui->label_222->setStyleSheet("color: yellow");
    ui->label_222->setText("ENLAT1: "+enlatency1);

    uint32_t exlat1 = identifycontlr[0].psdx[i].exlat;
    QString exlatency1 = QString::number(exlat1);
    ui->label_223->setStyleSheet("color: yellow");
    ui->label_223->setText("EXLAT1: "+exlatency1);

    uint8_t rrt1 = identifycontlr[0].psdx[i].rrt;
    QString rrthrough1 = QString::number(rrt1);
    ui->label_224->setStyleSheet("color: yellow");
    ui->label_224->setText("RRT1: "+rrthrough1);

    uint8_t rrl1 = identifycontlr[0].psdx[i].rrl;
    QString rrlatency1 = QString::number(rrl1);
    ui->label_225->setStyleSheet("color: yellow");
    ui->label_225->setText("RRL1: "+rrlatency1);

    uint8_t rwt1 = identifycontlr[0].psdx[i].rwt;
    QString rwthrough1 = QString::number(rwt1);
    ui->label_226->setStyleSheet("color: yellow");
    ui->label_226->setText("RWT1: "+rwthrough1);

    uint8_t rwl1 = identifycontlr[0].psdx[i].rwl;
    QString rwlatency1 = QString::number(rwl1);
    ui->label_227->setStyleSheet("color: yellow");
    ui->label_227->setText("RWL1: "+rwlatency1);

    uint16_t idlp1 = identifycontlr[0].psdx[i].idlp;
    QString idlpower1 = QString::number(idlp1);
    ui->label_228->setStyleSheet("color: yellow");
    ui->label_228->setText("IDLP1: "+idlpower1);

    uint16_t ips1 = identifycontlr[0].psdx[i].ips;
    QString ipscale1 = QString::number(ips1);
    ui->label_39->setStyleSheet("color: yellow");
    ui->label_39->setText("IPS1: "+ipscale1);

    uint16_t actp1 = identifycontlr[0].psdx[i].actp;
    QString actpower1 = QString::number(actp1);
    ui->label_239->setStyleSheet("color: yellow");
    ui->label_239->setText("ACTP1: "+actpower1);

    uint8_t apw1 = identifycontlr[0].psdx[i].apw;
    QString apworkload1 = QString::number(apw1);
    ui->label_231->setStyleSheet("color: yellow");
    ui->label_231->setText("APW1: "+apworkload1);

    uint8_t aps1 = identifycontlr[0].psdx[i].aps;
    QString apscale1 = QString::number(aps1);
    ui->label_232->setStyleSheet("color: yellow");
    ui->label_232->setText("APS1: "+apscale1);

    i++;
    uint16_t mp2 = identifycontlr[0].psdx[i].mp;
    QString mpower2 = QString::number(mp2);
    ui->label_217->setStyleSheet("color: yellow");
    ui->label_217->setText("MP2: "+mpower2);

    uint16_t mps2 = identifycontlr[0].psdx[i].mps;
    QString mpscale2 = QString::number(mps2);
    ui->label_221->setStyleSheet("color: yellow");
    ui->label_221->setText("MPS2: "+mpscale2);

    uint16_t nops2 = identifycontlr[0].psdx[i].nops;
    QString nopstate2 = QString::number(nops2);
    ui->label_229->setStyleSheet("color: yellow");
    ui->label_229->setText("NOPS2: "+nopstate2);

    uint32_t enlat2 = identifycontlr[0].psdx[i].enlat;
    QString enlatency2 = QString::number(enlat2);
    ui->label_233->setStyleSheet("color: yellow");
    ui->label_233->setText("ENLAT2: "+enlatency2);

    uint32_t exlat2 = identifycontlr[0].psdx[i].exlat;
    QString exlatency2 = QString::number(exlat2);
    ui->label_234->setStyleSheet("color: yellow");
    ui->label_234->setText("EXLAT2: "+exlatency2);

    uint8_t rrt2 = identifycontlr[0].psdx[i].rrt;
    QString rrthrough2 = QString::number(rrt2);
    ui->label_235->setStyleSheet("color: yellow");
    ui->label_235->setText("RRT2: "+rrthrough2);

    uint8_t rrl2 = identifycontlr[0].psdx[i].rrl;
    QString rrlatency2= QString::number(rrl2);
    ui->label_236->setStyleSheet("color: yellow");
    ui->label_236->setText("RRL2: "+rrlatency2);

    uint8_t rwt2 = identifycontlr[0].psdx[i].rwt;
    QString rwthrough2 = QString::number(rwt2);
    ui->label_237->setStyleSheet("color: yellow");
    ui->label_237->setText("RWT2: "+rwthrough2);

    uint8_t rwl2 = identifycontlr[0].psdx[i].rwl;
    QString rwlatency2 = QString::number(rwl2);
    ui->label_238->setStyleSheet("color: yellow");
    ui->label_238->setText("RWL2: "+rwlatency2);

    uint16_t idlp2 = identifycontlr[0].psdx[i].idlp;
    QString idlpower2 = QString::number(idlp2);
    ui->label_239->setStyleSheet("color: yellow");
    ui->label_239->setText("IDLP2: "+idlpower2);

    uint16_t ips2 = identifycontlr[0].psdx[i].ips;
    QString ipscale2 = QString::number(ips2);
    ui->label_240->setStyleSheet("color: yellow");
    ui->label_240->setText("IPS2: "+ipscale2);

    uint16_t actp2 = identifycontlr[0].psdx[i].actp;
    QString actpower2 = QString::number(actp2);
    ui->label_241->setStyleSheet("color: yellow");
    ui->label_241->setText("ACT2: "+actpower2);

    uint8_t apw2 = identifycontlr[0].psdx[i].apw;
    QString apworkload2 = QString::number(apw2);
    ui->label_242->setStyleSheet("color: yellow");
    ui->label_242->setText("APW2: "+apworkload2);

    uint8_t aps2 = identifycontlr[0].psdx[i].aps;
    QString apscale2 = QString::number(aps2);
    ui->label_243->setStyleSheet("color: yellow");
    ui->label_243->setText("APS2: "+apscale2);

    i++;
    uint16_t mp3 = identifycontlr[0].psdx[i].mp;
    QString mpower3 = QString::number(mp3);
    ui->label_245->setStyleSheet("color: yellow");
    ui->label_245->setText("MP3: "+mpower3);

    uint16_t mps3 = identifycontlr[0].psdx[i].mps;
    QString mpscale3 = QString::number(mps3);
    ui->label_246->setStyleSheet("color: yellow");
    ui->label_246->setText("MPS3: "+mpscale3);

    uint16_t nops3 = identifycontlr[0].psdx[i].nops;
    QString nopstate3 = QString::number(nops3);
    ui->label_247->setStyleSheet("color: yellow");
    ui->label_247->setText("NOPS3: "+nopstate3);

    uint32_t enlat3 = identifycontlr[0].psdx[i].enlat;
    QString enlatency3 = QString::number(enlat3);
    ui->label_248->setStyleSheet("color: yellow");
    ui->label_248->setText("ENLAT3: "+enlatency3);

    uint32_t exlat3 = identifycontlr[0].psdx[i].exlat;
    QString exlatency3 = QString::number(exlat3);
    ui->label_249->setStyleSheet("color: yellow");
    ui->label_249->setText("EXLAT3: "+exlatency3);

    uint8_t rrt3 = identifycontlr[0].psdx[i].rrt;
    QString rrthrough3 = QString::number(rrt3);
    ui->label_250->setStyleSheet("color: yellow");
    ui->label_250->setText("RRT3: "+rrthrough3);

    uint8_t rrl3 = identifycontlr[0].psdx[i].rrl;
    QString rrlatency3 = QString::number(rrl3);
    ui->label_251->setStyleSheet("color: yellow");
    ui->label_251->setText("RRL3: "+rrlatency3);

    uint8_t rwt3 = identifycontlr[0].psdx[i].rwt;
    QString rwthrough3 = QString::number(rwt3);
    ui->label_252->setStyleSheet("color: yellow");
    ui->label_252->setText("RWT3: "+rwthrough3);

    uint8_t rwl3 = identifycontlr[0].psdx[i].rwl;
    QString rwlatency3 = QString::number(rwl3);
    ui->label_253->setStyleSheet("color: yellow");
    ui->label_253->setText("RWL3: "+rwlatency3);

    uint16_t idlp3 = identifycontlr[0].psdx[i].idlp;
    QString idlpower3 = QString::number(idlp3);
    ui->label_254->setStyleSheet("color: yellow");
    ui->label_254->setText("IDLP3: "+idlpower3);

    uint16_t ips3 = identifycontlr[0].psdx[i].ips;
    QString ipscale3 = QString::number(ips3);
    ui->label_255->setStyleSheet("color: yellow");
    ui->label_255->setText("IPS3: "+ipscale3);

    uint16_t actp3 = identifycontlr[0].psdx[i].actp;
    QString actpower3 = QString::number(actp3);
    ui->label_256->setStyleSheet("color: yellow");
    ui->label_256->setText("ACTP3: "+actpower3);

    uint8_t apw3 = identifycontlr[0].psdx[i].apw;
    QString apworkload3 = QString::number(apw3);
    ui->label_257->setStyleSheet("color: yellow");
    ui->label_257->setText("APW3: "+apworkload3);

    uint8_t aps3 = identifycontlr[0].psdx[i].aps;
    QString apscale3 = QString::number(aps3);
    ui->label_258->setStyleSheet("color: yellow");
    ui->label_258->setText("APS3: "+apscale3);


    int idx;
    for (idx = 0; idx < 9; idx++)  // 1024 orig
    {
        identifycontlr[0].vs[idx] = id_control.vs[idx];
    }

    idx = 0;
    uint8_t vs0 = identifycontlr[0].vs[idx];
    QString vspec0 = QString::number(vs0);
    ui->label_259->setStyleSheet("color: yellow");
    ui->label_259->setText("VS0: "+vspec0);

    idx++;
    uint8_t vs1 = identifycontlr[0].vs[idx];
    QString vspec1 = QString::number(vs1);
    ui->label_260->setStyleSheet("color: yellow");
    ui->label_260->setText("VS1: "+vspec1);

    idx++;
    uint8_t vs2 = identifycontlr[0].vs[idx];
    QString vspec2 = QString::number(vs2);
    ui->label_261->setStyleSheet("color: yellow");
    ui->label_261->setText("VS2: "+vspec2);

    idx++;
    uint8_t vs3 = identifycontlr[0].vs[idx];
    QString vspec3 = QString::number(vs3);
    ui->label_262->setStyleSheet("color: yellow");
    ui->label_262->setText("VS3: "+vspec3);

    idx++;
    uint8_t vs4 = identifycontlr[0].vs[idx];
    QString vspec4 = QString::number(vs4);
    ui->label_263->setStyleSheet("color: yellow");
    ui->label_263->setText("VS4: "+vspec4);

    idx++;
    uint8_t vs5 = identifycontlr[0].vs[idx];
    QString vspec5 = QString::number(vs5);
    ui->label_264->setStyleSheet("color: yellow");
    ui->label_264->setText("VS5: "+vspec5);

    idx++;
    uint8_t vs6 = identifycontlr[0].vs[idx];
    QString vspec6 = QString::number(vs6);
    ui->label_265->setStyleSheet("color: yellow");
    ui->label_265->setText("VS6: "+vspec6);

    idx++;
    uint8_t vs7 = identifycontlr[0].vs[idx];
    QString vspec7 = QString::number(vs7);
    ui->label_266->setStyleSheet("color: yellow");
    ui->label_266->setText("VS7: "+vspec7);

    idx++;
    uint8_t vs8 = identifycontlr[0].vs[idx];
    QString vspec8 = QString::number(vs8);
    ui->label_267->setStyleSheet("color: yellow");
    ui->label_267->setText("VS8: "+vspec8);

    idx++;
    uint8_t vs9 = identifycontlr[0].vs[idx];
    QString vspec9 = QString::number(vs9);
    ui->label_268->setStyleSheet("color: yellow");
    ui->label_268->setText("VS9: "+vspec9);



/*  nvme 1.3
    configDialog->configdata->cfgoption[0].identifycontlr.ctratt = id_control.ctratt;

    for (int i = 0; i < 16; i++)
    {
        configDialog->configdata->cfgoption[0].identifycontlr.fguid[i] = id_control.fguid[i];

        uint64_t fguid = configDialog->configdata->cfgoption[0].identifycontlr.fguid[i];
        QString fguidstr = QString::number(fguid);
        fguidstr = QString(fguidstr).append(fguidstr);
    }

    configDialog->configdata->cfgoption[0].identifycontlr.edstt = id_control.edstt;
    configDialog->configdata->cfgoption[0].identifycontlr.dsto = id_control.dsto;
    configDialog->configdata->cfgoption[0].identifycontlr.fwug = id_control.fwug;
    configDialog->configdata->cfgoption[0].identifycontlr.hctma = id_control.hctma;

    identifycontlr[0].kas = id_control.kas;
    uint16_t kas = identifycontlr[0].kas;
    QString kasstr = QString::number(kas);
    ui->label_40->setStyleSheet("color: yellow");
    ui->label_40->setText(kasstr);

    configDialog->configdata->cfgoption[0].identifycontlr.hctma = id_control.hctma;

    identifycontlr[0].mntmt = id_control.mntmt;
    uint16_t mntmt = identifycontlr[0].mntmt;
    QString mntmtstr = QString::number(mntmt);
    ui->label_48->setStyleSheet("color: yellow");
    ui->label_48->setText(mntmtstr);

    identifycontlr[0].mxtmt= id_control.mxtmt;
    uint16_t mxtmt = identifycontlr[0].mxtmt;
    QString mxtmtstr = QString::number(mxtmt);
    ui->label_47->setStyleSheet("color: yellow");
    ui->label_47->setText(mxtmtstr);

    identifycontlr[0].sanicap = id_control.sanicap;
    uint32_t sanicap = identifycontlr[0].sanicap;
    QString sanicapstr = QString::number(sanicap);
    ui->label_25->setStyleSheet("color: yellow");
    ui->label_25->setText(sanicapstr);

    identifycontlr[0].maxcmd = id_control.maxcmd;
    uint16_t maxcmd = identifycontlr[0].maxcmd;
    QString maxcmdstr = QString::number(maxcmd);
    ui->label_39->setStyleSheet("color: yellow");
    ui->label_39->setText(maxcmdstr);

    configDialog->configdata->cfgoption[0].identifycontlr.hctma = id_control.nvme;

    identifycontlr[0].nvme_over_fabrics_resv[256] = id_control.nvme_over_fabrics_resv[256];
*/

/*
    uint32_t comptemp = xphyhealth[0].info1;
    comptemp = (comptemp & INFO1_COMPOSITE_TEMP_MASK) >> 8;
    float temp = comptemp - 273.15; // kelvin to celcius
    QString compositetemp = QString::number(temp);
//    ui->label_50->setStyleSheet("color: yellow");
//    ui->label_50->setText(compositetemp+" 'C");
    ui->label_13->setStyleSheet("color: yellow");
    ui->label_13->setText(compositetemp+" 'C");
*/
}

void MainWindow::get_smart_health_info(log_smart_info_struct smart_info_global)
{
//    QString logmsg;

    uint8_t critwarning = smart_info_global.criticalWarn;
    QString critwarn = QString::number(critwarning);
    ui->label_46->setStyleSheet("color: yellow");
    ui->label_46->setText(critwarn);

    uint8_t temp1 = smart_info_global.temperature[0];
    uint8_t temp2 = smart_info_global.temperature[1];
    float temp;
    temp = ((temp2 << 8) | temp1) - 273.15;
    QString compositetemp = QString::number(temp);
    ui->pushButton_4->setStyleSheet("color: yellow");
    ui->pushButton_4->setText(compositetemp+" 'C");

    uint8_t comptemp1 = smart_info_global.temperature[0];
    uint8_t comptemp2 = smart_info_global.temperature[1];
    uint16_t comptemperate1 = (comptemp2 << 8) | comptemp1;
    QString comptemperate2 = QString::number(comptemperate1);
    ui->label_52->setStyleSheet("color: yellow");
    ui->label_52->setText(comptemperate2);

    uint8_t availspare = smart_info_global.availableSpare;
    QString availsparetemp = QString::number(availspare);
    ui->label_53->setStyleSheet("color: yellow");
    ui->label_53->setText(availsparetemp);

    uint8_t availthresh = smart_info_global.avail_spare_threshold;
    QString availthreshold = QString::number(availthresh);
    ui->label_44->setStyleSheet("color: yellow");
    ui->label_44->setText(availthreshold);

    xphyhealth[0].percentage_used = smart_info_global.percentage_used;
    uint8_t percentuse = xphyhealth[0].percentage_used;
    QString percentused = QString::number(percentuse);
    ui->label_55->setStyleSheet("color: yellow");
    ui->label_55->setText(percentused);

    xphyhealth[0].data_units_read.hi = smart_info_global.data_units_read.hi;
    xphyhealth[0].data_units_read.lo = smart_info_global.data_units_read.lo;
    uint64_t dataunitsread = xphyhealth[0].data_units_read.lo;
    QString dataunitsreadlo = QString::number(dataunitsread);
    ui->label_56->setStyleSheet("color: yellow");
    ui->label_56->setText(dataunitsreadlo);

    xphyhealth[0].data_units_written.hi = smart_info_global.data_units_written.hi;
    xphyhealth[0].data_units_written.lo = smart_info_global.data_units_written.lo;
    uint64_t dataunitswrite = xphyhealth[0].data_units_written.lo;
    QString dataunitswritelo = QString::number(dataunitswrite);
    ui->label_57->setStyleSheet("color: yellow");
    ui->label_57->setText(dataunitswritelo);

    xphyhealth[0].host_read_commands.hi = smart_info_global.host_read_commands.hi;
    xphyhealth[0].host_read_commands.lo = smart_info_global.host_read_commands.lo;
    uint64_t hostreadcmd = xphyhealth[0].host_read_commands.lo;
    QString hostreadcmdlo = QString::number(hostreadcmd);
    ui->label_58->setStyleSheet("color: yellow");
    ui->label_58->setText(hostreadcmdlo);

    xphyhealth[0].host_write_commands.hi = smart_info_global.host_write_commands.hi;
    xphyhealth[0].host_write_commands.lo = smart_info_global.host_write_commands.lo;
    uint64_t hostwritecmd = xphyhealth[0].host_write_commands.lo;
    QString hostwritecmdlo = QString::number(hostwritecmd);
    ui->label_59->setStyleSheet("color: yellow");
    ui->label_59->setText(hostwritecmdlo);

    xphyhealth[0].controller_busy_time.hi = smart_info_global.controller_busy_time.hi;
    xphyhealth[0].controller_busy_time.lo = smart_info_global.controller_busy_time.lo;
    uint64_t controlbusytime = xphyhealth[0].controller_busy_time.lo;
    QString controlbusytimelo = QString::number(controlbusytime);
    ui->label_76->setStyleSheet("color: yellow");
    ui->label_76->setText(controlbusytimelo);

    xphyhealth[0].power_cycles.hi = smart_info_global.power_cycles.hi;
    xphyhealth[0].power_cycles.lo = smart_info_global.power_cycles.lo;
    uint64_t powercyc = xphyhealth[0].power_cycles.lo;
    QString powercyclo = QString::number(powercyc);
    ui->label_63->setStyleSheet("color: yellow");
    ui->label_63->setText(powercyclo);

    xphyhealth[0].power_on_hours.hi = smart_info_global.power_on_hours.hi;
    xphyhealth[0].power_on_hours.lo = smart_info_global.power_on_hours.lo;
    uint64_t poweronhour = xphyhealth[0].power_on_hours.lo;
    QString poweronhourlo = QString::number(poweronhour);
    ui->label_62->setStyleSheet("color: yellow");
    ui->label_62->setText(poweronhourlo);

    xphyhealth[0].unsafe_shutdowns.hi = smart_info_global.unsafe_shutdowns.hi;
    xphyhealth[0].unsafe_shutdowns.lo = smart_info_global.unsafe_shutdowns.lo;
    uint64_t unsafeshutdown = xphyhealth[0].unsafe_shutdowns.lo;
    QString unsafeshutdownlo = QString::number(unsafeshutdown);
    ui->label_51->setStyleSheet("color: yellow");
    ui->label_51->setText(unsafeshutdownlo);

    xphyhealth[0].data_integrity_errors.hi = smart_info_global.data_integrity_errors.hi;
    xphyhealth[0].data_integrity_errors.lo = smart_info_global.data_integrity_errors.lo;
    uint64_t datainterror = xphyhealth[0].data_integrity_errors.lo;
    QString datainterrorlo = QString::number(datainterror);
    ui->label_64->setStyleSheet("color: yellow");
    ui->label_64->setText(datainterrorlo);

    xphyhealth[0].number_of_error_info_log_entries.hi = smart_info_global.number_of_error_info_log_entries.hi;
    xphyhealth[0].number_of_error_info_log_entries.lo = smart_info_global.number_of_error_info_log_entries.lo;
    uint64_t numberlogerr = xphyhealth[0].number_of_error_info_log_entries.lo;
    QString numberlogerrlo = QString::number(numberlogerr);
    ui->label_79->setStyleSheet("color: yellow");
    ui->label_79->setText(numberlogerrlo);

    xphyhealth[0].warning_composite_temp_time = smart_info_global.warning_composite_temp_time;
    uint32_t warncomptemptime = xphyhealth[0].warning_composite_temp_time;
    QString warningcomptemptime = QString::number(warncomptemptime);
    ui->label_66->setStyleSheet("color: yellow");
    ui->label_66->setText(warningcomptemptime);

    xphyhealth[0].critical_composite_temp_time = smart_info_global.critical_composite_temp_time;
    uint32_t critcomptemptime = xphyhealth[0].critical_composite_temp_time;
    QString criticalcomptemptime = QString::number(critcomptemptime);
    ui->label_67->setStyleSheet("color: yellow");
    ui->label_67->setText(criticalcomptemptime);

    xphyhealth[0].temp_sensor[0].tst = smart_info_global.temp_sensor[0].tst;
    uint16_t temp_sensor0 = xphyhealth[0].temp_sensor[0].tst;
    QString tempsensor0 = QString::number(temp_sensor0);
    ui->label_68->setStyleSheet("color: yellow");
    ui->label_68->setText(tempsensor0);

    xphyhealth[0].temp_sensor[1].tst = smart_info_global.temp_sensor[1].tst;
    uint16_t temp_sensor1 = xphyhealth[0].temp_sensor[1].tst;
    QString tempsensor1 = QString::number(temp_sensor1);
    ui->label_80->setStyleSheet("color: yellow");
    ui->label_80->setText(tempsensor1);

    xphyhealth[0].temp_sensor[2].tst = smart_info_global.temp_sensor[2].tst;
    uint16_t temp_sensor2 = xphyhealth[0].temp_sensor[2].tst;
    QString tempsensor2 = QString::number(temp_sensor2);
    ui->label_87->setStyleSheet("color: yellow");
    ui->label_87->setText(tempsensor2);

    xphyhealth[0].temp_sensor[3].tst = smart_info_global.temp_sensor[3].tst;
    uint16_t temp_sensor3 = xphyhealth[0].temp_sensor[3].tst;
    QString tempsensor3 = QString::number(temp_sensor3);
    ui->label_88->setStyleSheet("color: yellow");
    ui->label_88->setText(tempsensor3);

    xphyhealth[0].temp_sensor[4].tst = smart_info_global.temp_sensor[4].tst;
    uint16_t temp_sensor4 = xphyhealth[0].temp_sensor[4].tst;
    QString tempsensor4 = QString::number(temp_sensor4);
    ui->label_89->setStyleSheet("color: yellow");
    ui->label_89->setText(tempsensor4);

    xphyhealth[0].temp_sensor[5].tst = smart_info_global.temp_sensor[5].tst;
    uint16_t temp_sensor5 = xphyhealth[0].temp_sensor[5].tst;
    QString tempsensor5 = QString::number(temp_sensor5);
    ui->label_90->setStyleSheet("color: yellow");
    ui->label_90->setText(tempsensor5);

    xphyhealth[0].temp_sensor[6].tst = smart_info_global.temp_sensor[6].tst;
    uint16_t temp_sensor6 = xphyhealth[0].temp_sensor[6].tst;
    QString tempsensor6 = QString::number(temp_sensor6);
    ui->label_91->setStyleSheet("color: yellow");
    ui->label_91->setText(tempsensor6);

    xphyhealth[0].temp_sensor[7].tst = smart_info_global.temp_sensor[7].tst;
    uint16_t temp_sensor7 = xphyhealth[0].temp_sensor[7].tst;
    QString tempsensor7 = QString::number(temp_sensor1);
    ui->label_92->setStyleSheet("color: yellow");
    ui->label_92->setText(tempsensor7);

    xphyhealth[0].thermal_mgmt_temp_transition_count[0] = smart_info_global.thermal_mgmt_temp_transition_count[0];
    uint32_t tmttc1 = xphyhealth[0].thermal_mgmt_temp_transition_count[0];
    QString tmtemp1 = QString::number(tmttc1);
    ui->label_273->setStyleSheet("color: yellow");
    ui->label_273->setText(tmtemp1);

    xphyhealth[0].thermal_mgmt_temp_transition_count[1] = smart_info_global.thermal_mgmt_temp_transition_count[1];
    uint32_t tmttc2 = xphyhealth[0].thermal_mgmt_temp_transition_count[0];
    QString tmtemp2 = QString::number(tmttc2);
    ui->label_274->setStyleSheet("color: yellow");
    ui->label_274->setText(tmtemp2);

    xphyhealth[0].total_time_for_thermal_mgmt_temp[0] = smart_info_global.total_time_for_thermal_mgmt_temp[0];
    uint32_t ttftmt1 = xphyhealth[0].thermal_mgmt_temp_transition_count[0];
    QString ttftmtemp1 = QString::number(ttftmt1);
    ui->label_275->setStyleSheet("color: yellow");
    ui->label_275->setText(ttftmtemp1);

    xphyhealth[0].total_time_for_thermal_mgmt_temp[1] = smart_info_global.total_time_for_thermal_mgmt_temp[1];
    uint32_t ttftmt2 = xphyhealth[0].thermal_mgmt_temp_transition_count[0];
    QString ttftmtemp2 = QString::number(ttftmt2);
    ui->label_276->setStyleSheet("color: yellow");
    ui->label_276->setText(ttftmtemp2);
}

void MainWindow::display_miscdata(identify_namespace_struct id_namespace)
{
    /*
    ui->pushButton_2->setStyleSheet("color: yellow");
    ui->pushButton_2->setText("2.5");

    ui->pushButton_3->setStyleSheet("color: yellow");
    ui->pushButton_3->setText("4.5");

    ui->pushButton_4->setStyleSheet("color: yellow");
    ui->pushButton_4->setText("6.5");
    */

    int lbasizeidx = id_namespace.flbas;    // formatted lba size
    int lbasize = 1 << id_namespace.lbaf[lbasizeidx].lbads;

    double diskcapinbytes = id_namespace.ncap/lbasize;
    uint64_t sizegb = diskcapinbytes/(1024*4);

    double totcapacity = (512 * 1024) * 1024;
    uint64_t freespace = (sizegb * 1024) * 1024;
    uint64_t remaining = totcapacity - freespace;

    // free space
    QString sizegigabyte = QString::number(sizegb);
    ui->pushButton_3->setStyleSheet("color: yellow");
    ui->pushButton_3->setText(sizegigabyte+" GB");

    // used space
    QString remaindrive = QString::number(remaining);
    ui->pushButton_2->setStyleSheet("color: yellow");
    ui->pushButton_2->setText(remaindrive+" B");
}

void MainWindow::get_id_namespace(identify_namespace_struct id_namespace)
{

    uint64_t nsze = id_namespace.nsze;
    QString nsize = QString::number(nsze);
    ui->label_106->setStyleSheet("color: yellow");
    ui->label_106->setText(nsize);

    uint64_t ncap = id_namespace.ncap;
    QString nscap = QString::number(ncap);
    ui->label_107->setStyleSheet("color: yellow");
    ui->label_107->setText(nscap);

    uint64_t nuse = id_namespace.nuse;
    QString nsutil = QString::number(nuse);
    ui->label_108->setStyleSheet("color: yellow");
    ui->label_108->setText(nsutil);

    uint8_t nsfeat = id_namespace.nsfeat;
    QString nsfea = QString::number(nsfeat);
    ui->label_109->setStyleSheet("color: yellow");
    ui->label_109->setText(nsfea);

    uint8_t nlbaf = id_namespace.nlbaf;
    QString nlbaformat = QString::number(nlbaf);
    ui->label_110->setStyleSheet("color: yellow");
    ui->label_110->setText(nlbaformat);

    uint8_t flbas = id_namespace.flbas;
    QString flbasize = QString::number(flbas);
    ui->label_111->setStyleSheet("color: yellow");
    ui->label_111->setText(flbasize);

    uint8_t mc = id_namespace.mc;
    QString mcap = QString::number(mc);
    ui->label_112->setStyleSheet("color: yellow");
    ui->label_112->setText(mcap);

    uint8_t dpc = id_namespace.dpc;
    QString dpcap = QString::number(dpc);
    ui->label_113->setStyleSheet("color: yellow");
    ui->label_113->setText(dpcap);

    uint8_t dps = id_namespace.dps;
    QString dpsetting = QString::number(dps);
    ui->label_114->setStyleSheet("color: yellow");
    ui->label_114->setText(dpsetting);

    uint8_t nmic = id_namespace.nmic;
    QString nmisharing = QString::number(nmic);
    ui->label_115->setStyleSheet("color: yellow");
    ui->label_115->setText(nmisharing);

    uint8_t rescap = id_namespace.rescap;
    QString rescapab = QString::number(rescap);
    ui->label_116->setStyleSheet("color: yellow");
    ui->label_116->setText(rescapab);

    uint8_t fpi = id_namespace.fpi;
    QString fpindicator = QString::number(fpi);
    ui->label_117->setStyleSheet("color: yellow");
    ui->label_117->setText(fpindicator);

    // added in 1.3, not included in 1.2
    uint8_t dlfeat = id_namespace.dlfeat;
    QString dlfeature = QString::number(dlfeat);
    ui->label_130->setStyleSheet("color: yellow");
    ui->label_130->setText(dlfeature);

    uint16_t nawun = id_namespace.nawun;
    QString nawunormal = QString::number(nawun);
    ui->label_131->setStyleSheet("color: yellow");
    ui->label_131->setText(nawunormal);

    uint16_t nawupf = id_namespace.nawupf;
    QString nawupfail = QString::number(nawupf);
    ui->label_132->setStyleSheet("color: yellow");
    ui->label_132->setText(nawupfail);

    uint16_t nacwu = id_namespace.nacwu;
    QString nacwunit = QString::number(nacwu);
    ui->label_133->setStyleSheet("color: yellow");
    ui->label_133->setText(nacwunit);

    uint16_t nabsn = id_namespace.nabsn;
    QString nabsnormal = QString::number(nabsn);
    ui->label_134->setStyleSheet("color: yellow");
    ui->label_134->setText(nabsnormal);

    uint16_t nabo = id_namespace.nabo;
    QString naboffset = QString::number(nabo);
    ui->label_135->setStyleSheet("color: yellow");
    ui->label_135->setText(naboffset);

    uint16_t nabspf = id_namespace.nabspf;
    QString nabspfail = QString::number(nabspf);
    ui->label_136->setStyleSheet("color: yellow");
    ui->label_136->setText(nabspfail);

    uint16_t noiob = id_namespace.noiob;
    QString noioboundary = QString::number(noiob);
    ui->label_137->setStyleSheet("color: yellow");
    ui->label_137->setText(noioboundary);

    uint64_t nvmcap2 = id_namespace.nvmcap[1];
    uint64_t nvmcap1 = id_namespace.nvmcap[0];
    QString nvmcaphi = QString::number(nvmcap2);
    QString nvmcaplo = QString::number(nvmcap1);
    QString nvmcap = QString(nvmcaphi).append(nvmcaplo);
    ui->label_138->setStyleSheet("color: yellow");
    ui->label_138->setText(nvmcap);

    QString nguidentifier;
    for (int i = 0; i < 16; i++)
    {
        uint8_t nguid = id_namespace.nguid[i];
//        char nguident = static_cast<char>(nguid);
//        nguidentifier[i] = nguident;
        nguidentifier[i] = nguid;
    }
    ui->label_139->setStyleSheet("color: yellow");
    ui->label_139->setText(nguidentifier);

    uint64_t eui64 = id_namespace.eui64;
    QString eui64ieee = QString::number(eui64);
    ui->label_140->setStyleSheet("color: yellow");
    ui->label_140->setText(eui64ieee);

    QString index, msstr, lbadsstr, rpstr;
    int i;
    for (i = 0; i < 16; i++)
    {
        uint16_t ms = id_namespace.lbaf[0].ms;
        msstr[i] = ms;

        uint8_t lbads = id_namespace.lbaf[0].lbads;
        lbadsstr[i] = lbads;

        uint16_t rp = id_namespace.lbaf[0].rp;
        rpstr[i] = rp;

        char idx = static_cast<char>(i);
        index[i] = idx;
        ui->label_141->setStyleSheet("color: yellow");
        ui->label_141->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);
    }

    i = 1;
    ui->label_157->setStyleSheet("color: yellow");
    ui->label_157->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_158->setStyleSheet("color: yellow");
    ui->label_158->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_159->setStyleSheet("color: yellow");
    ui->label_159->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_160->setStyleSheet("color: yellow");
    ui->label_160->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_161->setStyleSheet("color: yellow");
    ui->label_161->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_162->setStyleSheet("color: yellow");
    ui->label_162->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_163->setStyleSheet("color: yellow");
    ui->label_163->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_164->setStyleSheet("color: yellow");
    ui->label_164->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_165->setStyleSheet("color: yellow");
    ui->label_165->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_166->setStyleSheet("color: yellow");
    ui->label_166->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_167->setStyleSheet("color: yellow");
    ui->label_167->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_168->setStyleSheet("color: yellow");
    ui->label_168->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_169->setStyleSheet("color: yellow");
    ui->label_169->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_170->setStyleSheet("color: yellow");
    ui->label_170->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);

    i++;
    ui->label_171->setStyleSheet("color: yellow");
    ui->label_171->setText("LBAF "+index[i]+" ""ms: "+msstr[i]+"lbads:    "+lbadsstr[i]+"rp:   "+rpstr[i]);
}

#endif
