#include "encdecdialog.h"
#include "ui_encdecdialog.h"
#include <QMessageBox>
#include <QDateTime>
#include <QMediaPlayer>
#include <QFile>

//bool encdecns = false;
bool encdecnspacestate = false;
bool encenable = false;

bool fileenable = false;

encdecDialog::encdecDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::encdecDialog)
{
    ui->setupUi(this);

    progressbar = new QProgressBar();
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->hide();
    progressval = 0;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(updateprogress()));
    timer->start(1000);


    QFile file("../../reports/config.csv");
    QStringList wordlist;

    if (fileenable == false)
    {
        fileenable = true;

        if (!file.open(QIODevice::ReadOnly))
        {
            qDebug() << file.errorString();
            while (1);
        }

        QByteArray line;
        QString string ;
        QString encdata[1000];
        int encstart = 0;
        int index = 0;

        while (!file.atEnd())
        {
            line = file.readLine();
            string = line.data();
            encdata[index] = line.data();

            if (string[0] == "e")
            {
                index++;
                continue;
            }
            else
            {
                break;
            }
        }

        QString feature, encnsstate, encnsval, encenab, encval;
        QString str = encdata[index-1]; // get the recent setup;
        int idx = 0;
        int i;

        for (i = 0; i < 8; i++)
        {
            if (str[i] == ",")
            {
                break;
            }
            feature[i] = str[i];
        }

        i++;
        for (; i < 15; i++)
        {
            if (str[i] == ",")
            {
                if ((encnsval == "true") | (encnsval == "TRUE"))
                {
                    encdecnspacestate = true;
                }
                else
                {
                    encdecnspacestate = false;
                }

                break;
            }
            encnsval[idx] = str[i];
            idx++;
        }

        idx = 0;
        i++;
        for (; i < 22; i++)
        {
            if ((str[i] == ",") | (str[i] == "\n"))
            {
                if ((encnsval == "true") | (encnsval == "TRUE"))
                {
                    encenable = true;
                }
                else
                {
                    encenable = false;
                }
                break;
            }
            encval[idx] = str[i];
            idx++;
        }

        file.close();
    }
}

encdecDialog::~encdecDialog()
{
    delete ui;
}

void encdecDialog::savenamespace(QString ns)
{
    encdecitemns = ns;
}

void encdecDialog::savexphy(QString xphy)
{
    encdecitemxphy = xphy;

    if (xphy == "XPHY1")
    {
        setcomboitem(0);
    }
    else
    {
        setcomboitem(1);
    }
}

void encdecDialog::setcomboitem(int idx)
{
    ui->comboBox->setCurrentIndex(idx);
}

void encdecDialog::saveData(CONFIG_DATA *ConfigData)
{
    this->ConfigData =  ConfigData;

    return;
}

void encdecDialog::enadiscombobox(bool val)
{
    ui->comboBox->setEnabled(val);
}

void encdecDialog::enadiscomboboxns(bool val)
{
    ui->comboBox_2->setEnabled(val);
}

void encdecDialog::enadispbutton2(bool val)
{
    ui->pushButton_2->setEnabled(val);

    ui->pushButton_2->setText("Encryption");
}

void encdecDialog::setcombotext(QString str)
{
    ui->comboBox->addItem(str);
}

void encdecDialog::setcombotextns(QString str)
{
    ui->comboBox_2->addItem(str);
}

void encdecDialog::setfocus(void)
{
    ui->lineEdit->setFocus();
}

void encdecDialog::disableradioall()
{
    ui->checkBox->setEnabled(false);
    ui->checkBox_2->setEnabled(false);
    ui->checkBox_3->setEnabled(false);
    ui->checkBox_4->setEnabled(false);
    ui->checkBox_5->setEnabled(false);
    ui->checkBox_6->setEnabled(false);
    ui->checkBox_7->setEnabled(false);
    ui->checkBox_8->setEnabled(false);
}
/*
void encdecDialog::enableradioall()
{
    ui->checkBox->setEnabled(true);
    ui->checkBox_2->setEnabled(true);
    ui->checkBox_3->setEnabled(true);
    ui->checkBox_4->setEnabled(true);
    ui->checkBox_5->setEnabled(true);
    ui->checkBox_6->setEnabled(true);
    ui->checkBox_7->setEnabled(true);
    ui->checkBox_8->setEnabled(true);
}
*/
void encdecDialog::enadisradio(int pos, bool val)
{
    switch(pos)
    {
        case 1: ui->checkBox->setEnabled(val);
        break;

        case 2: ui->checkBox_2->setEnabled(val);
        break;

        case 3: ui->checkBox_3->setEnabled(val);
        break;

        case 4: ui->checkBox_4->setEnabled(val);
        break;

        case 5: ui->checkBox_5->setEnabled(val);
        break;

        case 6: ui->checkBox_6->setEnabled(val);
        break;

        case 7: ui->checkBox_7->setEnabled(val);
        break;

        case 8: ui->checkBox_8->setEnabled(val);
        break;

        default:
        break;
    }

    return;
}

#include "flexxlib.h"
extern flexxon::flexxDriveList driveList;

void encdecDialog::on_pushButton_clicked()
{
    QString         FileReport = QString("../../reports/") + "report.csv";
    QFile           ReportFile(FileReport);
    QTextStream     Data(&ReportFile);
    static QMutex   wait;

    if (ui->comboBox != NULL)
    {
        encdecitemxphy = ui->comboBox->currentText();
    }

    if (encdecitemxphy == "XPHY1")
    {
        if (ConfigData->cfgoption[0].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../media/passed.mp3"));
            music->setVolume(50);
            music->play();

            ui->comboBox_2->setEnabled(true);
            ui->pushButton_2->setEnabled(true);
            enadisradio(1, true);   // checkbox default to namespace1

            if (encdecnspacestate == false)
            {
                ui->pushButton_2->setText("Encryption");
                ui->checkBox->setChecked(false);
            }
            else
            {
                ui->pushButton_2->setText("Decryption");
                ui->checkBox->setChecked(true);
            }

            // call api here for encrypt/decrypt enable
            flexxon::FlexxDecorator flexxdisk(driveList[0]);
            char temp_pass[] = "password";

            wait.lock();
            flexxdisk.encryption(temp_pass, strlen(temp_pass), 1);
            wait.unlock();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password Verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(encdecitemxphy)
                                                .arg(ui->comboBox_2->currentText())
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }

        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password Verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(encdecitemxphy)
                                                .arg(ui->comboBox_2->currentText())
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }
    else if (encdecitemxphy == "XPHY2")
    {
        if (ConfigData->cfgoption[1].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            ui->comboBox_2->setEnabled(true);
            ui->pushButton_2->setEnabled(true);
            enadisradio(1, true);   // checkbox default to namespace1

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = " Password Verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(encdecitemxphy)
                                                .arg(ui->comboBox_2->currentText())
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = " Password Verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(encdecitemxphy)
                                                .arg(ui->comboBox_2->currentText())
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }
    else
    {
        // for debug once it happens
        while(1);
    }

    ui->lineEdit->clear();
    ui->lineEdit->setFocus();
}

void encdecDialog::on_pushButton_2_clicked()
{
    QString     FileReport = QString("../../reports/") + "report.csv";
    QFile       ReportFile(FileReport);
    QTextStream Data(&ReportFile);
    bool        checkboxstate;
    QString     state, nsstr, xphystr;
    int         nsno, xphyno;
    bool        statebool;

    QString     config = QString("../../reports/") + "config.csv";
    QFile       configfile(config);
    QTextStream CONFIG(&configfile);


    nsstr = ui->comboBox_2->currentText();  // get ns
    nsno = nsstr.right(nsstr.size()-9).toInt();    // get string number only

    xphystr = ui->comboBox->currentText();          // xphy
    xphyno = xphystr.right(xphystr.size()-4).toInt();

    checkboxstate = getcheckboxstate(nsno);

    if (checkboxstate == true)// ui->checkBox->checkState())
    {
        if (encdecnspacestate == false)
        {
            encdecnspacestate = true;
        }
        else
        {
            encdecnspacestate = false;
        }

        state = "enabled";
        statebool = true;
    }
    else
    {
        if (encdecnspacestate == true)
        {
            encdecnspacestate = false;
        }
        else
        {
            encdecnspacestate = true;
        }

        state = "disabled";
        statebool = false;
    }

    QMediaPlayer *music = new QMediaPlayer(this);
    music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
    music->setVolume(50);
    music->play();

    ConfigData->xphy = ui->comboBox->currentText();
    ConfigData->cfgoption[xphyno-1].nspace[nsno-1] = ui->comboBox_2->currentText();
    ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].encdec = statebool;

    QString reportDate = QDate::currentDate().toString("MM/dd/yyyy");
    QString Time = QTime::currentTime().toString("hh:mm:ss");
    QString LogMessage = ui->comboBox->currentText();   // xphy
    LogMessage = QString(LogMessage).append("--> "+reportDate);
    LogMessage = QString(LogMessage).append("  "+Time);
    LogMessage = QString(LogMessage).append("  "+nsstr);

    if (encenable == false)
    {
        encenable = true;
        LogMessage = QString(LogMessage).append(" Encryption is");
    }
    else
    {
        encenable = false;
        LogMessage = QString(LogMessage).append(" Decryption is");
    }
    LogMessage = QString(LogMessage).append("  "+state);
    emit eventsignal(LogMessage);

//    if (configfile.open(QIODevice::WriteOnly))
    if (configfile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        QString feature = "encdec";
        QString tfstate;
        QString encstate;


        if (encdecnspacestate == true)
        {
            encdecnspacestate = true;
            tfstate = "true";
        }
        else
        {
            encdecnspacestate = false;
            tfstate = "false";
        }

        if (encenable == true)
        {
            encenable = true;
            encstate = "true";
        }
        else
        {
            encenable = false;
            encstate = "false";
        }

        CONFIG << QString(CONFIG_CONTENTS).arg(feature)
                                          .arg(tfstate)
                                          .arg(encstate);

        configfile.close();
    }


    if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        Data << QString(REPORT_CONTENTS).arg(reportDate).arg(Time).arg(encdecitemxphy)
                                        .arg(nsstr)
                                        .arg("Encryptiong/Decryption is "+state);

        ReportFile.close();
    }
    else
    {
        QString logmsg;
        logmsg = "Failed to create a report file!  ";
        logmsg = QString(logmsg).append("  "+Time);
        logmsg = QString(logmsg).append("  "+reportDate);
        emit eventsignal(logmsg);
    }

    ui->comboBox_2->setEnabled(false);
    ui->pushButton_2->setEnabled(false);
//    enadisradio(1, true);   // checkbox default to namespace1
    enadisradio(1, false);   // checkbox default to namespace1
}

void encdecDialog::on_comboBox_2_activated(int index)
{
    disableradioall();
    enadisradio(index+1, true);
}

bool encdecDialog::getcheckboxstate(int checkboxsel)
{
    bool state;

    switch(checkboxsel)
    {
        case 1: state = ui->checkBox->checkState();
        break;

        case 2: state = ui->checkBox_2->checkState();
        break;

        case 3: state = ui->checkBox_3->checkState();
        break;

        case 4: state = ui->checkBox_4->checkState();
        break;

        case 5: state = ui->checkBox_5->checkState();
        break;

        case 6: state = ui->checkBox_6->checkState();
        break;

        case 7: state = ui->checkBox_7->checkState();
        break;

        case 8: state = ui->checkBox_8->checkState();
        break;

        default:
        break;
    }

    return state;
}


void encdecDialog::on_comboBox_2_activated(const QString &arg1)
{
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;
    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    nsstr = ui->comboBox_2->currentText();          // get ns
    nsno = arg1.right(arg1.size()-9).toInt();     // get string number only

    xphystr = ui->comboBox->currentText();          // xphy
    xphyno = xphystr.right(xphystr.size()-4).toInt();

    if (ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].encdec == false)
    {
        enadisradio(nsno, false);

        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
        music->setVolume(50);
        music->play();

        ui->pushButton_2->setEnabled(false);
        logmsg = ui->comboBox->currentText();
        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  Encryption/Decryption is not available to this");
        logmsg = QString(logmsg).append("  "+nsstr);
        emit eventsignal(logmsg);

        if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            Data << QString(REPORT_CONTENTS).arg(date).arg(time).arg(xphystr)
                                            .arg(nsstr)
                                            .arg("Encryption/Decryption is not available to this "+nsstr);

            ReportFile.close();
        }
        else
        {
            logmsg = "Failed to create a report file!  ";
            logmsg = QString(logmsg).append("  "+time);
            logmsg = QString(logmsg).append("  "+date);
            emit eventsignal(logmsg);
        }

        return;
    }

    ui->pushButton_2->setEnabled(true);
}

void encdecDialog::updateprogress()
{
/*    ui->progressBar->show();

    progressval++;
    ui->progressBar->setValue(progressval);*/
}


