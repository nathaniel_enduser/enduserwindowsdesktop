#include "genconfigdialog.h"
#include "ui_genconfigdialog.h"
#include <QMessageBox>
#include <QDateTime>
#include <QMediaPlayer>
#include <QFile>

genconfigDialog::genconfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::genconfigDialog)
{
    ui->setupUi(this);

    configdata = new CONFIG_DATA;
    //memset((void*)configdata, 0, sizeof(CONFIG_DATA));

    configdata->cfgoption[0].password = PWORD_DEFAULT1;
    configdata->cfgoption[1].password = PWORD_DEFAULT2;
}

genconfigDialog::~genconfigDialog()
{
    delete configdata;
    delete ui;
}

void genconfigDialog::setcombotext(QString str)
{
    ui->comboBox->addItem(str);
}

void genconfigDialog::setcombotextns(QString str)
{
    ui->comboBox_2->addItem(str);
}

void genconfigDialog::setcombostate(bool val)
{
    ui->comboBox_2->setEnabled(val);
}

void genconfigDialog::setgroupboxns(bool val)
{
    ui->groupBox_3->setEnabled(val);
}

void genconfigDialog::setgroupboxpol(bool val)
{
    ui->groupBox->setEnabled(val);
}

void genconfigDialog::setpushbuttonupdcfg(bool val)
{
    ui->pushButton->setEnabled(val);
}

void genconfigDialog::setpushbuttoncreapw(bool val)
{
    ui->pushButton_19->setEnabled(val);
}

CONFIG_DATA *genconfigDialog::returnData()
{
    return configdata;
}

/*void genconfigDialog::savedata(CONFIG_DATA *ConfigData)
{
    this->ConfigData =  ConfigData;
    return;
}*/

void genconfigDialog::on_update_config_clicked()
{

    // would you like to config another XPHY?
    // yes will re-launch dialog
    // no update will update the exit

}

void genconfigDialog::setfocus()
{
    ui->lineEdit->setFocus();
}

void genconfigDialog::on_pushButton_19_clicked()
{
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
    QString Oldpassword;
    QString Newpassword;

    // create password button

    // verify new password with old password
    // send to device thru nvme security send/receive command
    // once verify from the drive, enable option

    Oldpassword = ui->lineEdit->text();
    Newpassword = ui->lineEdit_2->text();

    if (ui->comboBox->currentText() == "XPHY1")     // this should be sent to drive using NVMe Security Send/Receive
    {       
        if (configdata->cfgoption[0].password == Oldpassword)
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            configdata->xphy = ui->comboBox->currentText();
            configdata->cfgoption[0].password = Newpassword;    // update password

            // tie-up the configuration to xphy nvme device
            // do not use this configuration if the xphy nvme device is different!

            // enable/disable options
            setgroupboxns(true);
            setgroupboxpol(true);
            setpushbuttonupdcfg(true);
            setcombostate(true);

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password Verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(ui->comboBox->currentText())
                                                .arg(ui->comboBox_2->currentText())
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password Verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(ui->comboBox->currentText())
                                                .arg(ui->comboBox_2->currentText())
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }
    else
    {
        if (configdata->cfgoption[1].password == Oldpassword)
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            configdata->xphy = ui->comboBox->currentText();
            configdata->cfgoption[1].password = Newpassword;    // update password

            // enable/disable options
            setgroupboxns(true);
            setgroupboxpol(true);
            setpushbuttonupdcfg(true);
            setcombostate(true);

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password Verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(ui->comboBox->currentText())
                                                .arg(ui->comboBox_2->currentText())
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+time);
            logmsg = QString(logmsg).append("  "+date);
            pw = "Password Verified failed!  ";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(ui->comboBox->currentText())
                                                .arg(ui->comboBox_2->currentText())
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }

    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit->setFocus();

    return;

}

void genconfigDialog::on_pushButton_clicked()
{
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;
    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    nsstr = ui->comboBox_2->currentText();  // get ns
    nsno = nsstr.right(nsstr.size()-9).toInt();

    xphystr = ui->comboBox->currentText();
    xphystr = xphystr.right(xphystr.size()-4);
    xphyno = xphystr.toInt();

    configdata->cfgoption[xphyno-1].nspace[nsno-1] = nsstr;

    QMediaPlayer *music = new QMediaPlayer(this);
    music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
    music->setVolume(50);
    music->play();

    logmsg = ui->comboBox->currentText();
    QString date = QDate::currentDate().toString("MM/dd/yyyy");
    QString time = QTime::currentTime().toString("hh:mm:ss");    
    logmsg = QString(logmsg).append("--> "+date);
    logmsg = QString(logmsg).append("  "+time);
    logmsg = QString(logmsg).append(" Configure Namespace option and policies!");
    emit eventsignal(logmsg);

    if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        Data << QString(REPORT_CONTENTS).arg(date).arg(time).arg(xphystr)
                                        .arg(nsstr)
                                        .arg("Configure Namespace option and policies!");

        ReportFile.close();
    }
    else
    {
        logmsg = "Failed to create a report file!  ";
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  "+date);
        emit eventsignal(logmsg);
    }

    // get ns option state
    configdata->cfgoption[xphyno-1].enablefeature[nsno-1].protzone = ui->checkBox->checkState();
    if (ui->checkBox->checkState())
    {
        logmsg = "Enable Protect Namespace";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno-1].enablefeature[nsno-1].signupdate = ui->checkBox_2->checkState();
    if (ui->checkBox_2->checkState())
    {
        logmsg = "Enable Update Signature";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno-1].enablefeature[nsno-1].lock = ui->checkBox_21->checkState();
    if (ui->checkBox_21->checkState())
    {
        logmsg = "Enable Lock File/Folder";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno-1].enablefeature[nsno-1].policy = ui->checkBox_22->checkState();
    if (ui->checkBox_22->checkState())
    {
        logmsg = "Enable Change Policy";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno-1].enablefeature[nsno-1].secerase = ui->checkBox_23->checkState();
    if (ui->checkBox_23->checkState())
    {
        logmsg = "Enable Secure Erase";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno-1].enablefeature[nsno-1].encdec = ui->checkBox_87->checkState();
    if (ui->checkBox_87->checkState())
    {
        logmsg = "Enable Encryption/Decryption";
        emit eventsignal(logmsg);
    }

    // get policy state
    if (ui->checkBox_3->checkState())
    {
        logmsg = "Enable";
        logmsg = QString(logmsg).append("  "+nsstr);
        logmsg = QString(logmsg).append("  "+xphystr);
        emit eventsignal(logmsg);

        savelogpolicy(xphyno-1, nsno);
    }
}

void genconfigDialog::on_comboBox_currentIndexChanged(int index)
{
    Q_UNUSED(index)
    setgroupboxns(false);
    setgroupboxpol(false);
    setpushbuttonupdcfg(false);
    setcombostate(false);
    setfocus();
}

void genconfigDialog::savelogpolicy(int xphyno, int nsno)
{
    QString logmsg;

    QMediaPlayer *music = new QMediaPlayer(this);
    music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
    music->setVolume(50);
    music->play();

    configdata->cfgoption[xphyno].policies[nsno].policy1 = ui->checkBox_3->checkState();
    if (ui->checkBox_3->checkState())
    {
        logmsg = "Enable Policy 1";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno].policies[nsno].policy2 = ui->checkBox_4->checkState();
    if (ui->checkBox_4->checkState())
    {
        logmsg = "Enable Policy 2";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno].policies[nsno].policy3 = ui->checkBox_5->checkState();
    if (ui->checkBox_5->checkState())
    {
        logmsg = "Enable Policy 3";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno].policies[nsno].policy4 = ui->checkBox_6->checkState();
    if (ui->checkBox_6->checkState())
    {
        logmsg = "Enable Policy 4";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno].policies[nsno].policy5 = ui->checkBox_7->checkState();
    if (ui->checkBox_7->checkState())
    {
        logmsg = "Enable Policy 5";
        emit eventsignal(logmsg);
    }
    configdata->cfgoption[xphyno].policies[nsno].policy6 = ui->checkBox_8->checkState();
    if (ui->checkBox_8->checkState())
    {
        logmsg = "Enable Policy 6";
        emit eventsignal(logmsg);
    }
}

void genconfigDialog::on_checkBox_22_toggled(bool checked)
{
    if (checked == true)
    {
        setgroupboxpol(true);
    }
    else
    {
        setgroupboxpol(false);
    }
}
