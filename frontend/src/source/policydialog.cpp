#include "policydialog.h"
#include "ui_policydialog.h"
#include <QMessageBox>
#include <QDateTime>
#include <QDateTime>
#include <QMediaPlayer>
#include <QFile>
#include <QMutex>

bool policystate[6];
bool policyenable = false;
bool policyinit = false;

policyDialog::policyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::policyDialog)
{
    ui->setupUi(this);

    progressbar = new QProgressBar();
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->hide();
    progressval = 0;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(updateprogress()));

    timer->start(1000);


    QFile file("../../reports/config.csv");
    QStringList wordlist;

    if (policyinit == false)
    {
        policyinit = true;

        if (!file.open(QIODevice::ReadOnly))
        {
            qDebug() << file.errorString();
            while (1);
        }

        QByteArray line;
        QString string ;
        QString policydata[1000];
        int index = 0;
        int polstart = 0;

        while (!file.atEnd())
        {
            line = file.readLine();
            string = line.data();
            policydata[index] = line.data();

            if (string[0] == "e")
            {
                index++;
                continue;
            }

            if (string[8] == "l")
            {
                index++;
                continue;
            }

            if (string[18] == "p")
            {
                index++;
                continue;
            }
            else
            {
                break;
            }
            index++;
        }

        QString str = policydata[index-1]; // get the recent setup;
        polstart = index-1;

        while (str[18] == "p")  // all unlock data have been collected
        {
            QChar polenab = str[27];
            QString polen = polenab;
            policyenable = polen.toInt();

            int polstateidx = 30;
            for (int i = 0; i < 6; i++)
            {
                QChar polst = str[polstateidx];
                QString polstate  = polst ;
                policystate[i] = polstate.toInt();
                polstateidx += 3;
            }
            polstart++;
            str = policydata[polstart];
        }

        file.close();
    }
}

policyDialog::~policyDialog()
{
    delete ui;
}

void policyDialog::savenamespace(QString ns)
{
    policyitemns = ns;
}

void policyDialog::savephxy(QString xphy)
{
    policyitemxphy = xphy;

    if (xphy == "XPHY1")
    {
        setcomboitem(0);
    }
    else
    {
        setcomboitem(1);
    }
}

void policyDialog::setcomboitem(int idx)
{
    ui->comboBox->setCurrentIndex(idx);
}

void policyDialog::savedata(CONFIG_DATA *ConfigData)
{
    this->ConfigData = ConfigData;
}

void policyDialog::enadiscombobox(bool val)
{
    ui->comboBox->setEnabled(val);
}

void policyDialog::enadiscomboboxns(bool val)
{
    ui->comboBox_2->setEnabled(val);
}

void policyDialog::enadispbutton(bool val)
{
    ui->pushButton->setEnabled(val);
}

void policyDialog::enadispbuttonns(bool val)
{
    ui->pushButton_2->setEnabled(val);
}

void policyDialog::setcombotext(QString str)
{
    ui->comboBox->addItem(str);
}

void policyDialog::setcombotextns(QString str)
{
    ui->comboBox_2->addItem(str);
}

void policyDialog::setfocus(void)
{
    ui->lineEdit->setFocus();
}

void policyDialog::enadischeckbox(bool val)
{
    ui->checkBox->setEnabled(val);
    ui->checkBox_2->setEnabled(val);
    ui->checkBox_3->setEnabled(val);
    ui->checkBox_4->setEnabled(val);
    ui->checkBox_5->setEnabled(val);
    ui->checkBox_8->setEnabled(val);
}

#include "flexxlib.h"
extern flexxon::flexxDriveList driveList;

void policyDialog::on_pushButton_clicked()
{
    QString         FileReport = QString("../../reports/") + "report.csv";
    QFile           ReportFile(FileReport);
    QTextStream     Data(&ReportFile);
    static QMutex   wait;


    if (ui->comboBox != NULL)
    {
        policyitemxphy = ui->comboBox->currentText();
    }

    if (policyitemxphy == "XPHY1")
    {
        if (ConfigData->cfgoption[0].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            enadischeckbox(false);
            ui->comboBox_2->setEnabled(true);
            ui->pushButton_2->setEnabled(true);

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(policyitemxphy)
                                                .arg(policyitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }

            if (policyenable == true)
            {
                if (policystate[0] == true)
                {
                    ui->checkBox->setChecked(true);
                }
                if (policystate[1] == true)
                {
                    ui->checkBox_2->setChecked(true);
                }
                if (policystate[2] == true)
                {
                    ui->checkBox_3->setChecked(true);
                }
                if (policystate[3] == true)
                {
                    ui->checkBox_4->setChecked(true);
                }
                if (policystate[4] == true)
                {
                    ui->checkBox_5->setChecked(true);
                }
                if (policystate[5] == true)
                {
                    ui->checkBox_8->setChecked(true);
                }
            }
/*
            flexxon::FlexxDecorator flexxdisk(driveList[0]);

            wait.lock();
            flexxdisk.change_policy(const uint32_t policyhi,
                                    const uint32_t policylo,
                                    char *password,
                                    const int psize);
            wait.unlock();
*/
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            // call api here for change policy

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(policyitemxphy)
                                                .arg(policyitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }
    else
    {
        if (ConfigData->cfgoption[1].password == ui->lineEdit->text())
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
            music->setVolume(50);
            music->play();

            enadischeckbox(true);
            ui->comboBox_2->setEnabled(true);
            ui->pushButton_2->setEnabled(true);

            QString logmsg, pw;
            logmsg= ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+date);
            logmsg = QString(logmsg).append("  "+time);
            pw = "Password verified passed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(policyitemxphy)
                                                .arg(policyitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
        else
        {
            QMediaPlayer *music = new QMediaPlayer(this);
            music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
            music->setVolume(50);
            music->play();

            QString logmsg, pw;
            logmsg = ui->comboBox->currentText();
            QString date = QDate::currentDate().toString("MM/dd/yyyy");
            QString time = QTime::currentTime().toString("hh:mm:ss");
            logmsg = QString(logmsg).append("--> "+time);
            logmsg = QString(logmsg).append("  "+date);
            pw = "Password verified failed!";
            logmsg = QString(logmsg).append("  "+pw);
            emit eventsignal(logmsg);

            if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                Data << QString(REPORT_CONTENTS).arg(date)
                                                .arg(time)
                                                .arg(policyitemxphy)
                                                .arg(policyitemns)
                                                .arg(pw);

                ReportFile.close();
            }
            else
            {
                logmsg = "Failed to create a report file!  ";
                logmsg = QString(logmsg).append("  "+time);
                logmsg = QString(logmsg).append("  "+date);
                emit eventsignal(logmsg);
            }
        }
    }

    ui->lineEdit->clear();
    ui->lineEdit->setFocus();
}

void policyDialog::on_pushButton_2_clicked()
{
    // change policy
    QString FileReport = QString("../../reports/") + "report.csv";
    QFile ReportFile(FileReport);
    QTextStream Data(&ReportFile);
//    bool FileExist = false;

    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno;
    int xphyno;

    nsstr = ui->comboBox_2->currentText();          // get ns
    nsno = nsstr.right(nsstr.size()-9).toInt();     // get string number only

    xphystr = ui->comboBox->currentText();          // xphy
    xphyno = xphystr.right(xphystr.size()-4).toInt();
/*
    if (ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].policy == false)
    {
        logmsg = ui->comboBox->currentText();
        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  Policy to this");
        logmsg = QString(logmsg).append("  "+nsstr);
        logmsg = QString(logmsg).append("  is disabled");
        emit eventsignal(logmsg);
    }
*/
    QMediaPlayer *music = new QMediaPlayer(this);
    music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
    music->setVolume(50);
    music->play();

    ConfigData->xphy = xphystr;
    ConfigData->cfgoption[xphyno-1].nspace[nsno-1] = nsstr;

    logmsg = ui->comboBox->currentText();
    QString date = QDate::currentDate().toString("MM/dd/yyyy");
    QString time = QTime::currentTime().toString("hh:mm:ss");
    logmsg = QString(logmsg).append("--> "+date);
    logmsg = QString(logmsg).append("  "+time);
    logmsg = QString(logmsg).append("  Change Policies!");
    emit eventsignal(logmsg);

    if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        Data << QString(REPORT_CONTENTS).arg(date)
                                        .arg(time)
                                        .arg(policyitemxphy)
                                        .arg(policyitemns)
                                        .arg("Change Policies");

        ReportFile.close();
    }
    else
    {
        logmsg = "Failed to create a report file!  ";
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append("  "+date);
        emit eventsignal(logmsg);
    }

    // get policy state
    ConfigData->cfgoption[xphyno-1].policies[nsno-1].policy1 = ui->checkBox->checkState();
    if (ui->checkBox->checkState())
    {
        policystate[0] = true;
        logmsg = "Free space";
        emit eventsignal(logmsg);
    }
    else
    {
        policystate[0] = false;
    }

    ConfigData->cfgoption[xphyno-1].policies[nsno-1].policy2 = ui->checkBox_2->checkState();
    if (ui->checkBox_2->checkState())
    {
        policystate[1] = true;
        logmsg = "Temperature";
        emit eventsignal(logmsg);
    }
    else
    {
        policystate[1] = false;
    }

    ConfigData->cfgoption[xphyno-1].policies[nsno-1].policy3 = ui->checkBox_3->checkState();
    if (ui->checkBox_3->checkState())
    {
        policystate[2] = true;
        logmsg = "Unsafe Shutdown";
        emit eventsignal(logmsg);
    }
    else
    {
        policystate[2] = false;
    }

    ConfigData->cfgoption[xphyno-1].policies[nsno-1].policy4 = ui->checkBox_4->checkState();
    if (ui->checkBox_4->checkState())
    {
        policystate[3] = true;
        logmsg = "Media and Data Integrity Error";
        emit eventsignal(logmsg);
    }
    else
    {
        policystate[3] = false;
    }

    ConfigData->cfgoption[xphyno-1].policies[nsno-1].policy5 = ui->checkBox_5->checkState();
    if (ui->checkBox_5->checkState())
    {
        policystate[4] = true;
        logmsg = "Number of error Information log entires";
        emit eventsignal(logmsg);
    }
    else
    {
        policystate[4] = false;
    }

    ConfigData->cfgoption[xphyno-1].policies[nsno-1].policy6 = ui->checkBox_8->checkState();
    if (ui->checkBox_8->checkState())
    {
        policystate[5] = true;
        logmsg = "Critical Warning";
        emit eventsignal(logmsg);
    }
    else
    {
        policystate[5] = false;
    }

    policyenable = true;

    QString     config = QString("../../reports/") + "config.csv";
    QFile       configfile(config);
    QTextStream POLICY(&configfile);

    if (configfile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        QString feature = "policy";

        POLICY << QString(POLICY_CONTENTS).arg(feature)
                                          .arg(policyenable)
                                          .arg(policystate[0])
                                          .arg(policystate[1])
                                          .arg(policystate[2])
                                          .arg(policystate[3])
                                          .arg(policystate[4])
                                          .arg(policystate[5]);
        configfile.close();
    }

    ui->pushButton_2->setEnabled(false);
}

void policyDialog::on_comboBox_2_activated(const QString &arg1)
{
    int nsno, xphyno;
    QString xphystr, nsstr;
    QString logmsg;

    nsno = arg1.right(arg1.size()-9).toInt();

    xphystr = ui->comboBox->currentText();
    xphyno = xphystr.right(xphystr.size()-4).toInt();

    if (ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].policy == false)
    {
        logmsg = "Policies to this namespace are disable";
        emit eventsignal(logmsg);

        enadischeckbox(false);
        return;
    }

    enadischeckbox(true);
}

void policyDialog::updateprogress()
{
/*    ui->progressBar->show();

    progressval++;
    ui->progressBar->setValue(progressval);*/
}
