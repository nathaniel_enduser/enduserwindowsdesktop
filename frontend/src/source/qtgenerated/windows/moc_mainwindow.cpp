/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../include/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[36];
    char stringdata0[446];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 16), // "right_click_menu"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 3), // "pos"
QT_MOC_LITERAL(4, 33, 12), // "item_pressed"
QT_MOC_LITERAL(5, 46, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(6, 63, 4), // "item"
QT_MOC_LITERAL(7, 68, 3), // "Col"
QT_MOC_LITERAL(8, 72, 12), // "item_clicked"
QT_MOC_LITERAL(9, 85, 9), // "log_event"
QT_MOC_LITERAL(10, 95, 14), // "generatereport"
QT_MOC_LITERAL(11, 110, 14), // "changepassword"
QT_MOC_LITERAL(12, 125, 11), // "protectzone"
QT_MOC_LITERAL(13, 137, 12), // "changepolicy"
QT_MOC_LITERAL(14, 150, 8), // "lockfile"
QT_MOC_LITERAL(15, 159, 20), // "encryptdecryptenable"
QT_MOC_LITERAL(16, 180, 11), // "secureerase"
QT_MOC_LITERAL(17, 192, 15), // "signatureupdate"
QT_MOC_LITERAL(18, 208, 16), // "genconfiguration"
QT_MOC_LITERAL(19, 225, 14), // "updatefirmware"
QT_MOC_LITERAL(20, 240, 5), // "about"
QT_MOC_LITERAL(21, 246, 16), // "create_namespace"
QT_MOC_LITERAL(22, 263, 16), // "delete_namespace"
QT_MOC_LITERAL(23, 280, 16), // "attach_namespace"
QT_MOC_LITERAL(24, 297, 16), // "detach_namespace"
QT_MOC_LITERAL(25, 314, 11), // "lock_unlock"
QT_MOC_LITERAL(26, 326, 11), // "QModelIndex"
QT_MOC_LITERAL(27, 338, 5), // "index"
QT_MOC_LITERAL(28, 344, 18), // "lock_unlock_dclick"
QT_MOC_LITERAL(29, 363, 18), // "execute_lockunlock"
QT_MOC_LITERAL(30, 382, 10), // "fwdownload"
QT_MOC_LITERAL(31, 393, 8), // "fwcommit"
QT_MOC_LITERAL(32, 402, 13), // "fwstatusreset"
QT_MOC_LITERAL(33, 416, 8), // "uint32_t"
QT_MOC_LITERAL(34, 425, 6), // "status"
QT_MOC_LITERAL(35, 432, 13) // "reloadluparam"

    },
    "MainWindow\0right_click_menu\0\0pos\0"
    "item_pressed\0QTreeWidgetItem*\0item\0"
    "Col\0item_clicked\0log_event\0generatereport\0"
    "changepassword\0protectzone\0changepolicy\0"
    "lockfile\0encryptdecryptenable\0secureerase\0"
    "signatureupdate\0genconfiguration\0"
    "updatefirmware\0about\0create_namespace\0"
    "delete_namespace\0attach_namespace\0"
    "detach_namespace\0lock_unlock\0QModelIndex\0"
    "index\0lock_unlock_dclick\0execute_lockunlock\0"
    "fwdownload\0fwcommit\0fwstatusreset\0"
    "uint32_t\0status\0reloadluparam"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  144,    2, 0x08 /* Private */,
       4,    2,  147,    2, 0x08 /* Private */,
       8,    2,  152,    2, 0x08 /* Private */,
       9,    1,  157,    2, 0x08 /* Private */,
      10,    0,  160,    2, 0x08 /* Private */,
      11,    0,  161,    2, 0x08 /* Private */,
      12,    0,  162,    2, 0x08 /* Private */,
      13,    0,  163,    2, 0x08 /* Private */,
      14,    0,  164,    2, 0x08 /* Private */,
      15,    0,  165,    2, 0x08 /* Private */,
      16,    0,  166,    2, 0x08 /* Private */,
      17,    0,  167,    2, 0x08 /* Private */,
      18,    0,  168,    2, 0x08 /* Private */,
      19,    0,  169,    2, 0x08 /* Private */,
      20,    0,  170,    2, 0x08 /* Private */,
      21,    0,  171,    2, 0x08 /* Private */,
      22,    0,  172,    2, 0x08 /* Private */,
      23,    0,  173,    2, 0x08 /* Private */,
      24,    0,  174,    2, 0x08 /* Private */,
      25,    1,  175,    2, 0x08 /* Private */,
      28,    1,  178,    2, 0x08 /* Private */,
      29,    0,  181,    2, 0x08 /* Private */,
      30,    0,  182,    2, 0x08 /* Private */,
      31,    0,  183,    2, 0x08 /* Private */,
      32,    1,  184,    2, 0x08 /* Private */,
      35,    0,  187,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QPoint,    3,
    QMetaType::Void, 0x80000000 | 5, QMetaType::Int,    6,    7,
    QMetaType::Void, 0x80000000 | 5, QMetaType::Int,    6,    7,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 26,   27,
    QMetaType::Void, 0x80000000 | 26,   27,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 33,   34,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->right_click_menu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 1: _t->item_pressed((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->item_clicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->log_event((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->generatereport(); break;
        case 5: _t->changepassword(); break;
        case 6: _t->protectzone(); break;
        case 7: _t->changepolicy(); break;
        case 8: _t->lockfile(); break;
        case 9: _t->encryptdecryptenable(); break;
        case 10: _t->secureerase(); break;
        case 11: _t->signatureupdate(); break;
        case 12: _t->genconfiguration(); break;
        case 13: _t->updatefirmware(); break;
        case 14: _t->about(); break;
        case 15: _t->create_namespace(); break;
        case 16: _t->delete_namespace(); break;
        case 17: _t->attach_namespace(); break;
        case 18: _t->detach_namespace(); break;
        case 19: _t->lock_unlock((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 20: _t->lock_unlock_dclick((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 21: _t->execute_lockunlock(); break;
        case 22: _t->fwdownload(); break;
        case 23: _t->fwcommit(); break;
        case 24: _t->fwstatusreset((*reinterpret_cast< uint32_t(*)>(_a[1]))); break;
        case 25: _t->reloadluparam(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
