#include "lockunlockdialog.h"
#include "ui_lockunlockdialog.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDateTime>
#include <QMediaPlayer>
#include <QMutex>


lockunlockDialog::lockunlockDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::lockunlockDialog)
{
    ui->setupUi(this);

    loginerrcnt = 0;
    xphystr = "xphy";

    progressbar = new QProgressBar();
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->hide();
    progressval = 0;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(updateprogress()));

    timer->start(1000);

}

lockunlockDialog::~lockunlockDialog()
{
    delete ui;
}

void lockunlockDialog::saveData(CONFIG_DATA *configdata)
{
    this->ConfigData =  configdata;

    return;
}

void lockunlockDialog::savenamespace(QString ns, QString xphy)
{
    int nsno, nsidx;

    lockitemns = ns;
    nsno = ns.right(ns.size()-9).toInt();

    switch(nsno-1)
    {
        case 0: nsidx = 0;
        break;
        case 1: nsidx = 1;
        break;
        case 2: nsidx = 2;
        break;
        case 3: nsidx = 3;
        break;
        case 4: nsidx = 4;
        break;
        case 5: nsidx = 5;
        break;
        case 6: nsidx = 6;
        break;
        case 7: nsidx = 7;
        break;
        default:
            while(1);   // for debug once it happened
    }

    if (xphy == "XPHY1")
    {
        ConfigData->cfgoption[0].nspace[nsidx] = ns;
        ConfigData->cfgoption[0].currnsidx = nsidx;
    }
    else
    {
        ConfigData->cfgoption[1].nspace[nsidx] = ns;
        ConfigData->cfgoption[1].currnsidx = nsidx;
    }
}

void lockunlockDialog::setcombotext(QString str)
{
    ui->comboBox->addItem(str);
}

void lockunlockDialog::savexphy(QString xphy)
{
    if (xphy == "XPHY1")
    {
        ConfigData->xphy = xphy;
        ui->comboBox->setCurrentIndex(0);
    }
    else
    {
        ConfigData->xphy = xphy;
    }
    lockitemxphy = xphy;
}

void lockunlockDialog::savefilefolder(QString ffolder, QString xphy)
{
    if (xphy == "XPHY1")
    {
        ConfigData->cfgoption[0].filefolder = ffolder;
    }
    else
    {
        ConfigData->cfgoption[1].filefolder = ffolder;
    }
}

void lockunlockDialog::enadispbutton(bool val)
{
    ui->pushButton->setEnabled(val);
}

void lockunlockDialog::enadistextbox(bool val)
{
    ui->lineEdit->setEnabled(val);
}

void lockunlockDialog::setfocus(void)
{
    ui->lineEdit->setFocus();
}

void lockunlockDialog::lockfile(bool state)
{
    locked = state;
}

#include <flexxlib.h>
extern flexxon::flexxDriveList driveList;

void lockunlockDialog::on_pushButton_clicked()
{
    QString         FileReport = QString("../../reports/") + "report.csv";
    QFile           ReportFile(FileReport);
    QTextStream     Data(&ReportFile);
    int             nsno, xphyno;
    QString         nsstr, strxphy;
    SECTOR_RANGE    lba_range[128];
    int             actual_range = 0;
    int             lbacnts;
    static QMutex   wait;

    while (loginerrcnt < PASSWORDTRIES)
    {
        loginerrcnt++;

        if (ConfigData->xphy == "XPHY1")
        {
            xphystr = "XPHY1";

            if (ConfigData->cfgoption[0].password == ui->lineEdit->text())
            {
                enadispbutton(false);
                enadistextbox(false);

                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
                music->setVolume(50);
                music->play();

                QString logmsg, pw;
                logmsg = ConfigData->xphy;
                QString date = QDate::currentDate().toString("MM/dd/yyyy");
                QString time = QTime::currentTime().toString("hh:mm:ss");
                logmsg = QString(logmsg).append("--> "+date);
                logmsg = QString(logmsg).append("  "+time);
                pw = "Lock/Unlock feature Password Verified passed!";
                logmsg = QString(logmsg).append("  "+pw);
                emit eventsignal(logmsg);

                if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
                {
                    Data << QString(REPORT_CONTENTS).arg(date)
                                                    .arg(time)
                                                    .arg(ConfigData->xphy)
                                                    .arg(ConfigData->cfgoption[0].nspace[ConfigData->cfgoption[0].currnsidx])
                                                    .arg(pw);

                    ReportFile.close();
                }
                else
                {
                    logmsg = "Failed to create a report file!  ";
                    logmsg = QString(logmsg).append("  "+time);
                    logmsg = QString(logmsg).append("  "+date);
                    emit eventsignal(logmsg);
                }

                QString filepath = ConfigData->cfgoption[0].filefolder;
                QByteArray ba = filepath.toLocal8Bit();
                char *path = ba.data();

                flexxon::FlexxDecorator flexxdisk(driveList[0]);
                char temp_pass[] = "password";

                wait.lock();
                flexxdisk.lock(temp_pass, strlen(temp_pass),
                               path, strlen(path), 1,
                               lba_range, 128,
                               &actual_range);
                wait.unlock();

                lbacnts = actual_range;
                uint64_t startsector;
                uint64_t numsects;

                for (int i = 0; i < lbacnts; i++)
                {
                    startsector = lba_range[i].start_sector;
                    numsects = lba_range[i].num_sects;

                    QString msglog = QString::number(startsector);
                    QString messages1;
                    messages1 = "LBA Start: ";
                    messages1 = QString(messages1).append(msglog);
                    emit eventsignal(messages1);

                    QString msglog2 = QString::number(numsects);
                    QString messages2;
                    messages2 = "LBA Count: ";
                    messages2 = QString(messages2).append(msglog2);
                    emit eventsignal(messages2);
                }

                QString filetolock;
                filetolock = filepath;
                QString Lockfile;

                if (locked == true)
                {
                    Lockfile = "Locked this file ";
                }
                else
                {
                    Lockfile = "UnLocked this file ";
                }
                Lockfile = QString(Lockfile).append(filetolock);
                emit eventsignal(Lockfile);
            }
            else
            {
                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
                music->setVolume(50);
                music->play();

                QString logmsg, pw;
                logmsg = ConfigData->xphy;
                QString date = QDate::currentDate().toString("MM/dd/yyyy");
                QString time = QTime::currentTime().toString("hh:mm:ss");
                logmsg = QString(logmsg).append("--> "+date);
                logmsg = QString(logmsg).append("  "+time);
                pw = "Lock/Unlock feature Password Verified failed!";
                logmsg = QString(logmsg).append("  "+pw);
                emit eventsignal(logmsg);

                if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
                {
                    Data << QString(REPORT_CONTENTS).arg(date)
                                                    .arg(time)
                                                    .arg(ConfigData->xphy)
                                                    .arg(ConfigData->cfgoption[0].nspace[ConfigData->cfgoption[0].currnsidx])
                                                    .arg(pw);

                    ReportFile.close();
                }
                else
                {
                    logmsg = "Failed to create a report file!  ";
                    logmsg = QString(logmsg).append("  "+time);
                    logmsg = QString(logmsg).append("  "+date);
                    emit eventsignal(logmsg);
                }
            }
            return;
        }
        else if (ConfigData->xphy == "XPHY2")
        {            
            xphystr = "XPHY2";
            if (ConfigData->cfgoption[1].password == ui->lineEdit->text())
            {
                enadispbutton(false);
                enadistextbox(false);

                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/passed.mp3"));
                music->setVolume(50);
                music->play();

                // call api here for lock file
                // ConfigData->cfgoption[1].filefolder

                QString logmsg, pw;
                logmsg = ConfigData->xphy;
                QString date = QDate::currentDate().toString("MM/dd/yyyy");
                QString time = QTime::currentTime().toString("hh:mm:ss");
                logmsg = QString(logmsg).append("--> "+date);
                logmsg = QString(logmsg).append("  "+time);
                pw = "Lock/Unlock feature Password Verified passed!";
                logmsg = QString(logmsg).append("  "+pw);
                emit eventsignal(logmsg);

                if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
                {
                    Data << QString(REPORT_CONTENTS).arg(date)
                                                    .arg(time)
                                                    .arg(ConfigData->xphy)
                                                    .arg(ConfigData->cfgoption[1].nspace[ConfigData->cfgoption[1].currnsidx])
                                                    .arg(pw);

                    ReportFile.close();
                }
                else
                {
                    logmsg = "Failed to create a report file!  ";
                    logmsg = QString(logmsg).append("  "+time);
                    logmsg = QString(logmsg).append("  "+date);
                    emit eventsignal(logmsg);
                }
            }
            else
            {
                QMediaPlayer *music = new QMediaPlayer(this);
                music->setMedia(QUrl::fromLocalFile("../../resources/media/ErrorSound.wav"));
                music->setVolume(50);
                music->play();

                QString logmsg, pw;
                logmsg = ConfigData->xphy;
                QString date = QDate::currentDate().toString("MM/dd/yyyy");
                QString time = QTime::currentTime().toString("hh:mm:ss");
                logmsg = QString(logmsg).append("--> "+date);
                logmsg = QString(logmsg).append("  "+time);
                pw = "Lock/Unlock feature Password Verified failed!";
                logmsg = QString(logmsg).append("  "+pw);
                emit eventsignal(logmsg);

                if (ReportFile.open(QIODevice::WriteOnly | QIODevice::Append))
                {
                    Data << QString(REPORT_CONTENTS).arg(date)
                                                    .arg(time)
                                                    .arg(ConfigData->xphy)
                                                    .arg(ConfigData->cfgoption[1].nspace[ConfigData->cfgoption[1].currnsidx])
                                                    .arg(pw);

                    ReportFile.close();
                }
                else
                {
                    logmsg = "Failed to create a report file!  ";
                    logmsg = QString(logmsg).append("  "+time);
                    logmsg = QString(logmsg).append("  "+date);
                    emit eventsignal(logmsg);
                }
            }
            return;
        }
        else
        {
            // it shouldn't happen
            // for debug it enters here...
            while(1);
        }
    }

    enadispbutton(false);
    enadistextbox(false);

    xphyno = xphystr.right(xphystr.size()-4).toInt();
    nsno = ConfigData->cfgoption[xphyno-1].currnsidx;
    ConfigData->cfgoption[xphyno-1].featurelock[nsno] = true;

    QMediaPlayer *music = new QMediaPlayer(this);
    music->setMedia(QUrl::fromLocalFile("../../resources/media/SmokeAlarm.wav"));
    music->setVolume(50);
    music->play();

    QString logmsg, pw;
    logmsg = xphystr;
    QString date = QDate::currentDate().toString("MM/dd/yyyy");
    QString time = QTime::currentTime().toString("hh:mm:ss");
    logmsg = QString(logmsg).append("--> "+date);
    logmsg = QString(logmsg).append("  "+time);
    pw = "Lock/Unlock feature, Invalid Password has reached its limit!";
    logmsg = QString(logmsg).append("  "+pw);
    emit eventsignal(logmsg);

    return;
}

void lockunlockDialog::on_buttonBox_rejected()
{
}

void lockunlockDialog::on_pushButton_2_clicked()
{

    QString filename = QFileDialog::getOpenFileName(this, "Open a File", QDir::homePath());
//    QMessageBox::information(this, "..",filename);
//    QString text;
//    QMessageBox box;
//    box.setText(filename);

    QMediaPlayer *music = new QMediaPlayer(this);
    music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
    music->setVolume(50);
    music->play();

    QString logmsg;
    if (ConfigData->xphy == "XPHY1")
    {
        logmsg = ConfigData->xphy;
    }
    else if (ConfigData->xphy == "XPHY2")
    {
        logmsg = ConfigData->xphy;
    }
    else
    {
        // for debug once it happens
        while (1);
    }
    QString date = QDate::currentDate().toString("MM/dd/yyyy");
    QString time = QTime::currentTime().toString("hh:mm:ss");
    logmsg = QString(logmsg).append("--> "+date);
    logmsg = QString(logmsg).append("  "+time);
    logmsg = QString(logmsg).append("  "+filename);
    logmsg = QString(logmsg).append(" file has been locked!");
    emit eventsignal(logmsg);
}

/*void lockunlockDialog::on_comboBox_2_activated(const QString &arg1)
{
    Q_UNUSED(arg1)
}*/
#if 0
void lockunlockDialog::on_comboBox_2_activated(const QString &arg1)
{
    Q_UNUSED(arg1)

    QString state, nsstr, xphystr;
    QString logmsg;
    int nsno = 0;
    int xphyno = 0;
/*
    nsstr = ui->comboBox_2->currentText();          // get ns
    nsno = arg1.right(arg1.size()-9).toInt();     // get string number only
*/
    if (ConfigData->cfgoption[0].xphy == "XPHY1")
    {
        xphystr = ConfigData->cfgoption[0].xphy;
    }
    else
    {
        xphystr = ConfigData->cfgoption[1].xphy;
    }

    xphyno = xphystr.right(xphystr.size()-4).toInt();

    if (ConfigData->cfgoption[xphyno-1].enablefeature[nsno-1].lock == false)
    {
        QMediaPlayer *music = new QMediaPlayer(this);
        music->setMedia(QUrl::fromLocalFile("../../resources/media/tarakatak.mp3"));
        music->setVolume(50);
        music->play();

//        ui->pushButton_2->setEnabled(false);
        logmsg = xphystr;
        QString date = QDate::currentDate().toString("MM/dd/yyyy");
        QString time = QTime::currentTime().toString("hh:mm:ss");
        logmsg = QString(logmsg).append("--> "+date);
        logmsg = QString(logmsg).append("  "+time);
        logmsg = QString(logmsg).append(" Lock/Unlock is not available to this");
        logmsg = QString(logmsg).append("  "+nsstr);
        emit eventsignal(logmsg);
        return;
    }

//    ui->pushButton_2->setEnabled(true);
}
#endif

void lockunlockDialog::updateprogress()
{
/*    ui->progressBar->show();

    progressval++;
    ui->progressBar->setValue(progressval);*/
}

