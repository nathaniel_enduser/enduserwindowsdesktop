#include <QApplication>

#include "darkstyle.h"
#include "framelesswindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setStyle(new DarkStyle);
    FramelessWindow framelessWindow;
    framelessWindow.setWindowIcon(QIcon(":/icons/icons/flexxon.png"));
    framelessWindow.set_splashscreen();

    return a.exec();
}
