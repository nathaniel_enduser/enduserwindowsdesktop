#include "calculationworker.h"

CalculationWorker::CalculationWorker(QObject *parent) : QObject(parent)
{

}

void CalculationWorker::calculate()
{
    m_stopFlag = false;

    int quantity = 1000;
    for ( int i = 0; i < quantity; i++ ) {
        if ( m_stopFlag ) {
            emit stopped();
            return;
        }

        // Do calculation here
        QThread::msleep( 10 );

        emit progress( i, quantity );
    }

    emit completed();
}

void CalculationWorker::stopCalculation()
{
    m_stopFlag = true;
}

