#ifndef PROTZONEDIALOG_H
#define PROTZONEDIALOG_H

#include <QDialog>
#include <QTimer>
#include <genconfigdialog.h>

class QProgressBar;

namespace Ui {
class protzoneDialog;
}

class protzoneDialog : public QDialog
{
    Q_OBJECT

public:
    explicit protzoneDialog(QWidget *parent = nullptr);
    ~protzoneDialog();

    void        savenamespace(QString ns);
    void        setcomboitem(int idx);
    void        savephxy(QString xphy);
    void        savedata(CONFIG_DATA *ConfigData);
    void        enadiscombobox(bool val);
    void        enadiscomboboxns(bool val);
    void        enadispbutton(bool val);
    void        enadispbutton2(bool val);
//    void        enadispbutton3(bool val);
    void        setcombotext(QString str);
    void        setcombotextns(QString str);
    void        setfocus(void);

    QString     protitemns;
    QString     protitemxphy;
    bool        protns = false;

    int         progressval;
    QTimer      *timer;

public slots:
    void        updateprogress();

private slots:
    void        on_pushButton_clicked();
    void        on_pushButton_2_clicked();
    void        on_comboBox_2_activated(const QString &arg1);
//    void        on_pushButton_3_clicked();

signals:
    void        eventsignal(QString);

private:
    Ui::protzoneDialog  *ui;
    CONFIG_DATA         *ConfigData;
    QProgressBar        *progressbar;
};

#endif // PROTZONEDIALOG_H
