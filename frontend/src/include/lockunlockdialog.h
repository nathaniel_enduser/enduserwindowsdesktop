#ifndef LOCKUNLOCKDIALOG_H
#define LOCKUNLOCKDIALOG_H

#include <QDialog>
#include <genconfigdialog.h>
#include <QTimer>

class QProgressBar;

namespace Ui {
class lockunlockDialog;
}

class lockunlockDialog : public QDialog
{
    Q_OBJECT

public:
    explicit lockunlockDialog(QWidget *parent = nullptr);
    ~lockunlockDialog();


    void    savenamespace(QString ns, QString xphy);
    void    enadispbutton(bool val);
    void    enadistextbox(bool val);
    void    savexphy(QString xphy);
    void    saveData(CONFIG_DATA *ConfigData);
    void    savefilefolder(QString filefolder, QString xphy);
    void    setcombotextns(QString str);
    void    setfocus(void);
    void    setcombotext(QString str);
    void    lockfile(bool state);

    QString     lockitemns;
    QString     lockitemxphy;
    QString     filefolder;
    int         progressval;
    QTimer      *timer;
    bool        locked;

public slots:
    void        updateprogress();

signals:
    void        eventsignal(QString);

private slots:
//    void    on_buttonBox_accepted();
    void        on_buttonBox_rejected();
    void        on_pushButton_clicked();
    void        on_pushButton_2_clicked();

//    void on_comboBox_2_activated(const QString &arg1);

private:
    Ui::lockunlockDialog    *ui;
    CONFIG_DATA             *ConfigData;
    int                     loginerrcnt;
//    bool                    featurelock=false;
    QString                 xphystr;
    QProgressBar            *progressbar;

};

#endif // LOCKUNLOCKDIALOG_H
