#ifndef PASSWORDDIALOG_H
#define PASSWORDDIALOG_H

#include <QDialog>
#include <QString>
#include <QTimer>
#include <enduserdesktop.h>

class QProgressBar;

namespace Ui {
class passwordDialog;
}

class passwordDialog : public QDialog
{
    Q_OBJECT

public:
    explicit passwordDialog(QWidget *parent = nullptr);
    ~passwordDialog();

    void            setLabelText(QString str);
    void            SetFocus(void);
    void            savenamespace(QString nspace);
    void            savexphy(QString xphy);
    void            saveData(CONFIG_DATA *ConfigData);
    void            setcomboitem(int idx);
    void            Disablecbox(void);
    void            addcombotext(QString str);
    void            enadiscombobox(bool val);
    void            Settoolbar(bool val);
    void            savepw(QString xphy, QString pw);
    void            setfocus();

    QString         pwmainwindowdata[8];
    QString         pwitemnamespace;    // current namespace1..8
    QString         pwitemxphy;         // xphy name

    int             progressval;
    QTimer          *timer;

public slots:
    void            updateprogress();

signals:
    void            eventsignal(QString);

private slots:
    void            on_pushButton_clicked();

private:
    Ui::passwordDialog  *ui;
    CONFIG_DATA         *ConfigData;
    QProgressBar        *progressbar;

};

#endif // PASSWORDDIALOG_H
