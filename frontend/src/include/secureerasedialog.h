#ifndef SECUREERASEDIALOG_H
#define SECUREERASEDIALOG_H

#include <QDialog>
#include <QTimer>
#include <genconfigdialog.h>

class QProgressBar;

namespace Ui {
class secureeraseDialog;
}

class secureeraseDialog : public QDialog
{
    Q_OBJECT

public:
    explicit secureeraseDialog(QWidget *parent = nullptr);
    ~secureeraseDialog();

    void        savenamespace(QString ns);
    void        savephxy(QString xphy);
    void        setcomboitem(int idx);
    void        saveData(CONFIG_DATA *ConfigData);
    void        enadiscombobox(bool val);
    void        enadiscomboboxns(bool val);
    void        enadispbutton(bool val);
    void        enadispbutton2(bool val);
    void        setcombotext(QString str);
    void        setcombotextns(QString str);
    void        setfocus(void);

    QString     seceraseitemns;
    QString     seceraseitemxphy;

    int         progressval;
    QTimer      *timer;

public slots:
    void            updateprogress();

signals:
    void            eventsignal(QString);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_comboBox_2_activated(const QString &arg1);

private:
    Ui::secureeraseDialog   *ui;
    CONFIG_DATA             *ConfigData;
    QProgressBar            *progressbar;
};

#endif // SECUREERASEDIALOG_H
