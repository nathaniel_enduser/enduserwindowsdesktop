#ifndef FRAMELESSWINDOW_H
#define FRAMELESSWINDOW_H

#include <QWidget>
#include <QLabel>
#include <QMovie>
#include <QTimer>

#include "mainwindow.h"
#include "enduserdesktop.h"

namespace Ui {
class FramelessWindow;
}

class FramelessWindow : public QWidget
{
    Q_OBJECT

public:
    explicit FramelessWindow(QWidget *parent = Q_NULLPTR);
    virtual ~FramelessWindow();
    void set_splashscreen();

private:
    bool leftBorderHit(const QPoint &pos);
    bool rightBorderHit(const QPoint &pos);
    bool topBorderHit(const QPoint &pos);
    bool bottomBorderHit(const QPoint &pos);
    void styleWindow(bool bActive, bool bNoState);

public slots:
    void setWindowTitle(const QString &text);
    void setWindowIcon(const QIcon &ico);

private slots:
    void on_applicationStateChanged(Qt::ApplicationState state);
    void on_minimizeButton_clicked();
    void on_restoreButton_clicked();
    void on_maximizeButton_clicked();
    void on_closeButton_clicked();
    void on_windowTitlebar_doubleClicked();
    void splashscreen_end();

protected:
    virtual void changeEvent(QEvent *event);
    virtual void mouseDoubleClickEvent(QMouseEvent *event);
    virtual void checkBorderDragging(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual bool eventFilter(QObject *obj, QEvent *event);

private:
    Ui::FramelessWindow *ui;
    QRect m_StartGeometry;
    MainWindow *Main;
    const quint8 CONST_DRAG_BORDER_SIZE = 15;
    bool m_bMousePressed;
    bool m_bDragTop;
    bool m_bDragLeft;
    bool m_bDragRight;
    bool m_bDragBottom;
    //Splashscreen
#if SPLASHSCREEN
    QLabel                      *Label;
    QMovie                      *Movie;
    QTimer                      *Timer;
#endif
};

#endif  // FRAMELESSWINDOW_H
