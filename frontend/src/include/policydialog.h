#ifndef POLICYDIALOG_H
#define POLICYDIALOG_H

#include <QDialog>
#include <QTimer>
#include <genconfigdialog.h>

class QProgressBar;

namespace Ui {
class policyDialog;
}

class policyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit policyDialog(QWidget *parent = nullptr);
    ~policyDialog();

    void        savenamespace(QString ns);
    void        savephxy(QString xphy);
    void        setcomboitem(int idx);
    void        savedata(CONFIG_DATA *ConfigData);
    void        enadiscombobox(bool val);
    void        enadiscomboboxns(bool val);
    void        enadispbutton(bool val);
    void        enadispbuttonns(bool val);
    void        enadischeckbox(bool val);
    void        setcombotext(QString str);
    void        setcombotextns(QString str);
    void        setfocus(void);

    QString     policyitemns;
    QString     policyitemxphy;
    int         progressval;
    QTimer      *timer;

public slots:
    void            updateprogress();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_comboBox_2_activated(const QString &arg1);

signals:
    void            eventsignal(QString);

private:
    Ui::policyDialog    *ui;
    CONFIG_DATA         *ConfigData;
    QProgressBar        *progressbar;

};

#endif // POLICYDIALOG_H
