#ifndef ATTACHNS_H
#define ATTACHNS_H

#include <QDialog>

namespace Ui {
class attachns;
}

class attachns : public QDialog
{
    Q_OBJECT

public:
    explicit attachns(QWidget *parent = nullptr);
    ~attachns();

private:
    Ui::attachns *ui;
};

#endif // ATTACHNS_H
