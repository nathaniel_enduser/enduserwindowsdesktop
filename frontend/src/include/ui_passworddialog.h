/********************************************************************************
** Form generated from reading UI file 'passworddialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PASSWORDDIALOG_H
#define UI_PASSWORDDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_passwordDialog
{
public:
    QGridLayout *gridLayout;
    QComboBox *comboBox;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QLabel *label;
    QProgressBar *progressBar;
    QPushButton *pushButton;

    void setupUi(QDialog *passwordDialog)
    {
        if (passwordDialog->objectName().isEmpty())
            passwordDialog->setObjectName(QString::fromUtf8("passwordDialog"));
        passwordDialog->resize(340, 217);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/changepassword.png"), QSize(), QIcon::Normal, QIcon::Off);
        passwordDialog->setWindowIcon(icon);
        gridLayout = new QGridLayout(passwordDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        comboBox = new QComboBox(passwordDialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 0, 0, 1, 1);

        label_3 = new QLabel(passwordDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font;
        font.setPointSize(14);
        label_3->setFont(font);

        gridLayout->addWidget(label_3, 0, 1, 1, 1);

        lineEdit = new QLineEdit(passwordDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        QFont font1;
        font1.setPointSize(12);
        lineEdit->setFont(font1);
        lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEdit, 1, 0, 1, 1);

        label_2 = new QLabel(passwordDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        gridLayout->addWidget(label_2, 1, 1, 1, 1);

        lineEdit_2 = new QLineEdit(passwordDialog);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setFont(font1);
        lineEdit_2->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEdit_2, 2, 0, 1, 1);

        label = new QLabel(passwordDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        gridLayout->addWidget(label, 2, 1, 1, 1);

        progressBar = new QProgressBar(passwordDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        QFont font2;
        font2.setPointSize(13);
        progressBar->setFont(font2);
        progressBar->setValue(0);

        gridLayout->addWidget(progressBar, 4, 0, 1, 2);

        pushButton = new QPushButton(passwordDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font);

        gridLayout->addWidget(pushButton, 3, 0, 1, 2);


        retranslateUi(passwordDialog);

        QMetaObject::connectSlotsByName(passwordDialog);
    } // setupUi

    void retranslateUi(QDialog *passwordDialog)
    {
        passwordDialog->setWindowTitle(QCoreApplication::translate("passwordDialog", "Change Password", nullptr));
        label_3->setText(QCoreApplication::translate("passwordDialog", "XPHY", nullptr));
        label_2->setText(QCoreApplication::translate("passwordDialog", "Old Password", nullptr));
        label->setText(QCoreApplication::translate("passwordDialog", "Password", nullptr));
        pushButton->setText(QCoreApplication::translate("passwordDialog", "Change Password", nullptr));
    } // retranslateUi

};

namespace Ui {
    class passwordDialog: public Ui_passwordDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PASSWORDDIALOG_H
