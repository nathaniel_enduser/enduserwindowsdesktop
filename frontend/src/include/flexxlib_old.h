#ifndef __FLEXXLIB_H__
#define __FLEXXLIB_H__

#include <windows.h>
#include <vector>
#include "flexx-nvme.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _DEBUG
#define FRIENDLY(fn)        friend void fn(void)
#else
#define FRIENDLY(fn)        /* none */
#endif

#ifdef _DEBUG
#define COMPILE_TIME_ASSERT(cond) {\
        char assert_this[(cond) ? 1 : -1]; \
        assert_this[0] = 'a';\
}
#else
#define COMPILE_TIME_ASSERT()
#endif

#define FLEXXON_VID     0x1d89
#define FLEXXON_SSVID   0x1d89

#define FLEXX_PASSED    1
#define FLEXX_FAILED    (!FLEXX_PASSED)

#define MAX_XPHY        128
#define MAX_NS_PER_XPHY 8

#define MAX_FILENAME_LEN        1024

#ifdef FLEXXLIB_EXPORTS
#define __flexxlib_api __declspec(dllexport)
#else
#define __flexxlib_api __declspec(dllimport)
#endif 

class RawDevice {
        virtual int open(void) = 0;
        virtual void close(void) = 0;
        virtual int ioctl(int request, ...) = 0;
};

class __flexxlib_api _Device : public RawDevice {
public:
        _Device(void);
        _Device(char* filename);
        _Device(const _Device& d);
        int setTarget(char* filename);
        ~_Device(void);
        int open(void);
        void close(void);
        int ioctl(int request, ...);
        friend __flexxlib_api int getFileTyp(_Device &d);
private:
        int setTargetFile(char *filename);
        HANDLE hdl;
        char* filename;
};

class NvmeDevice {
        virtual uint32_t get_nsid(void) = 0;
        virtual int identify(const uint32_t nsid, const uint32_t cns,
                             void *buffer, const uint32_t sz) = 0;
        virtual int namespace_management(const uint32_t nsid, const uint32_t sel,
                                         void *buffer, const uint32_t sz) = 0;
        virtual int log_page13(const uint32_t nsid, const uint32_t cdw10, const uint32_t cdw11,
                               const uint32_t cdw12, const uint32_t cdw13,
                               void *buffer, const uint32_t size) = 0;
        virtual int feature12(const uint32_t nsid, const uint32_t opcode,
                              const uint32_t cdw10, const uint32_t cdw11,
                              const uint32_t cdw12, void* buffer, const uint32_t sz,
                              uint32_t *cpl_dw0) = 0;
};

class __flexxlib_api NvmeDisk : public NvmeDevice {
public:
        NvmeDisk(_Device &d);
        ~NvmeDisk(void);
        uint32_t get_nsid(void) override;
        int identify(const uint32_t nsid, const uint32_t cns,
                     void *buffer, const uint32_t sz) override;
        int namespace_management(const uint32_t nsid, const uint32_t sel,
                                 void *buffer, const uint32_t sz) override;
        int log_page13(const uint32_t nsid, const uint32_t cdw10, const uint32_t cdw11,
                       const uint32_t cdw12, const uint32_t cdw13,
                       void *buffer, const uint32_t size) override;
        int feature12(const uint32_t nsid, const uint32_t opcode,
                      const uint32_t cdw10, const uint32_t cdw11,
                      const uint32_t cdw12, void* buffer, const uint32_t sz,
                      uint32_t *cpl_dw0) override;

public: /* overloaded some with implied namespace id given a disk file */

        /* identify */
        int identify(struct identify_controller_struct &idctrl);
        int identify(struct identify_namespace_struct &idns);

        /* namespace management */
        int create_namespace(struct create_ns_template_struct &tmpl,
                             uint32_t &new_nsid);
        int delete_namespace(void);

        /* log pages */
        int logpage(const uint32_t nsid, const uint32_t lid, const uint32_t lsp, uint32_t rae,
                    const uint64_t lpo, void *buffer, const uint32_t size);
        int logpage(std::vector<struct log_err_info_struct> &err_info,
                    const uint32_t count);
        int logpage(const int global, struct log_smart_info_struct &smart);
        int logpage(struct log_fw_slot_info_struct &fw_slot);

        /* features */
        int get_feature(const uint32_t nsid, const uint32_t fid,  const uint32_t sel,
                        const uint32_t cdw11, void *buffer,
                        const uint32_t size, uint32_t *cpldw0);
        int set_feature(const uint32_t nsid, const uint32_t fid, const uint32_t save,
                        const uint32_t cdw11, const uint32_t cdw12, void *buffer,
                        const uint32_t size, uint32_t *cpldw0);
private:
        NvmeDisk(void) { }
        _Device dev;

private:
        FRIENDLY(identify_controller_ct_assert);
        FRIENDLY(identify_namespace_ct_assert);
        FRIENDLY(create_namespace_ct_assert);
        FRIENDLY(log_page_ct_assert);
};

typedef std::vector<NvmeDisk> flexxDriveList;

#ifdef _DEBUG
extern void identify_controller_ct_assert(void);
extern void identify_namespace_ct_assert(void);
extern void create_namespace_ct_assert(void);
extern void log_page_ct_assert(void);
#else
#define identify_controller_ct_assert()
#define identify_namespace_ct_assert()
#define create_namespace_ct_assert()
#define log_page_ct_assert()
#endif

extern __flexxlib_api int flexx_next_device(char* filename, const int len, const int reset);
extern __flexxlib_api int flexx_criteria(_Device& d);
extern __flexxlib_api void scan_devices(flexxDriveList & driveList,
        int (*next_device)(char *filename, const int len, const int reset) = flexx_next_device,
        int (*criteria)(_Device& d) = flexx_criteria);
extern __flexxlib_api int is_same_xphy(NvmeDisk &d1, NvmeDisk &d2);

/* helper functions */
static inline int ns_mgmt_supported(struct identify_controller_struct &idcontrol)
{
        unsigned int oacs = idcontrol.oacs;
        return ((oacs & OACS_NS_MANAGEMENT) == OACS_NS_MANAGEMENT) ? 1 : 0;
}

static inline int getlog_lpo_supported(struct identify_controller_struct &idcontrol)
{
        unsigned int lpa = idcontrol.lpa;
        return ((lpa & LPA_GETLOG_EXT_DATA) == LPA_GETLOG_EXT_DATA) ? 1 : 0;
}

static inline int getlog_smart_per_ns_supported(struct identify_controller_struct &idcontrol)
{
        unsigned int lpa = idcontrol.lpa;
        return ((lpa & LPA_SMART_LOG) == LPA_SMART_LOG) ? 1 : 0;
}

#ifdef __cplusplus
}
#endif

#endif /* __FLEXXLIB_H__  */
