/********************************************************************************
** Form generated from reading UI file 'lockunlockdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOCKUNLOCKDIALOG_H
#define UI_LOCKUNLOCKDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_lockunlockDialog
{
public:
    QGridLayout *gridLayout;
    QComboBox *comboBox;
    QLabel *label;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QPushButton *pushButton;
    QProgressBar *progressBar;

    void setupUi(QDialog *lockunlockDialog)
    {
        if (lockunlockDialog->objectName().isEmpty())
            lockunlockDialog->setObjectName(QString::fromUtf8("lockunlockDialog"));
        lockunlockDialog->resize(426, 166);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/lockfile.png"), QSize(), QIcon::Normal, QIcon::Off);
        lockunlockDialog->setWindowIcon(icon);
        gridLayout = new QGridLayout(lockunlockDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        comboBox = new QComboBox(lockunlockDialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 0, 0, 1, 1);

        label = new QLabel(lockunlockDialog);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(14);
        label->setFont(font);

        gridLayout->addWidget(label, 0, 1, 1, 1);

        lineEdit = new QLineEdit(lockunlockDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setFont(font);
        lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEdit, 1, 0, 1, 1);

        label_2 = new QLabel(lockunlockDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        gridLayout->addWidget(label_2, 1, 1, 1, 1);

        pushButton = new QPushButton(lockunlockDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font);

        gridLayout->addWidget(pushButton, 1, 2, 1, 1);

        progressBar = new QProgressBar(lockunlockDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout->addWidget(progressBar, 2, 0, 1, 3);


        retranslateUi(lockunlockDialog);

        QMetaObject::connectSlotsByName(lockunlockDialog);
    } // setupUi

    void retranslateUi(QDialog *lockunlockDialog)
    {
        lockunlockDialog->setWindowTitle(QCoreApplication::translate("lockunlockDialog", "Lock/Unlock Namespace", nullptr));
        label->setText(QCoreApplication::translate("lockunlockDialog", "XPHY", nullptr));
        label_2->setText(QCoreApplication::translate("lockunlockDialog", "Password", nullptr));
        pushButton->setText(QCoreApplication::translate("lockunlockDialog", "Verify Password", nullptr));
    } // retranslateUi

};

namespace Ui {
    class lockunlockDialog: public Ui_lockunlockDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOCKUNLOCKDIALOG_H
