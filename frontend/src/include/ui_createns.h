/********************************************************************************
** Form generated from reading UI file 'createns.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATENS_H
#define UI_CREATENS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_createns
{
public:
    QGridLayout *gridLayout;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_11;
    QLabel *label_10;
    QLabel *label_5;
    QLabel *label_9;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label;
    QLabel *label_6;
    QLabel *label_14;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *label_18;
    QPushButton *pushButton;

    void setupUi(QDialog *createns)
    {
        if (createns->objectName().isEmpty())
            createns->setObjectName(QString::fromUtf8("createns"));
        createns->resize(352, 251);
        gridLayout = new QGridLayout(createns);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_7 = new QLabel(createns);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 9, 0, 1, 1);

        label_8 = new QLabel(createns);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 11, 0, 1, 1);

        label_11 = new QLabel(createns);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 1, 1, 1, 2);

        label_10 = new QLabel(createns);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 0, 1, 1, 2);

        label_5 = new QLabel(createns);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 5, 0, 1, 1);

        label_9 = new QLabel(createns);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 12, 0, 1, 1);

        label_2 = new QLabel(createns);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(createns);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_4 = new QLabel(createns);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        label = new QLabel(createns);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_6 = new QLabel(createns);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 7, 0, 1, 1);

        label_14 = new QLabel(createns);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout->addWidget(label_14, 5, 1, 1, 2);

        label_12 = new QLabel(createns);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 2, 1, 1, 2);

        label_13 = new QLabel(createns);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout->addWidget(label_13, 3, 1, 1, 2);

        label_15 = new QLabel(createns);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout->addWidget(label_15, 7, 1, 1, 2);

        label_16 = new QLabel(createns);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 9, 1, 1, 2);

        label_17 = new QLabel(createns);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout->addWidget(label_17, 11, 1, 1, 2);

        label_18 = new QLabel(createns);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout->addWidget(label_18, 12, 1, 1, 2);

        pushButton = new QPushButton(createns);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout->addWidget(pushButton, 21, 0, 1, 3);


        retranslateUi(createns);

        QMetaObject::connectSlotsByName(createns);
    } // setupUi

    void retranslateUi(QDialog *createns)
    {
        createns->setWindowTitle(QCoreApplication::translate("createns", "Create Namespace", nullptr));
        label_7->setText(QCoreApplication::translate("createns", "NVM Set iden:", nullptr));
        label_8->setText(QCoreApplication::translate("createns", "Timeout val, in ms:", nullptr));
        label_11->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        label_10->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        label_5->setText(QCoreApplication::translate("createns", "Multipath and Sharing Cap:", nullptr));
        label_9->setText(QCoreApplication::translate("createns", "Target block size:", nullptr));
        label_2->setText(QCoreApplication::translate("createns", "Capacity of NS:", nullptr));
        label_3->setText(QCoreApplication::translate("createns", "FLBA Size:", nullptr));
        label_4->setText(QCoreApplication::translate("createns", "Data Prot Cap:", nullptr));
        label->setText(QCoreApplication::translate("createns", "Size of NS:", nullptr));
        label_6->setText(QCoreApplication::translate("createns", "ANA Group Iden:", nullptr));
        label_14->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        label_12->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        label_13->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        label_15->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        label_16->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        label_17->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        label_18->setText(QCoreApplication::translate("createns", "TextLabel", nullptr));
        pushButton->setText(QCoreApplication::translate("createns", "Update Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class createns: public Ui_createns {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATENS_H
