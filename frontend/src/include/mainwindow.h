#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QSystemTrayIcon>
#include <QAction>
#include <QtCharts/QChart>
#include <QtCharts/QChartGlobal>
#include <QtCharts/QBarSeries>
#include <QRandomGenerator>
#include <QFileSystemModel>
#include <QThread>
#include <QTimer>
#include "enduserdesktop.h"
#include "genconfigdialog.h"

#include "calculationworker.h"
//#include "progressdialog.h"


QT_CHARTS_BEGIN_NAMESPACE
class QChartView;
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void                DriveDetection();
    QString             get_log(void);
    QSystemTrayIcon     *TrayIcon;
    bool                featurelock;

    int         currentrecord;
    QString     lockfilerecord[50];
    bool        lock;
    bool        xphydetected;

#if BACKEND_ENABLE
    void        initializebackend();
#endif

protected:
    void        closeEvent(QCloseEvent *event) override;


private:
    void        create_actions();
    void        create_chart();
    void        create_tray_icon();
    void        set_hardcoded_data();
    void        set_tree_widget();
    void        set_tree_directory();
    void        hide_displays();
    void        device_clicked();
    void        namespace_clicked();
    void        device_health();
    void        get_identify_controller(identify_controller_struct id_control);
    void        get_smart_health_info(log_smart_info_struct smart_info_global);
    void        display_miscdata(identify_namespace_struct id_namespace);
    void        get_id_namespace(identify_namespace_struct id_namespace);

private slots:
    void        right_click_menu(const QPoint &pos);
    void        item_pressed(QTreeWidgetItem *item, int Col);
    void        item_clicked(QTreeWidgetItem *item, int Col);
    void        log_event(QString);
    void        generatereport();
    void        changepassword();
    void        protectzone();
    void        changepolicy();
    void        lockfile();
    void        encryptdecryptenable();
    void        secureerase();
    void        signatureupdate();
    void        genconfiguration();
    void        updatefirmware();
    void        about();
    void        create_namespace();
    void        delete_namespace();
    void        attach_namespace();
    void        detach_namespace();
    void        lock_unlock(const QModelIndex &index);
    void        lock_unlock_dclick(const QModelIndex &index);
    void        execute_lockunlock();
    void        fwdownload();
    void        fwcommit();
    void        fwstatusreset(uint32_t status);
    void        reloadluparam();

private:
    Ui::MainWindow              *ui;
    genconfigDialog             *configDialog;
    QHash<QString, DEVICE_DATA> *DeviceDataHash;
    QString                     ItemName;
    QString                     CurrItemName;
    QString                     ItemType;
    QString                     CurrentXphyName;
    QTreeWidgetItem             *CurrentItem;

    identify_controller_struct  identifycontlr[DEVICE_COUNT];                     // identify controller, from xphy device
    identify_namespace_struct   identifynamespace[DEVICE_COUNT];                  // identify namespace, from xphy device
    log_smart_info_struct       xphyhealth[DEVICE_COUNT];                         // smart health information, from xphy thru log page

    // Namespace Menu options
    QMenu                       *NamespaceMenu;
    QAction                     *Password;
    QAction                     *Protect;
    QAction                     *Policy;
    QAction                     *EncDec;
    QAction                     *SecErase;
    QAction                     *SignUpdate;

    // Device Menu options
    QMenu                       *DeviceMenu;
    QAction                     *Create;
    QAction                     *Attach;
    QAction                     *Detach;
    QAction                     *Delete;
    QAction                     *FwDownload;

    //Chart
    QChart                      *BarChart;
    QBarSeries                  *BarSeries;
    QRandomGenerator            Rand;

    //System Tray Menu Options
    QMenu                       *TrayMenu;
    QAction                     *Open;
    QAction                     *Exit;

    QFileSystemModel            *SystemModel;
    QString                     SelectedFile;
};
#endif // MAINWINDOW_H
