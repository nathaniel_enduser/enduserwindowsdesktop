/********************************************************************************
** Form generated from reading UI file 'signupdatedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGNUPDATEDIALOG_H
#define UI_SIGNUPDATEDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_signupdateDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QComboBox *comboBox;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QFrame *line;
    QLabel *label_3;
    QComboBox *comboBox_2;
    QPushButton *pushButton_2;
    QProgressBar *progressBar;

    void setupUi(QDialog *signupdateDialog)
    {
        if (signupdateDialog->objectName().isEmpty())
            signupdateDialog->setObjectName(QString::fromUtf8("signupdateDialog"));
        signupdateDialog->resize(332, 260);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/signatureupdate.png"), QSize(), QIcon::Normal, QIcon::Off);
        signupdateDialog->setWindowIcon(icon);
        gridLayout = new QGridLayout(signupdateDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(signupdateDialog);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(14);
        label->setFont(font);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        comboBox = new QComboBox(signupdateDialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 1, 0, 1, 1);

        label_2 = new QLabel(signupdateDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        lineEdit = new QLineEdit(signupdateDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEdit, 3, 0, 1, 1);

        pushButton = new QPushButton(signupdateDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font);

        gridLayout->addWidget(pushButton, 3, 1, 1, 1);

        line = new QFrame(signupdateDialog);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 4, 0, 1, 2);

        label_3 = new QLabel(signupdateDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        gridLayout->addWidget(label_3, 5, 0, 1, 1);

        comboBox_2 = new QComboBox(signupdateDialog);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        gridLayout->addWidget(comboBox_2, 6, 0, 1, 1);

        pushButton_2 = new QPushButton(signupdateDialog);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font);

        gridLayout->addWidget(pushButton_2, 6, 1, 1, 1);

        progressBar = new QProgressBar(signupdateDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout->addWidget(progressBar, 7, 0, 1, 2);

        QWidget::setTabOrder(lineEdit, comboBox);
        QWidget::setTabOrder(comboBox, comboBox_2);
        QWidget::setTabOrder(comboBox_2, pushButton_2);
        QWidget::setTabOrder(pushButton_2, pushButton);

        retranslateUi(signupdateDialog);

        QMetaObject::connectSlotsByName(signupdateDialog);
    } // setupUi

    void retranslateUi(QDialog *signupdateDialog)
    {
        signupdateDialog->setWindowTitle(QCoreApplication::translate("signupdateDialog", "Update Signature", nullptr));
        label->setText(QCoreApplication::translate("signupdateDialog", "XPHY", nullptr));
        label_2->setText(QCoreApplication::translate("signupdateDialog", "Password", nullptr));
        pushButton->setText(QCoreApplication::translate("signupdateDialog", "Verify Password", nullptr));
        label_3->setText(QCoreApplication::translate("signupdateDialog", "XPHY", nullptr));
        pushButton_2->setText(QCoreApplication::translate("signupdateDialog", "Sign Update", nullptr));
    } // retranslateUi

};

namespace Ui {
    class signupdateDialog: public Ui_signupdateDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGNUPDATEDIALOG_H
