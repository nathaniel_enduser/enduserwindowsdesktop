#ifndef SIGNUPDATEDIALOG_H
#define SIGNUPDATEDIALOG_H

#include <QDialog>
#include <QTimer>
#include <genconfigdialog.h>

class QProgressBar;

namespace Ui {
class signupdateDialog;
}

class signupdateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit signupdateDialog(QWidget *parent = nullptr);
    ~signupdateDialog();

    void        savenamespace(QString ns);
    void        savexphy(QString xphy);
    void        setcomboitem(int idx);
    void        saveData(CONFIG_DATA *ConfigData);
    void        enadiscombobox(bool val);
    void        setcombotext(QString str);
    void        enadiscomboboxns(bool val);
    void        setcombotextns(QString str);
    void        enadispbutton2(bool val);
    void        enadispbutton3(bool val);
    void        disableradioall(void);
    void        savefilename(QString filename, QString CurrentXphyName);
    void        setfocus(void);

    QString     signitemns;
    QString     signitemxphy;

    int         progressval;
    QTimer      *timer;

public slots:
    void            updateprogress();

signals:
    void            eventsignal(QString);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_comboBox_2_activated(const QString &arg1);

private:
    Ui::signupdateDialog    *ui;
    CONFIG_DATA             *ConfigData;
    QProgressBar            *progressbar;
};

#endif // SIGNUPDATEDIALOG_H
