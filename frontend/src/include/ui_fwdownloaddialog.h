/********************************************************************************
** Form generated from reading UI file 'fwdownloaddialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FWDOWNLOADDIALOG_H
#define UI_FWDOWNLOADDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_fwdownloadDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *label_5;
    QPushButton *pushButton;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_6;

    void setupUi(QDialog *fwdownloadDialog)
    {
        if (fwdownloadDialog->objectName().isEmpty())
            fwdownloadDialog->setObjectName(QString::fromUtf8("fwdownloadDialog"));
        fwdownloadDialog->resize(372, 130);
        gridLayout = new QGridLayout(fwdownloadDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_5 = new QLabel(fwdownloadDialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 3, 1, 1, 1);

        pushButton = new QPushButton(fwdownloadDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout->addWidget(pushButton, 7, 0, 1, 2);

        label_4 = new QLabel(fwdownloadDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 2, 1, 1, 1);

        label_3 = new QLabel(fwdownloadDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 4, 0, 1, 1);

        label_2 = new QLabel(fwdownloadDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 3, 0, 1, 1);

        label = new QLabel(fwdownloadDialog);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 2, 0, 1, 1);

        label_6 = new QLabel(fwdownloadDialog);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 4, 1, 1, 1);


        retranslateUi(fwdownloadDialog);

        QMetaObject::connectSlotsByName(fwdownloadDialog);
    } // setupUi

    void retranslateUi(QDialog *fwdownloadDialog)
    {
        fwdownloadDialog->setWindowTitle(QCoreApplication::translate("fwdownloadDialog", "Dialog", nullptr));
        label_5->setText(QCoreApplication::translate("fwdownloadDialog", "TextLabel", nullptr));
        pushButton->setText(QCoreApplication::translate("fwdownloadDialog", "PushButton", nullptr));
        label_4->setText(QCoreApplication::translate("fwdownloadDialog", "TextLabel", nullptr));
        label_3->setText(QCoreApplication::translate("fwdownloadDialog", "Starting Dword offset, def 0:", nullptr));
        label_2->setText(QCoreApplication::translate("fwdownloadDialog", "Transfer Chunksize limit:", nullptr));
        label->setText(QCoreApplication::translate("fwdownloadDialog", "Firmware File:", nullptr));
        label_6->setText(QCoreApplication::translate("fwdownloadDialog", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class fwdownloadDialog: public Ui_fwdownloadDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FWDOWNLOADDIALOG_H
