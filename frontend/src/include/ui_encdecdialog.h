/********************************************************************************
** Form generated from reading UI file 'encdecdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ENCDECDIALOG_H
#define UI_ENCDECDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_encdecDialog
{
public:
    QGridLayout *gridLayout_2;
    QLabel *label;
    QPushButton *pushButton;
    QComboBox *comboBox_2;
    QLabel *label_2;
    QPushButton *pushButton_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QCheckBox *checkBox;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_7;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_8;
    QProgressBar *progressBar;
    QLineEdit *lineEdit;
    QLabel *label_3;
    QComboBox *comboBox;

    void setupUi(QDialog *encdecDialog)
    {
        if (encdecDialog->objectName().isEmpty())
            encdecDialog->setObjectName(QString::fromUtf8("encdecDialog"));
        encdecDialog->resize(412, 365);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/encryption.png"), QSize(), QIcon::Normal, QIcon::Off);
        encdecDialog->setWindowIcon(icon);
        gridLayout_2 = new QGridLayout(encdecDialog);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label = new QLabel(encdecDialog);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(14);
        label->setFont(font);

        gridLayout_2->addWidget(label, 0, 1, 1, 1);

        pushButton = new QPushButton(encdecDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font);

        gridLayout_2->addWidget(pushButton, 1, 2, 1, 1);

        comboBox_2 = new QComboBox(encdecDialog);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        gridLayout_2->addWidget(comboBox_2, 2, 0, 1, 1);

        label_2 = new QLabel(encdecDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        gridLayout_2->addWidget(label_2, 1, 1, 1, 1);

        pushButton_2 = new QPushButton(encdecDialog);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font);

        gridLayout_2->addWidget(pushButton_2, 2, 2, 1, 1);

        groupBox = new QGroupBox(encdecDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkBox = new QCheckBox(groupBox);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        gridLayout->addWidget(checkBox, 0, 0, 1, 1);

        checkBox_5 = new QCheckBox(groupBox);
        checkBox_5->setObjectName(QString::fromUtf8("checkBox_5"));

        gridLayout->addWidget(checkBox_5, 0, 1, 1, 1);

        checkBox_2 = new QCheckBox(groupBox);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));

        gridLayout->addWidget(checkBox_2, 1, 0, 1, 1);

        checkBox_6 = new QCheckBox(groupBox);
        checkBox_6->setObjectName(QString::fromUtf8("checkBox_6"));

        gridLayout->addWidget(checkBox_6, 1, 1, 1, 1);

        checkBox_3 = new QCheckBox(groupBox);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));

        gridLayout->addWidget(checkBox_3, 2, 0, 1, 1);

        checkBox_7 = new QCheckBox(groupBox);
        checkBox_7->setObjectName(QString::fromUtf8("checkBox_7"));

        gridLayout->addWidget(checkBox_7, 2, 1, 1, 1);

        checkBox_4 = new QCheckBox(groupBox);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));

        gridLayout->addWidget(checkBox_4, 3, 0, 1, 1);

        checkBox_8 = new QCheckBox(groupBox);
        checkBox_8->setObjectName(QString::fromUtf8("checkBox_8"));

        gridLayout->addWidget(checkBox_8, 3, 1, 1, 1);


        gridLayout_2->addWidget(groupBox, 4, 0, 2, 3);

        progressBar = new QProgressBar(encdecDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout_2->addWidget(progressBar, 6, 0, 1, 3);

        lineEdit = new QLineEdit(encdecDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout_2->addWidget(lineEdit, 1, 0, 1, 1);

        label_3 = new QLabel(encdecDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        gridLayout_2->addWidget(label_3, 2, 1, 1, 1);

        comboBox = new QComboBox(encdecDialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_2->addWidget(comboBox, 0, 0, 1, 1);


        retranslateUi(encdecDialog);

        QMetaObject::connectSlotsByName(encdecDialog);
    } // setupUi

    void retranslateUi(QDialog *encdecDialog)
    {
        encdecDialog->setWindowTitle(QCoreApplication::translate("encdecDialog", "Encrypt/Decrypt Enable/Disable", nullptr));
        label->setText(QCoreApplication::translate("encdecDialog", "XPHY", nullptr));
        pushButton->setText(QCoreApplication::translate("encdecDialog", "Verify Password", nullptr));
        label_2->setText(QCoreApplication::translate("encdecDialog", "Password", nullptr));
        pushButton_2->setText(QCoreApplication::translate("encdecDialog", "Enable/Disable", nullptr));
        groupBox->setTitle(QCoreApplication::translate("encdecDialog", "Encryption/Decryption Status Per Namespace", nullptr));
        checkBox->setText(QCoreApplication::translate("encdecDialog", "namespace1", nullptr));
        checkBox_5->setText(QCoreApplication::translate("encdecDialog", "namespace5", nullptr));
        checkBox_2->setText(QCoreApplication::translate("encdecDialog", "namespace2", nullptr));
        checkBox_6->setText(QCoreApplication::translate("encdecDialog", "namespace6", nullptr));
        checkBox_3->setText(QCoreApplication::translate("encdecDialog", "namespace3", nullptr));
        checkBox_7->setText(QCoreApplication::translate("encdecDialog", "namespace7", nullptr));
        checkBox_4->setText(QCoreApplication::translate("encdecDialog", "namespace4", nullptr));
        checkBox_8->setText(QCoreApplication::translate("encdecDialog", "namespace8", nullptr));
        label_3->setText(QCoreApplication::translate("encdecDialog", "Namespace", nullptr));
    } // retranslateUi

};

namespace Ui {
    class encdecDialog: public Ui_encdecDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ENCDECDIALOG_H
