#ifndef DELETENS_H
#define DELETENS_H

#include <QDialog>

namespace Ui {
class deletens;
}

class deletens : public QDialog
{
    Q_OBJECT

public:
    explicit deletens(QWidget *parent = nullptr);
    ~deletens();

private:
    Ui::deletens *ui;
};

#endif // DELETENS_H
