#ifndef CREATENS_H
#define CREATENS_H

#include <QDialog>

namespace Ui {
class createns;
}

class createns : public QDialog
{
    Q_OBJECT

public:
    explicit createns(QWidget *parent = nullptr);
    ~createns();

private:
    Ui::createns *ui;
};

#endif // CREATENS_H
