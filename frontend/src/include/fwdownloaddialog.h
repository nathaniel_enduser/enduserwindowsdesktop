#ifndef FWDOWNLOADDIALOG_H
#define FWDOWNLOADDIALOG_H

#include <QDialog>

namespace Ui {
class fwdownloadDialog;
}

class fwdownloadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit fwdownloadDialog(QWidget *parent = nullptr);
    ~fwdownloadDialog();

private:
    Ui::fwdownloadDialog *ui;
};

#endif // FWDOWNLOADDIALOG_H
