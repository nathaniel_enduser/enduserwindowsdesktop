#ifndef CREPORTDIALOG_H
#define CREPORTDIALOG_H

#include <QDialog>

namespace Ui {
class CReportDialog;
}

class CReportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CReportDialog(QWidget *parent = nullptr);
    ~CReportDialog();

private:
    Ui::CReportDialog *ui;
};

#endif // CREPORTDIALOG_H
