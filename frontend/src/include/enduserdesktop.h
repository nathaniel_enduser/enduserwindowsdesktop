#ifndef USEENDDESKTOP_H
#define USEENDDESKTOP_H

#include <QString>

#include "flexx-nvme.h"


//#define REPORT_HEADER     "Date,Time,Device,Namespace,Data1,Data2,Data3\n"
#define REPORT_HEADER       "Date,Time,Device,Namespace,Activity\n"
#define REPORT_CONTENTS     "%1,%2,%3,%4,%5\n"
#define CONFIG_HEADER       "Feature, encdecval, encval\n"
#define CONFIG_CONTENTS     "%1,%2,%3\n"
#define LOCK_HEADER         "Feature, currrec, lockfilerec, lockstr\n"
#define LOCK_CONTENTS       ", , , , %5 , %6, %7, %8\n"
#define POLICY_HEADER       "Feature, polenab, pstate0, pstate1, pstate2, pstate3, pstate4, pstate5\n"
#define POLICY_CONTENTS     ", , , , , , , , , %10 , %11, %12, %13, %14, %15, %16, %17\n"
#define PROTECT_HEADER      "Feature, protns\n"
#define PROTECT_CONTENTS    ", , , , , , , , , , , , , , , , , , %18, %19\n"
#define DEVICE_COUNT        2
#define NAMESPACE_COUNT     8
#define FILELOCK_COUNT      32
#define FOLDERLOCK_COUNT    32
#define XPHY_NAME           "XPHY"
#define NAMESPACE_NAME      "namespace"
#define PWORD_DEFAULT1      "old0"
#define PWORD_DEFAULT2      "old1"
#define LOCKED              1
#define UNLOCKED            0
#define BACKEND_ENABLE      1
#define SPLASHSCREEN        0
#define PASSWORDTRIES       2
#define BUFFERSIZE          1024
#define DETECTED            0
#define XPHYDETECTED        0

typedef struct Namespace
{
    QString NsName;
    QString Data1;
    QString Data2;
    QString Data3;
} NAMESPACE_DATA;

typedef struct Device
{
    int NsCount;
    NAMESPACE_DATA NS[NAMESPACE_COUNT];
} DEVICE_DATA;

// option enable/disable features
typedef struct optstruct
{
    bool        lock;
    bool        policy;
    bool        secerase;
    bool        encdec;
    bool        signupdate;
    bool        protzone;
} OPT;

// type of policies
typedef struct polstruct
{
    bool        policy1;
    bool        policy2;
    bool        policy3;
    bool        policy4;
    bool        policy5;
    bool        policy6;
} POLICY;

typedef struct lockfilestruct
{
    QString                     filelocked[FILELOCK_COUNT];         // up to 32 files can be locked
} LOCKFILESTRUCT;

typedef struct lockfolderstruct
{
    QString                     folderlocked[FOLDERLOCK_COUNT];     // up to 32 folder can be locked
} LOCKFOLDERSTRUCT;

typedef struct conf_opt
{
    POLICY                      policies[NAMESPACE_COUNT];          // policy means related to low threshold etc. 6 policies, not final yet, need final specs
    OPT                         enablefeature[NAMESPACE_COUNT];     // enable feature per namespace
    QString                     nspace[NAMESPACE_COUNT];            // namespace 1..8
    int                         currnsidx;                          // for lock file/folder current namespace index
    QString                     filefolder;                         // for lock file/folder selected file or folder
    QString                     filename;                           // signature update
    bool                        featurelock[NAMESPACE_COUNT];
    LOCKFILESTRUCT              lockedfile[NAMESPACE_COUNT];        // file locked reference that can be unlock
    LOCKFOLDERSTRUCT            lockedfolder[NAMESPACE_COUNT];      // folder locked reference that can be unlock
    QString                     protectnamespace[NAMESPACE_COUNT];  // protected namespace
    QString                     password;                           // xphy password
} CFG_OPT;

typedef struct config_data
{
    CFG_OPT                     cfgoption[DEVICE_COUNT];            // which xphy
    QString                     xphy;
} CONFIG_DATA;





#endif // USEENDDESKTOP_H
