/********************************************************************************
** Form generated from reading UI file 'attachns.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ATTACHNS_H
#define UI_ATTACHNS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_attachns
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label_4;
    QPushButton *pushButton;

    void setupUi(QDialog *attachns)
    {
        if (attachns->objectName().isEmpty())
            attachns->setObjectName(QString::fromUtf8("attachns"));
        attachns->resize(371, 114);
        gridLayout = new QGridLayout(attachns);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(attachns);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_3 = new QLabel(attachns);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 0, 1, 1, 1);

        label_2 = new QLabel(attachns);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_4 = new QLabel(attachns);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 1, 1, 1, 1);

        pushButton = new QPushButton(attachns);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout->addWidget(pushButton, 2, 0, 1, 2);


        retranslateUi(attachns);

        QMetaObject::connectSlotsByName(attachns);
    } // setupUi

    void retranslateUi(QDialog *attachns)
    {
        attachns->setWindowTitle(QCoreApplication::translate("attachns", "Attach Namespace", nullptr));
        label->setText(QCoreApplication::translate("attachns", "Namespace to attach:", nullptr));
        label_3->setText(QCoreApplication::translate("attachns", "TextLabel", nullptr));
        label_2->setText(QCoreApplication::translate("attachns", "Optional comma-sep contr id list:", nullptr));
        label_4->setText(QCoreApplication::translate("attachns", "TextLabel", nullptr));
        pushButton->setText(QCoreApplication::translate("attachns", "Update Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class attachns: public Ui_attachns {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ATTACHNS_H
