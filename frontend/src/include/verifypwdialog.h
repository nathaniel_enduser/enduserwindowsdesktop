#ifndef VERIFYFWDIALOG_H
#define VERIFYFWDIALOG_H

#include <QDialog>

namespace Ui {
class verifyfwDialog;
}

class verifyfwDialog : public QDialog
{
    Q_OBJECT

public:
    explicit        verifyfwDialog(QWidget *parent = nullptr);
    ~verifyfwDialog();

    void            SetFocus(void);
//    QString         inputpassword;
    QString         pwitemnsverify;
//    QString         pwitemnamespace;

private slots:
    void on_pushButton_clicked();


private:
    Ui::verifyfwDialog *ui;
};

#endif // VERIFYFWDIALOG_H
