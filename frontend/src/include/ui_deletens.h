/********************************************************************************
** Form generated from reading UI file 'deletens.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DELETENS_H
#define UI_DELETENS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_deletens
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label_4;
    QPushButton *pushButton;

    void setupUi(QDialog *deletens)
    {
        if (deletens->objectName().isEmpty())
            deletens->setObjectName(QString::fromUtf8("deletens"));
        deletens->resize(311, 114);
        gridLayout = new QGridLayout(deletens);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(deletens);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_3 = new QLabel(deletens);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 0, 1, 1, 1);

        label_2 = new QLabel(deletens);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_4 = new QLabel(deletens);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 1, 1, 1, 1);

        pushButton = new QPushButton(deletens);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout->addWidget(pushButton, 2, 0, 1, 2);


        retranslateUi(deletens);

        QMetaObject::connectSlotsByName(deletens);
    } // setupUi

    void retranslateUi(QDialog *deletens)
    {
        deletens->setWindowTitle(QCoreApplication::translate("deletens", "Delete Namespace", nullptr));
        label->setText(QCoreApplication::translate("deletens", "Namespace to delete:", nullptr));
        label_3->setText(QCoreApplication::translate("deletens", "TextLabel", nullptr));
        label_2->setText(QCoreApplication::translate("deletens", "Timeout value in ms", nullptr));
        label_4->setText(QCoreApplication::translate("deletens", "TextLabel", nullptr));
        pushButton->setText(QCoreApplication::translate("deletens", "Update Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class deletens: public Ui_deletens {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DELETENS_H
