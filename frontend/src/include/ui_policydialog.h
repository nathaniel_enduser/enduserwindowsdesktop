/********************************************************************************
** Form generated from reading UI file 'policydialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_POLICYDIALOG_H
#define UI_POLICYDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_policyDialog
{
public:
    QGridLayout *gridLayout_2;
    QLabel *label_3;
    QComboBox *comboBox;
    QLabel *label;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QFrame *line;
    QLabel *label_4;
    QComboBox *comboBox_2;
    QPushButton *pushButton_2;
    QProgressBar *progressBar;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox;
    QCheckBox *checkBox_8;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_4;

    void setupUi(QDialog *policyDialog)
    {
        if (policyDialog->objectName().isEmpty())
            policyDialog->setObjectName(QString::fromUtf8("policyDialog"));
        policyDialog->resize(435, 360);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/changepolicy.png"), QSize(), QIcon::Normal, QIcon::Off);
        policyDialog->setWindowIcon(icon);
        gridLayout_2 = new QGridLayout(policyDialog);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_3 = new QLabel(policyDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font;
        font.setPointSize(14);
        label_3->setFont(font);

        gridLayout_2->addWidget(label_3, 0, 0, 1, 1);

        comboBox = new QComboBox(policyDialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_2->addWidget(comboBox, 1, 0, 1, 1);

        label = new QLabel(policyDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        gridLayout_2->addWidget(label, 2, 0, 1, 1);

        lineEdit = new QLineEdit(policyDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout_2->addWidget(lineEdit, 3, 0, 1, 1);

        pushButton = new QPushButton(policyDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setWeight(50);
        pushButton->setFont(font1);

        gridLayout_2->addWidget(pushButton, 3, 1, 1, 1);

        line = new QFrame(policyDialog);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line, 4, 0, 1, 2);

        label_4 = new QLabel(policyDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        gridLayout_2->addWidget(label_4, 5, 0, 1, 1);

        comboBox_2 = new QComboBox(policyDialog);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        gridLayout_2->addWidget(comboBox_2, 6, 0, 1, 1);

        pushButton_2 = new QPushButton(policyDialog);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font1);

        gridLayout_2->addWidget(pushButton_2, 6, 1, 1, 1);

        progressBar = new QProgressBar(policyDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout_2->addWidget(progressBar, 8, 0, 1, 2);

        groupBox = new QGroupBox(policyDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkBox_3 = new QCheckBox(groupBox);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));

        gridLayout->addWidget(checkBox_3, 2, 0, 1, 1);

        checkBox_2 = new QCheckBox(groupBox);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));

        gridLayout->addWidget(checkBox_2, 1, 0, 1, 1);

        checkBox = new QCheckBox(groupBox);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        gridLayout->addWidget(checkBox, 0, 0, 1, 1);

        checkBox_8 = new QCheckBox(groupBox);
        checkBox_8->setObjectName(QString::fromUtf8("checkBox_8"));

        gridLayout->addWidget(checkBox_8, 2, 1, 1, 1);

        checkBox_5 = new QCheckBox(groupBox);
        checkBox_5->setObjectName(QString::fromUtf8("checkBox_5"));

        gridLayout->addWidget(checkBox_5, 1, 1, 1, 1);

        checkBox_4 = new QCheckBox(groupBox);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));

        gridLayout->addWidget(checkBox_4, 0, 1, 1, 1);


        gridLayout_2->addWidget(groupBox, 7, 0, 1, 2);

        QWidget::setTabOrder(lineEdit, comboBox_2);
        QWidget::setTabOrder(comboBox_2, comboBox);
        QWidget::setTabOrder(comboBox, pushButton);
        QWidget::setTabOrder(pushButton, pushButton_2);
        QWidget::setTabOrder(pushButton_2, checkBox_3);
        QWidget::setTabOrder(checkBox_3, checkBox_2);
        QWidget::setTabOrder(checkBox_2, checkBox);

        retranslateUi(policyDialog);

        QMetaObject::connectSlotsByName(policyDialog);
    } // setupUi

    void retranslateUi(QDialog *policyDialog)
    {
        policyDialog->setWindowTitle(QCoreApplication::translate("policyDialog", "Change Policy", nullptr));
        label_3->setText(QCoreApplication::translate("policyDialog", "XPHY", nullptr));
        label->setText(QCoreApplication::translate("policyDialog", "Password", nullptr));
        pushButton->setText(QCoreApplication::translate("policyDialog", "Verify Password", nullptr));
        label_4->setText(QCoreApplication::translate("policyDialog", "Namespace", nullptr));
        pushButton_2->setText(QCoreApplication::translate("policyDialog", "Change Policy", nullptr));
        groupBox->setTitle(QCoreApplication::translate("policyDialog", "Warning or Critical Policies", nullptr));
        checkBox_3->setText(QCoreApplication::translate("policyDialog", "Unsafe Shutdown", nullptr));
        checkBox_2->setText(QCoreApplication::translate("policyDialog", "Temperature", nullptr));
        checkBox->setText(QCoreApplication::translate("policyDialog", "Free Space", nullptr));
        checkBox_8->setText(QCoreApplication::translate("policyDialog", "Critical Warning", nullptr));
        checkBox_5->setText(QCoreApplication::translate("policyDialog", "Number of error Information log entires", nullptr));
        checkBox_4->setText(QCoreApplication::translate("policyDialog", "Media and Data Integrity Errors", nullptr));
    } // retranslateUi

};

namespace Ui {
    class policyDialog: public Ui_policyDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_POLICYDIALOG_H
