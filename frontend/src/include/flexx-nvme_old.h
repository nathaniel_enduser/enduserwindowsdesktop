#ifndef __FLEXX_NVME_H__
#define __FLEXX_NVME_H__

#include <stdint.h>

enum {
        admin_delete_iosq = 0x00,
        admin_create_iosq = 0x01,
        admin_get_log_page = 0x02,
        admin_delete_iocq = 0x04,
        admin_create_iocq = 0x05,
        admin_identify = 0x06,
        admin_abort = 0x08,
        admin_set_features = 0x09,
        admin_get_features = 0x0a,
        admin_async_event_request = 0x0c,
        admin_namespace_management = 0x0d,
        admin_fw_commit = 0x10,
        admin_fw_img_download = 0x11,
        admin_device_self_test = 0x14,
        admin_namespace_attachment = 0x15,
        admin_keep_alive = 0x18,
        admin_directive_send = 0x19,
        admin_directive_recv = 0x1a,
        admin_virt_management = 0x1c,
        admin_nvme_mi_send = 0x1d,
        admin_nvme_mi_recv = 0x1e,
        admin_db_buffer_config = 0x7c,
        admin_format_nvm = 0x80,
        admin_security_send = 0x81,
        admin_security_recv = 0x82,
        admin_sanitize = 0x3,
};

enum {
        cns_idns = 0x00,
        cns_idcntrl = 0x01,
        cns_nsid_list = 0x2, 
        cns_nsid_desc = 0x3, 
        cns_mgmt_nsid_list = 0x10,
        cns_mgmt_idns = 0x11,
        cns_mgmt_attached_cntrl_list = 0x12,
        cns_mgmt_cntrl_list = 0x13,
        cns_mgmt_primary_cntrl = 0x14,
        cns_mgmt_secondary_cntrl = 0x15,
};

enum {
        namespace_mgmt_create = 0x00,
        namespace_mgmt_delete = 0x01,
};

enum {
        arbitration_feat = 0x01,
        pwm_feat = 0x02,
        lba_range_type_feat = 0x03,
        temp_threshold_feat = 0x04,
        error_recovery_feat = 0x05,
        volatile_write_cache_feat = 0x06,
        number_of_queues_feat = 0x07,
        interrupt_coalescing_feat = 0x08,
        interrupt_vector_configuration_feat = 0x09,
        write_atomicity_feat = 0x0a,
        async_event_configuration_feat = 0x0b,
        autonomous_power_state_transition_feat = 0x0c,
        host_memory_buffer_feat = 0x0d,
        timestamp_feat = 0x0e,
        keep_alive_timer_feat = 0x0f,
        host_controlled_thermal_management_feat = 0x10,
        non_operational_power_state_config_feat = 0x11,

        software_progress_marker_feat = 0x80,
        host_identifier_feat = 0x81,
        resv_notification_mask_feat = 0x82,
        resv_persistence_feat = 0x83,
};

enum {
        sel_current = 0x0,
        sel_default = 0x1,
        sel_saved = 0x2,
        sel_supported_cap = 0x3,
};

enum {
        lpid_error_info = 0x01,
        lpid_smart_health_info = 0x02,
        lpid_firmware_slot_info = 0x03,
        lpid_changed_ns_list = 0x04,
        lpid_cmds_supported_and_effects = 0x05,
        lpid_device_self_test = 0x06,
        lpid_telemetry_host = 0x07,
        lpid_telemetry_controller = 0x08,
        lpid_discover = 0x70,
        lpid_resv_notification = 0x80,
        lpid_sanitize_status = 0x81,
};

struct nvme_command_packet {
        uint8_t opcode;
        uint8_t flags;
        uint16_t cid;
        uint32_t nsid;
        uint32_t cdw2[2];
        uint64_t metadata;
        uint64_t prp1;
        uint64_t prp2;
        uint32_t cdw10;
        uint32_t cdw11;
        uint32_t cdw12;
        uint32_t cdw13;
        uint32_t cdw14;
        uint32_t cdw15;
};

struct power_state_desciptor {
        uint16_t mp;            /* 15:00 */
        uint8_t reserved0;      /* 23:16 */
        uint8_t nops_mxps;      /* 31:24 */
#define NOPS_MXPS_MXPS          (1 << 0)
#define NOPS_MXPS_NOPS          (1 << 1)

        uint32_t enlat;         /* 63:32 */
        uint32_t exlat;         /* 95:64 */
        
        uint8_t rrt;            /* 103:96 */
        uint8_t rrl;            /* 111:104 */
        uint8_t rwt;            /* 119:112 */
        uint8_t rwl;            /* 127:120 */

        uint16_t idlp;          /* 143:128 */

        uint8_t ips;            /* 151:144 */
#define IPS_NOT_REPORTED        (0 << 6)
#define IPS_0_0001W             (1 << 6)
#define IPS_0_01W               (2 << 6)

        uint8_t reserverd1;     /* 159:152 */
        uint16_t actp;          /* 175:160 */

        uint8_t aps_apw;        /* 183:176 */
#define APS_APW_APW_MASK        (7 << 0)
#define APS_APW_APS_MASK        (3 << 6)

        uint8_t reserved2[9];   /* 255:184 */
};

struct identify_controller_struct {
        uint16_t vid;       /* 01:00 */
        uint16_t ssvid;     /* 03:02 */
        uint8_t  sn[20];    /* 23:04 */
        uint8_t  mn[40];    /* 53:24 */
        uint8_t  fr[8];     /* 71:64 */
        uint8_t  rab;       /* 72    */
        uint8_t  ieee[3];   /* 75:73 */
        uint8_t  cmic;      /* 76    */
        uint8_t  mdts;      /* 77    */
        uint8_t  cntlid[2]; /* 79:78 */
        struct {
                uint8_t ter;
                uint8_t mnr;
                uint16_t mjr;
        } ver;              /* 83: 80 */
#define MAJOR_VER_1 0x01
#define MINOR_VER_0 0
#define MINOR_VER_1 0x01
#define MINOR_VER_2 0x02
#define MINOR_VER_3 0x03
#define TERTIARY_VERSION_0 0
        uint32_t  rtd3r;   /* 87:84 */
        uint32_t  rtd3e;   /* 91:88 */
        uint32_t  oaes;    /* 95:92 */
#define OAES_FW_ACTIVATION_NOTICE       (1 << 9)
#define OAES_NS_ATTRIBUTE_NOTICE        (1 << 8)

        uint32_t  ctratt;  /* 99:96 */
#define NOP_STATE_MODE          (1 << 1)
#define HOST_ID_128             (1 << 0)

        uint8_t reserved0[12];  /* 111:100 */
        uint8_t fguid[16];      /* 127:112 */
        uint8_t reserved1[112]; /* 239:128 */
        uint8_t nvme_management_intf_spec_resv[16];  /* 255:240 */
        uint16_t oacs;          /* 257:256 */
#define OACS_SECURITY_SEND_RECV         (1 << 0)
#define OACS_FORMAT_NVM                 (1 << 1)
#define OACS_FW_COMMIT_DOWNLOAD         (1 << 2)
#define OACS_NS_MANAGEMENT              (1 << 3)
#define OACS_DEVICE_SELF_TEST           (1 << 4)
#define OACS_DIRECTIVES                 (1 << 5)
#define OACS_NVMEMI_SUPPORTED           (1 << 6)
#define OACS_VIRT_MANAGEMENT            (1 << 7)
#define OACS_DOORBELL_BUFFER_CONFIG     (1 << 8)

        uint8_t acl;           /* 258 */
        uint8_t aerl;          /* 259 */
        uint8_t frmw;          /* 260 */
        uint8_t lpa;           /* 261 */
#define LPA_SMART_LOG           (1 << 0)
#define LPA_CMDSUPP_EFFECTS_LOG (1 << 1)
#define LPA_GETLOG_EXT_DATA     (1 << 2)
#define LPA_TELEMETERY          (1 << 3)

        uint8_t elpe;           /* 262 */
        uint8_t npss;           /* 263 */
        uint8_t avscc;          /* 264 */
#define AVSCC_ADMIN_VS          (1 << 0)

        uint8_t apsta;          /* 265 */
#define APSTA_AUTONOMOUS_PS     (1 << 0)

        uint16_t wctemp;        /* 267:266 */
        uint16_t cctemp;        /* 269:268 */
        uint16_t mtfa;          /* 271:270 */
        uint32_t hmpre;         /* 275:272 */
        uint32_t hmmin;         /* 279:276 */
        
        struct {
                uint64_t lo;
                uint64_t hi;
        } tnvmcap;              /* 295:280 */

        struct {
                uint64_t lo;
                uint64_t hi;
        } unvmcap;              /* 311:296 */

        uint32_t rpmbs;         /* 315:312 */ 
        uint16_t edstt;         /* 317:316 */
        uint8_t dsto;           /* 318     */
        uint8_t fwug;           /* 319     */
        uint16_t kas;           /* 321:320 */
        uint16_t hctma;         /* 323:322 */
        uint16_t mntmt;         /* 325:324 */
        uint16_t mxtmt;         /* 327:326 */
        uint32_t sanicap;       /* 331:328 */
#define SANICAP_CRYPTO_ERASE            (1 << 0)
#define SANICAP_BLK_ERASE_SANITIZE      (1 << 1)
#define SANICAP_OVERWRITE_SANITIZE      (1 << 2)

        uint8_t reserved2[180]; /* 511:332 */
        uint8_t sqes;           /* 512     */
        uint8_t cqes;           /* 513     */
        uint16_t maxcmd;        /* 515:514 */
        uint32_t nn;            /* 519:516 */
        uint16_t oncs;          /* 521:520 */
#define ONCS_COMPARE            (1 << 0)
#define ONCS_WR_UNCORR          (1 << 1)
#define ONCS_DATASET_MGMT       (1 << 2)
#define ONCS_WR_ZEROES          (1 << 3)
#define ONCS_SAVE_FIELD_NON_ZERO        (1 << 4)
#define ONCS_RESERVATIONS       (1 << 5)
#define ONCS_TIMESTAMP          (1 << 6)

        uint16_t fuses;         /* 523:522 */
#define FUSES_COMP_WRITE_FUSED  (1 << 0)

        uint8_t fna;            /* 524     */
#define FNA_FORMAT              (1 << 0)
#define FNA_ERASE               (1 << 1)
#define FNA_CRYPT_ERASE         (1 << 2)

        uint8_t vwc;            /* 525     */
#define VWC_PRESENT             (1 << 0)

        uint16_t awun;          /* 527:526 */
        uint16_t awupf;         /* 529:528 */
        uint8_t nvscc;          /* 530     */
        uint8_t reserved3;      /* 531     */
        uint16_t acwu;          /* 533:532 */
        uint16_t reserved4;     /* 535:534 */
        uint32_t sgls;          /* 539:536 */
        uint8_t reserved5[228]; /* 767:540 */
        uint8_t subnqn[256];    /* 1023:768 */
        uint8_t reserved6[768]; /* 1791:1024 */
        uint8_t nvme_over_fabrics_resv[256]; /* 2047:1792 */
        struct power_state_desciptor psdx[32];
        uint8_t vs[1024];   /* 4095:3072 */
};

struct lba_formatx_struct {
        uint16_t ms;    /* 15:00 */
        uint8_t lbads;  /* 23:16 */
        uint8_t rp;    /* 31:24 */
#define RP_BEST_PERFORMANCE     0x0
#define RP_BETTER_PERFORMANCE   0x1
#define RP_GOOD_PERFORMANCE     0x2
#define RP_DEGRADED_PERFORMANCE 0x3
};

struct identify_namespace_struct {
        uint64_t nsze;  /* 7:0   */
        uint64_t ncap;  /* 15:8  */
        uint64_t nuse;  /* 23:16 */
        uint8_t nsfeat; /* 24    */
#define NSFEAT_THIN_PROV        (1 << 0)
#define NSFEAT_OPTS             (1 << 1)
#define NSFEAT_DEALLOC          (1 << 2)
#define NSFEAT_NZERO_FIELDS     (1 << 3)

        uint8_t nlbaf;  /* 25    */
        uint8_t flbas;  /* 26    */
#define FLBAS_SUPPORTED_FORM_MASK       (0xf << 0)
#define FLBAS_MDATA_XFER                (1 << 4)

        uint8_t mc;     /* 27    */
#define MC_MDATA_EXTDATA        (1 << 0)
#define MC_MDATA_SEPARATE       (1 << 1)

        uint8_t dpc;    /* 28    */
#define DPC_TYPE_1      (1 << 0)
#define DPC_TYPE_2      (1 << 1)
#define DPC_TYPE_3      (1 << 2)
#define DPC_FIRST_8     (1 << 3)
#define DPC_LAST_8      (1 << 4)

        uint8_t dps;    /* 29    */
#define DPS_PROT_INFO_MASK      (7 << 0)
#define DPS_PROT_DISABLED       (0)
#define DPS_PROT_TYPE_1         (1)
#define DPS_PROT_TYPE_2         (2)
#define DPS_PROT_TYPE_3         (3)
#define DPS_FIRST_8             (1 << 3)

        uint8_t nmic;   /* 30    */
#define NMIC_SHARED             (1 << 0)

        uint8_t rescap; /* 31    */
#define RESCAP_PERSIST_POWER_LOSS       (1 << 0)
#define RESCAP_WREX_RESERVATION         (1 << 1)
#define RESCAP_EXACCESS_RESERVATION     (1 << 2)
#define RESCAP_WREX_REGISTRANTS         (1 << 3)
#define RESCAP_EXACCESS_REGISTRANTS     (1 << 4)
#define RESCAP_WREX_ALL_REGISTRANTS     (1 << 5)
#define RESCAP_EXACCESS_ALL_REGISTRANTS (1 << 6)
#define RESCAP_IGNORE_EXISTING_KEY      (1 << 7)

        uint8_t fpi;    /* 32   */
#define FPI_PROGRESS_INDICATOR_MASK     (0x7f << 0)
#define FPI_PROGRESS_SUPPORTED          (1 << 7)

        uint8_t dlfeat; /* 33   */
#define DLFEAT_DEALLOC_VALUE_MASK               (7 << 0)
#define DLFEAT_DEALLOC_VALUE_NOT_REPORTED       (0)
#define DLFEAT_DEALLOC_VALUE_00                 (1)
#define DLFEAT_DEALLOC_VALUE_FF                 (2)
#define DLFEAT_DEALLOC_WRITE_ZONE               (1 << 3)
#define DLFEAT_GUARD_FIELD                      (1 << 4)

        uint16_t nawun;         /* 35:34 */
        uint16_t nawupf;        /* 37:36 */
        uint16_t nacwu;         /* 39:38 */
        uint16_t nabsn;         /* 41:40 */
        uint16_t nabo;          /* 43:42 */
        uint16_t nabspf;        /* 45:44 */
        uint16_t noiob;         /* 47:46 */

        struct {
                uint64_t lo;
                uint64_t hi;
        } nvmcap;               /* 63:48 */

        uint8_t reserved0[40];  /* 103:64 */

        uint8_t nguid[16];      /* 119:104 */
        uint64_t eui64;         /* 127:120 */
        struct lba_formatx_struct lbaf[16];

        uint8_t reserved2[192]; /* 383:192 */

        uint8_t vs[3712];       /* 4095:384 */

};

struct create_ns_template_struct {
        uint64_t nsze;
        uint64_t ncap;
        uint8_t flbas;
        uint8_t dps;
        uint8_t nmic;
};

struct log_err_info_struct {
        uint64_t error_count;   /* 07:00 */
        uint16_t sqid;          /* 09:08 */
        uint16_t cid;           /* 11:10 */
        uint16_t status_field;  /* 13:12 */
        uint16_t param_err_loc; /* 15:14 */
        uint64_t lba;           /* 23:16 */
        uint32_t namespc;       /* 27:24 */
        uint8_t vendor_info_available; /* 28 */
        uint8_t resv0[2];       /* 31:29 */
        uint64_t cmd_specific_info; /* 39:32 */
        uint8_t resv1[24];      /*  63:40 */
};

struct temp_sensor_data_struct {
        uint16_t tst;
};

struct log_smart_info_struct {
        uint32_t info1;       /* 3:0 */
#define INFO1_CRITICAL_WARNING_MASK     (1 << 0)
#define INFO1_COMPOSITE_TEMP_MASK       (3 << 1)
#define INFO1_AVAIL_SPARE_MAKE          (1 << 3)

        uint8_t avail_spare_threshold;  /* 4 */
        uint8_t percentage_used;        /* 5 */
        uint8_t resv0[26];              /* 31:6 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } data_units_read;              /* 47:32 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } data_units_written;           /* 63:48 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } host_read_commands;           /* 79:64 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } host_write_commands;          /* 95:80 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } controller_busy_time;         /* 111:96 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } power_cycles;                 /* 127:112 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } power_on_hours;               /* 143:128 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } unsafe_shutdowns;             /* 159:144 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } data_integrity_errors;        /* 175:160 */
        struct {
                uint64_t lo;
                uint64_t hi;
        } number_of_error_info_log_entries; /* 191:176 */
        uint32_t warning_composite_temp_time; /* 195:192 */
        uint32_t critical_composite_temp_time; /* 199:196 */
        struct temp_sensor_data_struct temp_sensor[8]; /* 215:200 */
        uint32_t thermal_mgmt_temp_transition_count[2]; /* 223:216 */
        uint32_t total_time_for_thermal_mgmt_temp[2]; /* 231:227 */
        uint8_t resv1[280];             /* 511:232 */
};

struct log_fw_slot_info_struct {
        uint8_t afi;    /* 0 */
#define AFI_FW_SLOT_MASK        (7 << 0)
#define AFI_NEXT_SLOT_ACTIVATE  (7 << 4)
        uint8_t resv0[7];   /* 07:01 */
        uint64_t frsx[7];   /* 63:08 */
        uint8_t resv1[448]; /* 511:64 */
};

#endif /* __FLEXX_NVME_H__ */
