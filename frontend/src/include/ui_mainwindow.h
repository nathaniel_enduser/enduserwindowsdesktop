/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_5;
    QSplitter *splitter_2;
    QWidget *widget;
    QGridLayout *gridLayout_8;
    QSplitter *splitter;
    QTreeWidget *treeWidget;
    QTabWidget *tabWidget;
    QWidget *tab1;
    QGridLayout *gridLayout;
    QWidget *wgtXPHYInfo;
    QGridLayout *gridLayout_5;
    QLabel *label_260;
    QLabel *label_235;
    QLabel *label_36;
    QLabel *label_224;
    QLabel *label_258;
    QLabel *label_31;
    QLabel *label_33;
    QLabel *label_179;
    QLabel *label_243;
    QLabel *lblModelName_27;
    QLabel *firmwarerevision;
    QLabel *label_257;
    QLabel *label_185;
    QLabel *label_219;
    QLabel *label_249;
    QLabel *label_34;
    QLabel *label_208;
    QLabel *label_174;
    QLabel *label_204;
    QLabel *label_197;
    QLabel *lblModelName_15;
    QLabel *label_17;
    QLabel *label_38;
    QLabel *label_216;
    QLabel *label_251;
    QLabel *lblModelName_12;
    QLabel *label_196;
    QLabel *label_240;
    QLabel *label_220;
    QLabel *label_223;
    QLabel *label_231;
    QLabel *lblModelName_19;
    QLabel *label_182;
    QLabel *label_253;
    QLabel *label_263;
    QLabel *label_267;
    QLabel *label_187;
    QLabel *label_214;
    QLabel *label_24;
    QLabel *label_40;
    QLabel *label_198;
    QLabel *label_26;
    QLabel *label_49;
    QLabel *lblModelName_14;
    QLabel *label_211;
    QLabel *label_181;
    QLabel *label_221;
    QLabel *label_28;
    QLabel *label_228;
    QLabel *label_16;
    QLabel *label_202;
    QLabel *label_252;
    QLabel *label_177;
    QLabel *label_192;
    QLabel *label_206;
    QLabel *lblModelName_31;
    QLabel *label_248;
    QLabel *label_255;
    QLabel *label_191;
    QLabel *label_254;
    QLabel *label_188;
    QLabel *label_32;
    QLabel *label_45;
    QLabel *label_190;
    QLabel *label_227;
    QLabel *label_29;
    QLabel *label_21;
    QLabel *label_193;
    QLabel *label_23;
    QLabel *lblModelName_25;
    QLabel *label_39;
    QLabel *lblModelName;
    QLabel *label_205;
    QLabel *label_178;
    QLabel *lblModelName_17;
    QLabel *label_186;
    QLabel *lblModelName_8;
    QLabel *label_35;
    QLabel *label_183;
    QLabel *label_245;
    QLabel *label_236;
    QLabel *label_210;
    QLabel *label_266;
    QLabel *label_247;
    QLabel *label_218;
    QLabel *label_239;
    QLabel *label_30;
    QLabel *label_203;
    QLabel *label_256;
    QLabel *lblModelName_2;
    QLabel *label_199;
    QLabel *label_201;
    QLabel *label_20;
    QLabel *label_184;
    QLabel *label_173;
    QLabel *label_195;
    QLabel *label_226;
    QLabel *lblModelName_18;
    QLabel *lblModelName_10;
    QLabel *label_265;
    QLabel *label_250;
    QLabel *lblFwVersion;
    QLabel *lblModelName_9;
    QLabel *lblModelName_11;
    QLabel *label_22;
    QLabel *label_207;
    QLabel *label_19;
    QLabel *label_42;
    QLabel *label_238;
    QLabel *label_244;
    QLabel *label_232;
    QLabel *label_25;
    QLabel *label_180;
    QLabel *lblModelName_16;
    QLabel *label_237;
    QLabel *lblModelName_28;
    QLabel *label_241;
    QLabel *label_268;
    QLabel *lblModelName_5;
    QLabel *lblModelName_13;
    QLabel *label_37;
    QLabel *label_217;
    QLabel *label_213;
    QLabel *label_246;
    QLabel *lblModelName_3;
    QLabel *lblModelName_20;
    QLabel *label_72;
    QLabel *label_264;
    QLabel *label_43;
    QLabel *label_189;
    QLabel *label_229;
    QLabel *lblModelName_23;
    QLabel *label_175;
    QLabel *label_234;
    QLabel *lblModelName_6;
    QLabel *label_262;
    QLabel *label_230;
    QLabel *lblModelName_4;
    QLabel *label_225;
    QLabel *label_47;
    QLabel *label_48;
    QLabel *label_233;
    QLabel *label_222;
    QLabel *label_200;
    QLabel *label_242;
    QLabel *label_194;
    QLabel *label_176;
    QLabel *label_215;
    QLabel *label_41;
    QLabel *label_212;
    QLabel *lblSerialNumber;
    QLabel *label_259;
    QLabel *label_27;
    QLabel *label_209;
    QLabel *label_172;
    QLabel *label_261;
    QLabel *label_18;
    QLabel *lblModelName_24;
    QWidget *tab2;
    QGridLayout *gridLayout_2;
    QWidget *wgtHealthInfo;
    QHBoxLayout *horizontalLayout;
    QWidget *widget_2;
    QGridLayout *gridLayout_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_6;
    QPushButton *pushButton_3;
    QLabel *label_73;
    QPushButton *pushButton_2;
    QLabel *label_65;
    QPushButton *pushButton_4;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer;
    QLabel *label_74;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_71;
    QLabel *label_3;
    QLabel *label_15;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_75;
    QLabel *label_61;
    QLabel *label_12;
    QLabel *label_50;
    QLabel *label_14;
    QLabel *label_77;
    QLabel *label_70;
    QLabel *label_69;
    QLabel *label_78;
    QLabel *label_81;
    QLabel *label_82;
    QLabel *label_83;
    QLabel *label_84;
    QLabel *label_85;
    QLabel *label_86;
    QLabel *label_269;
    QLabel *label_270;
    QLabel *label_271;
    QLabel *label_272;
    QLabel *label_46;
    QLabel *label_52;
    QLabel *label_53;
    QLabel *label_44;
    QLabel *label_55;
    QLabel *label_56;
    QLabel *label_57;
    QLabel *label_58;
    QLabel *label_59;
    QLabel *label_76;
    QLabel *label_63;
    QLabel *label_62;
    QLabel *label_51;
    QLabel *label_64;
    QLabel *label_79;
    QLabel *label_66;
    QLabel *label_67;
    QLabel *label_68;
    QLabel *label_80;
    QLabel *label_87;
    QLabel *label_88;
    QLabel *label_89;
    QLabel *label_90;
    QLabel *label_91;
    QLabel *label_92;
    QLabel *label_273;
    QLabel *label_274;
    QLabel *label_275;
    QLabel *label_276;
    QWidget *widget_3;
    QGridLayout *gridLayout_7;
    QWidget *tab;
    QGridLayout *gridLayout_4;
    QWidget *wgtLockUnlock;
    QVBoxLayout *verticalLayout_6;
    QTreeView *treeView;
    QPushButton *pushButton;
    QWidget *tab_4;
    QGridLayout *gridLayout_9;
    QLabel *label_94;
    QLabel *label_106;
    QLabel *label_126;
    QLabel *label_138;
    QLabel *label_95;
    QLabel *label_107;
    QLabel *label_127;
    QLabel *label_139;
    QLabel *label_96;
    QLabel *label_108;
    QLabel *label_128;
    QLabel *label_140;
    QLabel *label_97;
    QLabel *label_109;
    QLabel *label_129;
    QLabel *label_141;
    QLabel *label_98;
    QLabel *label_110;
    QLabel *label_142;
    QLabel *label_157;
    QLabel *label_99;
    QLabel *label_111;
    QLabel *label_143;
    QLabel *label_158;
    QLabel *label_100;
    QLabel *label_112;
    QLabel *label_144;
    QLabel *label_159;
    QLabel *label_101;
    QLabel *label_113;
    QLabel *label_145;
    QLabel *label_160;
    QLabel *label_102;
    QLabel *label_114;
    QLabel *label_146;
    QLabel *label_161;
    QLabel *label_103;
    QLabel *label_115;
    QLabel *label_147;
    QLabel *label_162;
    QLabel *label_104;
    QLabel *label_116;
    QLabel *label_148;
    QLabel *label_163;
    QLabel *label_105;
    QLabel *label_117;
    QLabel *label_149;
    QLabel *label_164;
    QLabel *label_118;
    QLabel *label_130;
    QLabel *label_150;
    QLabel *label_165;
    QLabel *label_119;
    QLabel *label_131;
    QLabel *label_151;
    QLabel *label_166;
    QLabel *label_120;
    QLabel *label_132;
    QLabel *label_152;
    QLabel *label_167;
    QLabel *label_121;
    QLabel *label_133;
    QLabel *label_153;
    QLabel *label_168;
    QLabel *label_122;
    QLabel *label_134;
    QLabel *label_154;
    QLabel *label_169;
    QLabel *label_123;
    QLabel *label_135;
    QLabel *label_155;
    QLabel *label_170;
    QLabel *label_124;
    QLabel *label_136;
    QLabel *label_156;
    QLabel *label_171;
    QLabel *label_125;
    QLabel *label_137;
    QTextBrowser *textBrowser;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1019, 761);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/flexxon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_5 = new QVBoxLayout(centralwidget);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        splitter_2 = new QSplitter(centralwidget);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        widget = new QWidget(splitter_2);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_8 = new QGridLayout(widget);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(0, 0, 0, 0);
        splitter = new QSplitter(widget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        treeWidget = new QTreeWidget(splitter);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/Images/icons/flexxon.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem(treeWidget);
        __qtreewidgetitem->setIcon(0, icon1);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setMinimumSize(QSize(180, 0));
        treeWidget->setMaximumSize(QSize(180, 16777215));
        splitter->addWidget(treeWidget);
        tabWidget = new QTabWidget(splitter);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setEnabled(true);
        tabWidget->setMaximumSize(QSize(16777215, 16777215));
        tab1 = new QWidget();
        tab1->setObjectName(QString::fromUtf8("tab1"));
        gridLayout = new QGridLayout(tab1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        wgtXPHYInfo = new QWidget(tab1);
        wgtXPHYInfo->setObjectName(QString::fromUtf8("wgtXPHYInfo"));
        gridLayout_5 = new QGridLayout(wgtXPHYInfo);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_260 = new QLabel(wgtXPHYInfo);
        label_260->setObjectName(QString::fromUtf8("label_260"));

        gridLayout_5->addWidget(label_260, 24, 4, 1, 1);

        label_235 = new QLabel(wgtXPHYInfo);
        label_235->setObjectName(QString::fromUtf8("label_235"));

        gridLayout_5->addWidget(label_235, 13, 4, 1, 1);

        label_36 = new QLabel(wgtXPHYInfo);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        QFont font;
        font.setBold(false);
        font.setWeight(50);
        label_36->setFont(font);

        gridLayout_5->addWidget(label_36, 3, 1, 1, 1);

        label_224 = new QLabel(wgtXPHYInfo);
        label_224->setObjectName(QString::fromUtf8("label_224"));

        gridLayout_5->addWidget(label_224, 5, 4, 1, 1);

        label_258 = new QLabel(wgtXPHYInfo);
        label_258->setObjectName(QString::fromUtf8("label_258"));

        gridLayout_5->addWidget(label_258, 22, 5, 1, 1);

        label_31 = new QLabel(wgtXPHYInfo);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setFont(font);

        gridLayout_5->addWidget(label_31, 11, 1, 1, 1);

        label_33 = new QLabel(wgtXPHYInfo);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setFont(font);

        gridLayout_5->addWidget(label_33, 17, 3, 1, 1);

        label_179 = new QLabel(wgtXPHYInfo);
        label_179->setObjectName(QString::fromUtf8("label_179"));

        gridLayout_5->addWidget(label_179, 25, 0, 1, 1);

        label_243 = new QLabel(wgtXPHYInfo);
        label_243->setObjectName(QString::fromUtf8("label_243"));

        gridLayout_5->addWidget(label_243, 14, 5, 1, 1);

        lblModelName_27 = new QLabel(wgtXPHYInfo);
        lblModelName_27->setObjectName(QString::fromUtf8("lblModelName_27"));

        gridLayout_5->addWidget(lblModelName_27, 20, 0, 1, 1);

        firmwarerevision = new QLabel(wgtXPHYInfo);
        firmwarerevision->setObjectName(QString::fromUtf8("firmwarerevision"));

        gridLayout_5->addWidget(firmwarerevision, 5, 0, 1, 1);

        label_257 = new QLabel(wgtXPHYInfo);
        label_257->setObjectName(QString::fromUtf8("label_257"));

        gridLayout_5->addWidget(label_257, 21, 5, 1, 1);

        label_185 = new QLabel(wgtXPHYInfo);
        label_185->setObjectName(QString::fromUtf8("label_185"));

        gridLayout_5->addWidget(label_185, 24, 2, 1, 1);

        label_219 = new QLabel(wgtXPHYInfo);
        label_219->setObjectName(QString::fromUtf8("label_219"));

        gridLayout_5->addWidget(label_219, 24, 3, 1, 1);

        label_249 = new QLabel(wgtXPHYInfo);
        label_249->setObjectName(QString::fromUtf8("label_249"));

        gridLayout_5->addWidget(label_249, 20, 4, 1, 1);

        label_34 = new QLabel(wgtXPHYInfo);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setFont(font);

        gridLayout_5->addWidget(label_34, 2, 1, 1, 1);

        label_208 = new QLabel(wgtXPHYInfo);
        label_208->setObjectName(QString::fromUtf8("label_208"));

        gridLayout_5->addWidget(label_208, 13, 2, 1, 1);

        label_174 = new QLabel(wgtXPHYInfo);
        label_174->setObjectName(QString::fromUtf8("label_174"));

        gridLayout_5->addWidget(label_174, 9, 0, 1, 1);

        label_204 = new QLabel(wgtXPHYInfo);
        label_204->setObjectName(QString::fromUtf8("label_204"));

        gridLayout_5->addWidget(label_204, 6, 3, 1, 1);

        label_197 = new QLabel(wgtXPHYInfo);
        label_197->setObjectName(QString::fromUtf8("label_197"));

        gridLayout_5->addWidget(label_197, 26, 1, 1, 1);

        lblModelName_15 = new QLabel(wgtXPHYInfo);
        lblModelName_15->setObjectName(QString::fromUtf8("lblModelName_15"));

        gridLayout_5->addWidget(lblModelName_15, 17, 2, 1, 1);

        label_17 = new QLabel(wgtXPHYInfo);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setFont(font);

        gridLayout_5->addWidget(label_17, 4, 1, 1, 1);

        label_38 = new QLabel(wgtXPHYInfo);
        label_38->setObjectName(QString::fromUtf8("label_38"));
        label_38->setFont(font);

        gridLayout_5->addWidget(label_38, 19, 1, 1, 1);

        label_216 = new QLabel(wgtXPHYInfo);
        label_216->setObjectName(QString::fromUtf8("label_216"));

        gridLayout_5->addWidget(label_216, 7, 4, 1, 1);

        label_251 = new QLabel(wgtXPHYInfo);
        label_251->setObjectName(QString::fromUtf8("label_251"));

        gridLayout_5->addWidget(label_251, 22, 4, 1, 1);

        lblModelName_12 = new QLabel(wgtXPHYInfo);
        lblModelName_12->setObjectName(QString::fromUtf8("lblModelName_12"));

        gridLayout_5->addWidget(lblModelName_12, 3, 2, 1, 1);

        label_196 = new QLabel(wgtXPHYInfo);
        label_196->setObjectName(QString::fromUtf8("label_196"));

        gridLayout_5->addWidget(label_196, 25, 1, 1, 1);

        label_240 = new QLabel(wgtXPHYInfo);
        label_240->setObjectName(QString::fromUtf8("label_240"));

        gridLayout_5->addWidget(label_240, 11, 5, 1, 1);

        label_220 = new QLabel(wgtXPHYInfo);
        label_220->setObjectName(QString::fromUtf8("label_220"));

        gridLayout_5->addWidget(label_220, 25, 3, 1, 1);

        label_223 = new QLabel(wgtXPHYInfo);
        label_223->setObjectName(QString::fromUtf8("label_223"));

        gridLayout_5->addWidget(label_223, 4, 4, 1, 1);

        label_231 = new QLabel(wgtXPHYInfo);
        label_231->setObjectName(QString::fromUtf8("label_231"));

        gridLayout_5->addWidget(label_231, 5, 5, 1, 1);

        lblModelName_19 = new QLabel(wgtXPHYInfo);
        lblModelName_19->setObjectName(QString::fromUtf8("lblModelName_19"));

        gridLayout_5->addWidget(lblModelName_19, 7, 0, 1, 1);

        label_182 = new QLabel(wgtXPHYInfo);
        label_182->setObjectName(QString::fromUtf8("label_182"));

        gridLayout_5->addWidget(label_182, 1, 2, 1, 1);

        label_253 = new QLabel(wgtXPHYInfo);
        label_253->setObjectName(QString::fromUtf8("label_253"));

        gridLayout_5->addWidget(label_253, 17, 5, 1, 1);

        label_263 = new QLabel(wgtXPHYInfo);
        label_263->setObjectName(QString::fromUtf8("label_263"));

        gridLayout_5->addWidget(label_263, 27, 4, 1, 1);

        label_267 = new QLabel(wgtXPHYInfo);
        label_267->setObjectName(QString::fromUtf8("label_267"));

        gridLayout_5->addWidget(label_267, 26, 5, 1, 1);

        label_187 = new QLabel(wgtXPHYInfo);
        label_187->setObjectName(QString::fromUtf8("label_187"));

        gridLayout_5->addWidget(label_187, 6, 2, 1, 1);

        label_214 = new QLabel(wgtXPHYInfo);
        label_214->setObjectName(QString::fromUtf8("label_214"));

        gridLayout_5->addWidget(label_214, 1, 4, 1, 1);

        label_24 = new QLabel(wgtXPHYInfo);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setFont(font);

        gridLayout_5->addWidget(label_24, 14, 3, 1, 1);

        label_40 = new QLabel(wgtXPHYInfo);
        label_40->setObjectName(QString::fromUtf8("label_40"));

        gridLayout_5->addWidget(label_40, 18, 2, 1, 1);

        label_198 = new QLabel(wgtXPHYInfo);
        label_198->setObjectName(QString::fromUtf8("label_198"));

        gridLayout_5->addWidget(label_198, 27, 1, 1, 1);

        label_26 = new QLabel(wgtXPHYInfo);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setFont(font);

        gridLayout_5->addWidget(label_26, 13, 1, 1, 1);

        label_49 = new QLabel(wgtXPHYInfo);
        label_49->setObjectName(QString::fromUtf8("label_49"));
        label_49->setFont(font);

        gridLayout_5->addWidget(label_49, 22, 1, 1, 1);

        lblModelName_14 = new QLabel(wgtXPHYInfo);
        lblModelName_14->setObjectName(QString::fromUtf8("lblModelName_14"));

        gridLayout_5->addWidget(lblModelName_14, 2, 2, 1, 1);

        label_211 = new QLabel(wgtXPHYInfo);
        label_211->setObjectName(QString::fromUtf8("label_211"));

        gridLayout_5->addWidget(label_211, 13, 3, 1, 1);

        label_181 = new QLabel(wgtXPHYInfo);
        label_181->setObjectName(QString::fromUtf8("label_181"));

        gridLayout_5->addWidget(label_181, 27, 0, 1, 1);

        label_221 = new QLabel(wgtXPHYInfo);
        label_221->setObjectName(QString::fromUtf8("label_221"));

        gridLayout_5->addWidget(label_221, 9, 4, 1, 1);

        label_28 = new QLabel(wgtXPHYInfo);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setFont(font);

        gridLayout_5->addWidget(label_28, 4, 3, 1, 1);

        label_228 = new QLabel(wgtXPHYInfo);
        label_228->setObjectName(QString::fromUtf8("label_228"));

        gridLayout_5->addWidget(label_228, 2, 5, 1, 1);

        label_16 = new QLabel(wgtXPHYInfo);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font);

        gridLayout_5->addWidget(label_16, 5, 1, 1, 1);

        label_202 = new QLabel(wgtXPHYInfo);
        label_202->setObjectName(QString::fromUtf8("label_202"));

        gridLayout_5->addWidget(label_202, 22, 3, 1, 1);

        label_252 = new QLabel(wgtXPHYInfo);
        label_252->setObjectName(QString::fromUtf8("label_252"));

        gridLayout_5->addWidget(label_252, 16, 5, 1, 1);

        label_177 = new QLabel(wgtXPHYInfo);
        label_177->setObjectName(QString::fromUtf8("label_177"));

        gridLayout_5->addWidget(label_177, 23, 0, 1, 1);

        label_192 = new QLabel(wgtXPHYInfo);
        label_192->setObjectName(QString::fromUtf8("label_192"));

        gridLayout_5->addWidget(label_192, 19, 3, 1, 1);

        label_206 = new QLabel(wgtXPHYInfo);
        label_206->setObjectName(QString::fromUtf8("label_206"));

        gridLayout_5->addWidget(label_206, 10, 3, 1, 1);

        lblModelName_31 = new QLabel(wgtXPHYInfo);
        lblModelName_31->setObjectName(QString::fromUtf8("lblModelName_31"));

        gridLayout_5->addWidget(lblModelName_31, 22, 0, 1, 1);

        label_248 = new QLabel(wgtXPHYInfo);
        label_248->setObjectName(QString::fromUtf8("label_248"));

        gridLayout_5->addWidget(label_248, 19, 4, 1, 1);

        label_255 = new QLabel(wgtXPHYInfo);
        label_255->setObjectName(QString::fromUtf8("label_255"));

        gridLayout_5->addWidget(label_255, 19, 5, 1, 1);

        label_191 = new QLabel(wgtXPHYInfo);
        label_191->setObjectName(QString::fromUtf8("label_191"));

        gridLayout_5->addWidget(label_191, 9, 1, 1, 1);

        label_254 = new QLabel(wgtXPHYInfo);
        label_254->setObjectName(QString::fromUtf8("label_254"));

        gridLayout_5->addWidget(label_254, 18, 5, 1, 1);

        label_188 = new QLabel(wgtXPHYInfo);
        label_188->setObjectName(QString::fromUtf8("label_188"));

        gridLayout_5->addWidget(label_188, 9, 2, 1, 1);

        label_32 = new QLabel(wgtXPHYInfo);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setFont(font);

        gridLayout_5->addWidget(label_32, 2, 3, 1, 1);

        label_45 = new QLabel(wgtXPHYInfo);
        label_45->setObjectName(QString::fromUtf8("label_45"));
        label_45->setFont(font);

        gridLayout_5->addWidget(label_45, 20, 1, 1, 1);

        label_190 = new QLabel(wgtXPHYInfo);
        label_190->setObjectName(QString::fromUtf8("label_190"));

        gridLayout_5->addWidget(label_190, 12, 2, 1, 1);

        label_227 = new QLabel(wgtXPHYInfo);
        label_227->setObjectName(QString::fromUtf8("label_227"));

        gridLayout_5->addWidget(label_227, 1, 5, 1, 1);

        label_29 = new QLabel(wgtXPHYInfo);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setFont(font);

        gridLayout_5->addWidget(label_29, 8, 3, 1, 1);

        label_21 = new QLabel(wgtXPHYInfo);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setFont(font);

        gridLayout_5->addWidget(label_21, 15, 1, 1, 1);

        label_193 = new QLabel(wgtXPHYInfo);
        label_193->setObjectName(QString::fromUtf8("label_193"));

        gridLayout_5->addWidget(label_193, 18, 1, 1, 1);

        label_23 = new QLabel(wgtXPHYInfo);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setFont(font);

        gridLayout_5->addWidget(label_23, 12, 1, 1, 1);

        lblModelName_25 = new QLabel(wgtXPHYInfo);
        lblModelName_25->setObjectName(QString::fromUtf8("lblModelName_25"));

        gridLayout_5->addWidget(lblModelName_25, 17, 0, 1, 1);

        label_39 = new QLabel(wgtXPHYInfo);
        label_39->setObjectName(QString::fromUtf8("label_39"));

        gridLayout_5->addWidget(label_39, 3, 5, 1, 1);

        lblModelName = new QLabel(wgtXPHYInfo);
        lblModelName->setObjectName(QString::fromUtf8("lblModelName"));

        gridLayout_5->addWidget(lblModelName, 7, 2, 1, 1);

        label_205 = new QLabel(wgtXPHYInfo);
        label_205->setObjectName(QString::fromUtf8("label_205"));

        gridLayout_5->addWidget(label_205, 9, 3, 1, 1);

        label_178 = new QLabel(wgtXPHYInfo);
        label_178->setObjectName(QString::fromUtf8("label_178"));

        gridLayout_5->addWidget(label_178, 24, 0, 1, 1);

        lblModelName_17 = new QLabel(wgtXPHYInfo);
        lblModelName_17->setObjectName(QString::fromUtf8("lblModelName_17"));

        gridLayout_5->addWidget(lblModelName_17, 5, 2, 1, 1);

        label_186 = new QLabel(wgtXPHYInfo);
        label_186->setObjectName(QString::fromUtf8("label_186"));

        gridLayout_5->addWidget(label_186, 25, 2, 1, 1);

        lblModelName_8 = new QLabel(wgtXPHYInfo);
        lblModelName_8->setObjectName(QString::fromUtf8("lblModelName_8"));

        gridLayout_5->addWidget(lblModelName_8, 13, 0, 1, 1);

        label_35 = new QLabel(wgtXPHYInfo);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setFont(font);

        gridLayout_5->addWidget(label_35, 5, 3, 1, 1);

        label_183 = new QLabel(wgtXPHYInfo);
        label_183->setObjectName(QString::fromUtf8("label_183"));

        gridLayout_5->addWidget(label_183, 22, 2, 1, 1);

        label_245 = new QLabel(wgtXPHYInfo);
        label_245->setObjectName(QString::fromUtf8("label_245"));

        gridLayout_5->addWidget(label_245, 16, 4, 1, 1);

        label_236 = new QLabel(wgtXPHYInfo);
        label_236->setObjectName(QString::fromUtf8("label_236"));

        gridLayout_5->addWidget(label_236, 14, 4, 1, 1);

        label_210 = new QLabel(wgtXPHYInfo);
        label_210->setObjectName(QString::fromUtf8("label_210"));

        gridLayout_5->addWidget(label_210, 16, 2, 1, 1);

        label_266 = new QLabel(wgtXPHYInfo);
        label_266->setObjectName(QString::fromUtf8("label_266"));

        gridLayout_5->addWidget(label_266, 25, 5, 1, 1);

        label_247 = new QLabel(wgtXPHYInfo);
        label_247->setObjectName(QString::fromUtf8("label_247"));

        gridLayout_5->addWidget(label_247, 18, 4, 1, 1);

        label_218 = new QLabel(wgtXPHYInfo);
        label_218->setObjectName(QString::fromUtf8("label_218"));

        gridLayout_5->addWidget(label_218, 23, 3, 1, 1);

        label_239 = new QLabel(wgtXPHYInfo);
        label_239->setObjectName(QString::fromUtf8("label_239"));

        gridLayout_5->addWidget(label_239, 10, 5, 1, 1);

        label_30 = new QLabel(wgtXPHYInfo);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setFont(font);

        gridLayout_5->addWidget(label_30, 3, 3, 1, 1);

        label_203 = new QLabel(wgtXPHYInfo);
        label_203->setObjectName(QString::fromUtf8("label_203"));

        gridLayout_5->addWidget(label_203, 27, 2, 1, 1);

        label_256 = new QLabel(wgtXPHYInfo);
        label_256->setObjectName(QString::fromUtf8("label_256"));

        gridLayout_5->addWidget(label_256, 20, 5, 1, 1);

        lblModelName_2 = new QLabel(wgtXPHYInfo);
        lblModelName_2->setObjectName(QString::fromUtf8("lblModelName_2"));

        gridLayout_5->addWidget(lblModelName_2, 21, 0, 1, 1);

        label_199 = new QLabel(wgtXPHYInfo);
        label_199->setObjectName(QString::fromUtf8("label_199"));

        gridLayout_5->addWidget(label_199, 1, 3, 1, 1);

        label_201 = new QLabel(wgtXPHYInfo);
        label_201->setObjectName(QString::fromUtf8("label_201"));

        gridLayout_5->addWidget(label_201, 21, 3, 1, 1);

        label_20 = new QLabel(wgtXPHYInfo);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setFont(font);

        gridLayout_5->addWidget(label_20, 21, 1, 1, 1);

        label_184 = new QLabel(wgtXPHYInfo);
        label_184->setObjectName(QString::fromUtf8("label_184"));

        gridLayout_5->addWidget(label_184, 23, 2, 1, 1);

        label_173 = new QLabel(wgtXPHYInfo);
        label_173->setObjectName(QString::fromUtf8("label_173"));

        gridLayout_5->addWidget(label_173, 8, 1, 1, 1);

        label_195 = new QLabel(wgtXPHYInfo);
        label_195->setObjectName(QString::fromUtf8("label_195"));

        gridLayout_5->addWidget(label_195, 24, 1, 1, 1);

        label_226 = new QLabel(wgtXPHYInfo);
        label_226->setObjectName(QString::fromUtf8("label_226"));

        gridLayout_5->addWidget(label_226, 27, 3, 1, 1);

        lblModelName_18 = new QLabel(wgtXPHYInfo);
        lblModelName_18->setObjectName(QString::fromUtf8("lblModelName_18"));

        gridLayout_5->addWidget(lblModelName_18, 3, 0, 1, 1);

        lblModelName_10 = new QLabel(wgtXPHYInfo);
        lblModelName_10->setObjectName(QString::fromUtf8("lblModelName_10"));

        gridLayout_5->addWidget(lblModelName_10, 4, 2, 1, 1);

        label_265 = new QLabel(wgtXPHYInfo);
        label_265->setObjectName(QString::fromUtf8("label_265"));

        gridLayout_5->addWidget(label_265, 24, 5, 1, 1);

        label_250 = new QLabel(wgtXPHYInfo);
        label_250->setObjectName(QString::fromUtf8("label_250"));

        gridLayout_5->addWidget(label_250, 21, 4, 1, 1);

        lblFwVersion = new QLabel(wgtXPHYInfo);
        lblFwVersion->setObjectName(QString::fromUtf8("lblFwVersion"));

        gridLayout_5->addWidget(lblFwVersion, 4, 0, 1, 1);

        lblModelName_9 = new QLabel(wgtXPHYInfo);
        lblModelName_9->setObjectName(QString::fromUtf8("lblModelName_9"));

        gridLayout_5->addWidget(lblModelName_9, 6, 0, 1, 1);

        lblModelName_11 = new QLabel(wgtXPHYInfo);
        lblModelName_11->setObjectName(QString::fromUtf8("lblModelName_11"));

        gridLayout_5->addWidget(lblModelName_11, 8, 2, 1, 1);

        label_22 = new QLabel(wgtXPHYInfo);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setFont(font);

        gridLayout_5->addWidget(label_22, 14, 1, 1, 1);

        label_207 = new QLabel(wgtXPHYInfo);
        label_207->setObjectName(QString::fromUtf8("label_207"));

        gridLayout_5->addWidget(label_207, 12, 3, 1, 1);

        label_19 = new QLabel(wgtXPHYInfo);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setFont(font);

        gridLayout_5->addWidget(label_19, 7, 3, 1, 1);

        label_42 = new QLabel(wgtXPHYInfo);
        label_42->setObjectName(QString::fromUtf8("label_42"));
        label_42->setFont(font);

        gridLayout_5->addWidget(label_42, 1, 1, 1, 1);

        label_238 = new QLabel(wgtXPHYInfo);
        label_238->setObjectName(QString::fromUtf8("label_238"));

        gridLayout_5->addWidget(label_238, 9, 5, 1, 1);

        label_244 = new QLabel(wgtXPHYInfo);
        label_244->setObjectName(QString::fromUtf8("label_244"));

        gridLayout_5->addWidget(label_244, 15, 4, 1, 1);

        label_232 = new QLabel(wgtXPHYInfo);
        label_232->setObjectName(QString::fromUtf8("label_232"));

        gridLayout_5->addWidget(label_232, 6, 5, 1, 1);

        label_25 = new QLabel(wgtXPHYInfo);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        gridLayout_5->addWidget(label_25, 26, 2, 1, 1);

        label_180 = new QLabel(wgtXPHYInfo);
        label_180->setObjectName(QString::fromUtf8("label_180"));

        gridLayout_5->addWidget(label_180, 26, 0, 1, 1);

        lblModelName_16 = new QLabel(wgtXPHYInfo);
        lblModelName_16->setObjectName(QString::fromUtf8("lblModelName_16"));

        gridLayout_5->addWidget(lblModelName_16, 2, 0, 1, 1);

        label_237 = new QLabel(wgtXPHYInfo);
        label_237->setObjectName(QString::fromUtf8("label_237"));

        gridLayout_5->addWidget(label_237, 8, 5, 1, 1);

        lblModelName_28 = new QLabel(wgtXPHYInfo);
        lblModelName_28->setObjectName(QString::fromUtf8("lblModelName_28"));

        gridLayout_5->addWidget(lblModelName_28, 16, 0, 1, 1);

        label_241 = new QLabel(wgtXPHYInfo);
        label_241->setObjectName(QString::fromUtf8("label_241"));

        gridLayout_5->addWidget(label_241, 12, 5, 1, 1);

        label_268 = new QLabel(wgtXPHYInfo);
        label_268->setObjectName(QString::fromUtf8("label_268"));

        gridLayout_5->addWidget(label_268, 27, 5, 1, 1);

        lblModelName_5 = new QLabel(wgtXPHYInfo);
        lblModelName_5->setObjectName(QString::fromUtf8("lblModelName_5"));

        gridLayout_5->addWidget(lblModelName_5, 12, 0, 1, 1);

        lblModelName_13 = new QLabel(wgtXPHYInfo);
        lblModelName_13->setObjectName(QString::fromUtf8("lblModelName_13"));

        gridLayout_5->addWidget(lblModelName_13, 11, 0, 1, 1);

        label_37 = new QLabel(wgtXPHYInfo);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setFont(font);

        gridLayout_5->addWidget(label_37, 7, 1, 1, 1);

        label_217 = new QLabel(wgtXPHYInfo);
        label_217->setObjectName(QString::fromUtf8("label_217"));

        gridLayout_5->addWidget(label_217, 8, 4, 1, 1);

        label_213 = new QLabel(wgtXPHYInfo);
        label_213->setObjectName(QString::fromUtf8("label_213"));

        gridLayout_5->addWidget(label_213, 16, 3, 1, 1);

        label_246 = new QLabel(wgtXPHYInfo);
        label_246->setObjectName(QString::fromUtf8("label_246"));

        gridLayout_5->addWidget(label_246, 17, 4, 1, 1);

        lblModelName_3 = new QLabel(wgtXPHYInfo);
        lblModelName_3->setObjectName(QString::fromUtf8("lblModelName_3"));

        gridLayout_5->addWidget(lblModelName_3, 15, 0, 1, 1);

        lblModelName_20 = new QLabel(wgtXPHYInfo);
        lblModelName_20->setObjectName(QString::fromUtf8("lblModelName_20"));

        gridLayout_5->addWidget(lblModelName_20, 19, 0, 1, 1);

        label_72 = new QLabel(wgtXPHYInfo);
        label_72->setObjectName(QString::fromUtf8("label_72"));
        label_72->setFont(font);

        gridLayout_5->addWidget(label_72, 16, 1, 1, 1);

        label_264 = new QLabel(wgtXPHYInfo);
        label_264->setObjectName(QString::fromUtf8("label_264"));

        gridLayout_5->addWidget(label_264, 23, 5, 1, 1);

        label_43 = new QLabel(wgtXPHYInfo);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setFont(font);

        gridLayout_5->addWidget(label_43, 17, 1, 1, 1);

        label_189 = new QLabel(wgtXPHYInfo);
        label_189->setObjectName(QString::fromUtf8("label_189"));

        gridLayout_5->addWidget(label_189, 10, 2, 1, 1);

        label_229 = new QLabel(wgtXPHYInfo);
        label_229->setObjectName(QString::fromUtf8("label_229"));

        gridLayout_5->addWidget(label_229, 10, 4, 1, 1);

        lblModelName_23 = new QLabel(wgtXPHYInfo);
        lblModelName_23->setObjectName(QString::fromUtf8("lblModelName_23"));

        gridLayout_5->addWidget(lblModelName_23, 11, 2, 1, 1);

        label_175 = new QLabel(wgtXPHYInfo);
        label_175->setObjectName(QString::fromUtf8("label_175"));

        gridLayout_5->addWidget(label_175, 21, 2, 1, 1);

        label_234 = new QLabel(wgtXPHYInfo);
        label_234->setObjectName(QString::fromUtf8("label_234"));

        gridLayout_5->addWidget(label_234, 12, 4, 1, 1);

        lblModelName_6 = new QLabel(wgtXPHYInfo);
        lblModelName_6->setObjectName(QString::fromUtf8("lblModelName_6"));

        gridLayout_5->addWidget(lblModelName_6, 14, 2, 1, 1);

        label_262 = new QLabel(wgtXPHYInfo);
        label_262->setObjectName(QString::fromUtf8("label_262"));

        gridLayout_5->addWidget(label_262, 26, 4, 1, 1);

        label_230 = new QLabel(wgtXPHYInfo);
        label_230->setObjectName(QString::fromUtf8("label_230"));

        gridLayout_5->addWidget(label_230, 4, 5, 1, 1);

        lblModelName_4 = new QLabel(wgtXPHYInfo);
        lblModelName_4->setObjectName(QString::fromUtf8("lblModelName_4"));

        gridLayout_5->addWidget(lblModelName_4, 14, 0, 1, 1);

        label_225 = new QLabel(wgtXPHYInfo);
        label_225->setObjectName(QString::fromUtf8("label_225"));

        gridLayout_5->addWidget(label_225, 6, 4, 1, 1);

        label_47 = new QLabel(wgtXPHYInfo);
        label_47->setObjectName(QString::fromUtf8("label_47"));

        gridLayout_5->addWidget(label_47, 19, 2, 1, 1);

        label_48 = new QLabel(wgtXPHYInfo);
        label_48->setObjectName(QString::fromUtf8("label_48"));

        gridLayout_5->addWidget(label_48, 20, 2, 1, 1);

        label_233 = new QLabel(wgtXPHYInfo);
        label_233->setObjectName(QString::fromUtf8("label_233"));

        gridLayout_5->addWidget(label_233, 11, 4, 1, 1);

        label_222 = new QLabel(wgtXPHYInfo);
        label_222->setObjectName(QString::fromUtf8("label_222"));

        gridLayout_5->addWidget(label_222, 3, 4, 1, 1);

        label_200 = new QLabel(wgtXPHYInfo);
        label_200->setObjectName(QString::fromUtf8("label_200"));

        gridLayout_5->addWidget(label_200, 20, 3, 1, 1);

        label_242 = new QLabel(wgtXPHYInfo);
        label_242->setObjectName(QString::fromUtf8("label_242"));

        gridLayout_5->addWidget(label_242, 13, 5, 1, 1);

        label_194 = new QLabel(wgtXPHYInfo);
        label_194->setObjectName(QString::fromUtf8("label_194"));

        gridLayout_5->addWidget(label_194, 23, 1, 1, 1);

        label_176 = new QLabel(wgtXPHYInfo);
        label_176->setObjectName(QString::fromUtf8("label_176"));

        gridLayout_5->addWidget(label_176, 18, 0, 1, 1);

        label_215 = new QLabel(wgtXPHYInfo);
        label_215->setObjectName(QString::fromUtf8("label_215"));

        gridLayout_5->addWidget(label_215, 2, 4, 1, 1);

        label_41 = new QLabel(wgtXPHYInfo);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setFont(font);

        gridLayout_5->addWidget(label_41, 11, 3, 1, 1);

        label_212 = new QLabel(wgtXPHYInfo);
        label_212->setObjectName(QString::fromUtf8("label_212"));

        gridLayout_5->addWidget(label_212, 15, 3, 1, 1);

        lblSerialNumber = new QLabel(wgtXPHYInfo);
        lblSerialNumber->setObjectName(QString::fromUtf8("lblSerialNumber"));

        gridLayout_5->addWidget(lblSerialNumber, 10, 0, 1, 1);

        label_259 = new QLabel(wgtXPHYInfo);
        label_259->setObjectName(QString::fromUtf8("label_259"));

        gridLayout_5->addWidget(label_259, 23, 4, 1, 1);

        label_27 = new QLabel(wgtXPHYInfo);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setFont(font);

        gridLayout_5->addWidget(label_27, 6, 1, 1, 1);

        label_209 = new QLabel(wgtXPHYInfo);
        label_209->setObjectName(QString::fromUtf8("label_209"));

        gridLayout_5->addWidget(label_209, 15, 2, 1, 1);

        label_172 = new QLabel(wgtXPHYInfo);
        label_172->setObjectName(QString::fromUtf8("label_172"));

        gridLayout_5->addWidget(label_172, 8, 0, 1, 1);

        label_261 = new QLabel(wgtXPHYInfo);
        label_261->setObjectName(QString::fromUtf8("label_261"));

        gridLayout_5->addWidget(label_261, 25, 4, 1, 1);

        label_18 = new QLabel(wgtXPHYInfo);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setFont(font);

        gridLayout_5->addWidget(label_18, 10, 1, 1, 1);

        lblModelName_24 = new QLabel(wgtXPHYInfo);
        lblModelName_24->setObjectName(QString::fromUtf8("lblModelName_24"));

        gridLayout_5->addWidget(lblModelName_24, 1, 0, 1, 1);


        gridLayout->addWidget(wgtXPHYInfo, 0, 0, 1, 1);

        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/icons/ssd.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab1, icon2, QString());
        tab2 = new QWidget();
        tab2->setObjectName(QString::fromUtf8("tab2"));
        gridLayout_2 = new QGridLayout(tab2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        wgtHealthInfo = new QWidget(tab2);
        wgtHealthInfo->setObjectName(QString::fromUtf8("wgtHealthInfo"));
        horizontalLayout = new QHBoxLayout(wgtHealthInfo);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        widget_2 = new QWidget(wgtHealthInfo);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setMaximumSize(QSize(16777215, 16777215));
        gridLayout_3 = new QGridLayout(widget_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        scrollArea = new QScrollArea(widget_2);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setMinimumSize(QSize(180, 0));
        scrollArea->setMaximumSize(QSize(16777215, 16777215));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        scrollArea->setFont(font1);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 784, 551));
        gridLayout_6 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        pushButton_3 = new QPushButton(scrollAreaWidgetContents);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        gridLayout_6->addWidget(pushButton_3, 6, 6, 1, 1);

        label_73 = new QLabel(scrollAreaWidgetContents);
        label_73->setObjectName(QString::fromUtf8("label_73"));

        gridLayout_6->addWidget(label_73, 5, 6, 1, 1);

        pushButton_2 = new QPushButton(scrollAreaWidgetContents);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        gridLayout_6->addWidget(pushButton_2, 3, 6, 1, 1);

        label_65 = new QLabel(scrollAreaWidgetContents);
        label_65->setObjectName(QString::fromUtf8("label_65"));

        gridLayout_6->addWidget(label_65, 2, 6, 1, 1);

        pushButton_4 = new QPushButton(scrollAreaWidgetContents);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        gridLayout_6->addWidget(pushButton_4, 9, 6, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 374, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer_2, 0, 5, 16, 1);

        verticalSpacer = new QSpacerItem(20, 374, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer, 0, 2, 16, 1);

        label_74 = new QLabel(scrollAreaWidgetContents);
        label_74->setObjectName(QString::fromUtf8("label_74"));

        gridLayout_6->addWidget(label_74, 8, 6, 1, 1);

        label = new QLabel(scrollAreaWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_6->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(scrollAreaWidgetContents);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_6->addWidget(label_2, 1, 0, 1, 1);

        label_71 = new QLabel(scrollAreaWidgetContents);
        label_71->setObjectName(QString::fromUtf8("label_71"));

        gridLayout_6->addWidget(label_71, 0, 3, 1, 1);

        label_3 = new QLabel(scrollAreaWidgetContents);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_6->addWidget(label_3, 2, 0, 1, 1);

        label_15 = new QLabel(scrollAreaWidgetContents);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_6->addWidget(label_15, 3, 0, 1, 1);

        label_5 = new QLabel(scrollAreaWidgetContents);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_6->addWidget(label_5, 4, 0, 1, 1);

        label_6 = new QLabel(scrollAreaWidgetContents);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_6->addWidget(label_6, 5, 0, 1, 1);

        label_7 = new QLabel(scrollAreaWidgetContents);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_6->addWidget(label_7, 6, 0, 1, 1);

        label_8 = new QLabel(scrollAreaWidgetContents);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_6->addWidget(label_8, 7, 0, 1, 1);

        label_9 = new QLabel(scrollAreaWidgetContents);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_6->addWidget(label_9, 8, 0, 1, 1);

        label_75 = new QLabel(scrollAreaWidgetContents);
        label_75->setObjectName(QString::fromUtf8("label_75"));

        gridLayout_6->addWidget(label_75, 9, 0, 1, 1);

        label_61 = new QLabel(scrollAreaWidgetContents);
        label_61->setObjectName(QString::fromUtf8("label_61"));

        gridLayout_6->addWidget(label_61, 10, 0, 1, 1);

        label_12 = new QLabel(scrollAreaWidgetContents);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_6->addWidget(label_12, 11, 0, 1, 1);

        label_50 = new QLabel(scrollAreaWidgetContents);
        label_50->setObjectName(QString::fromUtf8("label_50"));

        gridLayout_6->addWidget(label_50, 12, 0, 1, 1);

        label_14 = new QLabel(scrollAreaWidgetContents);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_6->addWidget(label_14, 13, 0, 1, 1);

        label_77 = new QLabel(scrollAreaWidgetContents);
        label_77->setObjectName(QString::fromUtf8("label_77"));

        gridLayout_6->addWidget(label_77, 14, 0, 1, 1);

        label_70 = new QLabel(scrollAreaWidgetContents);
        label_70->setObjectName(QString::fromUtf8("label_70"));

        gridLayout_6->addWidget(label_70, 1, 3, 1, 1);

        label_69 = new QLabel(scrollAreaWidgetContents);
        label_69->setObjectName(QString::fromUtf8("label_69"));

        gridLayout_6->addWidget(label_69, 2, 3, 1, 1);

        label_78 = new QLabel(scrollAreaWidgetContents);
        label_78->setObjectName(QString::fromUtf8("label_78"));

        gridLayout_6->addWidget(label_78, 3, 3, 1, 1);

        label_81 = new QLabel(scrollAreaWidgetContents);
        label_81->setObjectName(QString::fromUtf8("label_81"));

        gridLayout_6->addWidget(label_81, 4, 3, 1, 1);

        label_82 = new QLabel(scrollAreaWidgetContents);
        label_82->setObjectName(QString::fromUtf8("label_82"));

        gridLayout_6->addWidget(label_82, 5, 3, 1, 1);

        label_83 = new QLabel(scrollAreaWidgetContents);
        label_83->setObjectName(QString::fromUtf8("label_83"));

        gridLayout_6->addWidget(label_83, 6, 3, 1, 1);

        label_84 = new QLabel(scrollAreaWidgetContents);
        label_84->setObjectName(QString::fromUtf8("label_84"));

        gridLayout_6->addWidget(label_84, 7, 3, 1, 1);

        label_85 = new QLabel(scrollAreaWidgetContents);
        label_85->setObjectName(QString::fromUtf8("label_85"));

        gridLayout_6->addWidget(label_85, 8, 3, 1, 1);

        label_86 = new QLabel(scrollAreaWidgetContents);
        label_86->setObjectName(QString::fromUtf8("label_86"));

        gridLayout_6->addWidget(label_86, 9, 3, 1, 1);

        label_269 = new QLabel(scrollAreaWidgetContents);
        label_269->setObjectName(QString::fromUtf8("label_269"));

        gridLayout_6->addWidget(label_269, 10, 3, 1, 1);

        label_270 = new QLabel(scrollAreaWidgetContents);
        label_270->setObjectName(QString::fromUtf8("label_270"));

        gridLayout_6->addWidget(label_270, 11, 3, 1, 1);

        label_271 = new QLabel(scrollAreaWidgetContents);
        label_271->setObjectName(QString::fromUtf8("label_271"));

        gridLayout_6->addWidget(label_271, 12, 3, 1, 1);

        label_272 = new QLabel(scrollAreaWidgetContents);
        label_272->setObjectName(QString::fromUtf8("label_272"));

        gridLayout_6->addWidget(label_272, 13, 3, 1, 1);

        label_46 = new QLabel(scrollAreaWidgetContents);
        label_46->setObjectName(QString::fromUtf8("label_46"));
        label_46->setFont(font1);
        label_46->setToolTipDuration(4);

        gridLayout_6->addWidget(label_46, 0, 1, 1, 1);

        label_52 = new QLabel(scrollAreaWidgetContents);
        label_52->setObjectName(QString::fromUtf8("label_52"));
        label_52->setFont(font1);

        gridLayout_6->addWidget(label_52, 1, 1, 1, 1);

        label_53 = new QLabel(scrollAreaWidgetContents);
        label_53->setObjectName(QString::fromUtf8("label_53"));
        label_53->setFont(font1);

        gridLayout_6->addWidget(label_53, 2, 1, 1, 1);

        label_44 = new QLabel(scrollAreaWidgetContents);
        label_44->setObjectName(QString::fromUtf8("label_44"));

        gridLayout_6->addWidget(label_44, 3, 1, 1, 1);

        label_55 = new QLabel(scrollAreaWidgetContents);
        label_55->setObjectName(QString::fromUtf8("label_55"));
        label_55->setFont(font1);

        gridLayout_6->addWidget(label_55, 4, 1, 1, 1);

        label_56 = new QLabel(scrollAreaWidgetContents);
        label_56->setObjectName(QString::fromUtf8("label_56"));
        label_56->setFont(font1);

        gridLayout_6->addWidget(label_56, 5, 1, 1, 1);

        label_57 = new QLabel(scrollAreaWidgetContents);
        label_57->setObjectName(QString::fromUtf8("label_57"));
        label_57->setFont(font1);

        gridLayout_6->addWidget(label_57, 6, 1, 1, 1);

        label_58 = new QLabel(scrollAreaWidgetContents);
        label_58->setObjectName(QString::fromUtf8("label_58"));
        label_58->setFont(font1);

        gridLayout_6->addWidget(label_58, 7, 1, 1, 1);

        label_59 = new QLabel(scrollAreaWidgetContents);
        label_59->setObjectName(QString::fromUtf8("label_59"));
        label_59->setFont(font1);

        gridLayout_6->addWidget(label_59, 8, 1, 1, 1);

        label_76 = new QLabel(scrollAreaWidgetContents);
        label_76->setObjectName(QString::fromUtf8("label_76"));

        gridLayout_6->addWidget(label_76, 9, 1, 1, 1);

        label_63 = new QLabel(scrollAreaWidgetContents);
        label_63->setObjectName(QString::fromUtf8("label_63"));

        gridLayout_6->addWidget(label_63, 10, 1, 1, 1);

        label_62 = new QLabel(scrollAreaWidgetContents);
        label_62->setObjectName(QString::fromUtf8("label_62"));
        label_62->setFont(font1);

        gridLayout_6->addWidget(label_62, 11, 1, 1, 1);

        label_51 = new QLabel(scrollAreaWidgetContents);
        label_51->setObjectName(QString::fromUtf8("label_51"));

        gridLayout_6->addWidget(label_51, 12, 1, 1, 1);

        label_64 = new QLabel(scrollAreaWidgetContents);
        label_64->setObjectName(QString::fromUtf8("label_64"));
        label_64->setFont(font1);

        gridLayout_6->addWidget(label_64, 13, 1, 1, 1);

        label_79 = new QLabel(scrollAreaWidgetContents);
        label_79->setObjectName(QString::fromUtf8("label_79"));

        gridLayout_6->addWidget(label_79, 14, 1, 1, 1);

        label_66 = new QLabel(scrollAreaWidgetContents);
        label_66->setObjectName(QString::fromUtf8("label_66"));
        label_66->setFont(font1);

        gridLayout_6->addWidget(label_66, 0, 4, 1, 1);

        label_67 = new QLabel(scrollAreaWidgetContents);
        label_67->setObjectName(QString::fromUtf8("label_67"));
        label_67->setFont(font1);

        gridLayout_6->addWidget(label_67, 1, 4, 1, 1);

        label_68 = new QLabel(scrollAreaWidgetContents);
        label_68->setObjectName(QString::fromUtf8("label_68"));
        label_68->setFont(font1);

        gridLayout_6->addWidget(label_68, 2, 4, 1, 1);

        label_80 = new QLabel(scrollAreaWidgetContents);
        label_80->setObjectName(QString::fromUtf8("label_80"));

        gridLayout_6->addWidget(label_80, 3, 4, 1, 1);

        label_87 = new QLabel(scrollAreaWidgetContents);
        label_87->setObjectName(QString::fromUtf8("label_87"));

        gridLayout_6->addWidget(label_87, 4, 4, 1, 1);

        label_88 = new QLabel(scrollAreaWidgetContents);
        label_88->setObjectName(QString::fromUtf8("label_88"));

        gridLayout_6->addWidget(label_88, 5, 4, 1, 1);

        label_89 = new QLabel(scrollAreaWidgetContents);
        label_89->setObjectName(QString::fromUtf8("label_89"));

        gridLayout_6->addWidget(label_89, 6, 4, 1, 1);

        label_90 = new QLabel(scrollAreaWidgetContents);
        label_90->setObjectName(QString::fromUtf8("label_90"));

        gridLayout_6->addWidget(label_90, 7, 4, 1, 1);

        label_91 = new QLabel(scrollAreaWidgetContents);
        label_91->setObjectName(QString::fromUtf8("label_91"));

        gridLayout_6->addWidget(label_91, 8, 4, 1, 1);

        label_92 = new QLabel(scrollAreaWidgetContents);
        label_92->setObjectName(QString::fromUtf8("label_92"));

        gridLayout_6->addWidget(label_92, 9, 4, 1, 1);

        label_273 = new QLabel(scrollAreaWidgetContents);
        label_273->setObjectName(QString::fromUtf8("label_273"));

        gridLayout_6->addWidget(label_273, 10, 4, 1, 1);

        label_274 = new QLabel(scrollAreaWidgetContents);
        label_274->setObjectName(QString::fromUtf8("label_274"));

        gridLayout_6->addWidget(label_274, 11, 4, 1, 1);

        label_275 = new QLabel(scrollAreaWidgetContents);
        label_275->setObjectName(QString::fromUtf8("label_275"));

        gridLayout_6->addWidget(label_275, 12, 4, 1, 1);

        label_276 = new QLabel(scrollAreaWidgetContents);
        label_276->setObjectName(QString::fromUtf8("label_276"));

        gridLayout_6->addWidget(label_276, 13, 4, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_3->addWidget(scrollArea, 0, 0, 1, 1);


        horizontalLayout->addWidget(widget_2);

        widget_3 = new QWidget(wgtHealthInfo);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setMaximumSize(QSize(250, 16777215));
        gridLayout_7 = new QGridLayout(widget_3);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_7->setContentsMargins(0, 0, 0, 0);

        horizontalLayout->addWidget(widget_3);


        gridLayout_2->addWidget(wgtHealthInfo, 0, 0, 1, 1);

        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/icons/healthinfo.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab2, icon3, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_4 = new QGridLayout(tab);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(-1, 9, -1, -1);
        wgtLockUnlock = new QWidget(tab);
        wgtLockUnlock->setObjectName(QString::fromUtf8("wgtLockUnlock"));
        verticalLayout_6 = new QVBoxLayout(wgtLockUnlock);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        treeView = new QTreeView(wgtLockUnlock);
        treeView->setObjectName(QString::fromUtf8("treeView"));
        treeView->setRootIsDecorated(false);
        treeView->setSortingEnabled(true);

        verticalLayout_6->addWidget(treeView);

        pushButton = new QPushButton(wgtLockUnlock);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setEnabled(false);
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        pushButton->setFont(font2);

        verticalLayout_6->addWidget(pushButton);


        gridLayout_4->addWidget(wgtLockUnlock, 0, 0, 1, 1);

        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/icons/drive.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tab, icon4, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        gridLayout_9 = new QGridLayout(tab_4);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        label_94 = new QLabel(tab_4);
        label_94->setObjectName(QString::fromUtf8("label_94"));

        gridLayout_9->addWidget(label_94, 0, 0, 1, 1);

        label_106 = new QLabel(tab_4);
        label_106->setObjectName(QString::fromUtf8("label_106"));

        gridLayout_9->addWidget(label_106, 0, 1, 1, 1);

        label_126 = new QLabel(tab_4);
        label_126->setObjectName(QString::fromUtf8("label_126"));

        gridLayout_9->addWidget(label_126, 0, 2, 1, 1);

        label_138 = new QLabel(tab_4);
        label_138->setObjectName(QString::fromUtf8("label_138"));

        gridLayout_9->addWidget(label_138, 0, 3, 1, 1);

        label_95 = new QLabel(tab_4);
        label_95->setObjectName(QString::fromUtf8("label_95"));

        gridLayout_9->addWidget(label_95, 1, 0, 1, 1);

        label_107 = new QLabel(tab_4);
        label_107->setObjectName(QString::fromUtf8("label_107"));

        gridLayout_9->addWidget(label_107, 1, 1, 1, 1);

        label_127 = new QLabel(tab_4);
        label_127->setObjectName(QString::fromUtf8("label_127"));

        gridLayout_9->addWidget(label_127, 1, 2, 1, 1);

        label_139 = new QLabel(tab_4);
        label_139->setObjectName(QString::fromUtf8("label_139"));

        gridLayout_9->addWidget(label_139, 1, 3, 1, 1);

        label_96 = new QLabel(tab_4);
        label_96->setObjectName(QString::fromUtf8("label_96"));

        gridLayout_9->addWidget(label_96, 2, 0, 1, 1);

        label_108 = new QLabel(tab_4);
        label_108->setObjectName(QString::fromUtf8("label_108"));

        gridLayout_9->addWidget(label_108, 2, 1, 1, 1);

        label_128 = new QLabel(tab_4);
        label_128->setObjectName(QString::fromUtf8("label_128"));

        gridLayout_9->addWidget(label_128, 2, 2, 1, 1);

        label_140 = new QLabel(tab_4);
        label_140->setObjectName(QString::fromUtf8("label_140"));

        gridLayout_9->addWidget(label_140, 2, 3, 1, 1);

        label_97 = new QLabel(tab_4);
        label_97->setObjectName(QString::fromUtf8("label_97"));

        gridLayout_9->addWidget(label_97, 3, 0, 1, 1);

        label_109 = new QLabel(tab_4);
        label_109->setObjectName(QString::fromUtf8("label_109"));

        gridLayout_9->addWidget(label_109, 3, 1, 1, 1);

        label_129 = new QLabel(tab_4);
        label_129->setObjectName(QString::fromUtf8("label_129"));

        gridLayout_9->addWidget(label_129, 3, 2, 1, 1);

        label_141 = new QLabel(tab_4);
        label_141->setObjectName(QString::fromUtf8("label_141"));

        gridLayout_9->addWidget(label_141, 3, 3, 1, 1);

        label_98 = new QLabel(tab_4);
        label_98->setObjectName(QString::fromUtf8("label_98"));

        gridLayout_9->addWidget(label_98, 4, 0, 1, 1);

        label_110 = new QLabel(tab_4);
        label_110->setObjectName(QString::fromUtf8("label_110"));

        gridLayout_9->addWidget(label_110, 4, 1, 1, 1);

        label_142 = new QLabel(tab_4);
        label_142->setObjectName(QString::fromUtf8("label_142"));

        gridLayout_9->addWidget(label_142, 4, 2, 1, 1);

        label_157 = new QLabel(tab_4);
        label_157->setObjectName(QString::fromUtf8("label_157"));

        gridLayout_9->addWidget(label_157, 4, 3, 1, 1);

        label_99 = new QLabel(tab_4);
        label_99->setObjectName(QString::fromUtf8("label_99"));

        gridLayout_9->addWidget(label_99, 5, 0, 1, 1);

        label_111 = new QLabel(tab_4);
        label_111->setObjectName(QString::fromUtf8("label_111"));

        gridLayout_9->addWidget(label_111, 5, 1, 1, 1);

        label_143 = new QLabel(tab_4);
        label_143->setObjectName(QString::fromUtf8("label_143"));

        gridLayout_9->addWidget(label_143, 5, 2, 1, 1);

        label_158 = new QLabel(tab_4);
        label_158->setObjectName(QString::fromUtf8("label_158"));

        gridLayout_9->addWidget(label_158, 5, 3, 1, 1);

        label_100 = new QLabel(tab_4);
        label_100->setObjectName(QString::fromUtf8("label_100"));

        gridLayout_9->addWidget(label_100, 6, 0, 1, 1);

        label_112 = new QLabel(tab_4);
        label_112->setObjectName(QString::fromUtf8("label_112"));

        gridLayout_9->addWidget(label_112, 6, 1, 1, 1);

        label_144 = new QLabel(tab_4);
        label_144->setObjectName(QString::fromUtf8("label_144"));

        gridLayout_9->addWidget(label_144, 6, 2, 1, 1);

        label_159 = new QLabel(tab_4);
        label_159->setObjectName(QString::fromUtf8("label_159"));

        gridLayout_9->addWidget(label_159, 6, 3, 1, 1);

        label_101 = new QLabel(tab_4);
        label_101->setObjectName(QString::fromUtf8("label_101"));

        gridLayout_9->addWidget(label_101, 7, 0, 1, 1);

        label_113 = new QLabel(tab_4);
        label_113->setObjectName(QString::fromUtf8("label_113"));

        gridLayout_9->addWidget(label_113, 7, 1, 1, 1);

        label_145 = new QLabel(tab_4);
        label_145->setObjectName(QString::fromUtf8("label_145"));

        gridLayout_9->addWidget(label_145, 7, 2, 1, 1);

        label_160 = new QLabel(tab_4);
        label_160->setObjectName(QString::fromUtf8("label_160"));

        gridLayout_9->addWidget(label_160, 7, 3, 1, 1);

        label_102 = new QLabel(tab_4);
        label_102->setObjectName(QString::fromUtf8("label_102"));

        gridLayout_9->addWidget(label_102, 8, 0, 1, 1);

        label_114 = new QLabel(tab_4);
        label_114->setObjectName(QString::fromUtf8("label_114"));

        gridLayout_9->addWidget(label_114, 8, 1, 1, 1);

        label_146 = new QLabel(tab_4);
        label_146->setObjectName(QString::fromUtf8("label_146"));

        gridLayout_9->addWidget(label_146, 8, 2, 1, 1);

        label_161 = new QLabel(tab_4);
        label_161->setObjectName(QString::fromUtf8("label_161"));

        gridLayout_9->addWidget(label_161, 8, 3, 1, 1);

        label_103 = new QLabel(tab_4);
        label_103->setObjectName(QString::fromUtf8("label_103"));

        gridLayout_9->addWidget(label_103, 9, 0, 1, 1);

        label_115 = new QLabel(tab_4);
        label_115->setObjectName(QString::fromUtf8("label_115"));

        gridLayout_9->addWidget(label_115, 9, 1, 1, 1);

        label_147 = new QLabel(tab_4);
        label_147->setObjectName(QString::fromUtf8("label_147"));

        gridLayout_9->addWidget(label_147, 9, 2, 1, 1);

        label_162 = new QLabel(tab_4);
        label_162->setObjectName(QString::fromUtf8("label_162"));

        gridLayout_9->addWidget(label_162, 9, 3, 1, 1);

        label_104 = new QLabel(tab_4);
        label_104->setObjectName(QString::fromUtf8("label_104"));

        gridLayout_9->addWidget(label_104, 10, 0, 1, 1);

        label_116 = new QLabel(tab_4);
        label_116->setObjectName(QString::fromUtf8("label_116"));

        gridLayout_9->addWidget(label_116, 10, 1, 1, 1);

        label_148 = new QLabel(tab_4);
        label_148->setObjectName(QString::fromUtf8("label_148"));

        gridLayout_9->addWidget(label_148, 10, 2, 1, 1);

        label_163 = new QLabel(tab_4);
        label_163->setObjectName(QString::fromUtf8("label_163"));

        gridLayout_9->addWidget(label_163, 10, 3, 1, 1);

        label_105 = new QLabel(tab_4);
        label_105->setObjectName(QString::fromUtf8("label_105"));

        gridLayout_9->addWidget(label_105, 11, 0, 1, 1);

        label_117 = new QLabel(tab_4);
        label_117->setObjectName(QString::fromUtf8("label_117"));

        gridLayout_9->addWidget(label_117, 11, 1, 1, 1);

        label_149 = new QLabel(tab_4);
        label_149->setObjectName(QString::fromUtf8("label_149"));

        gridLayout_9->addWidget(label_149, 11, 2, 1, 1);

        label_164 = new QLabel(tab_4);
        label_164->setObjectName(QString::fromUtf8("label_164"));

        gridLayout_9->addWidget(label_164, 11, 3, 1, 1);

        label_118 = new QLabel(tab_4);
        label_118->setObjectName(QString::fromUtf8("label_118"));

        gridLayout_9->addWidget(label_118, 12, 0, 1, 1);

        label_130 = new QLabel(tab_4);
        label_130->setObjectName(QString::fromUtf8("label_130"));

        gridLayout_9->addWidget(label_130, 12, 1, 1, 1);

        label_150 = new QLabel(tab_4);
        label_150->setObjectName(QString::fromUtf8("label_150"));

        gridLayout_9->addWidget(label_150, 12, 2, 1, 1);

        label_165 = new QLabel(tab_4);
        label_165->setObjectName(QString::fromUtf8("label_165"));

        gridLayout_9->addWidget(label_165, 12, 3, 1, 1);

        label_119 = new QLabel(tab_4);
        label_119->setObjectName(QString::fromUtf8("label_119"));

        gridLayout_9->addWidget(label_119, 13, 0, 1, 1);

        label_131 = new QLabel(tab_4);
        label_131->setObjectName(QString::fromUtf8("label_131"));

        gridLayout_9->addWidget(label_131, 13, 1, 1, 1);

        label_151 = new QLabel(tab_4);
        label_151->setObjectName(QString::fromUtf8("label_151"));

        gridLayout_9->addWidget(label_151, 13, 2, 1, 1);

        label_166 = new QLabel(tab_4);
        label_166->setObjectName(QString::fromUtf8("label_166"));

        gridLayout_9->addWidget(label_166, 13, 3, 1, 1);

        label_120 = new QLabel(tab_4);
        label_120->setObjectName(QString::fromUtf8("label_120"));

        gridLayout_9->addWidget(label_120, 14, 0, 1, 1);

        label_132 = new QLabel(tab_4);
        label_132->setObjectName(QString::fromUtf8("label_132"));

        gridLayout_9->addWidget(label_132, 14, 1, 1, 1);

        label_152 = new QLabel(tab_4);
        label_152->setObjectName(QString::fromUtf8("label_152"));

        gridLayout_9->addWidget(label_152, 14, 2, 1, 1);

        label_167 = new QLabel(tab_4);
        label_167->setObjectName(QString::fromUtf8("label_167"));

        gridLayout_9->addWidget(label_167, 14, 3, 1, 1);

        label_121 = new QLabel(tab_4);
        label_121->setObjectName(QString::fromUtf8("label_121"));

        gridLayout_9->addWidget(label_121, 15, 0, 1, 1);

        label_133 = new QLabel(tab_4);
        label_133->setObjectName(QString::fromUtf8("label_133"));

        gridLayout_9->addWidget(label_133, 15, 1, 1, 1);

        label_153 = new QLabel(tab_4);
        label_153->setObjectName(QString::fromUtf8("label_153"));

        gridLayout_9->addWidget(label_153, 15, 2, 1, 1);

        label_168 = new QLabel(tab_4);
        label_168->setObjectName(QString::fromUtf8("label_168"));

        gridLayout_9->addWidget(label_168, 15, 3, 1, 1);

        label_122 = new QLabel(tab_4);
        label_122->setObjectName(QString::fromUtf8("label_122"));

        gridLayout_9->addWidget(label_122, 16, 0, 1, 1);

        label_134 = new QLabel(tab_4);
        label_134->setObjectName(QString::fromUtf8("label_134"));

        gridLayout_9->addWidget(label_134, 16, 1, 1, 1);

        label_154 = new QLabel(tab_4);
        label_154->setObjectName(QString::fromUtf8("label_154"));

        gridLayout_9->addWidget(label_154, 16, 2, 1, 1);

        label_169 = new QLabel(tab_4);
        label_169->setObjectName(QString::fromUtf8("label_169"));

        gridLayout_9->addWidget(label_169, 16, 3, 1, 1);

        label_123 = new QLabel(tab_4);
        label_123->setObjectName(QString::fromUtf8("label_123"));

        gridLayout_9->addWidget(label_123, 17, 0, 1, 1);

        label_135 = new QLabel(tab_4);
        label_135->setObjectName(QString::fromUtf8("label_135"));

        gridLayout_9->addWidget(label_135, 17, 1, 1, 1);

        label_155 = new QLabel(tab_4);
        label_155->setObjectName(QString::fromUtf8("label_155"));

        gridLayout_9->addWidget(label_155, 17, 2, 1, 1);

        label_170 = new QLabel(tab_4);
        label_170->setObjectName(QString::fromUtf8("label_170"));

        gridLayout_9->addWidget(label_170, 17, 3, 1, 1);

        label_124 = new QLabel(tab_4);
        label_124->setObjectName(QString::fromUtf8("label_124"));

        gridLayout_9->addWidget(label_124, 18, 0, 1, 1);

        label_136 = new QLabel(tab_4);
        label_136->setObjectName(QString::fromUtf8("label_136"));

        gridLayout_9->addWidget(label_136, 18, 1, 1, 1);

        label_156 = new QLabel(tab_4);
        label_156->setObjectName(QString::fromUtf8("label_156"));

        gridLayout_9->addWidget(label_156, 18, 2, 1, 1);

        label_171 = new QLabel(tab_4);
        label_171->setObjectName(QString::fromUtf8("label_171"));

        gridLayout_9->addWidget(label_171, 18, 3, 1, 1);

        label_125 = new QLabel(tab_4);
        label_125->setObjectName(QString::fromUtf8("label_125"));

        gridLayout_9->addWidget(label_125, 19, 0, 1, 1);

        label_137 = new QLabel(tab_4);
        label_137->setObjectName(QString::fromUtf8("label_137"));

        gridLayout_9->addWidget(label_137, 19, 1, 1, 1);

        tabWidget->addTab(tab_4, QString());
        splitter->addWidget(tabWidget);

        gridLayout_8->addWidget(splitter, 0, 0, 1, 1);

        splitter_2->addWidget(widget);
        textBrowser = new QTextBrowser(splitter_2);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setMaximumSize(QSize(16777215, 150));
        splitter_2->addWidget(textBrowser);

        verticalLayout_5->addWidget(splitter_2);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1019, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Flexxon Pte Ltd", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QCoreApplication::translate("MainWindow", "XPHY Detected", nullptr));

        const bool __sortingEnabled = treeWidget->isSortingEnabled();
        treeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = treeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QCoreApplication::translate("MainWindow", "XPHY Devices", nullptr));
        treeWidget->setSortingEnabled(__sortingEnabled);

        label_260->setText(QCoreApplication::translate("MainWindow", "vs 1:", nullptr));
        label_235->setText(QCoreApplication::translate("MainWindow", "RRT2:", nullptr));
        label_36->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_224->setText(QCoreApplication::translate("MainWindow", "RRT1:", nullptr));
        label_258->setText(QCoreApplication::translate("MainWindow", "APS3:", nullptr));
        label_31->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_33->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_179->setText(QCoreApplication::translate("MainWindow", "CCTEMP:", nullptr));
        label_243->setText(QCoreApplication::translate("MainWindow", "APS2:", nullptr));
        lblModelName_27->setText(QCoreApplication::translate("MainWindow", "ELPE:", nullptr));
        firmwarerevision->setText(QCoreApplication::translate("MainWindow", "Firmware Revision:", nullptr));
        label_257->setText(QCoreApplication::translate("MainWindow", "APW3:", nullptr));
        label_185->setText(QCoreApplication::translate("MainWindow", "RRT0:", nullptr));
        label_219->setText(QCoreApplication::translate("MainWindow", "APW0:", nullptr));
        label_249->setText(QCoreApplication::translate("MainWindow", "EXLAT3:", nullptr));
        label_34->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_208->setText(QCoreApplication::translate("MainWindow", "AWUPF:", nullptr));
        label_174->setText(QCoreApplication::translate("MainWindow", "MDTS:", nullptr));
        label_204->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_197->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        lblModelName_15->setText(QCoreApplication::translate("MainWindow", "SUBNQN:", nullptr));
        label_17->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_38->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_216->setText(QCoreApplication::translate("MainWindow", "Power State 2:", nullptr));
        label_251->setText(QCoreApplication::translate("MainWindow", "RRL3:", nullptr));
        lblModelName_12->setText(QCoreApplication::translate("MainWindow", "Unallocated NVM:", nullptr));
        label_196->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_240->setText(QCoreApplication::translate("MainWindow", "IPS2:", nullptr));
        label_220->setText(QCoreApplication::translate("MainWindow", "APS0:", nullptr));
        label_223->setText(QCoreApplication::translate("MainWindow", "EXLAT1:", nullptr));
        label_231->setText(QCoreApplication::translate("MainWindow", "APW1:", nullptr));
        lblModelName_19->setText(QCoreApplication::translate("MainWindow", "IEEE:", nullptr));
        label_182->setText(QCoreApplication::translate("MainWindow", "HMMIN:", nullptr));
        label_253->setText(QCoreApplication::translate("MainWindow", "RWL3:", nullptr));
        label_263->setText(QCoreApplication::translate("MainWindow", "vs 4:", nullptr));
        label_267->setText(QCoreApplication::translate("MainWindow", "vs 8:", nullptr));
        label_187->setText(QCoreApplication::translate("MainWindow", "CQES:", nullptr));
        label_214->setText(QCoreApplication::translate("MainWindow", "MPS1:", nullptr));
        label_24->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_40->setText(QCoreApplication::translate("MainWindow", "Power State 0:", nullptr));
        label_198->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_26->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_49->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        lblModelName_14->setText(QCoreApplication::translate("MainWindow", "Total NVM Cap:", nullptr));
        label_211->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_181->setText(QCoreApplication::translate("MainWindow", "HMPRE:", nullptr));
        label_221->setText(QCoreApplication::translate("MainWindow", "MPS2:", nullptr));
        label_28->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_228->setText(QCoreApplication::translate("MainWindow", "IDLP1:", nullptr));
        label_16->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_202->setText(QCoreApplication::translate("MainWindow", "IPS0:", nullptr));
        label_252->setText(QCoreApplication::translate("MainWindow", "RWT3:", nullptr));
        label_177->setText(QCoreApplication::translate("MainWindow", "APSTA:", nullptr));
        label_192->setText(QCoreApplication::translate("MainWindow", "RWT0:", nullptr));
        label_206->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        lblModelName_31->setText(QCoreApplication::translate("MainWindow", "AVSCC:", nullptr));
        label_248->setText(QCoreApplication::translate("MainWindow", "ENLAT3:", nullptr));
        label_255->setText(QCoreApplication::translate("MainWindow", "IPS3:", nullptr));
        label_191->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_254->setText(QCoreApplication::translate("MainWindow", "IDLP3:", nullptr));
        label_188->setText(QCoreApplication::translate("MainWindow", "FUSES:", nullptr));
        label_32->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_45->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_190->setText(QCoreApplication::translate("MainWindow", "AWUN:", nullptr));
        label_227->setText(QCoreApplication::translate("MainWindow", "RWL1:", nullptr));
        label_29->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_21->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_193->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_23->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        lblModelName_25->setText(QCoreApplication::translate("MainWindow", "AERL:", nullptr));
        label_39->setText(QCoreApplication::translate("MainWindow", "IPS1:", nullptr));
        lblModelName->setText(QCoreApplication::translate("MainWindow", "Namespace No.:", nullptr));
        label_205->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_178->setText(QCoreApplication::translate("MainWindow", "WCTEMP:", nullptr));
        lblModelName_17->setText(QCoreApplication::translate("MainWindow", "SQES:", nullptr));
        label_186->setText(QCoreApplication::translate("MainWindow", "RRL0:", nullptr));
        lblModelName_8->setText(QCoreApplication::translate("MainWindow", "RTD3e:", nullptr));
        label_35->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_183->setText(QCoreApplication::translate("MainWindow", "ENLAT0:", nullptr));
        label_245->setText(QCoreApplication::translate("MainWindow", "MP3:", nullptr));
        label_236->setText(QCoreApplication::translate("MainWindow", "RRL2:", nullptr));
        label_210->setText(QCoreApplication::translate("MainWindow", "SGLS:", nullptr));
        label_266->setText(QCoreApplication::translate("MainWindow", "vs 7:", nullptr));
        label_247->setText(QCoreApplication::translate("MainWindow", "NOPS3:", nullptr));
        label_218->setText(QCoreApplication::translate("MainWindow", "ACTP0:", nullptr));
        label_239->setText(QCoreApplication::translate("MainWindow", "IDLP2:", nullptr));
        label_30->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_203->setText(QCoreApplication::translate("MainWindow", "MP1:", nullptr));
        label_256->setText(QCoreApplication::translate("MainWindow", "ACTP3:", nullptr));
        lblModelName_2->setText(QCoreApplication::translate("MainWindow", "NPSS:", nullptr));
        label_199->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_201->setText(QCoreApplication::translate("MainWindow", "IDLP0:", nullptr));
        label_20->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_184->setText(QCoreApplication::translate("MainWindow", "EXLAT0:", nullptr));
        label_173->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_195->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_226->setText(QCoreApplication::translate("MainWindow", "RWT1:", nullptr));
        lblModelName_18->setText(QCoreApplication::translate("MainWindow", "Serial Number:", nullptr));
        lblModelName_10->setText(QCoreApplication::translate("MainWindow", "RPMBS:", nullptr));
        label_265->setText(QCoreApplication::translate("MainWindow", "vs 6:", nullptr));
        label_250->setText(QCoreApplication::translate("MainWindow", "RRT3:", nullptr));
        lblFwVersion->setText(QCoreApplication::translate("MainWindow", "Model Number:", nullptr));
        lblModelName_9->setText(QCoreApplication::translate("MainWindow", "RAB:", nullptr));
        lblModelName_11->setText(QCoreApplication::translate("MainWindow", "ONCS:", nullptr));
        label_22->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_207->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_19->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_42->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_238->setText(QCoreApplication::translate("MainWindow", "RWL2:", nullptr));
        label_244->setText(QCoreApplication::translate("MainWindow", "Power State 3:", nullptr));
        label_232->setText(QCoreApplication::translate("MainWindow", "APS1:", nullptr));
        label_25->setText(QCoreApplication::translate("MainWindow", "Power State 1:", nullptr));
        label_180->setText(QCoreApplication::translate("MainWindow", "MFTA:", nullptr));
        lblModelName_16->setText(QCoreApplication::translate("MainWindow", "SSVID:", nullptr));
        label_237->setText(QCoreApplication::translate("MainWindow", "RWT2:", nullptr));
        lblModelName_28->setText(QCoreApplication::translate("MainWindow", "ACL:", nullptr));
        label_241->setText(QCoreApplication::translate("MainWindow", "ACTP2:", nullptr));
        label_268->setText(QCoreApplication::translate("MainWindow", "vs 9:", nullptr));
        lblModelName_5->setText(QCoreApplication::translate("MainWindow", "RTD3r:", nullptr));
        lblModelName_13->setText(QCoreApplication::translate("MainWindow", "Version:", nullptr));
        label_37->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_217->setText(QCoreApplication::translate("MainWindow", "MP2:", nullptr));
        label_213->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_246->setText(QCoreApplication::translate("MainWindow", "MPS3:", nullptr));
        lblModelName_3->setText(QCoreApplication::translate("MainWindow", "OACS:", nullptr));
        lblModelName_20->setText(QCoreApplication::translate("MainWindow", "Log Page Attr:", nullptr));
        label_72->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_264->setText(QCoreApplication::translate("MainWindow", "vs 5:", nullptr));
        label_43->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_189->setText(QCoreApplication::translate("MainWindow", "FNA:", nullptr));
        label_229->setText(QCoreApplication::translate("MainWindow", "NOPS2:", nullptr));
        lblModelName_23->setText(QCoreApplication::translate("MainWindow", "Volatile Wr Cache:", nullptr));
        label_175->setText(QCoreApplication::translate("MainWindow", "NOPS0:", nullptr));
        label_234->setText(QCoreApplication::translate("MainWindow", "EXLAT2:", nullptr));
        lblModelName_6->setText(QCoreApplication::translate("MainWindow", "NVSCC:", nullptr));
        label_262->setText(QCoreApplication::translate("MainWindow", "vs 3:", nullptr));
        label_230->setText(QCoreApplication::translate("MainWindow", "ACTP1:", nullptr));
        lblModelName_4->setText(QCoreApplication::translate("MainWindow", "OAES:", nullptr));
        label_225->setText(QCoreApplication::translate("MainWindow", "RRL1:", nullptr));
        label_47->setText(QCoreApplication::translate("MainWindow", "MP0:", nullptr));
        label_48->setText(QCoreApplication::translate("MainWindow", "MPS0:", nullptr));
        label_233->setText(QCoreApplication::translate("MainWindow", "ENLAT2:", nullptr));
        label_222->setText(QCoreApplication::translate("MainWindow", "ENLAT1:", nullptr));
        label_200->setText(QCoreApplication::translate("MainWindow", "RWL0:", nullptr));
        label_242->setText(QCoreApplication::translate("MainWindow", "APW2:", nullptr));
        label_194->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_176->setText(QCoreApplication::translate("MainWindow", "FRMW:", nullptr));
        label_215->setText(QCoreApplication::translate("MainWindow", "NOPS1:", nullptr));
        label_41->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_212->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        lblSerialNumber->setText(QCoreApplication::translate("MainWindow", "Controller Id:", nullptr));
        label_259->setText(QCoreApplication::translate("MainWindow", "vs 0:", nullptr));
        label_27->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_209->setText(QCoreApplication::translate("MainWindow", "ACWU:", nullptr));
        label_172->setText(QCoreApplication::translate("MainWindow", "CMIC:", nullptr));
        label_261->setText(QCoreApplication::translate("MainWindow", "vs 2:", nullptr));
        label_18->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        lblModelName_24->setText(QCoreApplication::translate("MainWindow", "VID:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab1), QCoreApplication::translate("MainWindow", "XPHY Info", nullptr));
        pushButton_3->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        label_73->setText(QCoreApplication::translate("MainWindow", "Free Space", nullptr));
        pushButton_2->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        label_65->setText(QCoreApplication::translate("MainWindow", "Used Space", nullptr));
        pushButton_4->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        label_74->setText(QCoreApplication::translate("MainWindow", "Temperature", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Critical Warning:", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Composite Temp:", nullptr));
        label_71->setText(QCoreApplication::translate("MainWindow", "Warning Comp Temp:", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Avail Spare (Percent):", nullptr));
        label_15->setText(QCoreApplication::translate("MainWindow", "Avail Threshold:", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "Percentage Used:", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "Data Units Read:", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "Data Units Written:", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "Host Read Cmds:", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "Host Write Cmds:", nullptr));
        label_75->setText(QCoreApplication::translate("MainWindow", "Control Busy Time:", nullptr));
        label_61->setText(QCoreApplication::translate("MainWindow", "Power Cycle:", nullptr));
        label_12->setText(QCoreApplication::translate("MainWindow", "Power On Hours:", nullptr));
        label_50->setText(QCoreApplication::translate("MainWindow", "Unsafe Shutdown:", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "Media Data Integ Error:", nullptr));
        label_77->setText(QCoreApplication::translate("MainWindow", "Number of Error Info Log:", nullptr));
        label_70->setText(QCoreApplication::translate("MainWindow", "Critical Comp Temp:", nullptr));
        label_69->setText(QCoreApplication::translate("MainWindow", "Temp Sensor 1:", nullptr));
        label_78->setText(QCoreApplication::translate("MainWindow", "Temp Sensor 2:", nullptr));
        label_81->setText(QCoreApplication::translate("MainWindow", "Temp Sensor 3:", nullptr));
        label_82->setText(QCoreApplication::translate("MainWindow", "Temp Sensor 4:", nullptr));
        label_83->setText(QCoreApplication::translate("MainWindow", "Temp Sensor 5:", nullptr));
        label_84->setText(QCoreApplication::translate("MainWindow", "Temp Sensor 6:", nullptr));
        label_85->setText(QCoreApplication::translate("MainWindow", "Temp Sensor 7:", nullptr));
        label_86->setText(QCoreApplication::translate("MainWindow", "Temp Sensor 8:", nullptr));
        label_269->setText(QCoreApplication::translate("MainWindow", "Thermal Mgmt Temp 1:", nullptr));
        label_270->setText(QCoreApplication::translate("MainWindow", "Thermal Mgmt Temp 2:", nullptr));
        label_271->setText(QCoreApplication::translate("MainWindow", "Total Time Temp 1:", nullptr));
        label_272->setText(QCoreApplication::translate("MainWindow", "Total Time Temp 2:", nullptr));
        label_46->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_52->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_53->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_44->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_55->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_56->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_57->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_58->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_59->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_76->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_63->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_62->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_51->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_64->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_79->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_66->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_67->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_68->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_80->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_87->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_88->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_89->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_90->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_91->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_92->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_273->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_274->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_275->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_276->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab2), QCoreApplication::translate("MainWindow", "Health Info", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "Lock / Unlock", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "Namespace1 Lock File", nullptr));
        label_94->setText(QCoreApplication::translate("MainWindow", "Namespace Size:", nullptr));
        label_106->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_126->setText(QCoreApplication::translate("MainWindow", "NVM Capacity:", nullptr));
        label_138->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_95->setText(QCoreApplication::translate("MainWindow", "Namespace Capabity:", nullptr));
        label_107->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_127->setText(QCoreApplication::translate("MainWindow", "Namespace Globally Unique Ident:", nullptr));
        label_139->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_96->setText(QCoreApplication::translate("MainWindow", "Namespace Utilization:", nullptr));
        label_108->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_128->setText(QCoreApplication::translate("MainWindow", "IEEE Extended Unique Ident:", nullptr));
        label_140->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_97->setText(QCoreApplication::translate("MainWindow", "Namespace Features:", nullptr));
        label_109->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_129->setText(QCoreApplication::translate("MainWindow", "LBA Format 0 Support:", nullptr));
        label_141->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_98->setText(QCoreApplication::translate("MainWindow", "Number of LBA formats:", nullptr));
        label_110->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_142->setText(QCoreApplication::translate("MainWindow", "LBA Format 1 Support:", nullptr));
        label_157->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_99->setText(QCoreApplication::translate("MainWindow", "Formatted LBA Size:", nullptr));
        label_111->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_143->setText(QCoreApplication::translate("MainWindow", "LBA Format 2 Support:", nullptr));
        label_158->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_100->setText(QCoreApplication::translate("MainWindow", "Metadata Capabilities:", nullptr));
        label_112->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_144->setText(QCoreApplication::translate("MainWindow", "LBA Format 3 Support:", nullptr));
        label_159->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_101->setText(QCoreApplication::translate("MainWindow", "End-to-end Data Protection Capabilities:", nullptr));
        label_113->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_145->setText(QCoreApplication::translate("MainWindow", "LBA Format 4 Support:", nullptr));
        label_160->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_102->setText(QCoreApplication::translate("MainWindow", "End-to-end Data Protection Type Settings:", nullptr));
        label_114->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_146->setText(QCoreApplication::translate("MainWindow", "LBA Format 5 Support:", nullptr));
        label_161->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_103->setText(QCoreApplication::translate("MainWindow", "Ns Multi-path I/O and ns sharing capabilities:", nullptr));
        label_115->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_147->setText(QCoreApplication::translate("MainWindow", "LBA Format 6 Support:", nullptr));
        label_162->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_104->setText(QCoreApplication::translate("MainWindow", "Reservation Capabilities:", nullptr));
        label_116->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_148->setText(QCoreApplication::translate("MainWindow", "LBA Format 7 Support:", nullptr));
        label_163->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_105->setText(QCoreApplication::translate("MainWindow", "Format Progress Indicator:", nullptr));
        label_117->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_149->setText(QCoreApplication::translate("MainWindow", "LBA Format 8 Support:", nullptr));
        label_164->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_118->setText(QCoreApplication::translate("MainWindow", "Deallocate Logical Block Features:", nullptr));
        label_130->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_150->setText(QCoreApplication::translate("MainWindow", "LBA Format 9 Support:", nullptr));
        label_165->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_119->setText(QCoreApplication::translate("MainWindow", "Namespace Atomic Write Unit Normal:", nullptr));
        label_131->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_151->setText(QCoreApplication::translate("MainWindow", "LBA Format 10 Support:", nullptr));
        label_166->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_120->setText(QCoreApplication::translate("MainWindow", "Namespace Atomic Write Unit Power Fail:", nullptr));
        label_132->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_152->setText(QCoreApplication::translate("MainWindow", "LBA Format 11 Support:", nullptr));
        label_167->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_121->setText(QCoreApplication::translate("MainWindow", "Namespace Atomic Compare & Write Unit:", nullptr));
        label_133->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_153->setText(QCoreApplication::translate("MainWindow", "LBA Format 12 Support:", nullptr));
        label_168->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_122->setText(QCoreApplication::translate("MainWindow", "Namespace Atomic Boundary Size Normal:", nullptr));
        label_134->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_154->setText(QCoreApplication::translate("MainWindow", "LBA Format 13 Support:", nullptr));
        label_169->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_123->setText(QCoreApplication::translate("MainWindow", "Namespace Atomic Boundary Offset:", nullptr));
        label_135->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_155->setText(QCoreApplication::translate("MainWindow", "LBA Format 14 Support:", nullptr));
        label_170->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_124->setText(QCoreApplication::translate("MainWindow", "Namespace Atomic Boundary Size Power Fail:", nullptr));
        label_136->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_156->setText(QCoreApplication::translate("MainWindow", "LBA Format 15 Support:", nullptr));
        label_171->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_125->setText(QCoreApplication::translate("MainWindow", "Namespace Optimal IO Boundary:", nullptr));
        label_137->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("MainWindow", "NameSpace1 Info", nullptr));
        textBrowser->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'MS Shell Dlg 2';\"><br /></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
