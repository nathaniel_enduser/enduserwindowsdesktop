/********************************************************************************
** Form generated from reading UI file 'genconfigdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GENCONFIGDIALOG_H
#define UI_GENCONFIGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_genconfigDialog
{
public:
    QGridLayout *gridLayout_3;
    QComboBox *comboBox;
    QLabel *label;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QLabel *label_3;
    QPushButton *pushButton_19;
    QFrame *line_2;
    QPushButton *pushButton;
    QComboBox *comboBox_2;
    QLabel *label_4;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_7;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_8;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_2;
    QCheckBox *checkBox_87;
    QCheckBox *checkBox_22;
    QCheckBox *checkBox_21;
    QCheckBox *checkBox_23;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;

    void setupUi(QDialog *genconfigDialog)
    {
        if (genconfigDialog->objectName().isEmpty())
            genconfigDialog->setObjectName(QString::fromUtf8("genconfigDialog"));
        genconfigDialog->resize(464, 290);
        gridLayout_3 = new QGridLayout(genconfigDialog);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        comboBox = new QComboBox(genconfigDialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_3->addWidget(comboBox, 0, 0, 1, 1);

        label = new QLabel(genconfigDialog);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);

        gridLayout_3->addWidget(label, 0, 1, 1, 2);

        lineEdit = new QLineEdit(genconfigDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout_3->addWidget(lineEdit, 1, 0, 1, 1);

        label_2 = new QLabel(genconfigDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setPointSize(10);
        label_2->setFont(font1);

        gridLayout_3->addWidget(label_2, 1, 1, 1, 2);

        lineEdit_2 = new QLineEdit(genconfigDialog);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setEchoMode(QLineEdit::Password);

        gridLayout_3->addWidget(lineEdit_2, 2, 0, 1, 1);

        label_3 = new QLabel(genconfigDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        gridLayout_3->addWidget(label_3, 2, 1, 1, 2);

        pushButton_19 = new QPushButton(genconfigDialog);
        pushButton_19->setObjectName(QString::fromUtf8("pushButton_19"));
        QFont font2;
        font2.setPointSize(16);
        pushButton_19->setFont(font2);

        gridLayout_3->addWidget(pushButton_19, 2, 3, 1, 1);

        line_2 = new QFrame(genconfigDialog);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_3->addWidget(line_2, 3, 0, 2, 4);

        pushButton = new QPushButton(genconfigDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font3;
        font3.setPointSize(14);
        font3.setBold(false);
        font3.setWeight(50);
        pushButton->setFont(font3);

        gridLayout_3->addWidget(pushButton, 4, 3, 3, 1);

        comboBox_2 = new QComboBox(genconfigDialog);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        gridLayout_3->addWidget(comboBox_2, 5, 0, 1, 1);

        label_4 = new QLabel(genconfigDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);

        gridLayout_3->addWidget(label_4, 5, 1, 1, 1);

        groupBox = new QGroupBox(genconfigDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkBox_3 = new QCheckBox(groupBox);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));

        gridLayout->addWidget(checkBox_3, 0, 0, 1, 1);

        checkBox_6 = new QCheckBox(groupBox);
        checkBox_6->setObjectName(QString::fromUtf8("checkBox_6"));

        gridLayout->addWidget(checkBox_6, 0, 1, 1, 1);

        checkBox_4 = new QCheckBox(groupBox);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));

        gridLayout->addWidget(checkBox_4, 1, 0, 1, 1);

        checkBox_7 = new QCheckBox(groupBox);
        checkBox_7->setObjectName(QString::fromUtf8("checkBox_7"));

        gridLayout->addWidget(checkBox_7, 1, 1, 1, 1);

        checkBox_5 = new QCheckBox(groupBox);
        checkBox_5->setObjectName(QString::fromUtf8("checkBox_5"));

        gridLayout->addWidget(checkBox_5, 2, 0, 1, 1);

        checkBox_8 = new QCheckBox(groupBox);
        checkBox_8->setObjectName(QString::fromUtf8("checkBox_8"));

        gridLayout->addWidget(checkBox_8, 2, 1, 1, 1);


        gridLayout_3->addWidget(groupBox, 7, 2, 1, 2);

        groupBox_3 = new QGroupBox(genconfigDialog);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_2 = new QGridLayout(groupBox_3);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        checkBox_87 = new QCheckBox(groupBox_3);
        checkBox_87->setObjectName(QString::fromUtf8("checkBox_87"));

        gridLayout_2->addWidget(checkBox_87, 1, 1, 1, 1);

        checkBox_22 = new QCheckBox(groupBox_3);
        checkBox_22->setObjectName(QString::fromUtf8("checkBox_22"));

        gridLayout_2->addWidget(checkBox_22, 1, 0, 1, 1);

        checkBox_21 = new QCheckBox(groupBox_3);
        checkBox_21->setObjectName(QString::fromUtf8("checkBox_21"));

        gridLayout_2->addWidget(checkBox_21, 0, 0, 1, 1);

        checkBox_23 = new QCheckBox(groupBox_3);
        checkBox_23->setObjectName(QString::fromUtf8("checkBox_23"));

        gridLayout_2->addWidget(checkBox_23, 0, 1, 1, 1);

        checkBox = new QCheckBox(groupBox_3);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        gridLayout_2->addWidget(checkBox, 2, 0, 1, 1);

        checkBox_2 = new QCheckBox(groupBox_3);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));

        gridLayout_2->addWidget(checkBox_2, 2, 1, 1, 1);


        gridLayout_3->addWidget(groupBox_3, 7, 0, 1, 2);

        QWidget::setTabOrder(pushButton_19, comboBox);
        QWidget::setTabOrder(comboBox, pushButton);
        QWidget::setTabOrder(pushButton, checkBox_87);
        QWidget::setTabOrder(checkBox_87, checkBox_23);
        QWidget::setTabOrder(checkBox_23, checkBox_21);
        QWidget::setTabOrder(checkBox_21, checkBox_22);

        retranslateUi(genconfigDialog);

        QMetaObject::connectSlotsByName(genconfigDialog);
    } // setupUi

    void retranslateUi(QDialog *genconfigDialog)
    {
        genconfigDialog->setWindowTitle(QCoreApplication::translate("genconfigDialog", "General Configuration", nullptr));
        label->setText(QCoreApplication::translate("genconfigDialog", "XPHY ", nullptr));
        label_2->setText(QCoreApplication::translate("genconfigDialog", "Old Password", nullptr));
        label_3->setText(QCoreApplication::translate("genconfigDialog", "New Password", nullptr));
        pushButton_19->setText(QCoreApplication::translate("genconfigDialog", "Create Password", nullptr));
        pushButton->setText(QCoreApplication::translate("genconfigDialog", "Update Config", nullptr));
        label_4->setText(QCoreApplication::translate("genconfigDialog", "Namespace", nullptr));
        groupBox->setTitle(QCoreApplication::translate("genconfigDialog", "Policies", nullptr));
        checkBox_3->setText(QCoreApplication::translate("genconfigDialog", "policy1", nullptr));
        checkBox_6->setText(QCoreApplication::translate("genconfigDialog", "policy4", nullptr));
        checkBox_4->setText(QCoreApplication::translate("genconfigDialog", "policy2", nullptr));
        checkBox_7->setText(QCoreApplication::translate("genconfigDialog", "policy5", nullptr));
        checkBox_5->setText(QCoreApplication::translate("genconfigDialog", "policy3", nullptr));
        checkBox_8->setText(QCoreApplication::translate("genconfigDialog", "policy6", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("genconfigDialog", "Security Options", nullptr));
        checkBox_87->setText(QCoreApplication::translate("genconfigDialog", "Enc/Dec", nullptr));
        checkBox_22->setText(QCoreApplication::translate("genconfigDialog", "Change Policy", nullptr));
        checkBox_21->setText(QCoreApplication::translate("genconfigDialog", "Lock File/Folder", nullptr));
        checkBox_23->setText(QCoreApplication::translate("genconfigDialog", "Secure Erase", nullptr));
        checkBox->setText(QCoreApplication::translate("genconfigDialog", "Protect Namespace", nullptr));
        checkBox_2->setText(QCoreApplication::translate("genconfigDialog", "Update Signature", nullptr));
    } // retranslateUi

};

namespace Ui {
    class genconfigDialog: public Ui_genconfigDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GENCONFIGDIALOG_H
