#ifndef CALCULATIONWORKER_H
#define CALCULATIONWORKER_H

#include <QObject>
#include <QThread> // msleep()

class CalculationWorker : public QObject
{
    Q_OBJECT
public:
    explicit CalculationWorker(QObject *parent = nullptr);

signals:
    void progress( int processed, int quantity );
    void stopped();
    void completed();

public slots:
    void calculate();
    void stopCalculation();

private:
    bool m_stopFlag;
};

#endif // CALCULATIONWORKER_H
