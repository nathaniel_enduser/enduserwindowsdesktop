#ifndef ENCDECDIALOG_H
#define ENCDECDIALOG_H

#include <QDialog>
#include <QTimer>
#include <QMutex>
#include <QWaitCondition>
#include <genconfigdialog.h>

class QProgressBar;

namespace Ui {
class encdecDialog;
}

class encdecDialog : public QDialog
{
    Q_OBJECT

public:
    explicit encdecDialog(QWidget *parent = nullptr);
    ~encdecDialog();

    void        savenamespace(QString ns);
    void        savexphy(QString xphy);
    void        setcomboitem(int idx);
    void        saveData(CONFIG_DATA *ConfigData);
    void        enadiscombobox(bool val);
    void        enadiscomboboxns(bool val);
    void        enadispbutton2(bool val);
    void        setcombotext(QString str);
    void        setcombotextns(QString str);
    void        disableradioall();
    void        enableradioall(void);
    void        enadisradio(int pos, bool val);
    void        setfocus(void);
    bool        getcheckboxstate(int checkboxsel);

    QString     encdecitemns;
    QString     encdecitemxphy;
    int         checkboxsel;

    int         progressval;
    QTimer      *timer;

    QMutex          mutex;
    QWaitCondition  buffernotempty;
    QWaitCondition  buffernotfull;
    int             numusedbytes;
    char            buffer[BUFFERSIZE];
//    bool encdecnspacestate = false;
//    bool encenable = false;

public slots:
    void        updateprogress();

signals:
    void    eventsignal(QString);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_comboBox_2_activated(int index);

    void on_comboBox_2_activated(const QString &arg1);

private:
    Ui::encdecDialog    *ui;
    CONFIG_DATA         *ConfigData;
    QProgressBar        *progressbar;

};

#endif // ENCDECDIALOG_H
