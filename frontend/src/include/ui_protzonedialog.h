/********************************************************************************
** Form generated from reading UI file 'protzonedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROTZONEDIALOG_H
#define UI_PROTZONEDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_protzoneDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *label_3;
    QFrame *line;
    QComboBox *comboBox_2;
    QPushButton *pushButton;
    QLabel *label;
    QLabel *label_4;
    QComboBox *comboBox;
    QProgressBar *progressBar;
    QLineEdit *lineEdit;
    QPushButton *pushButton_2;

    void setupUi(QDialog *protzoneDialog)
    {
        if (protzoneDialog->objectName().isEmpty())
            protzoneDialog->setObjectName(QString::fromUtf8("protzoneDialog"));
        protzoneDialog->resize(396, 270);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/protectzone.png"), QSize(), QIcon::Normal, QIcon::Off);
        protzoneDialog->setWindowIcon(icon);
        gridLayout = new QGridLayout(protzoneDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_3 = new QLabel(protzoneDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font;
        font.setPointSize(14);
        label_3->setFont(font);

        gridLayout->addWidget(label_3, 0, 0, 1, 1);

        line = new QFrame(protzoneDialog);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 5, 0, 1, 2);

        comboBox_2 = new QComboBox(protzoneDialog);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette.setBrush(QPalette::Active, QPalette::Link, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Link, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Highlight, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Link, brush);
        comboBox_2->setPalette(palette);
        comboBox_2->setMaxVisibleItems(10);

        gridLayout->addWidget(comboBox_2, 7, 0, 1, 1);

        pushButton = new QPushButton(protzoneDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font);

        gridLayout->addWidget(pushButton, 3, 1, 1, 1);

        label = new QLabel(protzoneDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        gridLayout->addWidget(label, 2, 0, 1, 1);

        label_4 = new QLabel(protzoneDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        gridLayout->addWidget(label_4, 6, 0, 1, 1);

        comboBox = new QComboBox(protzoneDialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 1, 0, 1, 1);

        progressBar = new QProgressBar(protzoneDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout->addWidget(progressBar, 8, 0, 1, 2);

        lineEdit = new QLineEdit(protzoneDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setFont(font);
        lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEdit, 3, 0, 1, 1);

        pushButton_2 = new QPushButton(protzoneDialog);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font);

        gridLayout->addWidget(pushButton_2, 7, 1, 1, 1);


        retranslateUi(protzoneDialog);

        comboBox_2->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(protzoneDialog);
    } // setupUi

    void retranslateUi(QDialog *protzoneDialog)
    {
        protzoneDialog->setWindowTitle(QCoreApplication::translate("protzoneDialog", "Protect Namespace", nullptr));
        label_3->setText(QCoreApplication::translate("protzoneDialog", "XPHY", nullptr));
        pushButton->setText(QCoreApplication::translate("protzoneDialog", "Verify Password", nullptr));
        label->setText(QCoreApplication::translate("protzoneDialog", "Password", nullptr));
        label_4->setText(QCoreApplication::translate("protzoneDialog", "Namespace", nullptr));
        pushButton_2->setText(QCoreApplication::translate("protzoneDialog", "Protect Namespace", nullptr));
    } // retranslateUi

};

namespace Ui {
    class protzoneDialog: public Ui_protzoneDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROTZONEDIALOG_H
