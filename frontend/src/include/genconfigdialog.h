#ifndef GENCONFIGDIALOG_H
#define GENCONFIGDIALOG_H

#include <QDialog>
//#include <genconfigpwdialog.h>
#include "enduserdesktop.h"

namespace Ui {
class genconfigDialog;
}

class genconfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit genconfigDialog(QWidget *parent = nullptr);
    ~genconfigDialog();

    void            setcombotext(QString str);
    void            setcombotextns(QString str);
    void            setgroupboxns(bool);
    void            setgroupboxpol(bool);
    void            setfocus();
    void            setpushbuttonupdcfg(bool val);
    void            setpushbuttoncreapw(bool val);
    void            setcombostate(bool val);
    void            savedata(CONFIG_DATA *ConfigData);
    void            savelogpolicy(int xphyno, int nsno);

    CONFIG_DATA     *returnData();
    CONFIG_DATA     *configdata;

signals:
    void            eventsignal(QString);

private slots:
    void            on_pushButton_19_clicked();
    void            on_pushButton_clicked();
    void            on_comboBox_currentIndexChanged(int index);
//    void            on_comboBox_currentIndexChanged();
    void            on_checkBox_22_toggled(bool checked);

private:
    void            on_update_config_clicked();
    void            on_comboBox_activated(const QString &arg1);
    void            on_checkBox_clicked();

private:
    Ui::genconfigDialog *ui;
};

#endif // GENCONFIGDIALOG_H
