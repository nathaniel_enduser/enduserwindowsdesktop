QT       += core gui charts multimedia
#QMAKE_LFLAGS_WINDOWS += /MANIFESTUAC:"level='requireAdministrator'"

#LIBS     += -Lc:\flexxlib -lflexxlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG(debug, debug|release){
    TARGET = EndUserDesktopAppDebug
    UI_DIR = ../include
    win32 {
        DESTDIR = ../../plugins/windows
        OBJECTS_DIR = ../object/windows
        MOC_DIR = ../source/qtgenerated/windows
        RCC_DIR = ../source/qtgenerated/windows
    }
    unix {
        DESTDIR = ../../plugins/linux
        OBJECTS_DIR = ../object/linux
        MOC_DIR = ../source/qtgenerated/linux
        RCC_DIR = ../source/qtgenerated/linux
    }
}

CONFIG(release, debug|release){
    TARGET = EndUserDesktopApp
    UI_DIR = ../include
    win32 {
        DESTDIR = ../../plugins/windows
        OBJECTS_DIR = ../object/windows
        MOC_DIR = ../source/qtgenerated/windows
        RCC_DIR = ../source/qtgenerated/windows
    }
    unix {
        DESTDIR = ../../plugins/linux
        OBJECTS_DIR = ../object/linux
        MOC_DIR = ../source/qtgenerated/linux
        RCC_DIR = ../source/qtgenerated/linux
    }
}

SOURCES += \
    ../source/attachns.cpp \
    ../source/createns.cpp \
    ../source/deletens.cpp \
    ../source/genconfigdialog.cpp \
    ../source/lockunlockdialog.cpp \
    ../source/main.cpp \
    ../source/mainwindow.cpp \
    ../source/passworddialog.cpp \
    ../source/policydialog.cpp \
    ../source/protzonedialog.cpp \
    ../source/encdecdialog.cpp \
    ../source/secureerasedialog.cpp \
    ../source/signupdatedialog.cpp \
    ../source/framelesswindow.cpp \
    ../source/windowdragger.cpp \
    ../source/darkstyle.cpp

HEADERS += \
    ../include/attachns.h \
    ../include/createns.h \
    ../include/deletens.h \
    ../include/enduserdesktop.h \
    ../include/flexx-file.h \
    ../include/flexx-nvme.h \
    ../include/flexxlib.h \
    ../include/fwdownloaddialog.h \
    ../include/genconfigdialog.h \
    ../include/lockunlockdialog.h \
    ../include/mainwindow.h \
    ../include/passworddialog.h \
    ../include/policydialog.h \
    ../include/protzonedialog.h \
    ../include/encdecdialog.h \
    ../include/secureerasedialog.h \
    ../include/signupdatedialog.h \
    ../include/framelesswindow.h \
    ../include/windowdragger.h \
    ../include/darkstyle.h \

FORMS += \
    ../forms/attachns.ui \
    ../forms/createns.ui \
    ../forms/deletens.ui \
    ../forms/fwdownloaddialog.ui \
    ../forms/genconfigdialog.ui \
    ../forms/lockunlockdialog.ui \
    ../forms/mainwindow.ui \
    ../forms/passworddialog.ui \
    ../forms/policydialog.ui \
    ../forms/protzonedialog.ui \
    ../forms/encdecdialog.ui \
    ../forms/secureerasedialog.ui \
    ../forms/signupdatedialog.ui \
    ../forms/framelesswindow.ui \

RESOURCES += \
    ../../resources/qrc/Resource.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#win32: LIBS += -L$$PWD/../../../../../../../flexxlib/ -lflexxlib
#INCLUDEPATH += $$PWD/../../../../../../../flexxlib
#DEPENDPATH += $$PWD/../../../../../../../flexxlib
#win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../../../../../../flexxlib/flexxlib.lib
#else:win32-g++: PRE_TARGETDEPS += $$PWD/../../../../../../../flexxlib/libflexxlib.a

win32: LIBS += -L$$PWD/../../resources/library/ -lflexxlib

INCLUDEPATH += $$PWD/../../resources/library
DEPENDPATH += $$PWD/../../resources/library

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../resources/library/flexxlib.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../resources/library/libflexxlib.a

#win32 {
# QMAKE_POST_LINK += mt -nologo -manifest $$PWD/../../resources/manifest/manifest.xml -outputresource:$$OUT_PWD/../../plugins/windows/EndUserDesktopAppDebug.exe $$escape_expand(\n\t)
#}

